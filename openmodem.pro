TEMPLATE = app
DESTDIR  = lib
TARGET   = openmodem

QT       += core widgets network
QT       -= gui
CONFIG   += qt thread exceptions resources  console
unix:    { CONFIG += x11 }
windows: { CONFIG += windows }

OBJECTS_DIR = obj
MOC_DIR     = moc
RCC_DIR     = rcc

INCLUDEPATH += \
    includes \
    includes/utils \
    includes/widgets \
    includes/Winmor \
    includes/Pactor \
    /winmor \
    /pactor \
    .

unix:  { INCLUDEPATH += /home/ken/Projects/C++/qwt-6.1.0/src }
#win32: { INCLUDEPATH += "C:\\Projects\\C++\\qwt-6.1.0\\src" }

DEPENDPATH += \
    /lib \
    .

unix:  { DEPENDPATH += /usr/local/qwt-6.1.0/lib }
#win32: { DEPENDPATH += "C:\\qwt-6.1.0\\lib" }

HEADERS += \
    includes/Winmor/winmor.hpp \
    includes/Pactor/pactor.hpp

HEADERS += \
    includes/modem.hpp \
    includes/modem_globals.hpp



SOURCES += \
    modem.cpp \
    main.cpp

RESOURCES += modemres.qrc

LIBS += -Llib/ -lwmcodec
LIBS += -Llib/ -lpactormodem
unix:  { LIBS += -L/home/ken/Projects/C++/qwt-6.1.0/lib/ -lqwt }
#win32: { LIBS += -L"$$PWD/.."C:\\Projects\\C++\\qwt-6.1.0\\lib\\" -lqwt }

