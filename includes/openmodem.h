#ifndef OPENMODEM_H
#define OPENMODEM_H

#include "openmodem_global.h"
#include "psk-globals.h"
#include "psksession.h"

class OPENMODEMSHARED_EXPORT Openmodem : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool m_ready READ isReady)

public:
    Openmodem(QObject *parent=0);
    ~Openmodem();

    PskSession *psksession;
    //WinMorSession *winmorsession;
    //PactorSession *pactorsession;
protected:
    void modeStateChange();

private:
    bool m_initialized;
    bool m_ready;

    int m_samplerate;
    int m_samplesize;
    int m_stop;
    int m_sqlevel;

    enum Parity m_parity;

    AfcMode m_squelch;
    AfcMode m_afcon;

    Mode m_mode;

    StateOfReception m_rcvstate;
};
#endif // OPENMODEM_H
