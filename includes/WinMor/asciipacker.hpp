#ifndef ASCIIPACKER_HPP
#define ASCIIPACKER_HPP
#include <string>
#include <vector>
using namespace std;

#include <QtCore/QObject>
#include <QtCore/QChar>
#include <QtCore/QString>

typedef quint8   outbyte;
typedef QChar    inchar;
typedef QString  inchars;

typedef vector<outbyte> outbytes ;

class AsciiPacker : public QObject
{
    Q_OBJECT

public:
    explicit AsciiPacker(QObject *parent=0);

    static outbytes pack(inchars inchars);
    static outbytes unpack(outbytes);
signals:

public slots:

};
#endif // ASCIIPACKER_HPP
