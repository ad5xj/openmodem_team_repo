#ifndef WINMOR_HPP
#define WINMOR_HPP
#include <QtCore/QObject>

#include "winmor_globals.hpp"
#include "winmorsession.hpp"
#include "winmortnc.hpp"

class WM_EXPORT WinMor : public QObject
{
    Q_OBJECT

public:
    explicit WinMor(QObject *parent=0);
    virtual ~WinMor();

public:
    // TODO: we have to decide if we want to get these values from the modem interface
    //       or from the current stored settings. If from the modem interface
    //       it has to be setup before init(); If from settings, settings have
    //       to be updated each time a change is made then sync() issued to
    //       keep it current.
    //       ??? is modem responsible modem->winmor or
    //           is WinMor setup responsible winmor->modem ???
    //

    // The codec is controlled from these exposed interface methods
    // The backend methods may change but these should not.
    virtual void init_modem();
    virtual int  setBW(int bw);
    virtual bool autoBreak(bool ab);
    virtual bool blocked();
    virtual bool isReg() { return false; }
    virtual bool blockSignals(bool b);
    virtual int  buffers();
    virtual bool busy();
    virtual bool busylock(bool lock);
    virtual QString capture(QString devname);
    virtual QString captureDevices(QString devlist);
    virtual void close();
    virtual QString codec(QString codecname);
    virtual QString connectRemote(QString callsign);
    virtual QString curState();
    virtual bool cwID(bool id);
    virtual bool debugLog(bool log);
    virtual void dirtyDisconnect();
    virtual int  driveLevel(int lvl);
    virtual QString fault();  // return codec errString
    virtual bool fecRcv(bool frcv);
    virtual void fecsend(QString snd);
    virtual QString gridSquare(QString grid);
    virtual int  leaderExt(int ext);
    virtual bool listen(bool lis);
    virtual int  maxConReq(int max);
    virtual QString monCall();
    virtual QString myAux(QString aux);
    virtual QString myC(QString call);
    virtual QString newState(QString newstate);
    virtual int  offset();
    virtual int  outQueued(int out);
    virtual void over();
    virtual QString playback(QString dev);
    virtual QString playDevs();
    virtual QString ports();
    virtual int  responseDly(int dly);
    virtual bool robust(bool setRobust);
    virtual void sendBreak();
    virtual int sendID(int id);
    virtual QString setMode(QString set);
    virtual void setSuffix(QString x);
    virtual bool setVox(bool vox);
    virtual void showTNC(bool shw);
    virtual int  squelchLvl(int lvl);
    virtual void twoToneText(bool t);
    virtual QString version();

private:
    bool shwTNC;

    quint32 m_bw;

    QString errString;

    WinMorTNC *tnc;
    WinMorSession *session;
};  // namespace WM
#endif // WINMOR_H
