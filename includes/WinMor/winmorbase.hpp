#ifndef WINMORBASE_H
#define WINMORBASE_H
#include <QtCore/QObject>

#include "wm_winmor.hpp"

class WinMorTNC;

class WinMor : public QObject
{
    Q_OBJECT

public:
    explicit WinMor(QObject *parent = 0);
    //virtual ~WinMor();

    // local storage and control settings of WinMor mode //
    struct WM_control_block
    {
        bool CWId;
        bool AutoBREAK;
        bool BusyLock;
        bool robustON;
        bool useVOX;
        bool listenON;
        bool debugLogON;
        bool sessionLogON;
        bool fecRcv;
        quint16 Bandwidth;
        quint16 DriveLevel;
        quint32 TCPControlPort;
        quint16 responseDly;
        quint16 leaderExt;
        quint16 maxReqCon;
        quint16 squelchLvl;
        QString CaptureDevice;
        QString Mode;
        QString MyCallsign;
        QString MyGridsquare;
        QString PlaybackDevice;
        QString Registration;
        QString Suffix;
        QString tcpAddress;
    } WMCB;

    quint64 errno;       // local storage for codec error number
    QString errString;   // local storage for codec error strings

    void init();

public Q_SLOTS:
    void slotOutboundDataReady();

private:
    bool    shwTNC;
    int     m_bw;        // local storage of bandwidth in hz


    WinMorTNC  *tnc;
};
#endif // WINMORBASE_H
