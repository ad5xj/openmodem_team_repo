#ifndef STATIONID_HPP
#define STATIONID_HPP
#include <QtCore/QObject>
#include <QtCore/QRegExp>
#include <QtCore/QStringList>

class WMStationID : public QObject
{
    Q_OBJECT

public:
    explicit WMStationID(QObject *parent=0);

    bool setCallSign(const QString &callsign);
    bool setGridSquare(const QString &gridsquare);

    int getSsid();

    QString getCallSign();
    QString getGridSquare();
    QString getCallSignSsid();

signals:

public slots:

private:
    int ssid;

    QString callSign;
    QString gridSquare;
};
#endif // STATIONID_HPP
