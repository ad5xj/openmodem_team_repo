#ifndef WINMOR_GLOBALS_HPP
#define WINMOR_GLOBALS_HPP
#include <QtCore/QtGlobal>

#define WM_VERSION 0x010000
#define WM_VERSION_STR "1.0.0"

        #if defined(_MSC_VER) /* MSVC Compiler */
/* template-class specialization 'identifier' is already instantiated */
#pragma warning(disable: 4660)
/* inherits via dominance */
#pragma warning(disable: 4250)
#endif // _MSC_VER

#if defined(WM_SHARED)     // create a WinMor DLL library
#define WM_EXPORT Q_DECL_EXPORT
#else                        // use a WinMor DLL library
#define WM_EXPORT Q_DECL_IMPORT
#endif // WM_EXPORT

#define WINMORTNC_DEBUG(msg) qDebug() << "WinMorTNC debug - " << msg << __FILE__ << Q_FUNC_INFO << __LINE__ ;

#endif // WINMOR_GLOBALS_HPP
