#ifndef MODULATOR_HPP
#define MODULATOR_HPP
#include <queue>
using namespace std;

#include "qjackclient.h"
#include "qoutport.h"
using namespace QJack;

#include <QtCore/QObject>
#include <QtCore/QByteArray>

#include "inverse_complex_fft.hpp"
#include "reedsolomon.hpp"

typedef jack_default_audio_sample_t sample_t;
typedef unsigned char byte_t;
typedef QByteArray byte_vec;


/// \brief The Modulator class
/// This class encapsulates the conversion of encoded data to samples
/// and is responsible for feeding the resulting samples to JACK.
/// It inherits the QOutPort interface but does not know how to connect
/// itself to anything. Something else must create and manage the JACK client.
/// It is a QObject by virtue of QOutPort.
///
class Modulator : public QJack::QOutPort
{
    Q_OBJECT

public:
    ///
    /// \brief Modulator Plain constructor
    /// \param portname Name to give the JACK port.
    /// \param buffersize Size of buffer we want JACK to reserve for us.
    /// \param parent QObject parent - should be the client QObject.
    ///
    explicit Modulator(QString portname, quint32 buffersize, QObject *parent = 0);

    ///The system sampling rate. Currently fixed for no particular reason.
    ///TODO: Should be getting this out of JACK, which actually knows.
    static const int samplingRate = 12000;

    /// The center frequency specified in the protocol. It amounts to
    /// the center of the bandwidth available.
    static const double centerFrequency = 1500.; // Hz. Protocol specified.

    /// \brief appendPilot Construct a pilot tone according to the protocol.
    ///
    void appendPilot();

    /// \brief appendFrameType Construct a frame type 4FSK burst according
    /// to the protocol.
    void appendFrameType(int frameType);

    /// \brief modulateMultiCarrier4FSK Modulate the given symbols onto n-carrier 4FSK.
    /// \param symbols 2-bit symbols, one for each carrier
    /// \param nCarriers The number of 4FSK carriers to employ.
    void modulateMultiCarrier4FSK(byte_vec symbols, int nCarriers);

public slots:
    /*! \brief Start the transmission of the frame into the sound channel.
     */
    void startTx();

signals:
    /*! \brief The entire frame has been emptied into the sound channel.
     * Though the frame has gone into the channel it hasn't necessarily
     * been converted to sound but whatever is coming up next may start
     * sending more now.
     */
    void txFinished();

protected:
    /// The number of samples used to transmit each symbol.
    static const int samplesPerSymbol = 128; /// Must be a power of two.

    /// The protocol-defined pilot tone frequency in Hertz.
    static const double pilotFrequency = 1500;

    /// Peak amplitude of output waveform before clipping is applied.
    static const sample_t peakAmplitude = .5;    // Matches what Ken did.

    /// The protocol-defined number of pilot bursts to kick off with.
    static const int leaderLength = 20;

    /// The number of extra pilot bursts to add if
    /// configured for the use of vox PTT.
    static const int voxPilotExtension = 12; // TODO: this is a parameter?

    /// Hamming 8,4 encoding table for a single 4-bit word.
    static byte_t hamming84Table[16];

    /// Root Raised Cosine envelope table.
    double rootRaisedCosineWindow[samplesPerSymbol];

    /// TODO: this status variable is temporary until we have the actual
    /// TODO: state machine of the modulator implemented.
    bool txIdle;

    /// The buffer used to assemble the parts of the frame.
    /// It's a queue for the time being but this might change.
    queue<sample_t> waveBuffer;

    /// The fft processor objects;
    InverseComplexFFT* icfft128;
    InverseComplexFFT* icfft256;

    /// Reed-Solomon codecs
    ReedSolomon* reedSolomon8;

    /// \brief setData Provide a bufferful of samples to JACK
    ///
    /// \param data This is the buffer supplied by JACK
    /// \param nFrames The number of frames to supply to JACK
    ///
    void setData(void *data, quint32 nFrames);

};
#endif // MODULATOR_HPP
