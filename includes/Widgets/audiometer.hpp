#ifndef AUDIOMETER_H
#define AUDIOMETER_H
#include <qwt_thermo.h>

class AudioMeter: public QwtThermo
{
    Q_OBJECT

public:
    AudioMeter(QWidget *parent=0);
    ~AudioMeter();

    void setValue(double value);
    void setSize(int w, int h);
    void setMeterRange(double minval, double maxval);

signals:
    void overrange();

private:
};

#endif // AUDIOMETER_H
