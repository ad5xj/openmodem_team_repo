/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#ifndef WAVEFORM_H
#define WAVEFORM_H

#include <QtCore/QDebug>
#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QVector>
#include <QtGui/QPixmap>
#include <QtGui/QPaintEvent>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QWidget>

#include "globals.h"
#include "pactortnc.h"

/**
 * Widget which displays a section of the audio waveform.
 *
 * The waveform is rendered on a set of QPixmaps which form a group of tiles
 * whose extent covers the widget.  As the audio position is updated, these
 * tiles are scrolled from left to right; when the left-most tile scrolls
 * outside the widget, it is moved to the right end of the tile array and
 * painted with the next section of the waveform.
 */
class PACTORMODEM_EXPORT Waveform : public QWidget
{
        Q_OBJECT

public:
        Waveform(QWidget *parent=0);
        ~Waveform();

        void initialize(int tileWidth, int tileHeight, int windowSize);
        void reset();
        void setAutoUpdatePosition(bool enabled);

protected:
        void paintEvent(QPaintEvent *event);
        void resizeEvent(QResizeEvent *event);

public slots:
        void newData(ccharptr);

private:
        struct Tile
        {
            bool      painted; // Flag indicating whether this tile has been painted
            int       index;
            QPixmap  *pixmap;  // Pointer into parent m_pixmaps array
        };

        bool     m_active;

        qint64   m_dataLength;
        qint64   m_position;
        qint64   m_tileLength;     // Length of audio data in bytes depicted by each tile
        qint64   m_tileArrayStart; // Position in bytes of the first tile, relative to m_buffer
        qint64   m_windowPosition;
        qint64   m_windowLength;

        int      m_tiles_count;
        int      current_tile;
        int      first_tile_index;
        int      painted_tiles;
        int      next_free_tile;

        double   previousCurveValue;

        QSize    m_pixmapSize;

        Tile    *m_tiles;

        QVector<QPixmap *> m_pixmaps;

        static const int   NullIndex = -1;

        void deletePixmaps();
        /*
         * (Re)create all pixmaps, repaint and update the display.
         * Triggers an update();
         */
        void createPixmaps(const QSize &newSize);
        /*
         * Convert offset in bytes into a tile into an offset in pixels
         * within that tile.
         */
        int tilePixelOffset(qint64 positionOffset) const;
        /*
         * Paint the specified tile
         *
         * \pre Sufficient data is available to completely paint the tile, i.e.
         *      m_dataLength is greater than the upper bound of the tile.
         */
        void paintTile(int index, ccharptr buffer);
        /*
         * Move the first n tiles to the end of the array, and mark them as not
         * painted.
         */
        void shuffleTiles(int n);
};
#endif // WAVEFORM_H
