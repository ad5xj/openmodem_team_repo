/****************************************************************************
 **
 ** Copyright (C) 2012 Digia Plc and/or its subsidiary(-ies).
 ** Contact: http://www.qt-project.org/legal
 **
 ** This file is part of the examples of the Qt Toolkit.
 **
 ** $QT_BEGIN_LICENSE:BSD$
 ** You may use this file under the terms of the BSD license as follows:
 **
 ** "Redistribution and use in source and binary forms, with or without
 ** modification, are permitted provided that the following conditions are
 ** met:
 **   * Redistributions of source code must retain the above copyright
 **     notice, this list of conditions and the following disclaimer.
 **   * Redistributions in binary form must reproduce the above copyright
 **     notice, this list of conditions and the following disclaimer in
 **     the documentation and/or other materials provided with the
 **     distribution.
 **   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
 **     of its contributors may be used to endorse or promote products derived
 **     from this software without specific prior written permission.
 **
 **
 ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 **
 ** $QT_END_LICENSE$
 **
 **
 ** Widget which displays a section of the audio waveform.
 **
 ** The waveform is rendered on a set of QPixmaps which form a group of tiles
 ** whose extent covers the widget.  As the audio position is updated, these
 ** tiles are scrolled from left to right; when the left-most tile scrolls
 ** outside the widget, it is moved to the right end of the tile array and
 ** painted with the next section of the waveform.
 **
 ****************************************************************************/
#ifndef WAVEFORM_H
#define WAVEFORM_H
#include <qwt_plot.h>
#include <qwt_system_clock.h>

#include <QtCore/QEvent>
#include <QtGui/QWidget>
#include <QtGui/QShowEvent>

class QwtPlot;
class QwtPlotCurve;
class QwtPlotMarker;
class QwtPlotDirectPainter;

class Waveform : public QwtPlot
{
    Q_OBJECT

public:
    Waveform(QWidget *parent=NULL);
    ~Waveform();

    // QWidget
    //void initialize(const QAudioFormat &format, qint64 audioBufferSize, qint64 windowDurationUs);
    void reset();

    void setAutoUpdatePosition(bool enabled);
    void start();
    virtual void replot();

public Q_SLOTS:
    void setIntervalLength( double );

protected:
    virtual void showEvent(QShowEvent *);
    virtual void resizeEvent(QResizeEvent*);
    virtual void timerEvent(QTimerEvent *);



private:
    bool                    m_active;

    int d_paintedPoints;
    int d_timerId;


    QwtPlotMarker *d_origin;
    QwtPlotCurve *d_curve;
    QwtPlotDirectPainter *d_directPainter;
    QwtInterval d_interval;
    QwtSystemClock d_clock;

    static const int NullIndex = -1;

    void initGradient();
    void updateCurve();
    void incrementInterval();
};
#endif // WAVEFORM_H
