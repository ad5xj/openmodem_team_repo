#ifndef CRCWINMOR_H
#define CRCWINMOR_H

#include <QtCore/QByteArray>

typedef unsigned char byte_t;
typedef unsigned char crc8_t ; // Supposed to be 8 bits.
typedef unsigned short int crc16_t ; // Supposed to be 16 bits.
typedef QByteArray byte_vec;


class CrcWinMOR
{

public:
    static crc8_t crc8(byte_vec inbyts);
    static void appendCrc8(byte_vec &inbyts);

    static crc16_t crc16(byte_vec inbyts);
    static void appendCrc16(byte_vec &inbyts);
};
#endif // CRCWINMOR_H
