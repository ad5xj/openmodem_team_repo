#ifndef COMPLEX_FFT_HPP
#define COMPLEX_FFT_HPP
#include <complex>
using namespace std;

#include <fftw3.h>

#include <QtCore/QObject>


class ComplexFFT : public QObject
{
    Q_OBJECT
public:
    explicit ComplexFFT(int size, int direction, QObject *parent=0);
    ~ComplexFFT();

    std::complex<double>* inData;
    std::complex<double>* outData;

    void execute();

signals:

public slots:

protected:
    int size;
    fftw_plan plan;
};
#endif // COMPLEX_FFT_HPP
