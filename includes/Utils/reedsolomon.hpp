#ifndef REEDSOLOMON_HPP
#define REEDSOLOMON_HPP
#include <math.h>
#include <vector>
using namespace std;

#include <QtCore/QObject>
#include <QtCore/QByteArray>

typedef unsigned char symbol_t;
typedef QByteArray symbol_vec;
typedef int index_t;

class ReedSolomon : public QObject
{
    Q_OBJECT

public:
    ///
    /// \brief ReedSolomon Create a Reed-Solomon encoder/decoder.
    /// The Reed-Solomon algorithm requires some setup calculations
    /// which are particular to the symbols size and the error correction
    /// capacity. We do this calcs just once for each combination of
    /// parameters required because we'll be doing the encoding many times.
    /// \param mm The symbol size in bits.
    /// \param tt The number of errors we would like to correct.
    /// \param parent The QObject parent.
    explicit ReedSolomon(int mm, int tt, QObject *parent = 0);

    /// \brief encode Do a Reed-Solomon encode of 'indata'
    /// The symbol size is determined by the initialization of the object.
    /// The data array must have 'kk=2^mm - 1' or fewer elements in it.
    /// \param indata A reference to the symbol array - not changed.
    /// \returns The data followed by the parity words.
    ///
    symbol_vec encode(const symbol_vec &indata);

    ///
    /// \brief decode Perform a Reed-Solomon decode of 'rxdata'.
    /// 'rxdata' is received, possibly corrupted, data in the same form
    /// as was created by encode().
    /// \param rxdata Incoming data as prepared by encode()
    /// \return decoded data with errors corrected.
    ///
    symbol_vec decode(const symbol_vec &rxdata);

signals:

public slots:

private:
    int mm;
    int nn;
    int tt;
    int kk;

    ///
    /// \brief pp The irreducible polynomials, which come from a book.
    ///
    int *pp;    // Will be one of the following. TODO: fix this mess up.

    std::vector<index_t> alpha_to;
    std::vector<index_t> index_of;
    std::vector<index_t> gg;

    static int pp4[5];
    static int pp8[9];

    void generate_gf();
    void gen_poly();
};
#endif // REEDSOLOMON_HPP
