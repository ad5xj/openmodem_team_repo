#ifndef TCPSERVER_HPP
#define TCPSERVER_HPP
#include <QtCore/QStringList>
#include <QtNetwork/QTcpServer>

//#include "tcpsvrthread.hpp"

class TCPServer : public QTcpServer
{
    Q_OBJECT

public:
    TCPServer(QObject *parent=0);

    int socketDescriptor;

    QStringList data;

    QTcpSocket *svrconnection;

signals:
    void newMessage(const QString &from, const QString &message);
    void newConnection();
    void newParticipant(const QString &nick);
    void participantLeft(const QString &nick);

protected:
    void incomingConnection(qintptr handle);

private slots:
    void slotNewConnection();
    void slotConnStateChg();
    void slotReadyForUse();
    void slotUpdateServer();
    void slotDisconnected();
    void slotConnectionError(QAbstractSocket::SocketError socketError);
    void slotNewParticipant(const QString &nick);
    void slotParticipantLeft(const QString &nick);
    void appendMessage(const QString &from, const QString &message);

private:
    int bytesToWrite;
    int bytesWritten;
    int bytesReceived;
    int bytesToRcv;
    int TotalBytes;

    QByteArray inBytes;

    QString myNickName;
    QString text;
    QStringList datagrams;

    void sendMessage(const char msg);
    void processBytes();
    void removeConnection(QTcpSocket *connection);
//    ServerThread *thread;
    QString nickName();
};
#endif // TCPSERVER_HPP
