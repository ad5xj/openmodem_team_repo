/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#ifndef PACTORAUDIOENGINE_H
#define PACTORAUDIOENGINE_H

#include <QtCore/QObject>
#include <QtCore/QTime>
#include <QtCore/QByteArray>
#include <QtCore/QBuffer>
#include <QtCore/QVector>
#if QT_VERSION <= 0x040804
# ifdef Q_OS_WIN32
#  include <QtMultimedia/QAudio>
#  include <QtMultimedia/QAudioDeviceInfo>
# else
//#   include <QtMultimediaKit>
#   include <QtMultimedia>
# endif
#endif

extern "C" {
#include <jack/jack.h>
#include <jack/transport.h>
}

#include "pactorparams.h"
#include "pactordemodulator.h"
#include "pactormodulator.h"
#include "pactortnc.h"
#include "pactornetstruct.h"

#define PAE_NZEROS 5
#define PAE_NPOLES 5
#define PAE_GAIN   6.093427809e+03

#define CS_UNKNOWN  0
#define CS_IS_OK    1
#define CS_IS_KO    2
#define CS_IS_SPEED 3
#define CS_IS_CS1   4
#define CS_IS_CS2   5

#define     WAITING     0
#define     OK_TO_START 1
#define     OK_TO_START_DEF 2
#define     REJECTED_START  3
#define     STARTED     4
#define     FINISHED    5
#define     STOPPED     6
#define     END_UPDATED 7

#define     DEF_INPUT_GAIN      4.0
#define     DEF_OUTPUT_GAIN     1.0

#define     MIN_AUDIO_INPUT_GAIN    1.0
#define     MAX_AUDIO_INPUT_GAIN    20.0
#define     MIN_AUDIO_OUTPUT_GAIN   1.0
#define     MAX_AUDIO_OUTPUT_GAIN   2.0

#define     AUDIO_OFFSET_LP_GAIN    50.0

#define MODEM_JACK_INPUT_PORTS_COUNT        1
#ifdef DEBUG_WINDOWS
#define MODEM_JACK_OUTPUT_PORTS_COUNT       4
#define FIRST_OUTPUT_PORT                   0
#define SECOND_OUTPUT_PORT                  1
#define FIRST_DEBUG_PORT                    2
#define SECOND_DEBUG_PORT                   3
#else
#define MODEM_JACK_OUTPUT_PORTS_COUNT       2
#define FIRST_OUTPUT_PORT                   0
#define SECOND_OUTPUT_PORT                  1
#define FIRST_DEBUG_PORT                    0
#define SECOND_DEBUG_PORT                   0
#endif

class PactorAudioEngine;
class PactorDecoder;


QT_FORWARD_DECLARE_CLASS(QAudioInput)
QT_FORWARD_DECLARE_CLASS(QAudioOutput)
QT_FORWARD_DECLARE_CLASS(QFile)
QT_FORWARD_DECLARE_CLASS(PactorModulator)
QT_FORWARD_DECLARE_CLASS(PactorDemod)
QT_FORWARD_DECLARE_CLASS(PactorDecoder)

class PactorAudioEngine : public QObject
{
        Q_OBJECT

    public:
        explicit PactorAudioEngine(QObject *parent, PactorModulator *pmod, PactorDemod *pdemod);
        ~PactorAudioEngine();
        void startAudioIO(void);

    private:
        void initializeCommData(void);
#ifdef VOX_CONTROL
        void initialize_audio_vox(int vox_frequency);
#endif

    public slots:

    signals:
        void computeBurstAudio(unsigned char *, int, int, int, float *, int *);

    private slots:

    public:
        int                 jack_input_offset;
        int                 jack_output_offset;
        int                 packet_count[PACKET_END_COUNT];

        qint64              date_time_USecs;
        qint64              Tx_start_USecs;

        float               *tx_buffer;

        jack_thread_info_t  thread_info;
        PactorDemod         *m_pactorDemodulator;

        static void jack_shutdown(void *arg);
        static int  process_audio(jack_nframes_t nframes, void *arg);

    private:
        int input_sample_index;
        int tx_buffer_index;

        const char **ports;
        const char *client_name;
        const char *server_name;

        QByteArray      m_pactorBuffer;

        jack_latency_range_t latency_range;
        jack_options_t  options;
        jack_status_t   status;
        jack_port_t   **input_ports;
        jack_port_t   **output_ports;
        jack_client_t  *jack_client;

        PactorModulator *m_pactorModulator;
        PactorTNC       *m_pactorTNC;
};
#endif // PACTORAUDIOENGINE_H
