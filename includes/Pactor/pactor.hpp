/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#ifndef PACTOR_HPP
#define PACTOR_HPP
#include <QtCore/QObject>
#include <QtWidgets/QWidget>

#include "globals.h"
#include "pactortnc.h"

class PACTORMODEM_EXPORT Pactor : public QObject
{
    Q_OBJECT

public:
    Pactor(QObject *parent=0);
    virtual ~Pactor();

    // local storage and control settings of Pactor mode //
    struct WM_control_block
    {
        bool CWId;
        bool AutoBREAK;
        bool BusyLock;
        bool robustON;
        bool useVOX;
        bool listenON;
        bool debugLogON;
        bool sessionLogON;
        bool fecRcv;
        bool shwTNC;
        quint16 Bandwidth;
        quint16 DriveLevel;
        quint32 TCPControlPort;
        quint16 responseDly;
        quint16 leaderExt;
        quint16 maxReqCon;
        quint16 squelchLvl;
        QString CaptureDevice;
        QString Mode;
        QString MyCallsign;
        QString MyGridsquare;
        QString PlaybackDevice;
        QString Registration;
        QString Suffix;
        QString tcpAddress;
    } WMCB;

    PactorTNC     *tnc;
    PactorSession *session;

    // TODO: we have to decide if we want to get this from the modem interface or
    //       from the current stored settings. If from the modem interface it has
    //       to be setup before init(); If from settings, settings have to be updated
    //       each time a change is made then sync() issued to keep it current.
    //       ??? is modem responsible m->w or is Pactor setup responsible w->m ???
    //

    // The codec is controlled from these exposed interface methods
    // The backend methods may change but these should not.
    void sendBreak();         // Initiates a BREAK (link turnover request to ISS)
    // if in IRS or IRS Mode Shift state otherwise no effect.
    // Forces ISS to clear its outbound queue and acknowledge.
    // Normally not required if AUTOBREAK is enabled.
    void close();              // Provides an orderly shutdown of all connections,
    // release of all sound card resources and
    // closes the active codec.
    void dirtyDisconnect();    // An existing link has been disconnected or
    // a pending connect was not for this call sign
    // or not decoded.
    void fecsend(QString snd, int repeat); // Start/Stop FEC broadcast (unproto) mode
    // for 500 Hz or 1600 Hz. Must be in DISCONNECTED,
    // or FEC state or will generate a fault.
    // FECSEND <End> will prematurely end a FEC broadcast
    // and send an ID frame.
    void over();               // The actual reaction to this command will depend on the link protocol
    // of the codec selected. For instance, for the WinMOR codec - If
    // in IRS or IRS mode Shift states initiates a BREAK. IF In ISS or ISS
    // mode shift states clears the outbound Queue. Then ISS repeats an OVER
    // Command until a BREAK is received from IRS. Not normally used or
    // required if AUTOBREAK is enabled.
    void showTNC(bool shw);    // Show (True) or Hide (False) the codec TNC window
    void setSuffix(QString x); // Ascii chars (max 10 chars) This sets the optional CW ID suffix. It is
    // sent after the normal CW ID for MYC with a separating slash
    // e.g. DE W4ABC/XE2 if the “/” is omitted it will be inserted automatically.
    void twoToneText(bool t);  // Send 4 second test tone in current mode or two-tone burst

    bool autoBreak(bool on);  // Disables/enables automatic  link turnover (BREAK) by IRS
    // when IRS has outbound data pending and receives an IDLE
    // frame from ISS indicating its’ outbound queue is empty.
    // Default is True.
    bool blocked();           // Asyncronous, Read Only. Connect request blocked due to busy channel
    bool blockSignals(bool b); //
    bool busy();              // Returns channel status. Busy status also reported
    // asynchronously on a busy status change.
    // Originates from the TNC of the active codec.
    bool busylock(bool lock); // Disable/Enable Busy channel blocking.
    // Busy lock will block a connection request unless there
    // have been Tquiet ms of non busy status preceding the
    // connect request. Default = False
    bool cwID(bool id);        // true will requre ID sent as CW at end
    // if id is null returns current settings
    bool debugLog(bool log);   // true will create log file in local path
    // if log is null will return current setting
    bool fecRcv(bool frcv);    // Disable/Enable FEC (unproto) Receive mode
    // or return current status if frcv is null
    bool isReg();              // TODO: will indicate if user has registered for support
    bool listen(bool lis);     // set listen mode with lis = true or return status with NULL
    bool robust(bool setRobust); // If true will only use modes other than default where more than one mode
    // is possible, regardless of current data mode or BW settings above. If in
    // the ISS or ISSMODESHIFT states changes in ROBUST will be delayed
    // until the outbound queue is empty
    bool setVox(bool vox);     // Disable/Enable VOX leader extension (e.g. 128 ms for WinMOR) Default = True
    bool setHuffman(bool huff); // Huffman encoding authorization

    int  setUCMD(int index, int value); // special TNC parameters
    int  buffers();           // Gets the current buffers and current 1 minute average
    // throughput. Values are all integers and space delimited.
    // BUFFERS is also sent asynchronously whenever there is
    // a change in any of the BUFFER parameters.
    int  driveLevel(int lvl);  // set level 0-100 or return current level if null
    int  leaderExt(int ext);   // set with ext = 0 - 24? (mode dependent) or return setting
    int  maxConReq(int max);   // set max connect request or return setting is max is NULL
    int  setBW(int bw = 0);   // Set/gets bandwidth 500 Hz or 1500 Hz
    // (setting applies to server (inbound) connects only).
    // Attempting to change bandwidth while a connection is in
    // process should generate a FAULT. If not connected will
    // return a bandwidth of 0.
    int offset();              // asyncronously returns remote station offset freq +/- hz
    // Reported on every decoded frame but only updated when connected.
    int outQueued(int out);    // If sent with the parameter 0 (zero) it will clear the output QUEUE.
    // If sent without a parameter will return the current number of
    // outbound bytes queued.
    int responseDly(int dly);  // If dly is not zero will set response delay value. Zero or null will
    // return current delay value (0 - 2000).
    int sendID(int id);        // This will send an ID frame and if CWID above is enabled also a CW ID
    // after delay seconds. Only may be sent when in the DISCONNECTED state.
    // If SENDID is received while in any state other than the DISCONNECTED
    // state a FAULT response will be returned. Normally the codec
    // automatically takes care of ID at the session end and at 10 minute
    // intervals during the session.
    int  squelchLvl(int lvl);  // Returns or sets the current squelch value (default = 5). The default
    // value should be sufficient for most installations. Squelch only affects
    // the sensitivity of the busy detector (low values = higher sensitivity)

    QString capture(QString devname); // Sets desired sound card capture device.
    // If no device name will reply with the current assigned
    // capture device in use.
    QString captureDevices(QString devlist);  // send or return in CSV format
    QString codec(QString codecname); // empty codecname returns active codec or 0
    QString connectRemote(QString callsign); // empty call returns current connection
    // succesfull connect sends CONNECT IN PROGRESS
    // failure sends CONNECT FAILURE both async msgs
    QString fault();           // returns command or program fault or error condition.
    // in the format FAULT <error message>
    QString gridSquare(QString grid); // Sets or retrieves the 6 character grid
    // square (used in ID Frames for some codecs).
    // if grid is null will return active grid square or NONE
    QString setMode(QString set); // Sets or Gets current data mode.
    // MODE is also returned asynchronously upon any MODE change.
    QString monCall();         // Non connected monitor data includes call sign and
    // optional grid square in parenthesis. Returned asynchronously.
    QString myAux(QString aux); // Sets up to 10 auxiliary call signs that will answer
    // connect requests. Call signs must be valid radio call signs
    // and separated by commas. If sent with an illegal call sign
    // (e.g. “MYAUX x” it will clear the MYAUX list. If sent without
    // a parameter will return a comma delimited string of current
    // MYAUX call signs.
    QString myC(QString call); // up to 7 characters Sets current call sign.
    // Codec must be not running (CODEC FALSE)
    // If not a valid call sign or Codec is running generates a FAULT.
    QString newState(QString newstate); // Reports change of state See docs for state diagram.
    // The current state may also be polled using the STATE command
    // without parameters.
    QString playback(QString dev); // Sets desired sound card playback device.
    // If no device name will reply with the current assigned playback device.
    QString playDevs();        // TODO: Read Only. returns a list of available devices
    QString ports();           // TODO: Read Only. returns a list of available serial ports
    QString pid();             // TODO: Read Only. returns the system process id of the modem
    QString curState();        // Gets the current codec protocol state.  For instance:
    // <OFFLINE|DISCONNECTED|CONNECTPENDING|CONNECTING|IRS|IRSMODESHIFT|ISS|
    // ISSMODESHFT|IRSTOISS|DISCONNECTING|SENDID|FECRCV|FEC500|FEC1600>
    // Every State change is also reported asynchronously with the NEWSTATE.
    QString target();          // Identifies the target call sign of the connect request. The target call
    // will be either MYC or one of the MYAUX call signs. This command
    // immediately precedes the CONNECTED <remote Call> asynchronous response.
    // normally returns TARGET <target call>
    QString version();         // Read Only. Returns current software version as a string

private:
    int m_bw;                  // local storage of bandwidth in hz
};
#endif // PACTOR_HPP
