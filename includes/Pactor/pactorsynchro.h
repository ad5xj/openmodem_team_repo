/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#ifndef SYNCHRO_H
#define SYNCHRO_H

#undef  RECOVER_BIT_DEBUG

//#include "utils.h"
#include <QtCore/qglobal.h>

#include "pactorparams.h"

#define MAX_CARRIER_LOSSES  2
#define MIN_PEAKS_FOR_SYNC  4

#define MIN_CORREL_100      1.0
#define MIN_CORREL_200      0.5

#define MIN_THRESHOLD       -0.015
#define MAX_THRESHOLD       0.015

#define ZERO_CROSS_FIFO_DEPTH       6 // integrate values to remove noise and transients
#define VALID_ZERO_CROSSINGS        5 // consider burst as valid after 5 zero crossing
#define ZERO_CROSS_OFFSET           (1 * CORREL_BIT_LENGTH_100) // necessary because second zero crossing
// from low to high may be 6 bit from start of CS4 or CS3

#define TRANSITIONS_FIFO_DEPTH  32

typedef struct
{
    bool    new_burst;
    bool    valid;
    int     pos;
    int     fine_sync;
    int     first_trans_index;
    int     phase;
    int     CS_type;
    float   average_value;
    qint64  time_msecs;
    qint64  time_from_winstart;
} t_ident_info;

typedef struct
{
    t_ident_info    hdr100;
    t_ident_info    hdr200;
    t_ident_info    CS100;
    qint64          timestamp;
    float           average_value;
    int             first_trans_index;
    int             last_trans_index;
} t_burst_parameters;

typedef struct
{
    int             pos_sync_16;
    int             pos_sync_8;
    int             original_pos_sync_16;
    int             original_pos_sync_8;
    int             first_sync;
    int             first_sample;
} t_pos_sync;

enum en_correlator_state
{
    CORR_WAIT,
    CORR_IDLE,
    CORR_RESTART,
    CORR_START,
    CORR_INITIALIZING,
    CORR_INITIALIZED,
    CORR_SEARCH_SYNC,
    CORR_SYNC_FOUND,
    ACCUMULATE_SAMPLES,
    LONG_BURST_ACCUMULATE_SAMPLES,
    CORR_FOUND_LONG,
    CORR_FOUND_SHORT,
    CORR_MISSED_SYNC,
    CORR_STATE_COUNT
};

#endif // SYNCHRO_H
