/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#ifndef PactorTNC_H
#define PactorTNC_H

extern "C" {
#include <jack/jack.h>
}

#include <qwt_knob.h>

#include <QtCore/QObject>
#include <QtCore/QBuffer>
#include <QtCore/QByteArray>
#include <QtCore/QTimer>
#include <QtCore/QThread>
#include <QtCore/QSettings>
#include <QtCore/QSemaphore>
#include <QtCore/QMutex>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>

#include "globals.h"

#include "pactornetstruct.h"
#include "pactordecoder.h"
#include "pactorsession.h"

typedef const t_struct_msg *struct_msgptr;

#define     CS1_INDEX       2
#define     CS2_INDEX       4
#define     CS3_INDEX       6
#define     CS4_INDEX       8

#define     BREAK_BIT       (1 << 6)
#define     QRT_BIT         (1 << 7)

#define     TIMER_UNPROTO_MS        1000
#define     TIMER_SHORT_PATH_MS     1250
#define     TIMER_LONG_PATH_MS      1400

#define     CS_TX_DELAY_MS          (1000+20) //(1000-20)

#define     SHIFT_PACKET_START                  20
#define     PACKET_WINDOW_MARGIN_MS             (20+140)
#define     WINDOW_TIMEOUT          10000

#define     PACKET_WINDOW_CS_MARGIN_MS          250
#define     PACKET_WINDOW_CS_LONG_PATH_MARGIN_MS 400

#define     MAX_MISSING_BURSTS  10

#define     FREQ_SLIDER_MIN     800
#define     FREQ_SLIDER_MAX     2300
#define     FREQ_SLIDER_START   2200

#define     SWITCH_TO_IDLE      20

#define     VOX_SAMPLE_COUNT    120

#define     DWNSPLD_AUDIO_BUFF_COUNT    64

extern const int       bit_patterns[10];

// frequency change source
typedef enum
{
    e_freq_mod_client,
    e_freq_mod_demod,
    e_freq_mod_panel,
    e_freq_mod_init
} e_freq_mod_src;

// panel button values
typedef enum
{
    e_panel_wfrm_wave,
    e_panel_wfrm_wfall,
    e_panel_wfrm_spectr,
    e_panel_wfrm_none
} e_panel_wfrm_type;

// Pactor modem status
typedef enum
{
    PT_IDLE,
    PT_DISCONNECTED,
    PT_LISTENING,
    PT_UNPROTO,
    PT_WAITING_FOR_CALL
} e_PT_status;

// Pactor TNC state
typedef enum
{
    tnc_idle,
    tnc_master_call,
    tnc_slave_call,
    tnc_tx200cs2,
    tnc_tx200cs1,
    tnc_tx_to_rx200,
    tnc_rx200cs1,
    tnc_rx200cs2,
    tnc_rx_to_tx200,
    tnc_rx200_to_rx100,
    tnc_rx100_to_rx200cs2,
    tnc_rx100_to_rx200cs1,
    tnc_tx200_to_tx100,
    tnc_tx100_to_tx200cs2,
    tnc_tx100_to_tx200cs1,
    tnc_tx_to_rx100,
    tnc_rx100cs1,
    tnc_rx100cs2,
    tnc_rx_to_tx100,
    tnc_tx100cs1,
    tnc_tx100cs2,
    tnc_listen_ARQ,
    tnc_listen_unproto,
    tnc_tx_unproto,
    tnc_send_break,
    pactor_level_received_100,
    pactor_level_received_200,
    tnc_QRTreqcs1,
    tnc_QRTreqcs2
} e_PT_state;

// Pactor modem CS 1 or 2
typedef enum
{
    PT_CS1 = 0x55,
    PT_CS2 = 0xAA,
    PT_CS3 = 0x34b,
    PT_CS3_0    = 0x4b,
    PT_CS3_1    = 0x03,
    PT_CS3_200_0    = 0xcf,
    PT_CS3_200_1    = 0x30,
    PT_CS3_200_2    = 0x0f,
    PT_CS_ANY
} e_PT_CS;

// Pactor modem phase
typedef enum
{
    PT_PH_INVERTED = -1,
    USE_AUTO_PHASE = 0,
    PT_PH_NORMAL = 1,
    USE_ANY_PHASE = 2
} e_PT_phase;

// Pactor modes
typedef enum
{
    PT_Listen,
    PT_SLAVE,
    PT_MASTER,
    PT_UnProto,
    PT_Idle
} e_PT_mode;

// Modem state
typedef enum
{
    PT_TX,
    PT_RX
} e_PT_modem_state;

// Pactor event arguments
typedef enum
{
    e_PTIdle,
    e_PTMaster,
    e_PTUnprotoSend,
    e_PTListen,
    e_PTServer,
    e_PTPendingNone,
    e_PT_command_count
} e_PT_event;

// supervisor information type
typedef enum
{
    e_PT_si_pactor_level,
    e_PT_si_mycall,
    e_PT_si_ctrl_char
} e_si_type;

// receive window management state
typedef enum
{
    e_PT_rcv_win_not_init,
    e_PT_rcv_win_initialized,
    e_PT_rcv_win_waiting,
    e_PT_rcv_win_open,
    e_PT_rcv_win_closed,
    e_PT_rcv_win_closed_500ms,
    e_PT_rcv_win_updated,
    e_PT_rcv_win_finished
} e_rcv_win_state;

// PTConnect command
typedef struct
{
    QString *callsign;
    bool   short_path;
} t_PT_arg_PTConnect;

// PTTransmitData command
typedef struct
{
    const QByteArray *data;
} t_PT_arg_PTTransmitData;

// PTBreak command
typedef struct
{
    bool   chg_IRS_ISS;
} t_PT_arg_PTBreak;

// PTMode command
typedef struct
{
    bool    huffman_encode;
    bool    speed_200;
} t_PT_arg_PTMode;

// PTUnprotoSendcommand
typedef struct
{
    bool    speed_200;
    int     repeat_count;
} t_PT_arg_PTUnproto;

// PTMyCall command
typedef struct
{
    const QString *callsign;
    const QString *remote_call;
} t_PT_arg_PTMyCall;

// PTParams command
typedef struct
{
    int nb_correct_spd_up;
    int nb_wrong_spd_down;
    int nb_attempts_spd_up;
    int nb_mem_ARQ_pkts;
} t_PT_arg_PTParams;

// PTFrequency command
typedef struct
{
    int frequency;
} t_PT_arg_PTFrequency;

// Pactor command argument union
typedef union u_pt_args
{
    t_PT_arg_PTConnect          PTConnect;
    t_PT_arg_PTTransmitData     PTTransmitData;
    t_PT_arg_PTBreak            PTBreak;
    t_PT_arg_PTMode             PTMode;
    t_PT_arg_PTMyCall           PTMyCall;
    t_PT_arg_PTParams           PTParams;
    t_PT_arg_PTFrequency        PTFrequency;
    t_PT_arg_PTUnproto          PTUnproto;
} u_PT_arg;

class PactorAudioEngine;
typedef struct _thread_info
{
    PactorAudioEngine   *pactoraudioengine;
    jack_client_t       *client;
    jack_port_t         **input_ports;
    jack_port_t         **output_ports;
    qint64              *date_time_mSecs_addr;
    int                 hardware_audio_latency;
    int                 input_latency;
    int                 output_latency;
    int                 total_latency;
    unsigned int        channels;
    volatile bool       can_capture;
    volatile bool       can_process;
    volatile bool       can_transmit;
    volatile int        status;
    volatile int        transmit_state;
    volatile bool       receiving_CS;
    float               audio_in_gain;
    float               audio_out_gain;
    float               audio_offset;
    float               AGC_amplitude;
    bool                AGC_on;
    float               *packet_audio_samples[2];
    float               *CS_audio[2][4]; // 2 phases and 4 CS
    int                 CS_audio_count[4];
    int                 burst_audio_count;
    volatile bool       tx_data_or_CS3_burst;
    int                 next_phase;
    int                 expected_phase;
    int                 current_CS;
    int                 last_CS;
    volatile bool       new_burst;
    volatile bool       send_break;
    int                 pkt_counter;
    qint64              burst_gap_msecs;
    volatile qint64     tx_time_update;
    volatile qint64     next_tx_time;
    volatile qint64     next_vox_start_time;
    volatile qint64     next_vox_stop_time;
    int                 vox_state;
    qint64              last_new_burst_tx_time;
    e_rcv_win_state     window_state;
    int                 short_path;
    qint64              cycle_time;
    qint64              next_Rx_time_ON;
    qint64              next_Rx_time_OFF;
    bool                Rx_time_demod_update;
    bool                startup_time_ready;
    qint64              startup_time;
    e_PT_modem_state    PT_Tx_Rx;
    bool                unproto_mode;
    int                 unproto_repeat;
    int                 unproto_count;
    t_icon_struct       LED_args[LED_COUNT];
    bool                LED_modified;
    // audio input buffers
    float               audio_in_buffer[DWNSPLD_AUDIO_BUFF_COUNT *AUDIO_SAMPLE_COUNT_8KHZ];
    qint64              first_dwnspl_frame_time[DWNSPLD_AUDIO_BUFF_COUNT];
    qint64              current_window_open_msecs;
    qint64              current_window_close_msecs;
    int                 dwnspl_frame_time_index;
    int                 audio_in_write_buffer_index;
    int                 audio_in_read_buffer_index;
    // VOX audio
    int                 vox_frequency;
#ifdef VOX_CONTROL
    int                 vox_channel;
    int                 vox_start_delay;
    int                 vox_stop_delay;
    int                 vox_sample_count;
    int                 vox_sample_index;
    float               vox_buffer[VOX_SAMPLE_COUNT];
#endif
} jack_thread_info_t;

class PactorTNC;
class PactorDemod;
class PactorModulator;
class PactorTNCThreadPrivate;
class PactorSession;

class PactorTNCThread : public QObject
{
    Q_OBJECT

public:
    PactorTNCThread(PactorTNC *real_parent, QWidget *parent=0);
    virtual ~PactorTNCThread();

public:
    bool         PT_huffman_encode;  // if huffman buffer is not empty after huffman timer timeout, allows encoding
    bool         PT_allow_huffman;   // allows incoming text to be stored into huffman text buffer
    bool         si_encoded;

    int          packet_counter;
    int          PT_CS4_in_a_row_count;
    int          PT_pkt_counter;
    int          PT_wrong_CRC_count;
    int          m_timer_ms;
    int          unproto_repeat;
    int          unproto_retransmit;
    int          packet_number;
    int          last_packet_number;
    int          send_break;
    int          UCMD[4];

    e_PT_status  PT_state;
    e_PT_speed   PT_speed;
    e_PT_speed   PT_speed_limit;
    e_PT_phase   PT_phase;
    e_PT_CS      PT_CS;
    e_PT_event   PT_current_request;
    e_PT_state   tnc_state;

    jack_thread_info_t *engine_info;

    PactorTNC          *m_pactorTNC;

    void PTConnect(bool   short_path);
    void send_conn_request_packet(bool short_path);
    void supervisor_data(e_si_type si_type, char ctrl_char);
    inline void saveSentChars(int tx_char_count, bool PT_huffman_encode);

signals:
    void calculateModFSK(unsigned char *, int, int, int, float *, int *);
    void createNewPacket(const int event, ccharptr PT_argument);
    void setLED(int count, t_set_LED_arg   *LED_args);

public slots:
    void PTEvent(const int event, ccharptr PT_argument);

private slots:

private:
    qint64        previous_datetime;

    int           txt_rcv_count;
    int           conv_100_to_200[256];

    char         *txt_received;

    QThread      *m_thread;

    QByteArray   *callsign_message;

    e_PT_phase    next_PT_phase;
    t_set_LED_arg LED_args[LED_COUNT];

    void pactorSendTextToClient(int len, const char *output_string);
    void storeReceivedChars(int event, t_decode_output    *decode_output);
    void send_mycall_packet(void);
    void send_QRT_packet(void);
    void send_BREAK_packet(void);
    int  PTEnqueuePacket(e_PT_CS in_PT_CS = PT_CS_ANY, int break_request = 0);

    inline void restoreSentChars(bool PT_huffman_encode);
};


class PactorTNC : public QDialog
{
    Q_OBJECT

public:
    PactorTNC(QWidget *parent=0);
    virtual ~PactorTNC();

public:
    bool window_visible;
    bool debug_waveform;
    bool AFC_on;
    bool AGC_on;
    bool wait_callsign_ack;
    bool wait_pactor_level;
    bool QRTrequest;

    int  short_path;
    int  waveform_src;
    int  waveform_rate;
    int  center_frequency_start;
    int  center_frequency_AFC_limit;
    int  last_received_frequency;
    int  store_filter500_waveform;

    double waveform_ratio;

    QPoint      window_point;
    QSize       window_size;
    QRect       window_rectangle;

    QString     m_PTMyCall;
    QString     m_PTAuxCall;
    QString     m_PTremoteCall;

    QSettings  *settings;

    QByteArray *ascii_qbyte_array;
    QByteArray *callsign_qbyte_array;
    QByteArray *auxcallsign_qbyte_array;
    QByteArray *remote_callsign_qbyte_array;
    QByteArray *huffman_qbyte_array;
    QByteArray *supervisor_qbyte_array;
    QByteArray *tx_removed_qbyte_array;
    QByteArray *incoming_huffman_qbyte_array;

    PactorDemod     *m_pactorDemodulator;
    PactorTNCThread *m_pactorTNCThread;
    PactorModulator *m_pactorModulator;
    PactorSession   *m_pactor_session;
    PactorAudioEngine *m_pactoraudioengine;

    e_PT_mode        PT_mode;

    t_tx_text_buffs  tx_text_buffs;

    void setIdleMode(void);
    void breakRequest(void);

    QByteArray *huffman_encode_buffer(QByteArray *text_buffer);

    inline void sendMessageToClient(t_struct_msgptr, qint16 blocksize);

signals:
    void ChangeModulatorFrequency(float frequency);
    void createNewPacket(const int event, ccharptr PT_argument);
    //    void packet_timer_ONOFF(bool, int);
    void setLED(int count, t_set_LED_arg   *LED_args);
    void setMode(int buttonChecked);
    void sendWaveformToPanel(ccharptr);
    void setPanelAFC(bool);

public slots:
    // public entry to Pactor modem control
    void SetCenterFrequencyInt(int frequency, int source);
    void changeMode(int buttonChecked);
    void setSlaveMode(void);
    void emitSetLED(int count, t_set_LED_arg *LED_args);
    void LED_refresh_callback(void);
    void huffman_callback(void);
    void sendWaveform(const float *samples, int count, float);
    void readHighPrioMessage(t_struct_msgptr message_in, qint16 blocksize);

private slots:

private:
    bool comm_active;

    quint16 blockSize;

    int     m_notify_interval;
    int     speed_limit;
    int     display_buffer_count;
    int     tile_sample_count;

    char   *read_buffer;
    char   *write_buffer;
    char   *wave_buffer;

    QString output_string;

    QTimer *m_LEDRefreshTimer;
    QTimer *m_HuffmanTimer;

    QDataStream *in;

    t_struct_msg  message_in;
    t_set_LED_arg LED_args[LED_COUNT];

    void pactorDebugSendString(const QString &output_string);
    void closeWidget();
    void readARQCallSetting();
    void readAudioEngineSettings();
    void readSettings();
    void saveSettings();
    void writeAllSettings();
    void writeSettings(int setting_idx);
};
#endif // PactorTNC_H
