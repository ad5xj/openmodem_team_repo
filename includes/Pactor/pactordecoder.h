/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#ifndef PACTORDECODER_H
#define PACTORDECODER_H

#include <QtCore/QObject>
#include <QByteArray>
#include "pactorparams.h"
#include "pactorhuffman.h"
#include "pactornetstruct.h"
#include "pactortnc.h"

#undef IAD_EXTERNAL

#define AVG_CHUNKS  8
#define BIT_PER_CHAR  8
#define CHAR_COUNT_100BD    (LONG_BURST_BIT_COUNT_100/BIT_PER_CHAR)
#define CHAR_COUNT_200BD    (LONG_BURST_BIT_COUNT_200/BIT_PER_CHAR)

#define STATUS_BYTE_POS_100     9
#define CRC_BYTES_POS_100       10
#define STATUS_BYTE_POS_200     21
#define CRC_BYTES_POS_200       22

typedef enum
{
    DUMMY   = 0,    // do not change this value
    ACK_CS1,
    ACK_CS2,
    ACK_CS3,
    ACK_CS4,
    PACKET_CONNECT_100,
    PACKET_CONNECT_200,
    PACKET_ASCII_100,
    PACKET_HUFFMAN_100,
    PACKET_ASCII_200,
    PACKET_HUFFMAN_200,
    PACKET_QRT_100,
    PACKET_QRT_200,
    WRONG_CRC_100,
    WRONG_CRC_200,
    PACKET_BREAK_ASCII_100,
    PACKET_BREAK_ASCII_200,
    PACKET_BREAK_HUFFMAN_100,
    PACKET_BREAK_HUFFMAN_200,
    PACKET_MYCALL_100,
    PACKET_MYCALL_200,
    PACKET,
    PACKET_BREAK,
    PACKET_ANY,
    AUDIO_ENGINE_SENT_BURST,
    PACKET_CS3_ASCII_100,
    PACKET_CS3_ASCII_200,
    PACKET_CS3_HUFFMAN_100,
    PACKET_CS3_HUFFMAN_200,
    SPEED_DOWN,
    SPEED_UP,
    NO_CONNECTION,
    PACKET_END_COUNT
} e_packet_type;

typedef struct
{
    float   min_average;
    float   max_average;
    float   min_threshold;
    float   max_threshold;
    float   average;
} t_histo_values;

class   PactorDemodThread;

class PactorDecoder : public QObject
{
        Q_OBJECT
    public:
        explicit PactorDecoder(QObject *parent = 0);

#ifdef IAD_EXTERNAL
        void integrate_and_dump(const float decode_samples_buffer[], int start, int count, e_PT_speed speed, int phase, float average, int result_pos);
#endif
        t_decode_output   *DecodePacket(const float decode_samples_buffer[],
                                        int header_type,
                                        int expected_phase,
                                        float average,
                                        QString PTMyCall,
                                        int  pactor_mode,
                                        int  pactor_state,
                                        e_PT_speed   PT_speed);
        t_decode_output   *identifyPacket(const float decode_samples_buffer[],
                                          int header_type,
                                          int  expected_phase,
                                          float average,
                                          QString PTMyCall,
                                          int  pactor_mode,
                                          int  pactor_state,
                                          e_PT_speed   PT_speed);
#ifdef HUFFMAN_TEST
        // huffman decoder
        t_ps   *huffman_decode_packet(unsigned char *data, int pktlen);
#endif

    private:
        void buildChars(const char *bit_buffer, int start_position, int phase, int count, unsigned char *char_buffer);

    private:
        void            checkCallsign(t_tx_text_buffs *callsigns, const char *char_buffer, t_decode_output *decode_output);
#ifdef IAD_EXTERNAL
        t_histo_values   computeMinMaxHisto(float iad_bit_value[], int byte_count);
#endif
        unsigned char    data_char[LONG_BURST_BIT_COUNT_200 / BIT_PER_CHAR];
        t_decode_output  decode_output;
        PactorDemodThread *m_demod_thread;
#ifndef HUFFMAN_TEST
        // huffman decoder
        t_ps   *huffman_decode_packet(unsigned char *data, int pktlen);
#endif

    signals:

    public slots:

};

#endif // PACTORDECODER_H
