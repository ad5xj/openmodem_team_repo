/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#include <string.h>

#include <QtCore/QBuffer>
#include <QtWidgets/QLineEdit>

#include "crccalc.h"
#include "pactormodulator.h"
#include "pactordemodulator.h"
#include "pactordecoder.h"
#include "pactortnc.h"

extern qint64              global_date_time_mSecs;

/************************************************************************************/
// create QRT packet
/************************************************************************************/
void PactorTNCThread::send_QRT_packet(void)
{
    static char         send_QRT_Packet_buffer[LONG_BURST_BYTE_COUNT_200 + 5];
    int                 i, j;
    unsigned int        computed_CRC;
    char                burst_status;
    char                reverse_callsign[9];
    int                 callsign_max_length;
    int                 ichar;
    int                 bit_count;
    short               temp_100to200;

    int                 tx_speed = (PT_speed == PT_SPEED_100) ? SPEED_100 : SPEED_200;
    // create reverse callsign + header

    // header
    callsign_max_length     = qMin(7, m_pactorTNC->tx_text_buffs.my_callsign->count());
    reverse_callsign[7]     = 0x55;
    // reversed callsign
    i   = 0;
    for (ichar = 6; ichar >= (7 - callsign_max_length); ichar--, i++)
    {
        reverse_callsign[ichar] = m_pactorTNC->tx_text_buffs.my_callsign->operator [](i);
    }
    // pad the buffer
    for (; ichar >= 0; ichar--)
    {
        reverse_callsign[ichar] = '\x0f';
    }

    switch (PT_speed)
    {
            // generate 100 Bd QRT pachet
        case
                PT_SPEED_100:
            bit_count       = LONG_BURST_BIT_COUNT_100;
            // header
            send_QRT_Packet_buffer[0]  = PT_CS;
            // copy buffer content
            for (i = 0; i < LONG_BURST_BYTE_COUNT_100; i++)
            {
                send_QRT_Packet_buffer[i + 1]  = reverse_callsign[i];
            }
            // add status
            burst_status    = (PT_pkt_counter & 0x03) | QRT_BIT;
            send_QRT_Packet_buffer[LONG_BURST_BYTE_COUNT_100 + 1] = burst_status;
            // add CRC
            computed_CRC    = CRC_check(0x00, LONG_BURST_BYTE_COUNT_100 + 1, &send_QRT_Packet_buffer[1]);
            send_QRT_Packet_buffer[LONG_BURST_BYTE_COUNT_100 + 2] = computed_CRC & 0x0ff;
            send_QRT_Packet_buffer[LONG_BURST_BYTE_COUNT_100 + 3] = (computed_CRC >> 8) & 0x0ff;
#ifdef SEND_PKT_DISPLAY
            PACTOR_TNC_DEBUG << "*SEND QRT PACKET 100* at" << m_pactorTNC->m_pactoraudioengine->thread_info.next_tx_time
                             << "message" << QString(reverse_callsign)
                             << hex << "CRC" << computed_CRC << "data"
                             << (unsigned char)send_QRT_Packet_buffer[0]
                             << (unsigned char)send_QRT_Packet_buffer[1]
                             << (unsigned char)send_QRT_Packet_buffer[2]
                             << (unsigned char)send_QRT_Packet_buffer[3]
                             << (unsigned char)send_QRT_Packet_buffer[4]
                             << (unsigned char)send_QRT_Packet_buffer[5]
                             << (unsigned char)send_QRT_Packet_buffer[6]
                             << (unsigned char)send_QRT_Packet_buffer[7]
                             << (unsigned char)send_QRT_Packet_buffer[8]
                             << (unsigned char)send_QRT_Packet_buffer[9]
                             << (unsigned char)send_QRT_Packet_buffer[10]
                             << (unsigned char)send_QRT_Packet_buffer[11]
                             << "PT_CS" << (unsigned char)PT_CS
                             ;
#endif
            break;
            // generate 100 Bd QRT pachet
        case
                PT_SPEED_200:
            bit_count       = LONG_BURST_BIT_COUNT_200;
            // header
            send_QRT_Packet_buffer[0]  = PT_CS;
            send_QRT_Packet_buffer[1]  = 0x0F;
            // expand 8 chars at 100BD (fill 0x0F + reverse callsign + 0x55) to 200Bd buffer
            j   = 2;
            for (i = 0; i < 8; i++, j += 2)
            {
                temp_100to200   = conv_100_to_200[(unsigned int)reverse_callsign[i] & 0x0ff];
                send_QRT_Packet_buffer[j]       = (char)(temp_100to200 & 0x0ff);
                send_QRT_Packet_buffer[j + 1]   = (char)((temp_100to200 >> 8) & 0x0ff);
            }
            // fill buffer with 0x1e
            for (; j < LONG_BURST_BYTE_COUNT_200; j++)
            {
                send_QRT_Packet_buffer[j]  = 0x1e;
            }
            // add status
            burst_status    = (PT_pkt_counter & 0x03) | QRT_BIT;
            send_QRT_Packet_buffer[LONG_BURST_BYTE_COUNT_200 + 1] = burst_status;
            // add CRC
            computed_CRC    = CRC_check(0x00, LONG_BURST_BYTE_COUNT_200 + 1, &send_QRT_Packet_buffer[1]);
            send_QRT_Packet_buffer[LONG_BURST_BYTE_COUNT_200 + 2] = computed_CRC & 0x0ff;
            send_QRT_Packet_buffer[LONG_BURST_BYTE_COUNT_200 + 3] = (computed_CRC >> 8) & 0x0ff;

#ifdef SEND_PKT_DISPLAY
            PACTOR_TNC_DEBUG << "*SEND QRT PACKET 200* at" << m_pactorTNC->m_pactoraudioengine->thread_info.next_tx_time
                             << "message" << QString(reverse_callsign)
                             << hex << "CRC" << computed_CRC << "data"
                             << (unsigned char)send_QRT_Packet_buffer[0]
                             << (unsigned char)send_QRT_Packet_buffer[1]
                             << (unsigned char)send_QRT_Packet_buffer[2]
                             << (unsigned char)send_QRT_Packet_buffer[3]
                             << (unsigned char)send_QRT_Packet_buffer[4]
                             << (unsigned char)send_QRT_Packet_buffer[5]
                             << (unsigned char)send_QRT_Packet_buffer[6]
                             << (unsigned char)send_QRT_Packet_buffer[7]
                             << (unsigned char)send_QRT_Packet_buffer[8]
                             << (unsigned char)send_QRT_Packet_buffer[9]
                             << (unsigned char)send_QRT_Packet_buffer[10]
                             << (unsigned char)send_QRT_Packet_buffer[11]
                             << (unsigned char)send_QRT_Packet_buffer[12]
                             << (unsigned char)send_QRT_Packet_buffer[13]
                             << (unsigned char)send_QRT_Packet_buffer[14]
                             << (unsigned char)send_QRT_Packet_buffer[15]
                             << (unsigned char)send_QRT_Packet_buffer[16]
                             << (unsigned char)send_QRT_Packet_buffer[17]
                             << (unsigned char)send_QRT_Packet_buffer[18]
                             << (unsigned char)send_QRT_Packet_buffer[19]
                             << (unsigned char)send_QRT_Packet_buffer[20]
                             << (unsigned char)send_QRT_Packet_buffer[21]
                             << (unsigned char)send_QRT_Packet_buffer[22]
                             << (unsigned char)send_QRT_Packet_buffer[23]
                             << "PT_CS" << (unsigned char)PT_CS
                             ;
#endif
            break;
        default
                :
            break;
    }
    // send buffer to modulator
#ifndef PACKETS_TEST
    emit calculateModFSK((unsigned char *)send_QRT_Packet_buffer,
                         bit_count,
                         tx_speed,
                         PT_PH_NORMAL,
                         m_pactorTNC->m_pactoraudioengine->thread_info.packet_audio_samples[0],
                         &m_pactorTNC->m_pactoraudioengine->thread_info.burst_audio_count);
    emit calculateModFSK((unsigned char *)send_QRT_Packet_buffer,
                         bit_count,
                         tx_speed,
                         PT_PH_INVERTED,
                         m_pactorTNC->m_pactoraudioengine->thread_info.packet_audio_samples[1],
                         &m_pactorTNC->m_pactoraudioengine->thread_info.burst_audio_count);
    // start timeout timer
    m_pactorTNC->m_pactoraudioengine->thread_info.new_burst       = true;
#endif
}

/************************************************************************************/
// create BREAK packet WITH included CRC
/************************************************************************************/
void PactorTNCThread::send_BREAK_packet(void)
{
    static char         send_BREAK_Packet_buffer[LONG_BURST_BYTE_COUNT_200 + 5];
    int                 bit_count;
    unsigned int        computed_CRC;
    char                burst_status;
    int                 data_length;
    int                 i;
    QByteArray          *tx_buffer;

    int                 tx_speed = (PT_speed == PT_SPEED_100) ? SPEED_100 : SPEED_200;

    tx_buffer       = m_pactorTNC->tx_text_buffs.ascii_text;

    switch (PT_speed)
    {
        case
                PT_SPEED_100:
            bit_count       = LONG_BURST_BIT_COUNT_100;
            // compute data length that will be transmitted
            data_length = qMin(LONG_BURST_BYTE_COUNT_100 - 1, m_pactorTNC->tx_text_buffs.ascii_text->count());

            // header
            send_BREAK_Packet_buffer[0]  = PT_CS3_0;
            send_BREAK_Packet_buffer[1]  = PT_CS3_1;
            // copy buffer content
            for (i = 0; i < data_length; i++)
            {
                send_BREAK_Packet_buffer[i + 2]  = tx_buffer->at(i);
            }
            // fill with 0x1e chars)
            for (; i < LONG_BURST_BYTE_COUNT_100 - 1; i++)
            {
                send_BREAK_Packet_buffer[i + 2]  = 0x1e;
            }
            // add status
            burst_status    = 0;
            send_BREAK_Packet_buffer[LONG_BURST_BYTE_COUNT_100 + 1] = burst_status;
            // add CRC
            computed_CRC    = CRC_check(0x00, LONG_BURST_BYTE_COUNT_100, &send_BREAK_Packet_buffer[2]);
            send_BREAK_Packet_buffer[LONG_BURST_BYTE_COUNT_100 + 2] = computed_CRC & 0x0ff;
            send_BREAK_Packet_buffer[LONG_BURST_BYTE_COUNT_100 + 3] = (computed_CRC >> 8) & 0x0ff;
#ifdef SEND_PKT_DISPLAY
            PACTOR_TNC_DEBUG << "*SEND PACKET_BREAK_ASCII_100* at" << m_pactorTNC->m_pactoraudioengine->thread_info.next_tx_time
                             << hex << "CRC" << computed_CRC << "data"
                             << (unsigned char)send_BREAK_Packet_buffer[0]
                             << (unsigned char)send_BREAK_Packet_buffer[1]
                             << (unsigned char)send_BREAK_Packet_buffer[2]
                             << (unsigned char)send_BREAK_Packet_buffer[3]
                             << (unsigned char)send_BREAK_Packet_buffer[4]
                             << (unsigned char)send_BREAK_Packet_buffer[5]
                             << (unsigned char)send_BREAK_Packet_buffer[6]
                             << (unsigned char)send_BREAK_Packet_buffer[7]
                             << (unsigned char)send_BREAK_Packet_buffer[8]
                             << (unsigned char)send_BREAK_Packet_buffer[9]
                             << (unsigned char)send_BREAK_Packet_buffer[10]
                             << (unsigned char)send_BREAK_Packet_buffer[11];
#endif
            break;

        case
                PT_SPEED_200:
            bit_count       = LONG_BURST_BIT_COUNT_200;
            // compute data length that will be transmitted
            data_length = qMin(LONG_BURST_BYTE_COUNT_200 - 2, m_pactorTNC->tx_text_buffs.ascii_text->count());

            // header
            send_BREAK_Packet_buffer[0]  = PT_CS3_200_0;
            send_BREAK_Packet_buffer[1]  = PT_CS3_200_1;
            send_BREAK_Packet_buffer[2]  = PT_CS3_200_2;
            // copy buffer content
            for (i = 0; i < data_length; i++)
            {
                send_BREAK_Packet_buffer[i + 3]  = tx_buffer->at(i);
            }
            // fill with 0x1e chars)
            for (; i < LONG_BURST_BYTE_COUNT_200 - 2; i++)
            {
                send_BREAK_Packet_buffer[i + 3]  = 0x1e;
            }
            // add status
            burst_status    = 0;
            send_BREAK_Packet_buffer[LONG_BURST_BYTE_COUNT_200 + 1] = burst_status;
            // add CRC
            computed_CRC    = CRC_check(0x00, LONG_BURST_BYTE_COUNT_200 - 1, &send_BREAK_Packet_buffer[3]);
            send_BREAK_Packet_buffer[LONG_BURST_BYTE_COUNT_200 + 2] = computed_CRC & 0x0ff;
            send_BREAK_Packet_buffer[LONG_BURST_BYTE_COUNT_200 + 3] = (computed_CRC >> 8) & 0x0ff;
#ifdef SEND_PKT_DISPLAY
            PACTOR_TNC_DEBUG << "*SEND PACKET_BREAK_ASCII_200* at" << m_pactorTNC->m_pactoraudioengine->thread_info.next_tx_time
                             << hex << "CRC" << computed_CRC << "data"
                             << (unsigned char)send_BREAK_Packet_buffer[0]
                             << (unsigned char)send_BREAK_Packet_buffer[1]
                             << (unsigned char)send_BREAK_Packet_buffer[2]
                             << (unsigned char)send_BREAK_Packet_buffer[3]
                             << (unsigned char)send_BREAK_Packet_buffer[4]
                             << (unsigned char)send_BREAK_Packet_buffer[5]
                             << (unsigned char)send_BREAK_Packet_buffer[6]
                             << (unsigned char)send_BREAK_Packet_buffer[7]
                             << (unsigned char)send_BREAK_Packet_buffer[8]
                             << (unsigned char)send_BREAK_Packet_buffer[9]
                             << (unsigned char)send_BREAK_Packet_buffer[10]
                             << (unsigned char)send_BREAK_Packet_buffer[11]
                             << (unsigned char)send_BREAK_Packet_buffer[12]
                             << (unsigned char)send_BREAK_Packet_buffer[13]
                             << (unsigned char)send_BREAK_Packet_buffer[14]
                             << (unsigned char)send_BREAK_Packet_buffer[15]
                             << (unsigned char)send_BREAK_Packet_buffer[16]
                             << (unsigned char)send_BREAK_Packet_buffer[17]
                             << (unsigned char)send_BREAK_Packet_buffer[18]
                             << (unsigned char)send_BREAK_Packet_buffer[19]
                             << (unsigned char)send_BREAK_Packet_buffer[20]
                             << (unsigned char)send_BREAK_Packet_buffer[21]
                             << (unsigned char)send_BREAK_Packet_buffer[22]
                             << (unsigned char)send_BREAK_Packet_buffer[23];
#endif
            break;
        default
                :
            break;
    }
#ifndef PACKETS_TEST
    // transmit packet (because CRC is already included
    emit calculateModFSK((unsigned char *)send_BREAK_Packet_buffer,
                         bit_count,
                         tx_speed,
                         PT_PH_NORMAL,
                         m_pactorTNC->m_pactoraudioengine->thread_info.packet_audio_samples[0],
                         &m_pactorTNC->m_pactoraudioengine->thread_info.burst_audio_count);
    emit calculateModFSK((unsigned char *)send_BREAK_Packet_buffer,
                         bit_count,
                         tx_speed,
                         PT_PH_INVERTED,
                         m_pactorTNC->m_pactoraudioengine->thread_info.packet_audio_samples[1],
                         &m_pactorTNC->m_pactoraudioengine->thread_info.burst_audio_count);
    // start timeout timer
    m_pactorTNC->m_pactoraudioengine->thread_info.new_burst       = true;
#endif
    // become ISS
    engine_info->PT_Tx_Rx               = PT_TX;
    engine_info->tx_data_or_CS3_burst   = true;
    engine_info->receiving_CS           = true;
}

/************************************************************************************/
// create mycall packet
/************************************************************************************/
void PactorTNCThread::send_mycall_packet(void)
{
    static char         send_mycall_Packet_buffer[LONG_BURST_BYTE_COUNT_200 + 5];
    QByteArray          callsign_buffer;
    int                 data_length;
    int                 bit_count;
    int                 i;
    unsigned int        computed_CRC;
    char                burst_status;

    int                 tx_speed = (PT_speed == PT_SPEED_100) ? SPEED_100 : SPEED_200;

    // fill callsign buffer
    callsign_buffer = *m_pactorTNC->tx_text_buffs.my_callsign;

    // add callsign
    switch (PT_speed)
    {
        case
                PT_SPEED_100:
            // callsign
            // compute data length that will be transmitted. callsign is 6 bytes long maximum
            data_length = qMin(LONG_BURST_BYTE_COUNT_100 - 2, callsign_buffer.count());
            bit_count       = LONG_BURST_BIT_COUNT_100;

            // header
            send_mycall_Packet_buffer[0]  = PT_CS2;
            // Pactor level
            send_mycall_Packet_buffer[1]  = PACTOR_LEVEL;
            // copy buffer content
            for (i = 0; i < data_length; i++)
            {
                send_mycall_Packet_buffer[i + 2]  = callsign_buffer.at(i);
            }
            // add <CR>
            send_mycall_Packet_buffer[i + 2]  = 0x0d;
            i++;
            // fill with 0x1e chars)
            for (; i < LONG_BURST_BYTE_COUNT_100 - 1; i++)
            {
                send_mycall_Packet_buffer[i + 2]  = 0x1e;
            }
            // add status
            burst_status    = (PT_pkt_counter & 0x03);
            send_mycall_Packet_buffer[LONG_BURST_BYTE_COUNT_100 + 1] = burst_status;
            // add CRC
            computed_CRC    = CRC_check(0x00, LONG_BURST_BYTE_COUNT_100 + 1, &send_mycall_Packet_buffer[1]);
            send_mycall_Packet_buffer[LONG_BURST_BYTE_COUNT_100 + 2] = computed_CRC & 0x0ff;
            send_mycall_Packet_buffer[LONG_BURST_BYTE_COUNT_100 + 3] = (computed_CRC >> 8) & 0x0ff;
#ifdef SEND_PKT_DISPLAY
            PACTOR_TNC_DEBUG << "*SEND PACKET_MYCALL_100* at" << m_pactorTNC->m_pactoraudioengine->thread_info.next_tx_time
                             << hex << "CRC" << computed_CRC << "data"
                             << (unsigned char)send_mycall_Packet_buffer[0]
                             << (unsigned char)send_mycall_Packet_buffer[1]
                             << (unsigned char)send_mycall_Packet_buffer[2]
                             << (unsigned char)send_mycall_Packet_buffer[3]
                             << (unsigned char)send_mycall_Packet_buffer[4]
                             << (unsigned char)send_mycall_Packet_buffer[5]
                             << (unsigned char)send_mycall_Packet_buffer[6]
                             << (unsigned char)send_mycall_Packet_buffer[7]
                             << (unsigned char)send_mycall_Packet_buffer[8]
                             << (unsigned char)send_mycall_Packet_buffer[9]
                             << (unsigned char)send_mycall_Packet_buffer[10]
                             << (unsigned char)send_mycall_Packet_buffer[11];
#endif
            break;

        case
                PT_SPEED_200:
            // callsign
            // compute data length that will be transmitted. callsign is 6 bytes long maximum
            data_length = qMin(LONG_BURST_BYTE_COUNT_200 - 2, callsign_buffer.count());
            bit_count       = LONG_BURST_BIT_COUNT_200;

            // header
            send_mycall_Packet_buffer[0]  = PT_CS2;
            // Pactor level
            send_mycall_Packet_buffer[1]  = PACTOR_LEVEL;
            // copy buffer content
            for (i = 0; i < data_length; i++)
            {
                send_mycall_Packet_buffer[i + 2]  = callsign_buffer.at(i);
            }
            // add <CR>
            send_mycall_Packet_buffer[i + 2]  = 0x0d;
            i++;
            // fill with 0x1e chars)
            for (; i < LONG_BURST_BYTE_COUNT_200 - 1; i++)
            {
                send_mycall_Packet_buffer[i + 2]  = 0x1e;
            }
            // add status
            burst_status    = (PT_pkt_counter & 0x03);
            send_mycall_Packet_buffer[LONG_BURST_BYTE_COUNT_200 + 1] = burst_status;
            // add CRC
            computed_CRC    = CRC_check(0x00, LONG_BURST_BYTE_COUNT_200 + 1, &send_mycall_Packet_buffer[1]);
            send_mycall_Packet_buffer[LONG_BURST_BYTE_COUNT_200 + 2] = computed_CRC & 0x0ff;
            send_mycall_Packet_buffer[LONG_BURST_BYTE_COUNT_200 + 3] = (computed_CRC >> 8) & 0x0ff;
#ifdef SEND_PKT_DISPLAY
            PACTOR_TNC_DEBUG << "*SEND PACKET_MYCALL_200* at" << m_pactorTNC->m_pactoraudioengine->thread_info.next_tx_time
                             << hex << "CRC" << computed_CRC << "data"
                             << (unsigned char)send_mycall_Packet_buffer[0]
                             << (unsigned char)send_mycall_Packet_buffer[1]
                             << (unsigned char)send_mycall_Packet_buffer[2]
                             << (unsigned char)send_mycall_Packet_buffer[3]
                             << (unsigned char)send_mycall_Packet_buffer[4]
                             << (unsigned char)send_mycall_Packet_buffer[5]
                             << (unsigned char)send_mycall_Packet_buffer[6]
                             << (unsigned char)send_mycall_Packet_buffer[7]
                             << (unsigned char)send_mycall_Packet_buffer[8]
                             << (unsigned char)send_mycall_Packet_buffer[9]
                             << (unsigned char)send_mycall_Packet_buffer[10]
                             << (unsigned char)send_mycall_Packet_buffer[11]
                             << (unsigned char)send_mycall_Packet_buffer[12]
                             << (unsigned char)send_mycall_Packet_buffer[13]
                             << (unsigned char)send_mycall_Packet_buffer[14]
                             << (unsigned char)send_mycall_Packet_buffer[15]
                             << (unsigned char)send_mycall_Packet_buffer[16]
                             << (unsigned char)send_mycall_Packet_buffer[17]
                             << (unsigned char)send_mycall_Packet_buffer[18]
                             << (unsigned char)send_mycall_Packet_buffer[19]
                             << (unsigned char)send_mycall_Packet_buffer[20]
                             << (unsigned char)send_mycall_Packet_buffer[21]
                             << (unsigned char)send_mycall_Packet_buffer[22]
                             << (unsigned char)send_mycall_Packet_buffer[23];
#endif
            break;
        default
                :
            break;
    }

#ifndef PACKETS_TEST
    emit calculateModFSK((unsigned char *)send_mycall_Packet_buffer,
                         bit_count,
                         tx_speed,
                         PT_PH_NORMAL,
                         m_pactorTNC->m_pactoraudioengine->thread_info.packet_audio_samples[0],
                         &m_pactorTNC->m_pactoraudioengine->thread_info.burst_audio_count);
    emit calculateModFSK((unsigned char *)send_mycall_Packet_buffer,
                         bit_count,
                         tx_speed,
                         PT_PH_INVERTED,
                         m_pactorTNC->m_pactoraudioengine->thread_info.packet_audio_samples[1],
                         &m_pactorTNC->m_pactoraudioengine->thread_info.burst_audio_count);
    // start timeout timer
    m_pactorTNC->m_pactoraudioengine->thread_info.new_burst       = true;
#endif
}

/************************************************************************************/
// create connection request packet
/************************************************************************************/
void PactorTNCThread::send_conn_request_packet(bool short_path)
{
    static char         send_conn100_Packet_buffer[LONG_BURST_BYTE_COUNT_200 + 5];
    static char         send_conn200_Packet_buffer[LONG_BURST_BYTE_COUNT_200 + 5];
    int                 data_length;
    int                 i, j;
    short               temp_100to200;
    QByteArray          *tx_buffer;

    tx_buffer       = m_pactorTNC->tx_text_buffs.remote_callsign;


    // callsign
    // compute data length that will be transmitted
    data_length = qMin(LONG_BURST_BYTE_COUNT_100, tx_buffer->count());
    // header
    send_conn100_Packet_buffer[0]  = PT_CS1;
    // copy buffer content
    for (i = 0; i < data_length; i++)
    {
        send_conn100_Packet_buffer[i + 1]  = tx_buffer->at(i);
    }
    // invert 1st character if in long path mode
    if (short_path == false)
    {
        send_conn100_Packet_buffer[1]   = ~send_conn100_Packet_buffer[1];
        m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time   = TIMER_SHORT_PATH_MS;
        m_pactorTNC->m_pactorDemodulator->m_thread->sample_sync_count_initial   = 2000;
    }
    else
    {
        m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time   = TIMER_LONG_PATH_MS;
        m_pactorTNC->m_pactorDemodulator->m_thread->sample_sync_count_initial   = 2240;
    }
    // m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time   = TIMER_UNPROTO_MS;
    // fill with 0x1e chars)
    for (; i < LONG_BURST_BYTE_COUNT_100; i++)
    {
        send_conn100_Packet_buffer[i + 1]  = 0x1e;
    }

    // initialize 200 Bd buffer
    for (i = 0; i < LONG_BURST_BYTE_COUNT_200 + 4; i++)
    {
        send_conn200_Packet_buffer[i]  = 0x1e;
    }

    // expand 100BD part to 200Bd buffer
    for (i = 0; i < 9; i++)
    {
        temp_100to200   = conv_100_to_200[(unsigned int)send_conn100_Packet_buffer[i] & 0x0ff];
        send_conn200_Packet_buffer[i * 2]       = (char)(temp_100to200 & 0x0ff);
        send_conn200_Packet_buffer[(i * 2) + 1] = (char)((temp_100to200 >> 8) & 0x0ff);
    }
    // copy to 200 Bd buffer's 200Bd part
    // initialize 200 Bd buffer
    j   = 0;
    for (i = LONG_BURST_BYTE_COUNT_200 - 2; i < LONG_BURST_BYTE_COUNT_200 + 4; i++, j++)
    {
        if (j < tx_buffer->size())
        {
            send_conn200_Packet_buffer[i]  = tx_buffer->at(j);
        }
        else
        {
            send_conn200_Packet_buffer[i]  = 0x1e;
        }
    }
#ifdef SEND_PKT_DISPLAY
    PACTOR_TNC_DEBUG << "*SEND PACKET_CONNECT*100* at" << m_pactorTNC->m_pactoraudioengine->thread_info.next_tx_time << hex
                     << (unsigned char)send_conn100_Packet_buffer[0]
                     << (unsigned char)send_conn100_Packet_buffer[1]
                     << (unsigned char)send_conn100_Packet_buffer[2]
                     << (unsigned char)send_conn100_Packet_buffer[3]
                     << (unsigned char)send_conn100_Packet_buffer[4]
                     << (unsigned char)send_conn100_Packet_buffer[5]
                     << (unsigned char)send_conn100_Packet_buffer[6]
                     << (unsigned char)send_conn100_Packet_buffer[7]
                     << (unsigned char)send_conn100_Packet_buffer[8];
    PACTOR_TNC_DEBUG << "*SEND*200*"
                     << hex        << (unsigned char)send_conn200_Packet_buffer[18]
                     << (unsigned char)send_conn200_Packet_buffer[19]
                     << (unsigned char)send_conn200_Packet_buffer[20]
                     << (unsigned char)send_conn200_Packet_buffer[21]
                     << (unsigned char)send_conn200_Packet_buffer[22]
                     << (unsigned char)send_conn200_Packet_buffer[23];

#endif
#ifndef PACKETS_TEST
    // send buffer to modulator
    emit calculateModFSK((unsigned char *)send_conn200_Packet_buffer,
                         LONG_BURST_BIT_COUNT_200,
                         SPEED_200,
                         PT_PH_NORMAL,
                         m_pactorTNC->m_pactoraudioengine->thread_info.packet_audio_samples[0],
                         &m_pactorTNC->m_pactoraudioengine->thread_info.burst_audio_count);
    emit calculateModFSK((unsigned char *)send_conn200_Packet_buffer,
                         LONG_BURST_BIT_COUNT_200,
                         SPEED_200,
                         PT_PH_INVERTED,
                         m_pactorTNC->m_pactoraudioengine->thread_info.packet_audio_samples[1],
                         &m_pactorTNC->m_pactoraudioengine->thread_info.burst_audio_count);
    // start timeout timer
    m_pactorTNC->m_pactoraudioengine->thread_info.new_burst       = true;
#endif
}

/************************************************************************************/
// encode supervisor information
/************************************************************************************/
void PactorTNCThread::supervisor_data(e_si_type si_type, char ctrl_char)
{
    m_pactorTNC->tx_text_buffs.supervisor->clear();
    m_pactorTNC->tx_text_buffs.supervisor->append((char)0x1c);

    switch (si_type)
    {
        case
                e_PT_si_pactor_level:
            m_pactorTNC->tx_text_buffs.supervisor->append((char)PACTOR_LEVEL);
            si_encoded  = true;
            break;
        case
                e_PT_si_mycall:
            m_pactorTNC->tx_text_buffs.supervisor->append((const QByteArray &)m_pactorTNC->tx_text_buffs.my_callsign);
            si_encoded  = true;
            break;
        case
                e_PT_si_ctrl_char:
            if ((unsigned char)ctrl_char <= 0x1F)
            {
                m_pactorTNC->tx_text_buffs.supervisor->append((char)(ctrl_char + 0x60));
                si_encoded  = true;
            }
            break;
    }
    m_pactorTNC->tx_text_buffs.supervisor->append(' ');
}

/************************************************************************************/
// send a data packet or a CS3
/************************************************************************************/
int PactorTNCThread::PTEnqueuePacket(e_PT_CS in_PT_CS,
                                     int break_request)
{
    int             i;
    unsigned int    computed_CRC;
    char            burst_status;
    int             data_length;
    int             bit_size;
    int             byte_count;
    static char     PTEnqueuePacket_buffer[LONG_BURST_BYTE_COUNT_200 + 5];
    QByteArray      *tx_buffer;
    bool            PT_huffman;

    int             tx_speed = (PT_speed == PT_SPEED_100) ? SPEED_100 : SPEED_200;

    // use CS argument
    if (in_PT_CS != PT_CS_ANY)
    {
        PT_CS         = in_PT_CS;
    }
    else
    {
        // alternate CS if PT_CS_ANY has been passed in arguments
        if (PT_CS == PT_CS1)
        {
            PT_CS   = PT_CS2;
        }
        else
        {
            PT_CS   = PT_CS1;
        }
    }
    if (break_request != 0)
    {
        PACTOR_TNC_DEBUG << "break_request modutils l 612";
    }
    // Huffman finished?
    if (((PT_huffman_encode == true) && (m_pactorTNC->tx_text_buffs.huffman->size() == 0))
        || (si_encoded == true)
        || (PT_huffman_encode == false))
    {
        PT_huffman    = false;
    }
    // waiting for mycallsign ACK and downspeed
    if (m_pactorTNC->wait_callsign_ack)
    {
        tx_buffer       = m_pactorTNC->tx_text_buffs.my_callsign;
    }
    else
        // Huffman and data in text buffer
        if (PT_huffman == true)
        {
            tx_buffer       = m_pactorTNC->tx_text_buffs.huffman;
        }
        else
            // ASCII text
        {
            if (si_encoded == true)
            {
                m_pactorTNC->tx_text_buffs.ascii_text->push_front((const QByteArray &)m_pactorTNC->tx_text_buffs.supervisor);
                si_encoded  = false;
            }
            tx_buffer       = m_pactorTNC->tx_text_buffs.ascii_text;
        }
    // payload size
    if (PT_speed == PT_SPEED_100)
    {
        byte_count  = LONG_BURST_BYTE_COUNT_100;
        bit_size    = LONG_BURST_BIT_COUNT_100;
    }
    else
    {
        byte_count  = LONG_BURST_BYTE_COUNT_200;
        bit_size    = LONG_BURST_BIT_COUNT_200;
    }
    // change send LED status
    LED_args[0].led_index   = LED_TX; // Send LED
    LED_args[0].icon_color = RED_ICON;
    LED_args[1].led_index   = LED_TRANS; // transmitting data LED
    if (tx_buffer->size() > 0)
    {
        LED_args[1].icon_color = RED_ICON;
        m_pactorTNC->m_pactor_session->m_outputText->setText(QString(tx_buffer->constData()));
    }
    else
    {
        LED_args[1].icon_color = GREY_ICON;
        m_pactorTNC->m_pactor_session->m_outputText->setText("");
    }
    emit setLED(2, LED_args);

    //#ifdef TX_PKT_DISPLAY
#if 1
    PACTOR_TNC_DEBUG << "*TX* phase" << PT_phase << "next phase" << m_pactorTNC->m_pactoraudioengine->thread_info.next_phase
                     << "speed" << tx_speed << "pkt cnt" << (PT_pkt_counter & 0x03)
                     << "m_timer_ms" << m_timer_ms << "at" << global_date_time_mSecs
                     << "bytes" << byte_count << "bits" << bit_size
#ifdef VOX_CONTROL
                     << "vox chan" << m_pactorTNC->m_pactoraudioengine->thread_info.vox_channel
                     << "vox start" << m_pactorTNC->m_pactoraudioengine->thread_info.vox_start_delay
                     << "vox stop" << m_pactorTNC->m_pactoraudioengine->thread_info.vox_stop_delay
#endif
                     << hex << "CS" << PT_CS << "header" << PT_CS
                     ;
#endif
    // processing depends on packet type
    // both phase packets are generated by calls to calculateModFSK() with positive and negative phase arguemnt
    // compute data length that will be transmitted
    data_length = qMin(byte_count, tx_buffer->count());
    // header
    PTEnqueuePacket_buffer[0]  = PT_CS;
    // copy buffer content
    for (i = 0; i < data_length; i++)
    {
        PTEnqueuePacket_buffer[i + 1]  = tx_buffer->at(i);
    }
    // fill with 0x1e chars)
    for (; i < byte_count; i++)
    {
        PTEnqueuePacket_buffer[i + 1]  = 0x1e;
    }
    // add status
    burst_status    = (PT_pkt_counter & 0x03) | (break_request << 6) | (PT_huffman << 2);
    PTEnqueuePacket_buffer[byte_count + 1] = burst_status;
    // add CRC
    computed_CRC    = CRC_check(0x00, byte_count + 1, &PTEnqueuePacket_buffer[1]);
    PTEnqueuePacket_buffer[byte_count + 2] = computed_CRC & 0x0ff;
    PTEnqueuePacket_buffer[byte_count + 3] = (computed_CRC >> 8) & 0x0ff;

#ifdef SEND_PKT_DISPLAY
    if (PT_speed == PT_SPEED_100)
    {
        PACTOR_TNC_DEBUG << "*SEND PACKET 100Bd* bit cnt" << bit_size << " at" << m_pactorTNC->m_pactoraudioengine->thread_info.next_tx_time
                         << "break" << break_request << "huffman" << PT_huffman
                         << hex << "status byte" << (unsigned char)burst_status
                         << "CRC" << computed_CRC << "data"
                         << (unsigned char)PTEnqueuePacket_buffer[0]
                         << (unsigned char)PTEnqueuePacket_buffer[1]
                         << (unsigned char)PTEnqueuePacket_buffer[2]
                         << (unsigned char)PTEnqueuePacket_buffer[3]
                         << (unsigned char)PTEnqueuePacket_buffer[4]
                         << (unsigned char)PTEnqueuePacket_buffer[5]
                         << (unsigned char)PTEnqueuePacket_buffer[6]
                         << (unsigned char)PTEnqueuePacket_buffer[7]
                         << (unsigned char)PTEnqueuePacket_buffer[8]
                         << (unsigned char)PTEnqueuePacket_buffer[9]
                         << (unsigned char)PTEnqueuePacket_buffer[10]
                         << (unsigned char)PTEnqueuePacket_buffer[11];
    }
    else
    {
        PACTOR_TNC_DEBUG << "*SEND PACKET 200Bd* bit cnt" << bit_size << " at" << m_pactorTNC->m_pactoraudioengine->thread_info.next_tx_time
                         << "break" << break_request << "huffman" << PT_huffman
                         << hex << "status byte" << (unsigned char)burst_status
                         << "CRC" << computed_CRC << "data"
                         << (unsigned char)PTEnqueuePacket_buffer[0]
                         << (unsigned char)PTEnqueuePacket_buffer[1]
                         << (unsigned char)PTEnqueuePacket_buffer[2]
                         << (unsigned char)PTEnqueuePacket_buffer[3]
                         << (unsigned char)PTEnqueuePacket_buffer[4]
                         << (unsigned char)PTEnqueuePacket_buffer[5]
                         << (unsigned char)PTEnqueuePacket_buffer[6]
                         << (unsigned char)PTEnqueuePacket_buffer[7]
                         << (unsigned char)PTEnqueuePacket_buffer[8]
                         << (unsigned char)PTEnqueuePacket_buffer[9]
                         << (unsigned char)PTEnqueuePacket_buffer[10]
                         << (unsigned char)PTEnqueuePacket_buffer[11]
                         << (unsigned char)PTEnqueuePacket_buffer[12]
                         << (unsigned char)PTEnqueuePacket_buffer[13]
                         << (unsigned char)PTEnqueuePacket_buffer[14]
                         << (unsigned char)PTEnqueuePacket_buffer[15]
                         << (unsigned char)PTEnqueuePacket_buffer[16]
                         << (unsigned char)PTEnqueuePacket_buffer[17]
                         << (unsigned char)PTEnqueuePacket_buffer[18]
                         << (unsigned char)PTEnqueuePacket_buffer[19]
                         << (unsigned char)PTEnqueuePacket_buffer[20]
                         << (unsigned char)PTEnqueuePacket_buffer[21]
                         << (unsigned char)PTEnqueuePacket_buffer[22]
                         << (unsigned char)PTEnqueuePacket_buffer[23];
    }
#endif
#ifndef PACKETS_TEST
    // send buffer to modulator
    emit calculateModFSK((unsigned char *)PTEnqueuePacket_buffer,
                         bit_size,
                         tx_speed,
                         PT_PH_NORMAL,
                         m_pactorTNC->m_pactoraudioengine->thread_info.packet_audio_samples[0],
                         &m_pactorTNC->m_pactoraudioengine->thread_info.burst_audio_count);
    emit calculateModFSK((unsigned char *)PTEnqueuePacket_buffer,
                         bit_size,
                         tx_speed,
                         PT_PH_INVERTED,
                         m_pactorTNC->m_pactoraudioengine->thread_info.packet_audio_samples[1],
                         &m_pactorTNC->m_pactoraudioengine->thread_info.burst_audio_count);
#endif
    // start timeout timer
    m_pactorTNC->m_pactoraudioengine->thread_info.new_burst       = true;

    return data_length;
}


