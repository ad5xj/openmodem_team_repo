#include <QtCore/QSettings>
#include <QtCore/QBuffer>
#include <QtCore/QByteArray>
#include <QtCore/QThread>
#include <QtCore/QTimer>
#include <QtNetwork/QtNetwork>
#include <QtNetwork/QHostAddress>

#include "globals.h"
#include "crccalc.h"
#include "pactordecoder.h"
#include "pactortnc.h"
#include "pactormodulator.h"
#include "pactordemodulator.h"
#include "pactoraudioengine.h"
#include "pactornetstruct.h"

#undef TCP_DEBUG
//-----------------------------------------------------------------------------
// UDP broadcast, TCP server and client
//-----------------------------------------------------------------------------
void PactorTNC::openCommunications(void)
{
    bool    host_error;
    int     error_count;

    // UDP
    udpSocket = new QUdpSocket(this);

    // if we did not find one, use IPv4 localhost
    if (ipCommAddress.isEmpty())
    {
        ipCommAddress = QHostAddress(QHostAddress::LocalHost).toString();
    }
    // TCP client
    // find out which IP to connect to if connection fails
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();

    // use the first non-localhost IPv4 address
    error_count = 0;
    do
    {
        host_error  = false;
        // try other addresses
        if (ipCommAddress.isEmpty())
        {
            for (int i = 0; i < ipAddressesList.size(); ++i)
            {
                if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
                        ipAddressesList.at(i).toIPv4Address())
                {
                    ipCommAddress = ipAddressesList.at(i).toString();
                    break;
                }
            }
        }
        // if we did not find one, use IPv4 localhost
        if (ipCommAddress.isEmpty())
        {
            ipCommAddress = QHostAddress(QHostAddress::LocalHost).toString();
        }


        QNetworkConfigurationManager manager;
        if (manager.capabilities() & QNetworkConfigurationManager::NetworkSessionRequired)
        {
            // Get saved network configuration
            QSettings settings(QSettings::UserScope, QLatin1String("Trolltech"));
            settings.beginGroup(QLatin1String("QtNetwork"));
            const QString id = settings.value(QLatin1String("DefaultNetworkConfiguration")).toString();
            settings.endGroup();

            PACTOR_TNC_DEBUG << "Unable to start the TCP Port named " << id
                             << "at "  << ipCommAddress << tcp_port
                             << "error:"  << tcpServer->errorString();
            // If the saved network configuration is not currently discovered use the system default
            QNetworkConfiguration config = manager.configurationFromIdentifier(id);
            if ((config.state() & QNetworkConfiguration::Discovered) !=
                    QNetworkConfiguration::Discovered)
            {
                config = manager.defaultConfiguration();
            }
            networkSession = new QNetworkSession(config, this);
            connect(networkSession, SIGNAL(opened()), this, SLOT(sessionOpened()));

            networkSession->open();
        }

        tcpServer = new QTcpServer(this);
        if (!tcpServer->listen(QHostAddress(ipCommAddress), tcp_port))
        {
            PACTOR_TNC_DEBUG << "Unable to start the server at"
                             << ipCommAddress << tcp_port
                             << "error:"  << tcpServer->errorString();
            tcpServer->close();
            delete tcpServer;
            host_error  = true;
            ipCommAddress.clear();
            error_count++;
        }
        else
        {
            PACTOR_TNC_DEBUG << "Server started at" << ipCommAddress << tcp_port;
        }
    }
    while ((host_error) && (error_count <= 3));

    // give-up if too much errors
    if (error_count >= 3)
    {
        exit(1);
    }
    connect(tcpServer, SIGNAL(newConnection()), this, SLOT(acceptTcpConnection()));

    blockSize = 0;
    // save network settings
    saveNetSettings();
}

//-----------------------------------------------------------------------------
// UDP functions
//-----------------------------------------------------------------------------
void PactorTNC::sendBroadcast(t_struct_msg *message_out, int blocksize)

{
    QByteArray datagram((const char *)message_out, blocksize + sizeof(qint16));
    //QByteArray datagram = "Broadcast message " + QByteArray::number(messageNo);

    udpSocket->writeDatagram(datagram, QHostAddress(bcastCommAddress), udp_port);
    delete message_out;
}


//-----------------------------------------------------------------------------
// TCP client-server functions
//-----------------------------------------------------------------------------
void PactorTNC::acceptTcpConnection(void)
{
    PACTOR_TNC_DEBUG << "TCP accept connection on port" << tcp_port;
    clientConnection = tcpServer->nextPendingConnection();
    connect(clientConnection, SIGNAL(disconnected()),
            clientConnection, SLOT(deleteLater()));
    connect(clientConnection, SIGNAL(readyRead()), this, SLOT(readTcpMessage()));
    connect(clientConnection, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(displayError(QAbstractSocket::SocketError)));

    in  = new QDataStream(clientConnection);
    in->setVersion(QDataStream::Qt_4_0);
    in->setByteOrder(QDataStream::LittleEndian);

    blockSize           = 0;
    network_connected   = true;
    // get peer address
    bcastCommAddress    = clientConnection->peerAddress().toString();

}


void PactorTNC::sessionOpened()
{
    // Save the used configuration
    QNetworkConfiguration config = networkSession->configuration();
    QString id;
    if (config.type() == QNetworkConfiguration::UserChoice)
    {
        id = networkSession->sessionProperty(QLatin1String("UserChoiceConfiguration")).toString();
    }
    else
    {
        id = config.identifier();
    }

    QSettings settings(QSettings::UserScope, QLatin1String("Trolltech"));
    settings.beginGroup(QLatin1String("QtNetwork"));
    settings.setValue(QLatin1String("DefaultNetworkConfiguration"), id);
    settings.endGroup();
}

void PactorTNC::displayError(QAbstractSocket::SocketError socketError)
{
    bool b;

    switch (socketError)
    {
        case QAbstractSocket::RemoteHostClosedError:
            PACTOR_TNC_DEBUG << "Remote Host Closed Error.";
            network_connected   = false;
            // reset modem to Idle mode
            b = QMetaObject::invokeMethod(this, "changeMode",
                                          Qt::AutoConnection,
                                          Q_ARG(const int, SWITCH_TO_IDLE));
            Q_ASSERT(b);
            break;
        case QAbstractSocket::HostNotFoundError:
            PACTOR_TNC_DEBUG << "The host was not found."
                             << "Please check the host name and port settings.";
            break;
        case QAbstractSocket::ConnectionRefusedError:
            PACTOR_TNC_DEBUG << "The connection was refused by the peer. "
                             << "Make sure the server is running, "
                             << "and check that the host name and port "
                             <<  "settings are correct.";
            break;
        default:
            PACTOR_TNC_DEBUG << "Client"
                             << "The following error occurred:"
                             << clientConnection->errorString();
    }
}

void PactorTNC::readTcpMessage(void)
{
    int         bytes_available;
    int         temp_int;
    qint16      temp_qint16;

    bytes_available = clientConnection->bytesAvailable();
    if (blockSize == 0)
    {
        // wait until blocksize bytes received
        if (bytes_available < (int)sizeof(quint16))
        {
            return;
        }
        *in >> temp_qint16;
        blockSize   = qFromBigEndian(temp_qint16);
        PACTOR_TNC_DEBUG << "1 - blockSize" << blockSize;
    }
    // wait until 'blocksize' byte count received
    if ( clientConnection->bytesAvailable() < blockSize )
    {
        return;
    }
    // limit blocksize
    if (blockSize > MAX_TCP_UDP_BUFFER_SIZE)
    {
        blockSize       = MAX_TCP_UDP_BUFFER_SIZE;
    }
    // read data into message structure
    in->readRawData((char *)&message_in.opcode, blockSize);
    message_in.blocksize   = blockSize;
    blockSize       = 0;
#undef TCP_DEBUG
#ifdef TCP_DEBUG
    char disp_char[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    QString output_string;
    QByteArray  byte_array;

    byte_array.clear();
    int a, b;
    unsigned char *char_ptr    = (unsigned char *)&message_in;
    for ( int i = 0; i < message_in.blocksize + sizeof(qint16); i++, char_ptr++ )
    {
        a   = (unsigned int) * char_ptr & 0x00f;
        b   = ((unsigned int) * char_ptr >> 4) & 0x00f;

        byte_array.append(disp_char[b]);
        byte_array.append(disp_char[a]);
        byte_array.append(' ');
    }
    output_string = QString(byte_array);
    PACTOR_TNC_DEBUG << "tcp size: " << message_in.blocksize
                     << "in: " << output_string;
#endif

    // analyze message
    switch (qFromBigEndian(message_in.opcode))
    {
        case e_mode_button:
            // unproto: store speed and repeat count
            if (message_in.data.mode.mode_button == e_mode_UNPROTO)
            {
                temp_int = message_in.data.mode.speed;
                m_pactorTNCThread->PT_speed       = (e_PT_speed)message_in.data.mode.speed;
                m_pactorTNCThread->unproto_repeat = message_in.data.mode.repeat_count;
            }
            if (message_in.data.mode.mode_button == e_mode_MASTER)
            {
                //PTMyCall(m_callsign, m_auto_answer);
                m_PTremoteCall = QString(QByteArray((const char *)message_in.data.mode.remote_call.chars, qFromBigEndian(message_in.data.mode.remote_call.count)));

                m_pactorDemodulator->setThreadCallsign(m_PTMyCall,
                                                       m_PTremoteCall);

                tx_text_buffs.my_callsign->clear();
                tx_text_buffs.my_callsign->append(m_PTMyCall.toLatin1());
                // update settings
                writeSettings(e_remote_call);
            }
            PACTOR_TNC_DEBUG << "e_mode_button" << e_mode_button;
            // call modem slot (activates changeMode() slot)
            emit setMode(message_in.data.mode.mode_button);
            // update settings
            writeSettings(e_mode_button);
            break;

        case e_text_send:
            // store characters
            PACTOR_TNC_DEBUG << "UI:" << QString(message_in.data.text.chars);
            if (m_pactorTNCThread->PT_allow_huffman)
            {
                incoming_huffman_qbyte_array->append((const char *)message_in.data.text.chars, qFromBigEndian(message_in.data.text.count));
                m_HuffmanTimer->start(5000);
            }
            else
            {
                tx_text_buffs.ascii_text->append((const char *)message_in.data.text.chars, qFromBigEndian(message_in.data.text.count));
            }
            break;

        case e_frequency_value:
            SetCenterFrequencyInt(qFromBigEndian(message_in.data.freq.center_frequency));
            center_frequency_start      = qFromBigEndian(message_in.data.freq.center_frequency);
            center_frequency_AFC_limit  = qFromBigEndian(message_in.data.freq.AFC_limit);
            AFC_on                      = (bool)qFromBigEndian(message_in.data.freq.AFC_on);

            // update settings
            writeSettings(e_frequency_value);
            break;

        case e_send_mycall:
            m_PTMyCall         = QString(QByteArray((const char *)message_in.data.calls.chars, qFromBigEndian(message_in.data.calls.count)));
            m_pactorDemodulator->setThreadCallsign(m_PTMyCall,  m_PTremoteCall);

            tx_text_buffs.my_callsign->clear();
            tx_text_buffs.my_callsign->append(m_PTMyCall.toLatin1());
            // update settings
            writeSettings(e_send_mycall);
            break;

        case e_break:
            breakRequest();
            break;

        case e_qrt:
            // set QRT flag if in master mode
            if (m_pactorTNCThread->PT_current_request  == e_PTMaster)
            {
                // if currently the receiving station, become Tx
                if (m_pactorTNCThread->engine_info->PT_Tx_Rx == PT_RX)
                {
                    //        m_pactoraudioengine->thread_info.send_break  = true;
                    m_pactorTNCThread->send_break             = 1;
                }
                // set QRT request flag
                QRTrequest = true;
            }
            break;

        case e_shutdown:
            waveform_src        = e_waveform_none;
            PACTOR_TNC_DEBUG << "Received shutdown request";
            exit(0);
            break;

        case e_waveform_source:
            waveform_src        = qFromBigEndian(message_in.data.scope_src.source);
            waveform_rate       = qFromBigEndian(message_in.data.scope_src.rate);
            temp_int            = qFromBigEndian(message_in.data.scope_src.ratio);
            waveform_ratio      = *(float *)&temp_int;

            // update 500Hz filter waveform switch
            switch (waveform_src)
            {
                case e_waveform_output_filter_phase:
                    m_pactorDemodulator->m_thread->store_filter500_waveform    = 1;
                    break;
                case e_waveform_output_filter_ampl:
                    m_pactorDemodulator->m_thread->store_filter500_waveform    = 2;
                    break;
                default:
                    m_pactorDemodulator->m_thread->store_filter500_waveform    = 0;
                    break;
            }
            break;

        case e_general_parameters:
            switch (message_in.data.param.param_index)
            {
                case e_Huffman_param:
                    m_pactorTNCThread->PT_allow_huffman = (bool)message_in.data.param.param_value;
                    writeSettings(e_general_parameters);
                    break;

                case e_max_speed:
                    temp_int = qFromBigEndian(message_in.data.param.param_value);;
                    writeSettings(e_general_parameters);
                    break;

                case e_UCMDs:
                    switch (qFromBigEndian(message_in.data.UCMD_value.index))

                    {

                            // UCMD 0: 100Bd packet count to speedup to 200Bd

                        case 0:
                            if (qFromBigEndian(message_in.data.UCMD_value.value) > UCMD0_MAX)
                            {
                                m_pactorTNCThread->UCMD[0] = UCMD0_MAX;
                            }
                            else
                            {
                                m_pactorTNCThread->UCMD[0] = qFromBigEndian(message_in.data.UCMD_value.value);
                            }
                            break;

                            // UCMD 1: 200Bd packet error count to speeddown to 100Bd
                        case 1:
                            if (message_in.data.UCMD_value.value > UCMD1_MAX)
                            {
                                m_pactorTNCThread->UCMD[1] = UCMD1_MAX;
                            }
                            else
                            {
                                m_pactorTNCThread->UCMD[1] = message_in.data.UCMD_value.value;
                            }
                            break;

                            // UCMD 2: 200Bd packet count without error to stay to 200Bd
                        case 2:
                            if (qFromBigEndian(message_in.data.UCMD_value.value) > UCMD2_MAX)
                            {
                                m_pactorTNCThread->UCMD[2] = UCMD2_MAX;
                            }
                            else
                            {
                                m_pactorTNCThread->UCMD[2] = message_in.data.UCMD_value.value;
                            }
                            break;

                            // UCMD 3: Mem ARQ packet count combined before clear
                        case 3:
                            if (qFromBigEndian(message_in.data.UCMD_value.value) > UCMD3_MAX)
                            {
                                m_pactorTNCThread->UCMD[3] = UCMD3_MAX;
                            }
                            else
                            {
                                m_pactorTNCThread->UCMD[3] = message_in.data.UCMD_value.value;
                            }
                            break;

                        default:
                            break;
                    }
                    m_pactorTNCThread->UCMD[message_in.data.UCMD_value.index] = message_in.data.UCMD_value.value;
                    writeSettings(e_general_parameters);
                    break;

                default:
                    break;
            }
            break;

        default:
            break;
    }
}

void PactorTNC::sendTcpMessage(t_struct_msg *message_out, int blocksize)
{
    if (network_connected)
    {
        clientConnection->write((const char *)message_out, (qint64)blocksize + sizeof(qint16));
    }
    delete message_out;
}

void PactorTNC::huffman_callback(void)
{
    if (tx_text_buffs.huffman->size() > 0)
    {
        /* here, HUFFMAN encoder */
        tx_text_buffs.huffman  = huffman_encode_buffer(incoming_huffman_qbyte_array);
        m_pactorTNCThread->PT_huffman_encode    = true;
    }
}
