/****************************************************************************
** Meta object code from reading C++ file 'pactordemodulator.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.1.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../includes/Pactor/pactordemodulator.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'pactordemodulator.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.1.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_PactorDemodThread_t {
    QByteArrayData data[21];
    char stringdata[256];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_PactorDemodThread_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_PactorDemodThread_t qt_meta_stringdata_PactorDemodThread = {
    {
QT_MOC_LITERAL(0, 0, 17),
QT_MOC_LITERAL(1, 18, 19),
QT_MOC_LITERAL(2, 38, 0),
QT_MOC_LITERAL(3, 39, 19),
QT_MOC_LITERAL(4, 59, 10),
QT_MOC_LITERAL(5, 70, 5),
QT_MOC_LITERAL(6, 76, 8),
QT_MOC_LITERAL(7, 85, 11),
QT_MOC_LITERAL(8, 97, 6),
QT_MOC_LITERAL(9, 104, 5),
QT_MOC_LITERAL(10, 110, 14),
QT_MOC_LITERAL(11, 125, 8),
QT_MOC_LITERAL(12, 134, 13),
QT_MOC_LITERAL(13, 148, 12),
QT_MOC_LITERAL(14, 161, 7),
QT_MOC_LITERAL(15, 169, 7),
QT_MOC_LITERAL(16, 177, 21),
QT_MOC_LITERAL(17, 199, 17),
QT_MOC_LITERAL(18, 217, 13),
QT_MOC_LITERAL(19, 231, 5),
QT_MOC_LITERAL(20, 237, 17)
    },
    "PactorDemodThread\0calculationComplete\0"
    "\0PTModifiedFrequency\0burstEvent\0event\0"
    "ccharptr\0PT_argument\0setLED\0count\0"
    "t_set_LED_arg*\0LED_args\0send_waveform\0"
    "const float*\0samples\0divider\0"
    "send_spectrum_samples\0calculateDemodFSK\0"
    "demodSetState\0state\0release_audio_sem\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PactorDemodThread[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x05,
       3,    2,   60,    2, 0x05,
       4,    2,   65,    2, 0x05,
       8,    2,   70,    2, 0x05,
      12,    3,   75,    2, 0x05,
      16,    0,   82,    2, 0x05,

 // slots: name, argc, parameters, tag, flags
      17,    0,   83,    2, 0x0a,
      18,    1,   84,    2, 0x0a,
      20,    0,   87,    2, 0x0a,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    2,    2,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 6,    5,    7,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 10,    9,   11,
    QMetaType::Void, 0x80000000 | 13, QMetaType::Int, QMetaType::Float,   14,    9,   15,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void,

       0        // eod
};

void PactorDemodThread::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PactorDemodThread *_t = static_cast<PactorDemodThread *>(_o);
        switch (_id) {
        case 0: _t->calculationComplete(); break;
        case 1: _t->PTModifiedFrequency((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->burstEvent((*reinterpret_cast< const int(*)>(_a[1])),(*reinterpret_cast< ccharptr(*)>(_a[2]))); break;
        case 3: _t->setLED((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< t_set_LED_arg*(*)>(_a[2]))); break;
        case 4: _t->send_waveform((*reinterpret_cast< const float*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3]))); break;
        case 5: _t->send_spectrum_samples(); break;
        case 6: _t->calculateDemodFSK(); break;
        case 7: _t->demodSetState((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 8: _t->release_audio_sem(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (PactorDemodThread::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorDemodThread::calculationComplete)) {
                *result = 0;
            }
        }
        {
            typedef void (PactorDemodThread::*_t)(int , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorDemodThread::PTModifiedFrequency)) {
                *result = 1;
            }
        }
        {
            typedef void (PactorDemodThread::*_t)(const int , ccharptr );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorDemodThread::burstEvent)) {
                *result = 2;
            }
        }
        {
            typedef void (PactorDemodThread::*_t)(int , t_set_LED_arg * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorDemodThread::setLED)) {
                *result = 3;
            }
        }
        {
            typedef void (PactorDemodThread::*_t)(const float * , int , float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorDemodThread::send_waveform)) {
                *result = 4;
            }
        }
        {
            typedef void (PactorDemodThread::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorDemodThread::send_spectrum_samples)) {
                *result = 5;
            }
        }
    }
}

const QMetaObject PactorDemodThread::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_PactorDemodThread.data,
      qt_meta_data_PactorDemodThread,  qt_static_metacall, 0, 0}
};


const QMetaObject *PactorDemodThread::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PactorDemodThread::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PactorDemodThread.stringdata))
        return static_cast<void*>(const_cast< PactorDemodThread*>(this));
    return QObject::qt_metacast(_clname);
}

int PactorDemodThread::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void PactorDemodThread::calculationComplete()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void PactorDemodThread::PTModifiedFrequency(int _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void PactorDemodThread::burstEvent(const int _t1, ccharptr _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void PactorDemodThread::setLED(int _t1, t_set_LED_arg * _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void PactorDemodThread::send_waveform(const float * _t1, int _t2, float _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void PactorDemodThread::send_spectrum_samples()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}
struct qt_meta_stringdata_PactorDemod_t {
    QByteArrayData data[14];
    char stringdata[169];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_PactorDemod_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_PactorDemod_t qt_meta_stringdata_PactorDemod = {
    {
QT_MOC_LITERAL(0, 0, 11),
QT_MOC_LITERAL(1, 12, 19),
QT_MOC_LITERAL(2, 32, 0),
QT_MOC_LITERAL(3, 33, 12),
QT_MOC_LITERAL(4, 46, 7),
QT_MOC_LITERAL(5, 54, 10),
QT_MOC_LITERAL(6, 65, 11),
QT_MOC_LITERAL(7, 77, 8),
QT_MOC_LITERAL(8, 86, 13),
QT_MOC_LITERAL(9, 100, 12),
QT_MOC_LITERAL(10, 113, 19),
QT_MOC_LITERAL(11, 133, 5),
QT_MOC_LITERAL(12, 139, 18),
QT_MOC_LITERAL(13, 158, 9)
    },
    "PactorDemod\0PTchangeSampleCount\0\0"
    "sample_count\0PTerror\0error_code\0"
    "PTconnected\0callsign\0PTbusyChannel\0"
    "busy_channel\0setDemodThreadState\0state\0"
    "SetCenterFrequency\0frequency\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PactorDemod[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x05,
       4,    1,   47,    2, 0x05,
       6,    1,   50,    2, 0x05,
       8,    1,   53,    2, 0x05,
      10,    1,   56,    2, 0x05,

 // slots: name, argc, parameters, tag, flags
      12,    1,   59,    2, 0x0a,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::QByteArray,    7,
    QMetaType::Void, QMetaType::Bool,    9,
    QMetaType::Void, QMetaType::Int,   11,

 // slots: parameters
    QMetaType::Void, QMetaType::Float,   13,

       0        // eod
};

void PactorDemod::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PactorDemod *_t = static_cast<PactorDemod *>(_o);
        switch (_id) {
        case 0: _t->PTchangeSampleCount((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 1: _t->PTerror((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 2: _t->PTconnected((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 3: _t->PTbusyChannel((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 4: _t->setDemodThreadState((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 5: _t->SetCenterFrequency((*reinterpret_cast< float(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (PactorDemod::*_t)(const int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorDemod::PTchangeSampleCount)) {
                *result = 0;
            }
        }
        {
            typedef void (PactorDemod::*_t)(const int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorDemod::PTerror)) {
                *result = 1;
            }
        }
        {
            typedef void (PactorDemod::*_t)(const QByteArray & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorDemod::PTconnected)) {
                *result = 2;
            }
        }
        {
            typedef void (PactorDemod::*_t)(const bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorDemod::PTbusyChannel)) {
                *result = 3;
            }
        }
        {
            typedef void (PactorDemod::*_t)(const int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorDemod::setDemodThreadState)) {
                *result = 4;
            }
        }
    }
}

const QMetaObject PactorDemod::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_PactorDemod.data,
      qt_meta_data_PactorDemod,  qt_static_metacall, 0, 0}
};


const QMetaObject *PactorDemod::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PactorDemod::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PactorDemod.stringdata))
        return static_cast<void*>(const_cast< PactorDemod*>(this));
    return QObject::qt_metacast(_clname);
}

int PactorDemod::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void PactorDemod::PTchangeSampleCount(const int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void PactorDemod::PTerror(const int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void PactorDemod::PTconnected(const QByteArray & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void PactorDemod::PTbusyChannel(const bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void PactorDemod::setDemodThreadState(const int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_END_MOC_NAMESPACE
