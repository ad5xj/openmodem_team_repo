/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#include <unistd.h>

#include <QtCore/QSettings>
#include <QtCore/QBuffer>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QThread>
#include <QtCore/QTimer>

#include "globals.h"
#include "crccalc.h"
#include "pactordecoder.h"
#include "pactortnc.h"
#include "pactormodulator.h"
#include "pactordemodulator.h"
#include "pactoraudioengine.h"
#include "pactornetstruct.h"

#undef COMM_DEBUG

void PactorTNC::readHighPrioMessage(t_struct_msgptr message_in, qint16 blocksize)
{
    Q_UNUSED(blocksize);

    bool    c;
    int     i;

    QList<QString> string_list;
    QString wk_string;
#ifdef COMM_DEBUG
    char disp_char[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    QString output_string;
    QByteArray  byte_array;

    byte_array.clear();
    int a, b;
    unsigned char *char_ptr    = (unsigned char *)message_in;
    for (int i = 0; i < blocksize + sizeof(qint16); i++, char_ptr++)
    {
        a   = (unsigned int) * char_ptr & 0x00f;
        b   = ((unsigned int) * char_ptr >> 4) & 0x00f;

        byte_array.append(disp_char[b]);
        byte_array.append(disp_char[a]);
        byte_array.append(' ');
    }
    output_string = QString(byte_array);
    PACTOR_TNC_DEBUG << "size: " << blocksize
                     << "in: " << output_string;
#endif

    comm_active                 = true;
    // analyze message
    switch (message_in->opcode)
    {
        case
                e_mode_button:
            // unproto: store speed and repeat count
            if (message_in->data.mode.mode_button == e_mode_UNPROTO)
            {
                m_pactorTNCThread->PT_speed       = (e_PT_speed)message_in->data.mode.speed;
                m_pactorTNCThread->unproto_repeat = message_in->data.mode.repeat_count;
            }
            if (message_in->data.mode.mode_button == e_mode_MASTER)
            {
                //PTMyCall(m_callsign, m_auto_answer);
                m_PTremoteCall = QString(QByteArray((const char *)message_in->data.mode.remote_call.chars, message_in->data.mode.remote_call.count)).toUpper();

                tx_text_buffs.remote_callsign->clear();
                tx_text_buffs.remote_callsign->append(m_PTremoteCall.toLatin1());

                m_pactorDemodulator->setThreadCallsign(&tx_text_buffs);

                // update settings
                writeSettings(e_remote_call);
            }
            PACTOR_TNC_DEBUG << "e_mode_button" << (int)e_mode_button;
            // call modem slot (activates changeMode() slot)
            emit setMode(message_in->data.mode.mode_button);
            // update settings
            writeSettings(e_mode_button);
            break;

        case
                e_text_send:
            // store characters
            PACTOR_TNC_DEBUG << "UI:" << QString(message_in->data.text.chars);
            if (m_pactorTNCThread->PT_allow_huffman)
            {
                incoming_huffman_qbyte_array->append((const char *)message_in->data.text.chars, message_in->data.text.count);
                m_HuffmanTimer->start(5000);
            }
            else
            {
                tx_text_buffs.ascii_text->append((const char *)message_in->data.text.chars, message_in->data.text.count);
            }
            break;

        case
                e_frequency_value:
            last_received_frequency = message_in->data.freq.center_frequency;
            c = QMetaObject::invokeMethod(this, "SetCenterFrequencyInt",
                                          Qt::QueuedConnection,
                                          Q_ARG(int, (int)message_in->data.freq.center_frequency),
                                          Q_ARG(int, e_freq_mod_client));
            Q_ASSERT(c);
            center_frequency_start      = message_in->data.freq.center_frequency;
            center_frequency_AFC_limit  = message_in->data.freq.AFC_limit;
            AFC_on                      = (bool)message_in->data.freq.AFC_on;

            emit setPanelAFC(AFC_on);

            // update settings
            writeSettings(e_frequency_value);
            //            qDebug()<<"client set freq INT last"<<last_received_frequency;
            break;

        case
                e_send_mycall:
            m_PTMyCall         = QString(QByteArray((const char *)message_in->data.calls.chars, message_in->data.calls.count)).toUpper();
            tx_text_buffs.my_callsign->clear();
            tx_text_buffs.my_callsign->append(m_PTMyCall.toLatin1());

            m_pactorDemodulator->setThreadCallsign(&tx_text_buffs);

            // update settings
            writeSettings(e_send_mycall);
            break;

        case
                e_myaux:
            m_PTAuxCall = QString(QByteArray((const char *)message_in->data.calls.chars, message_in->data.calls.count)).toUpper();
            i = 0;
            string_list = m_PTAuxCall.split(',', QString::SkipEmptyParts, Qt::CaseInsensitive);

            foreach(wk_string, string_list)
            {
                tx_text_buffs.aux_callsign[i]->clear();
                tx_text_buffs.aux_callsign[i]->append(wk_string.toLatin1());
                PACTORUI_DEBUG << "ind M" << i << wk_string;
                i++;
                tx_text_buffs.aux_call_count    = i;
            }
            m_pactorDemodulator->setThreadCallsign(&tx_text_buffs);
            // update settings
            writeSettings(e_send_mycall);
            break;

        case
                e_break:
            breakRequest();
            break;

        case
                e_qrt:
            // set QRT flag if in master mode
            if (m_pactorTNCThread->PT_current_request  == e_PTMaster)
            {
                // if currently the receiving station, become Tx
                if (m_pactorTNCThread->engine_info->PT_Tx_Rx == PT_RX)
                {
                    //        m_pactoraudioengine->thread_info.send_break  = true;
                    m_pactorTNCThread->send_break             = 1;
                }
                // set QRT request flag
                QRTrequest = true;
            }
            break;

        case
                e_shutdown:
            PACTOR_TNC_DEBUG << "Received shutdown request";
            exit(0);
            break;

        case
                e_general_parameters:
            switch (message_in->data.param.param_index)
            {
                case
                        e_Huffman_param:
                    m_pactorTNCThread->PT_allow_huffman = (bool)message_in->data.param.param_value;
                    writeSettings(e_general_parameters);
                    break;

                case
                        e_max_speed:
                    writeSettings(e_general_parameters);
                    break;

                case
                        e_UCMDs:
                    switch (message_in->data.UCMD_value.index)

                    {

                            // UCMD 0: 100Bd packet count to speedup to 200Bd

                        case
                                0:
                            if (message_in->data.UCMD_value.value > UCMD0_MAX)
                            {
                                m_pactorTNCThread->UCMD[0] = UCMD0_MAX;
                            }
                            else
                            {
                                m_pactorTNCThread->UCMD[0] = message_in->data.UCMD_value.value;
                            }
                            break;

                            // UCMD 1: 200Bd packet error count to speeddown to 100Bd
                        case
                                1:
                            if (message_in->data.UCMD_value.value > UCMD1_MAX)
                            {
                                m_pactorTNCThread->UCMD[1] = UCMD1_MAX;
                            }
                            else
                            {
                                m_pactorTNCThread->UCMD[1] = message_in->data.UCMD_value.value;
                            }
                            break;

                            // UCMD 2: 200Bd packet count without error to stay to 200Bd
                        case
                                2:
                            if (message_in->data.UCMD_value.value > UCMD2_MAX)
                            {
                                m_pactorTNCThread->UCMD[2] = UCMD2_MAX;
                            }
                            else
                            {
                                m_pactorTNCThread->UCMD[2] = message_in->data.UCMD_value.value;
                            }
                            break;

                            // UCMD 3: Mem ARQ packet count combined before clear
                        case
                                3:
                            if (message_in->data.UCMD_value.value > UCMD3_MAX)
                            {
                                m_pactorTNCThread->UCMD[3] = UCMD3_MAX;
                            }
                            else
                            {
                                m_pactorTNCThread->UCMD[3] = message_in->data.UCMD_value.value;
                            }
                            break;

                        default
                                :
                            break;
                    }
                    m_pactorTNCThread->UCMD[message_in->data.UCMD_value.index] = message_in->data.UCMD_value.value;
                    writeSettings(e_general_parameters);
                    break;

                default
                        :
                    break;
            }
            break;

        case
                e_TNC_visibleToggle:
            if (window_visible == true)
            {
                window_visible  = false;
                m_pactor_session->hide();
            }
            else
            {
                window_visible  = true;
                m_pactor_session->show();
            }
            //  === Pactor settings   === //
            settings->beginGroup(QLatin1String("PactorTNC"));
            // TNC window position
            settings->beginGroup(QLatin1String("Window"));
            settings->setValue("visible", window_visible);
            settings->endGroup();
            settings->endGroup();
            break;

        default
                :
            break;
    }
    try
    {
        delete message_in;
    }
    catch
        (...)
    {
        PACTORUI_DEBUG << "delete error 2";
    }
    // comm LED update
    // m_pactor_panel->setLED(LED_COMM, RED_ICON);
    c = QMetaObject::invokeMethod(m_pactor_session, "setLEDdirect",
                                  Qt::QueuedConnection,
                                  Q_ARG(int, LED_COMM),
                                  Q_ARG(int, RED_ICON));
    Q_ASSERT(c);
}

void PactorTNC::huffman_callback(void)
{
    if (tx_text_buffs.huffman->size() > 0)
    {
        /* here, HUFFMAN encoder */
        tx_text_buffs.huffman  = huffman_encode_buffer(incoming_huffman_qbyte_array);
        m_pactorTNCThread->PT_huffman_encode    = true;
    }
}

