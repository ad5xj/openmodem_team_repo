/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#include <unistd.h>
#include <math.h>

#include <QtCore/QObject>
#include <QtCore/QDebug>
#include <QtCore/QThread>
#include <QtCore/QFile>
#include <QtCore/QTimer>
#include <QtCore/QCoreApplication>
#include <QtCore/QMetaObject>
#include <QtCore/QSet>
#include <QtCore/QSemaphore>

extern "C" {
#include <jack/jack.h>
#include <jack/transport.h>
}

#include "globals.h"
#include "pactorparams.h"
#include "pactortnc.h"
#include "pactormodulator.h"
#include "pactordemodulator.h"

#include "pactoraudioengine.h"

//#define DEBUG_WINDOWS
//#undef DEBUG_TIMINGS
//#define DEBUG_TX_TIMINGS

extern const int bit_patterns[10];

int phase_value[2] = {1, -1};

qint64  global_date_time_mSecs;

class PactorModulator;
class PactorDemodThread;
class PactorTNC;

PactorAudioEngine::PactorAudioEngine(QObject *parent, PactorModulator *pactorModulator, PactorDemod *pactorDemodulator) : QObject(parent)
    ,input_sample_index(-1)
    ,tx_buffer_index(0)
    ,m_pactorTNC((PactorTNC *)parent)
{
    m_pactorDemodulator    = (PactorDemod *)pactorDemodulator;
    m_pactorModulator      = (PactorModulator *)pactorModulator;

    thread_info.audio_in_gain   = DEF_INPUT_GAIN;
    thread_info.audio_out_gain  = DEF_OUTPUT_GAIN;
    thread_info.date_time_mSecs_addr = &global_date_time_mSecs;
    thread_info.PT_Tx_Rx        = PT_RX;
    thread_info.can_process     = false;
    thread_info.can_capture     = false;
    thread_info.can_transmit    = false;
    thread_info.window_state    = e_PT_rcv_win_not_init;
    thread_info.current_CS      = CS2_ID;
    thread_info.last_CS         = CS1_ID;
    thread_info.LED_modified    = false;
    thread_info.receiving_CS    = false;
    thread_info.hardware_audio_latency  = 0;
    // audio in downsampled indexes
    thread_info.audio_in_write_buffer_index = 0;
    thread_info.audio_in_read_buffer_index  = 0;
    thread_info.dwnspl_frame_time_index     = 0;
    // initialize status LED array
    for (int i = 0; i < LED_COUNT; i++)
    {
        thread_info.LED_args[i].is_modified = false;
    }
}


PactorAudioEngine::~PactorAudioEngine()
{
    delete thread_info.packet_audio_samples[0];
    delete thread_info.packet_audio_samples[1];
    // initialize CS audio patterns
    for (int phase = 0; phase < 2; phase++)
        // 2 phases (first is positive)
    {
        for (int CS_id = 0; CS_id < 4; CS_id++)
            // 4 CS
        {
            delete thread_info.CS_audio[phase][CS_id];
        }
    }
    //jack_client_close(jack_client);
    PACTOR_TNC_DEBUG << "PactorAudioEngine destructor"  ;
}

//-----------------------------------------------------------------------------
// Public functions
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Public slots
//-----------------------------------------------------------------------------

void PactorAudioEngine::startAudioIO()
{
    int  i;

    // initialize window process
    thread_info.startup_time_ready  = false;
    //    ENGINE_DEBUG << "Engine::create jack audio connections";
    /* open a client connection to the JACK server */
    server_name = NULL;
    options     = JackNullOption;

    jack_client = jack_client_open("jackPACTORmodem", options, &status, server_name);
    if (jack_client == NULL)
    {
        PACTOR_TNC_DEBUG << "jack_client_open() failed, status =" << status;
        if (status & JackServerFailed)
        {
            PACTOR_TNC_DEBUG <<  "Unable to connect to JACK server";
        }
        exit(1);
    }
    thread_info.client      = jack_client;
    //    PACTOR_TNC_DEBUG << "Engine::create jack audio connections, step 1, client:"<<jack_client;
    if (status & JackServerStarted)
    {
        PACTOR_TNC_DEBUG <<  "JACK server started";
    }
    if (status & JackNameNotUnique)
    {
        client_name = jack_get_client_name(jack_client);
        PACTOR_TNC_DEBUG << "unique name " << client_name << " assigned";
    }

    //    PACTOR_TNC_DEBUG << "Engine::create jack audio connections, step 2";

    /* tell the JACK server to call `process()' whenever
       there is work to be done.
    */
    jack_set_process_callback(jack_client, (JackProcessCallback)&PactorAudioEngine::process_audio, (void *)&thread_info);

    //    PACTOR_TNC_DEBUG << "Engine::create jack audio connections, step 3";
    /* tell the JACK server to call `jack_shutdown()' if
       it ever shuts down, either entirely, or if it
       just decides to stop calling us.
    */
    jack_on_shutdown(jack_client, (JackShutdownCallback)&PactorAudioEngine::jack_shutdown, (void *)&thread_info);

    /* create two ports pairs*/
    input_ports     = (jack_port_t **) calloc(MODEM_JACK_INPUT_PORTS_COUNT, sizeof(jack_port_t *));
    output_ports    = (jack_port_t **) calloc(MODEM_JACK_OUTPUT_PORTS_COUNT, sizeof(jack_port_t *));

    //    PACTOR_TNC_DEBUG << "Engine::create jack audio connections, step 4";
    char port_name[16];
    // input ports
    for (i = 0; i < MODEM_JACK_INPUT_PORTS_COUNT; i++)
    {
        sprintf(port_name, "input_%d", i + 1);
        input_ports[i] = jack_port_register(jack_client, port_name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
        if (input_ports[i] == NULL)
        {
            break;
        }
        //        PACTOR_TNC_DEBUG << "Engine::jack port"<<port_name;
    }
    // output ports
    for (i = 0; i < MODEM_JACK_OUTPUT_PORTS_COUNT; i++)
    {
        sprintf(port_name, "output_%d", i + 1);
        output_ports[i] = jack_port_register(jack_client, port_name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
        if (output_ports[i] == NULL)
        {
            break;
        }
        //        PACTOR_TNC_DEBUG << "Engine::jack port"<<port_name;
    }
    // any error?
    if ((input_ports[0] == NULL) || (output_ports[0] == NULL))
    {
        PACTOR_TNC_DEBUG << "no JACK ports available";
        exit(1);
    }
    //    PACTOR_TNC_DEBUG << "Engine::create jack audio connections, step 5. client"<<hex<<jack_client;
    thread_info.input_ports     = input_ports;
    thread_info.output_ports    = output_ports;
    /* Tell the JACK server that we are ready to roll.  Our
     * process() callback will start running now. */
    i   = jack_activate(jack_client);
    if (i != 0)
    {
        PACTOR_TNC_DEBUG << "cannot activate client. error" << i;
        exit(1);
    }
    //    PACTOR_TNC_DEBUG << "Engine::create jack audio connections, step 6";
    /* Connect the ports.  You can't do this before the client is
     * activated, because we can't make connections to clients
     * that aren't running.  Note the confusing (but necessary)
     * orientation of the driver backend ports: playback ports are
     * "input" to the backend, and capture ports are "output" from
     * it.
     */
    // system capture ports are outputs
    ports = jack_get_ports(jack_client, NULL, NULL, JackPortIsPhysical | JackPortIsOutput);
    if (ports == NULL)
    {
        PACTOR_TNC_DEBUG << "no physical capture ports\n";
        exit(1);
    }

    //    PACTOR_TNC_DEBUG << "Engine::create jack audio connections, step 7";
    bool    port_connected  = true;
    int     iteration       = 0;
    do
    {
        iteration++;
        for (i = 0; i < MODEM_JACK_INPUT_PORTS_COUNT; i++)
        {
            //        PACTOR_TNC_DEBUG << "jack input port"<<jack_port_name ( input_ports[i] )
            //                                   <<"connected to"<<ports[i+jack_input_offset];
            if (jack_connect(jack_client, ports[i + jack_input_offset], jack_port_name(input_ports[i])))
            {
                port_connected      = false;
                jack_input_offset  = 0;
                PACTOR_TNC_DEBUG << "cannot connect input ports";
                break;
            }
        }
    }
    while ((port_connected == false) && (iteration < 2));

    free(ports);

    //    PACTOR_TNC_DEBUG << "Engine::create jack audio connections, step 8";
    // system playback ports are inputs
    ports = jack_get_ports(jack_client, NULL, NULL, JackPortIsPhysical | JackPortIsInput);
    if (ports == NULL)
    {
        PACTOR_TNC_DEBUG << "no physical playback ports\n";
        exit(1);
    }
    //    PACTOR_TNC_DEBUG << "Engine::create jack audio connections, step 9";
#ifdef DEBUG_WINDOWS
    jack_output_offset  = 0;
#endif
    port_connected  = true;
    iteration       = 0;
    do
    {
        iteration++;
        for (i = 0; i < MODEM_JACK_OUTPUT_PORTS_COUNT; i++)
        {
            //        PACTOR_TNC_DEBUG << "jack output port"<<jack_port_name ( output_ports[i] )
            //                                   <<"connected to"<<ports[i+jack_output_offset];
            if (jack_connect(jack_client, jack_port_name(output_ports[i]), ports[i + jack_output_offset]))
            {
                port_connected      = false;
                jack_output_offset  = 0;
                PACTOR_TNC_DEBUG << "cannot connect output ports";
                break;
            }
        }
    }
    while ((port_connected == false) && (iteration < 2));

    free(ports);

    //    PACTOR_TNC_DEBUG << "Engine::create jack audio connections, step 10";
    jack_port_get_latency_range(input_ports[0], JackCaptureLatency, &latency_range);
    thread_info.input_latency   = ((latency_range.max * 1000000) / SAMPLE_RATE_48KHZ); // + ADJ_INPUT_LATENCY_USECS;
    int     min_input_latency   = ((latency_range.min * 1000000) / SAMPLE_RATE_48KHZ); // + ADJ_INPUT_LATENCY_USECS;
    jack_port_get_latency_range(output_ports[0], JackPlaybackLatency, &latency_range);
    thread_info.output_latency  = ((latency_range.max * 1000000) / SAMPLE_RATE_48KHZ); // + ADJ_OUTPUT_LATENCY_USECS;
    int     min_output_latency  = ((latency_range.min * 1000000) / SAMPLE_RATE_48KHZ); // + ADJ_INPUT_LATENCY_USECS;
    thread_info.total_latency   = thread_info.hardware_audio_latency / 1000;
    PACTOR_TNC_DEBUG << "input latency USECS" << thread_info.input_latency << min_input_latency << "output latency USECS" << thread_info.output_latency << min_output_latency << "total+hard msecs" << thread_info.total_latency;

    //    PACTOR_TNC_DEBUG << "Engine::create jack audio connections, initializing";
    // initialize CS audio patterns and buffers
    initializeCommData();
    PACTOR_TNC_DEBUG << "AudioEngine::jack audio connections created";

#ifdef VOX_CONTROL
    // initialize audio VOX channel if necessary
    if (thread_info.vox_channel != VOX_CHANNEL_DISABLED)
    {
        initialize_audio_vox(thread_info.vox_frequency);
    }
#endif
    // start demodulator
    const bool b = QMetaObject::invokeMethod(m_pactorDemodulator->m_thread, "calculateDemodFSK",
                   Qt::AutoConnection);
    //PACTOR_TNC_DEBUG << "AudioEngine: start demodulator. b=" << b;
    Q_UNUSED(b);
}

//-----------------------------------------------------------------------------
// Private slots
//-----------------------------------------------------------------------------
    /* no slots */
//-----------------------------------------------------------------------------
// Private functions
//-----------------------------------------------------------------------------
#ifdef VOX_CONTROL
// create VOX control audio waveform
void PactorAudioEngine::initialize_audio_vox(int vox_frequency)
{
    //PACTOR_TNC_DEBUG << "VOX init. Audio out gain" << thread_info.audio_out_gain;
    // compute sample count
    int min_samples = SAMPLE_RATE_48KHZ / vox_frequency;
    int max_samples = (int)(((float)SAMPLE_RATE_48KHZ / (float)vox_frequency) + 0.999);
    int iteration_count = VOX_SAMPLE_COUNT / max_samples;
    qreal    sine_incr    = (2.0 * 3.1415926535897932384626433832795 / (qreal)min_samples);
    // generate samples
    int count   = 0;
    for (int i = 0; i < iteration_count; i++)
    {
        for (int j = 0; j < min_samples; j++, count++)
        {
            thread_info.vox_buffer[count]   = sin((qreal)j * sine_incr) * thread_info.audio_out_gain;
        }
    }
    thread_info.vox_sample_count    = count;
    thread_info.vox_sample_index    = 0;
    thread_info.vox_state           = WAITING;
}
#endif

// initialize everything and create CS audio
void PactorAudioEngine::initializeCommData(void)
{
    //PACTOR_TNC_DEBUG << "CS init. Audio out gain" << thread_info.audio_out_gain;
    // initialize communication structure
    thread_info.channels        = 2;
    thread_info.new_burst       = false;
    thread_info.pactoraudioengine   = this;
    thread_info.packet_audio_samples[0]   = new float[LONG_BURST_AUDIO_SAMPLE_COUNT_48KHZ];
    thread_info.packet_audio_samples[1]   = new float[LONG_BURST_AUDIO_SAMPLE_COUNT_48KHZ];
    // initialize CS audio patterns
    for (int phase = 0; phase < 2; phase++)
        // 2 phases (first is positive)
    {
        for (int CS_id = 0; CS_id < 4; CS_id++)
            // 4 CS
        {
            // allocate audio buffers
            volatile float *float_ptr;
            float_ptr       = (volatile float *) new float[LONG_BURST_AUDIO_SAMPLE_COUNT_48KHZ];
            thread_info.CS_audio[phase][CS_id]  = (float *)float_ptr;
            //            thread_info.CS_audio[phase][CS_id]  = new float[CS_BURST_AUDIO_SAMPLE_COUNT_48KHZ];
            // pre-compute all CS's audio
            m_pactorModulator->m_thread->calculateModFSK((unsigned char *)&bit_patterns[2 * (CS_id + 1)],
                    CORREL_BITS_CS,
                    SPEED_100,
                    phase_value[phase],
                    thread_info.CS_audio[phase][CS_id],
                    &thread_info.CS_audio_count[CS_id]);
        }
    }
    thread_info.transmit_state      = WAITING;
    thread_info.next_phase          = PT_PH_INVERTED;
    thread_info.expected_phase      = USE_ANY_PHASE;
    thread_info.next_tx_time        = LLONG_MAX;
    thread_info.tx_time_update      = 0;
    thread_info.tx_data_or_CS3_burst          = true;
    thread_info.unproto_mode        = false;
    thread_info.can_process         = true;
    thread_info.audio_offset        = 0.0;
}

/**
 * JACK calls this shutdown_callback if the server ever shuts down or
 * decides to disconnect the client.
 */
void PactorAudioEngine::jack_shutdown(void *arg)
{
    jack_thread_info_t *info = (jack_thread_info_t *)arg;
    free(info->input_ports);
    free(info->output_ports);
    exit(1);
}

int PactorAudioEngine::process_audio(jack_nframes_t nframes, void *arg)
{
    quint32      i;

    qint64                          time_Usecs;
    qint64                          time_msecs;

    jack_nframes_t                  frame_number;
    jack_time_t                     time_Usecs_raw;
    jack_default_audio_sample_t     *inL;
    jack_default_audio_sample_t     *outL;
    jack_default_audio_sample_t     *outR;
    jack_thread_info_t              *info;

    PactorAudioEngine               *audioengine;

    static int             phase_index;
    static float           lpf_xv[PAE_NZEROS + 1] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    static float           lpf_yv[PAE_NPOLES + 1] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
#ifdef DEBUG_WINDOWS
    static float           out_tone[16] = {0.0, 0.38, 0.707, 0.92, 1.0,
                                           0.92, 0.707, 0.38, 0.0,
                                           -0.38, -0.707, -0.92, -1,
                                           -0.92, -0.707, -0.38
                                          };
#endif

    info            = (jack_thread_info_t *)arg;
    audioengine     =  info->pactoraudioengine;

    // compute modem time
    // jack_last_frame_time() returns time when this function starts running
    // subtract nframes to retrieve first frame timestamp
    frame_number        = jack_last_frame_time(info->client) - nframes;
    time_Usecs_raw      = jack_frames_to_time(info->client, frame_number);
    // compute startup time if necessary
    if (info->startup_time_ready == false)
    {
        info->startup_time_ready        = true;
        info->startup_time              = (qint64)jack_get_time();
        //PACTOR_TNC_DEBUG << "Itime" << info->startup_time / 1000 << "T1" << time_Usecs_raw / 1000;
    }
    time_Usecs      = (qint64)time_Usecs_raw - info->startup_time;

    time_msecs                          = time_Usecs / 1000;
    *(info->date_time_mSecs_addr)       = (jack_get_time() - info->startup_time) / 1000;

    /* Do nothing until we're ready to begin. */
    if (!info->can_process)
    {
        return 0;
    }
    //    PACTOR_TNC_DEBUG << "raw spl"<< nframes<<"time"<<time_msecs;

    /**********************************************************************************************************/
    // reception
    /**********************************************************************************************************/
    if (info->can_capture)
    {
        // store audio samples
        inL     = (jack_default_audio_sample_t *)jack_port_get_buffer(info->input_ports[0], nframes);
        for ( i = 0; i < nframes; ++i )
        {
            //*********************************************************************************
            //                  input lowpass filter
            /*
            filtertype  =   Butterworth
            passtype    =   Lowpass
            ripple  =   -2
            order   =   5
            samplerate  =   48000
            corner1     =   3000
            corner2     =
            adzero  =
            logmin  =

            Command line: /www/usr/fisher/helpers/mkfilter -Bu -Lp -o 5 -a 6.2500000000e-02 0.0000000000e+00
            raw alpha1    =   0.0625000000
            raw alpha2    =   0.0625000000
            warped alpha1 =   0.0633157730
            warped alpha2 =   0.0633157730
            gain at dc    :   mag = 6.093427809e+03   phase =   0.0000000000 pi
            gain at centre:   mag = 4.308704125e+03   phase =   0.7500000000 pi
            gain at hf    :   mag = 0.000000000e+00

            S-plane zeros:

            S-plane poles:
                 -0.1229346038 + j   0.3783538063
                 -0.3218469712 + j   0.2338355121
                 -0.3978247348 + j   0.0000000000
                 -0.3218469712 + j  -0.2338355121
                 -0.1229346038 + j  -0.3783538063

            Z-plane zeros:
                 -1.0000000000 + j   0.0000000000   5 times

            Z-plane poles:
                  0.8261791518 + j   0.3254654344
                  0.7054683618 + j   0.1717594108
                  0.6681786379 + j   0.0000000000
                  0.7054683618 + j  -0.1717594108
                  0.8261791518 + j  -0.3254654344

            Recurrence relation:
            y[n] = (  1 * x[n- 5])
                 + (  5 * x[n- 4])
                 + ( 10 * x[n- 3])
                 + ( 10 * x[n- 2])
                 + (  5 * x[n- 1])
                 + (  1 * x[n- 0])

                 + (  0.2777529978 * y[n- 5])
                 + ( -1.7411025201 * y[n- 4])
                 + (  4.4205122516 * y[n- 3])
                 + ( -5.6938879540 * y[n- 2])
                 + (  3.7314736649 * y[n- 1])
            */
            // lowpass 3000 Hz at 48 KHz sampling rate + downsampling to 8 ksps
            lpf_xv[0] = lpf_xv[1];
            lpf_xv[1] = lpf_xv[2];
            lpf_xv[2] = lpf_xv[3];
            lpf_xv[3] = lpf_xv[4];
            lpf_xv[4] = lpf_xv[5];
            lpf_xv[5] = inL[i] / PAE_GAIN;
            lpf_yv[0] = lpf_yv[1];
            lpf_yv[1] = lpf_yv[2];
            lpf_yv[2] = lpf_yv[3];
            lpf_yv[3] = lpf_yv[4];
            lpf_yv[4] = lpf_yv[5];
            lpf_yv[5] = (lpf_xv[0] + lpf_xv[5]) + 5 * (lpf_xv[1] + lpf_xv[4]) + 10 * (lpf_xv[2] + lpf_xv[3])
                        + (0.2777529978 * lpf_yv[0]) + (-1.7411025201 * lpf_yv[1])
                        + (4.4205122516 * lpf_yv[2]) + (-5.6938879540 * lpf_yv[3])
                        + (3.7314736649 * lpf_yv[4]);
            // downsample to 8 kHz
            if (audioengine->input_sample_index >= DOWNSAMLPE_RATIO_JACK)
            {
                // compute audio input offset value
                info->audio_offset  = info->audio_offset + ((lpf_yv[5] - info->audio_offset) / AUDIO_OFFSET_LP_GAIN);
                // Store downsampled data
                info->audio_in_buffer[info->audio_in_write_buffer_index] = ((lpf_yv[5] - info->audio_offset) * info->audio_in_gain);
                // store first sample timestamp
                if ((info->audio_in_write_buffer_index % AUDIO_SAMPLE_COUNT_8KHZ) == 0)
                {
                    info->first_dwnspl_frame_time[info->dwnspl_frame_time_index]  = ((qint64)jack_frames_to_time(info->client, frame_number + i - PAE_NPOLES) - info->startup_time) / 1000;
                    ;
                    // increment frame time write index
                    info->dwnspl_frame_time_index   = (info->dwnspl_frame_time_index + 1) % DWNSPLD_AUDIO_BUFF_COUNT;
                }
                // increment write index
                info->audio_in_write_buffer_index   = (info->audio_in_write_buffer_index + 1) % (DWNSPLD_AUDIO_BUFF_COUNT * AUDIO_SAMPLE_COUNT_8KHZ);
                // trigger demodulator if sample count == AUDIO_SAMPLE_COUNT_8KHZ
                if ((info->audio_in_write_buffer_index % AUDIO_SAMPLE_COUNT_8KHZ) == 0)
                {
                    const bool b = QMetaObject::invokeMethod(audioengine->m_pactorDemodulator->m_thread, "release_audio_sem", Qt::DirectConnection);
                    Q_ASSERT(b);
                    //                    PACTOR_TNC_DEBUG << "dwn spl"<< AUDIO_SAMPLE_COUNT_8KHZ<<"time"<<time_msecs;
                }
                // reset sample index
                audioengine->input_sample_index  = 0;
            }
            // increment input sample number
            audioengine->input_sample_index++;
        }
    }
    /**************************************************************************************************/
    // reception window management
    /**************************************************************************************************/
    switch (info->window_state)
    {
            // first time, no burst received -> open the window to capture any incoming burst
        case
                e_PT_rcv_win_not_init:
            info->window_state                  = e_PT_rcv_win_open;
            info->Rx_time_demod_update          = false;
            info->next_Rx_time_OFF              = LLONG_MAX;
            info->current_window_open_msecs     = info->next_Rx_time_ON;
            info->current_window_close_msecs    = LLONG_MAX;
            break;

            // window is closed, if current time is greater than next_time_ON, then open the window
        case
                e_PT_rcv_win_waiting:
            if (time_msecs >= info->next_Rx_time_ON)
            {
                info->window_state              = e_PT_rcv_win_open;
#ifdef DEBUG_TIMINGS
                PACTOR_TNC_DEBUG << "##Rx open" << time_msecs << "> ON" << info->next_Rx_time_ON;
#endif
                // if expected phase is known, invert it
                if ((info->expected_phase != USE_ANY_PHASE) && (info->next_Rx_time_OFF  != LLONG_MAX))
                {
                    info->expected_phase        = 0 - info->expected_phase;
#ifdef DEBUG_TIMINGS
                    PACTOR_TNC_DEBUG << "##Rx ph 2" << info->expected_phase;
#endif
                }
            }
            break;

        case
                e_PT_rcv_win_open:
            if (time_msecs >= info->next_Rx_time_OFF)
            {
                info->window_state  = e_PT_rcv_win_closed;
                // store last window timing
                info->current_window_open_msecs      = info->next_Rx_time_ON;
                info->current_window_close_msecs     = info->next_Rx_time_OFF;
                info->Rx_time_demod_update           = false;
#ifdef DEBUG_TIMINGS
                PACTOR_TNC_DEBUG << "##Rx close" << time_msecs << "> OFF" << info->next_Rx_time_OFF;
#endif
            }
            break;

        case
                e_PT_rcv_win_closed:
            if (time_msecs >= (info->next_Rx_time_OFF + 500))
            {
#ifdef DEBUG_TIMINGS
                PACTOR_TNC_DEBUG << "##Rx cl+500" << time_msecs << "> OFF" << info->next_Rx_time_OFF;
#endif
                // window is closed and Rx_time_ON was not updated by demodulator
                info->next_Rx_time_ON   = info->next_Rx_time_ON + info->cycle_time;
                info->next_Rx_time_OFF  = info->next_Rx_time_OFF + info->cycle_time;

#ifdef DEBUG_TIMINGS
                PACTOR_TNC_DEBUG << "##Rx time" << info->next_Rx_time_ON << "at" << time_msecs;
#endif
                info->window_state          = e_PT_rcv_win_waiting;
            }
            break;
        default
                :
            break;
    }
    /**********************************************************************************************************/
    // transmit audio data
    /**********************************************************************************************************/
    if (info->can_transmit)
    {
        outL     = (jack_default_audio_sample_t *)jack_port_get_buffer(info->output_ports[FIRST_OUTPUT_PORT], nframes);
        outR     = (jack_default_audio_sample_t *)jack_port_get_buffer(info->output_ports[SECOND_OUTPUT_PORT], nframes);

        switch (info->transmit_state)
        {
            case
                    WAITING:
                if (info->tx_time_update == 0)
                    // no tx time set by demodulator
                {
                    // tx_time_update is set by demodulator when a good CRC is received
                    // it is the difference between default (next_tx_time) and demodulator computed tx time
                    // therefore, next_tx_time is added to tx_time_update to know the exact transmission time
                    if (time_msecs >= info->next_tx_time)
                        // then use default tx time
                    {
#ifdef DEBUG_TX_TIMINGS
                        PACTOR_TNC_DEBUG << "1-OK_TO_START_DEF default Tx" << info->next_tx_time
                                         << "current" << time_msecs;
                        info->transmit_state      = OK_TO_START;
                    }
                }
                else
                {
                    // tx_time_update is set by demodulator when a good CRC is received
                    // it is the difference between default (next_tx_time) and demodulator computed tx time
                    // therefore, next_tx_time is added to tx_time_update to know the exact transmission time
                    if (time_msecs >= (info->next_tx_time + info->tx_time_update))
                        // then use default tx time
                    {
#ifdef DEBUG_TX_TIMINGS
                        PACTOR_TNC_DEBUG << "1-OK_TO_START_DEF default Tx" << (info->next_tx_time + info->tx_time_update)
                                         << "current" << time_msecs;
#endif
                        info->transmit_state      = OK_TO_START;
                    }
                }
                // not time to transmit
                if (info->transmit_state != OK_TO_START)
                {
                    // send 0.0 data to jack
                    for (i = 0; i < nframes; ++i)
                    {
                        outL[i]  = 0.0;
                        outR[i]  = 0.0;
                    }
                    break;
                }

            case
                    OK_TO_START:
                // invert phase
                info->next_phase        = 0 - info->next_phase;
                // reset audio buffer index
                audioengine->tx_buffer_index    = 0;
                // change send LED status
                info->LED_args[LED_TX].is_modified = true;
                info->LED_args[LED_TX].icon_color = RED_ICON;
                info->LED_modified          = true;

                // transmitting CS
                if (info->tx_data_or_CS3_burst == false)
                {
                    // CS changed
                    if (info->current_CS != info->last_CS)
                    {
                        info->last_CS   = info->current_CS;
                        // timeout variable set to global time on each new burst
                        info->last_new_burst_tx_time   = info->next_tx_time;
#ifdef DEBUG_TX_TIMINGS
                        PACTOR_TNC_DEBUG << "### new CS" << info->current_CS + 1 << "ph" << info->next_phase << "time" << time_msecs;
#endif
                    }
                    else
                    {
                        PACTOR_TNC_DEBUG << "### repeat CS" << info->current_CS + 1 << "ph" << info->next_phase << "TO sec" << (RETRY_TIMEOUT_MSECS - (time_msecs - info->last_new_burst_tx_time)) / 1000;
#endif
                    }
                }
                else
                {
                    // start timeout counter if a new burst is transmitted
                    if (info->new_burst == true)
                    {
                        info->new_burst     = false;
                        // timeout variable set to global time on each new burst
                        info->last_new_burst_tx_time   = info->next_tx_time;
                        // change phase LED status
                        info->LED_args[LED_REQ].is_modified = true;
                        info->LED_args[LED_REQ].icon_color = GREY_ICON;
                        info->LED_modified          = true;
#ifdef DEBUG_TX_TIMINGS
                        PACTOR_TNC_DEBUG << "### PKT. ph" << info->next_phase << "time" << time_msecs;
#endif
                    }
                    else
                        // repeat same burst
                    {
                        // change phase LED status
                        info->LED_args[LED_REQ].is_modified = true;
                        info->LED_args[LED_REQ].icon_color = RED_ICON;
                        info->LED_modified          = true;
#ifdef DEBUG_TX_TIMINGS
                        PACTOR_TNC_DEBUG << "### repeat PKT. ph" << info->next_phase << "TO sec" << (RETRY_TIMEOUT_MSECS - (time_msecs - info->last_new_burst_tx_time)) / 1000;
#endif
                    }
                }
                // see if tx timeout
                if ((time_msecs - info->last_new_burst_tx_time) > RETRY_TIMEOUT_MSECS)
                {
                    // switch to idle mode
                    /*                    const bool b = QMetaObject::invokeMethod(audioengine->m_pactorTNC, "changeMode",
                                                       Qt::QueuedConnection,
                                                       Q_ARG(const int, SWITCH_TO_IDLE));
                                        //Q_ASSERT(b);*/
                    PACTOR_TNC_DEBUG << "audio engine switch to idle";
                }
                // compute phase index for CS or burst
                if (info->next_phase == PT_PH_NORMAL)
                {
                    phase_index = 0;
                }
                else
                {
                    phase_index = 1;
                }

#ifdef VOX_CONTROL
                // compute vox start time for following burst
                info->next_vox_start_time = info->next_tx_time + info->burst_gap_msecs + info->vox_start_delay;
                // compute vox stop time for current burst
                info->next_vox_stop_time    = info->next_tx_time + (qint64)(((double)info->burst_audio_count * SAMPLE_48K_DURATION_US / 1000.0) + 0.5)
                                              + info->vox_stop_delay;
                info->vox_state             = END_UPDATED;
#endif
                // update receive window if  receiving CS's
                if (info->receiving_CS == true)
                {
                    info->window_state                  = e_PT_rcv_win_waiting;
                    info->current_window_open_msecs     = info->next_Rx_time_ON;
                    info->current_window_close_msecs    = info->next_Rx_time_OFF;
                    switch (info->short_path)
                    {
                        case
                                SHORT_PATH_TIMING: // short PATH
                            info->next_Rx_time_ON   = info->next_tx_time + CS_TX_DELAY_MS;
                            info->next_Rx_time_OFF  = info->next_Rx_time_ON + PACKET_WINDOW_CS_MARGIN_MS;
                            break;
                        case
                                LONG_PATH_TIMING: // long PATH
                            info->next_Rx_time_ON   = info->next_tx_time + CS_TX_DELAY_MS;
                            info->next_Rx_time_OFF  = info->next_Rx_time_ON + PACKET_WINDOW_CS_LONG_PATH_MARGIN_MS;
                            break;
                    }
                }
                // compute next transmission time from Tx time set by demodulator
                info->next_tx_time      = info->next_tx_time + info->burst_gap_msecs;
                info->tx_time_update    = 0;
#ifdef DEBUG_TIMINGS
                PACTOR_TNC_DEBUG << "### TX_TIME" << info->next_tx_time << "gap" << info->burst_gap_msecs << "now" << time_msecs;
#endif
                // change state
                info->transmit_state      = STARTED;

            case
                    STARTED:
                /*******************************************************************************************/
                // Burst processing
                /*******************************************************************************************/
                if (info->tx_data_or_CS3_burst == true)
                {
                    // send data to jack
                    for ( i = 0; i < nframes; ++i )
                    {
                        if (audioengine->tx_buffer_index < info->burst_audio_count)
                        {
                            //                    processed_sample_count++;
                            outL[i]  = info->packet_audio_samples[phase_index][audioengine->tx_buffer_index] * info->audio_out_gain;
                            outR[i]  = info->packet_audio_samples[phase_index][audioengine->tx_buffer_index] * info->audio_out_gain;
                            audioengine->tx_buffer_index++;
                        }
                        // buffer done
                        else
                        {
                            outL[i]  = 0.0;
                            outR[i]  = 0.0;
                        }
                    }
                    // done?
                    if (audioengine->tx_buffer_index >= info->burst_audio_count)
                    {
                        if (info->transmit_state != FINISHED)
                        {
                            info->transmit_state                        = FINISHED;
                        }
                        // creation is done when expected CS is received, otherwise current burst
                        // is transmitted.
                        // phase has been inverted at the end of preceeding transmission

                        // trigger next burst creation (unproto mode)
                        if (info->unproto_mode == true)
                        {
                            info->unproto_count++;
                            if (info->unproto_count >= info->unproto_repeat)
                            {
                                info->unproto_count     = 0;
                                // request a new burst
                                const bool b = QMetaObject::invokeMethod(audioengine->m_pactorTNC, "createNewPacket",
                                               Qt::DirectConnection,
                                               Q_ARG(const int, AUDIO_ENGINE_SENT_BURST),
                                               Q_ARG(ccharptr, NULL));
                                Q_UNUSED(b);
                            }
                        }
                    }
                }
                //*******************************************************************************************
                // CS processing
                /*******************************************************************************************/
                else
                {
                    // send data to jack
                    for ( i = 0; i < nframes; ++i )
                    {
                        if (audioengine->tx_buffer_index < info->CS_audio_count[info->current_CS])
                        {
                            //                    processed_sample_count++;
                            outL[i]  = info->CS_audio[phase_index][info->current_CS][audioengine->tx_buffer_index] * info->audio_out_gain;
                            outR[i]  = info->CS_audio[phase_index][info->current_CS][audioengine->tx_buffer_index] * info->audio_out_gain;
                            audioengine->tx_buffer_index++;
                        }
                        // buffer done
                        else
                        {
                            outL[i]  = 0.0;
                            outR[i]  = 0.0;
                        }
                    }
                    // done?
                    if (audioengine->tx_buffer_index >= info->CS_audio_count[info->current_CS])
                    {
                        if (info->transmit_state != FINISHED)
                        {
                            info->transmit_state                        = FINISHED;
                        }
                        // If a burst is received without error, pactormodem shall
                        // request a CS inversion, otherwise the same CS will be sent.
                    }
                }
                // change state when finished waiting
                if (info->transmit_state == FINISHED)
                {
                    info->transmit_state    = WAITING;
                    // change send LED status
                    info->LED_args[LED_TX].is_modified = true;
                    info->LED_args[LED_TX].icon_color = GREY_ICON;
                    info->LED_modified          = true;
                }
                break;

            default
                    :
                // send 0.0 data to jack
                for ( i = 0; i < nframes; ++i )
                {
                    outL[i]  = 0.0;
                    outR[i]  = 0.0;
                }
                break;
        }

#ifdef VOX_CONTROL
        /*******************************************************************************************/
        // vox control tone generation
        /*******************************************************************************************/
        if (info->vox_channel != VOX_CHANNEL_DISABLED)
        {
            outR     = (jack_default_audio_sample_t *)jack_port_get_buffer(info->output_ports[info->vox_channel], nframes);
            // send audio only if between start and stop time
            if (info->vox_state == WAITING)
            {
                if (time_msecs >= info->next_vox_start_time)
                {
                    info->vox_state     = STARTED;
                    // vox end may be updated by audio engine ou by modem
                    // it is updated at strt of data transmission by audioengine
                    // and at the same time than start time by modem
                    if (info->next_vox_stop_time > info->next_vox_start_time)
                    {
                        info->vox_state     = END_UPDATED;
                    }
                }
            }
            else
                if (info->vox_state == END_UPDATED)
                {
                    if (time_msecs >= info->next_vox_stop_time)
                    {
                        info->vox_state     = WAITING;
                    }
                }
            if (info->vox_state != WAITING)
            {
                // change send LED status
                if (info->LED_args[LED_COMM].icon_color == GREY_ICON)
                {
                    info->LED_args[LED_COMM].is_modified = true;
                    info->LED_args[LED_COMM].icon_color = RED_ICON;
                    info->LED_modified          = true;
                }
                for (i = 0; i < nframes; ++i)
                {
                    outR[i]  = info->vox_buffer[info->vox_sample_index];
                    info->vox_sample_index++;
                    // wraparound vox audio samples buffer
                    if (info->vox_sample_index >= info->vox_sample_count)
                    {
                        info->vox_sample_index = 0;
                    }
                }
            }
            else
                // if not in a vox sequence, send 0's
            {
                // change send LED status
                if (info->LED_args[LED_COMM].icon_color == RED_ICON)
                {
                    info->LED_args[LED_COMM].is_modified = true;
                    info->LED_args[LED_COMM].icon_color = GREY_ICON;
                    info->LED_modified          = true;
                }
                for (i = 0; i < nframes; ++i)
                {
                    outR[i]  = 0.0;
                }
            }
        }
#endif
    }
#ifdef DEBUG_WINDOWS
    inL      = (jack_default_audio_sample_t *)jack_port_get_buffer(info->input_ports[0], nframes);
    outL     = (jack_default_audio_sample_t *)jack_port_get_buffer(info->output_ports[FIRST_DEBUG_PORT], nframes);
    outR     = (jack_default_audio_sample_t *)jack_port_get_buffer(info->output_ports[SECOND_DEBUG_PORT], nframes);
    for (i = 0; i < nframes; ++i)
    {
        outL[i]  = (inL[i] - info->audio_offset) * 5.0;
        //        outL[i]  = 0.0;
        if (info->window_state == e_PT_rcv_win_open)
        {
            if (info->next_Rx_time_OFF  == LLONG_MAX)
            {
                outR[i]  = out_tone[(i % 16)] * 0.06;
            }
            else
            {
                outR[i]  = out_tone[(i % 16)] * 0.1;
            }
        }
        else
        {
            outR[i]  = 0.0;
        }
    }
#endif

    return 0;
}

