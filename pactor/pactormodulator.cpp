/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#include <math.h>
//#include <features.h>
#include <time.h>

#include <QtCore/QThread>

#include "globals.h"
#include "pactormodulator.h"
#include "pactortnc.h"
#include "pactoraudioengine.h"

extern qint64              global_date_time_mSecs;

static const qreal    sine_incr    = (2.0 * 3.1415926535897932384626433832795 / (qreal)SINE_TABLE_SIZE);

class PactorModulator;
class PactorTNC;
class PactorAudioEngine;

PactorModulatorThread::PactorModulatorThread(QObject *parent, QObject *realparent) :
    QObject(parent)
    ,   m_thread(new QThread(this))
{
    moveToThread(m_thread);
    m_thread->start();
    tnc_parent   = (PactorTNC *)realparent;

    // initialize DDS counter for FSK generation
    fl_DDS_adder    = 0.0;
}

PactorModulatorThread::~PactorModulatorThread()
{
    try
    {
        PACTOR_TNC_DEBUG << "PactorModulatorThread destructor";
        free(m_thread);
        free(sine_table);
    }
    catch
        (...)
    {
        PACTOR_TNC_DEBUG << "PactorModulatorThread dealloc buffers error"  ;
    }
}

/********************************************************************/
/*   This function computes bit content from character buffer,      */
/*   then data bit are converted to pre-audio samples.              */
/*   Pre-audio samples are low-pass filtered to "smooth" waveform   */
/*   and reduce audio bandpass. Audio samples are sent to a DDS     */
/*   which creates the baseband signal which is an audio frequency  */
/*   with spectrum extended from center freq. - 100 HZ to center    */
/*   freq. + 100 Hz.                                                */
/*                                                                  */
/*  arguments are:                                                  */
/*  - const unsigned char in_buffer[]: character buffer             */
/*  - const int bit_size: bitsize (necessary because CS are on 12   */
/*                        for example.                              */
/*  - const int speed: 100 Bd or 200 Bd                             */
/*                                                                  */
/*  return:                                                         */
/*  - none                                                          */
/********************************************************************/
void PactorModulatorThread::calculateModFSK(unsigned char *in_buffer,
        int bit_size,
        int speed,
        int phase,
        float *baseband_data_buffer,
        int *audio_samples_count)
{
    int sample_count;
    int in_char_sample_count;
    int sample_index;

#undef  PREPOSTAMBLE
#ifdef  PREPOSTAMBLE
#define PREPOSTAMBLE_COUNT  10
#endif

    /***************************************************/
    /*    new data set received                        */
    /***************************************************/
    Q_ASSERT(bit_size >= 12 && bit_size <= LONG_BURST_BIT_COUNT_200);
    /***************************************************/
    /*    convert bit to floating point +1.0 - -1.0    */
    /***************************************************/
    // compute sample count per bit
    if (speed == SPEED_100)
    {
        in_char_sample_count    = MOD_SMPL_BIT_LENGTH_100; // 80
    }
    else
    {
        in_char_sample_count    = MOD_SMPL_BIT_LENGTH_200; // 40
    }
    sample_index  = 0;

    int out_char_index = 0;
    int out_char_shift  = 0;
    int out_bit;

#ifdef  PREPOSTAMBLE
    // add a preamble at center frequency
    for (int j = 0; j < PREPOSTAMBLE_COUNT; j++, sample_index++)
    {
        raw_data_buffer[sample_index]   = 0;
    }
#endif
    //    split characters into data bits
    for (int i = 0 ; i < bit_size; i++)
    {
        // extract data bit from data char
        out_bit = (in_buffer[out_char_index] >> out_char_shift) & 1;
        // update bit position
        out_char_shift++;
        // if one complete char has been encoded, jump to next one
        if (out_char_shift >= 8)
        {
            out_char_shift  = 0;
            out_char_index++;
        }
        // extend each bit on sample_count samples
        for (int j = 0; j < in_char_sample_count; j++, sample_index++)
        {
            raw_data_buffer[sample_index]   = (out_bit == 0) ? -phase : phase;
        }
    }
#ifdef  PREPOSTAMBLE
    // add a postamble at center frequency
    for (int j = 0; j < PREPOSTAMBLE_COUNT; j++, sample_index++)
    {
        raw_data_buffer[sample_index]   = 0;
    }
#endif
    // scope display
    if (tnc_parent->waveform_src == e_waveform_encoder_output)
    {
        emit send_waveform((const float *)raw_data_buffer, sample_index, 0.2);
    }

    /***************************************************/
    /*   baseband generation        */
    /***************************************************/
    // audio samples generation
    sample_count    = computeDDS(raw_data_buffer, baseband_data_buffer, sample_index);
    // return completed buffer size
    *audio_samples_count =  sample_count;
    //    PACTOR_TNC_DEBUG <<"creation buffer audio count"<<sample_count;
}

#undef CLEANUP_WAVEFORM
/*------------------------------------------------------------------
DDS function
------------------------------------------------------------------*/
int PactorModulatorThread::computeDDS(int in_data_buffer[], float baseband_data_buffer[], int data_length)
{
    float   frequency;
    float   fl_incr;
    float   new_sample;
#ifdef CLEANUP_WAVEFORM
    float   last_sample;
    int     transition_index;
#endif
    int     table_index;
    int     out_index;

    out_index           = 0;
#ifdef CLEANUP_WAVEFORM
    last_sample         = 0.0;
    transition_index    = 0;
#endif
    // initialize DDS counter for FSK generation
    fl_DDS_adder    = 0.0;

    for (int i = 0; i < data_length; i++)
    {
        // compute frequency for this particular sample
        // frequency is varying smoothly between negative and positive shifts
        // due to 500 Hz filter
        frequency   = center_frequency + (in_data_buffer[i] * FREQ_SHIFT);
        // compute increment floating point value in DDS adder
        fl_incr     = frequency * ((float)SINE_TABLE_SIZE / (float)SAMPLE_RATE_48KHZ);
        for (int j = 0 ; j < SAMPLE_RATE_RATIO; j++)
        {
            // compute DDS adder
            fl_DDS_adder    += fl_incr;
            // limit variations to 0 - 4095
            while (fl_DDS_adder >= (float)SINE_TABLE_SIZE)
            {
                fl_DDS_adder    -= (float)SINE_TABLE_SIZE;
            }
            // compute rounded integer value
            table_index     = ((int)(fl_DDS_adder + 0.5)) % SINE_TABLE_SIZE;
            // compute baseband sample value
            new_sample = sine_table[table_index];
            baseband_data_buffer[out_index]     = new_sample;
#ifdef CLEANUP_WAVEFORM
            // memorize last zero crossing
            if (((last_sample >= 0.0) && (new_sample < 0.0))            // downward crossing
                || ((last_sample < 0.0) && (new_sample >= 0.0)))
                // upward crossing
            {
                transition_index    = out_index;
            }
            // store last sample
            last_sample = new_sample;
#endif
            // increment sample index
            out_index++;
        }
    }
#ifdef CLEANUP_WAVEFORM
    // cleanup waveform to remove glitch
    //    PACTOR_TNC_DEBUG << "modulator removed"<<out_index - transition_index<<"samples";
    for (int i = transition_index; i < out_index; i++)
    {
        baseband_data_buffer[i]     = 0.0;
    }

    return transition_index;
#else
    return out_index;
#endif
}

/*------------------------------------------------------------------
initialization of sine/cosine tables used by mixer function
------------------------------------------------------------------*/
void PactorModulatorThread::InitSineTable(void)
{
    int     i;

    try
    {
        sine_table   = new float[SINE_TABLE_SIZE];
    }
    catch
        (...)
    {
        PACTOR_TNC_DEBUG << "erreur alloc sine_table"  ;
    }

    for (i = 0 ; i < SINE_TABLE_SIZE ; i++)
    {
        sine_table[i]       = sin((qreal)i * sine_incr);
    }
}

/*------------------------------------------------------------------
initialization of sine/cosine table when
frequency changes
------------------------------------------------------------------*/
void PactorModulatorThread::SetCenterFrequency(float frequency)
{
    //PACTOR_TNC_DEBUG << "Modulator center frequency now:"  << frequency;
    // store center frequency
    center_frequency        = frequency;
}

//-----------------------------------------------------------------------------
// PactorModulator functions
//-----------------------------------------------------------------------------

PactorModulator::PactorModulator(QObject *parent) :
    QObject(parent)
    ,   m_thread(new PactorModulatorThread(0x0, parent))
{
    // do initializations
    m_pactor_tnc  = (PactorTNC *)parent;

    CHECKED_CONNECT(this, SIGNAL(startModCalculation(unsigned char *, int, int, int, float *, int *)),
                    m_thread, SLOT(calculateModFSK(unsigned char *, int, int, int, float *, int *)));

    CHECKED_CONNECT(m_thread, SIGNAL(send_waveform(const float *, int, float)),
                    m_pactor_tnc, SLOT(sendWaveform(const float *, int, float)));

    initialize();
}
//-----------------------------------------------------------------------------
// Public functions
//-----------------------------------------------------------------------------

void PactorModulator::initialize(void)
{
    //    PACTOR_TNC_DEBUG << "PactorModulator initialization";
    //    SetCenterFrequency(m_pactor_tnc->center_frequency_start);
    const bool b = QMetaObject::invokeMethod(this, "SetCenterFrequency",
                   Qt::AutoConnection,
                   Q_ARG(float, m_pactor_tnc->center_frequency_start));
    Q_ASSERT(b);
    m_thread->InitSineTable();
}

void PactorModulator::SetCenterFrequency(float frequency)
{
    //    PACTOR_TNC_DEBUG << "PactorModulator new freq:"  << frequency;
    const bool b = QMetaObject::invokeMethod(m_thread, "SetCenterFrequency",
                   Qt::AutoConnection,
                   Q_ARG(float, frequency));
    Q_ASSERT(b);
    Q_UNUSED(b) // suppress warnings in release builds
}

void PactorModulator::slCalculateModFSK(unsigned char *in_buffer,
                                        int bit_size,
                                        int speed,
                                        int phase,
                                        float *baseband_data_buffer,
                                        int *audio_samples_count)
{
    //    PACTOR_TNC_DEBUG << "slCalculateModFSK"<<bit_size;
    emit startModCalculation(in_buffer, bit_size, speed, phase,
                             baseband_data_buffer,
                             audio_samples_count);
}


