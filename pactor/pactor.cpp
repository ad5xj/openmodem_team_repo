/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**
** - - - The OpenMOR development team.
****************************************************************************/
extern "C" {
#include <jack/jack.h>
}

#include <QtCore/QString>
#include <QtGui/QFont>
#include <QtWidgets/QApplication>

#include "globals.h"
#include "pactoraudioengine.h"

#include "pactor.hpp"

Pactor::Pactor(QObject *parent) : QObject(parent)
{
    // This is the public codec interface to the modem module.
    session = new PactorSession(); // create session UI
    // PactorSession constructor will create Pactor TNC
    tnc = new PactorTNC;
    // show panels
    session->show();
    if ( WMCB.shwTNC ) tnc->show();
}

Pactor::~Pactor() {}

// talk to your codec from here to respond to commands from API
int  Pactor::setBW(int bw)
{
    // this one changes bandwidth in the modulator, demodulator using the TNC
    if (bw != 0)
    {
        if ((bw == SPEED_100) || (bw == SPEED_200))
        {
            tnc->m_pactorTNCThread->PT_speed       = (e_PT_speed)bw;
            qDebug() << "Changing speed to " << bw << "Bd";
            return tnc->m_pactorTNCThread->PT_speed;
        }
        else
        {
            qDebug() << "speed error";
            return -1;
        }
    }
    else
    {
        qDebug() << "Returning Bandwidth " << m_bw << " hz";
        return tnc->m_pactorTNCThread->PT_speed;
    }
}

bool Pactor::autoBreak(bool ab) { Q_UNUSED(ab); return false; }
bool Pactor::blocked() { return false; }
bool Pactor::blockSignals(bool b) { Q_UNUSED(b); return false; }
int  Pactor::buffers() { return 0; }
bool Pactor::busy() { return false; }
bool Pactor::busylock(bool lock) { Q_UNUSED(lock); return false; }
QString Pactor::capture(QString devname)
{
    static QString  capture_string;

    Q_UNUSED(devname);
    capture_string.clear();
    for (int i = 0; i < MODEM_JACK_INPUT_PORTS_COUNT; i++)
    {
        capture_string.append(jack_port_name(tnc->m_pactorTNCThread->engine_info->input_ports[0]));
        capture_string.append(";");
    }
    return capture_string;
}

QString Pactor::captureDevices(QString devlist)
{
    static QString  capture_string;

    Q_UNUSED(devlist);
    capture_string.clear();
    for (int i = 0; i < MODEM_JACK_INPUT_PORTS_COUNT; i++)
    {
        capture_string.append(jack_port_name(tnc->m_pactorTNCThread->engine_info->input_ports[0]));
        capture_string.append(";");
    }
    return capture_string;
}

void Pactor::close()
{
    // set QRT flag if in master mode
    if (tnc->m_pactorTNCThread->PT_current_request  == e_PTMaster)
    {
        // if currently the receiving station, become Tx
        if (tnc->m_pactorTNCThread->engine_info->PT_Tx_Rx == PT_RX)
        {
            //        m_pactoraudioengine->thread_info.send_break  = true;
            tnc->m_pactorTNCThread->send_break             = 1;
        }
        // set QRT request flag
        tnc->QRTrequest = true;
    }
}

QString Pactor::codec(QString codecname) { Q_UNUSED(codecname); return ""; }

QString Pactor::connectRemote(QString callsign)
{
    //PTMyCall(m_callsign, m_auto_answer);
    tnc->m_PTremoteCall = callsign.toUpper();

    tnc->tx_text_buffs.remote_callsign->clear();
    tnc->tx_text_buffs.remote_callsign->append(tnc->m_PTremoteCall.toLatin1());

    tnc->m_pactorDemodulator->setThreadCallsign(&tnc->tx_text_buffs);

    // update settings
    //tnc->writeSettings(e_remote_call);
    // call modem slot (activates changeMode() slot)
    //!    emit setMode(e_mode_MASTER);
    return "";
}

QString Pactor::curState() { return "";}
bool Pactor::cwID(bool id) { Q_UNUSED(id); return false; }
bool Pactor::debugLog(bool log) { Q_UNUSED(log); return false; }
void Pactor::dirtyDisconnect()
{
    // set QRT flag if in master mode
    if (tnc->m_pactorTNCThread->PT_current_request  == e_PTMaster)
    {
        // if currently the receiving station, become Tx
        if (tnc->m_pactorTNCThread->engine_info->PT_Tx_Rx == PT_RX)
        {
            //        m_pactoraudioengine->thread_info.send_break  = true;
            tnc->m_pactorTNCThread->send_break             = 1;
        }
        // set QRT request flag
        tnc->QRTrequest = true;
        //!        emit setMode(e_mode_STOPALL);
    }
}

int  Pactor::driveLevel(int lvl)
{
    tnc->m_pactorTNCThread->engine_info->audio_out_gain  = lvl;
    return 0;
}

QString Pactor::fault() { return ""; }  // return codec errString

bool Pactor::fecRcv(bool frcv)
{
    // call modem slot (activates changeMode() slot)
    if (frcv == true)
    {
        //!        emit setMode(e_mode_LISTEN);
    }
    else
    {
        //!        emit setMode(e_mode_STOPALL);
    }
    // update settings
    //tnc->writeSettings(e_mode_button);
    return false;
}

void Pactor::fecsend(QString snd, int repeat)
{
    tnc->m_pactorTNCThread->unproto_repeat = repeat;
    PACTOR_TNC_DEBUG << "e_mode_button" << (int)e_mode_button;
    // call modem slot (activates changeMode() slot)
    //!    emit setMode(tnc->message_in->data.mode.mode_button);
    // update settings
    //tnc->writeSettings(e_mode_button);
    Q_UNUSED(snd);
}

QString Pactor::gridSquare(QString grid) { Q_UNUSED(grid); return ""; }
int  Pactor::leaderExt(int ext) { Q_UNUSED(ext); return 0; }
bool Pactor::listen(bool lis)
{
    // call modem slot (activates changeMode() slot)
    if (lis == true)
    {
        //!        emit setMode(e_mode_LISTEN_ARQ);
    }
    else
    {
        //!        emit setMode(e_mode_STOPALL);
    }
    // update settings
    //tnc->writeSettings(e_mode_button);
    return false;
}

int  Pactor::maxConReq(int max) { Q_UNUSED(max); return 0; }
QString Pactor::monCall() { return ""; }

QString Pactor::myAux(QString aux)
{
    QStringList     string_list;
    QString         wk_string;
    int             i;

    tnc->m_PTAuxCall = aux.toUpper();
    i = 0;
    string_list = tnc->m_PTAuxCall.split(',', QString::SkipEmptyParts, Qt::CaseInsensitive);

    foreach(wk_string, string_list)
    {
        tnc->tx_text_buffs.aux_callsign[i]->clear();
        tnc->tx_text_buffs.aux_callsign[i]->append(wk_string.toLatin1());
        PACTORUI_DEBUG << "ind M" << i << wk_string;
        i++;
        tnc->tx_text_buffs.aux_call_count    = i;
    }
    tnc->m_pactorDemodulator->setThreadCallsign(&tnc->tx_text_buffs);
    // update settings
    //tnc->writeSettings(e_send_mycall);
    Q_UNUSED(aux);
    return "";
}

QString Pactor::myC(QString call)
{
    tnc->m_PTMyCall         = call.toUpper();
    tnc->tx_text_buffs.my_callsign->clear();
    tnc->tx_text_buffs.my_callsign->append(tnc->m_PTMyCall.toLatin1());

    tnc->m_pactorDemodulator->setThreadCallsign(&tnc->tx_text_buffs);

    // update settings
    //tnc->writeSettings(e_send_mycall);
    return "";
}

QString Pactor::newState(QString newstate) { Q_UNUSED(newstate); return ""; }
int  Pactor::offset() { return 0; }
int  Pactor::outQueued(int out) { Q_UNUSED(out); return 0; }
void Pactor::over() { tnc->breakRequest(); }

QString Pactor::playback(QString dev) { Q_UNUSED(dev); return ""; }
QString Pactor::playDevs()            { return ""; }
QString Pactor::ports()               { return ""; }
int  Pactor::responseDly(int dly)     { Q_UNUSED(dly); return 0; }
bool Pactor::robust(bool setRobust)   { Q_UNUSED(setRobust); /*tnc->writeSettings(e_general_parameters);*/ return false; }

void Pactor::sendBreak()              { tnc->breakRequest(); }

int Pactor::sendID(int id)            { Q_UNUSED(id); return 0; }

QString Pactor::setMode(QString set)
{
    // unproto: store speed and repeat count
    //    if (message_in->data.mode.mode_button == e_mode_UNPROTO)
    {
        //        tnc->m_pactorTNCThread->PT_speed       = (e_PT_speed)message_in->data.mode.speed;
        //        tnc->m_pactorTNCThread->unproto_repeat = message_in->data.mode.repeat_count;
    }
    //    if (message_in->data.mode.mode_button == e_mode_MASTER)
    {
        //PTMyCall(m_callsign, m_auto_answer);
        //        tnc->m_PTremoteCall = QString(QByteArray((const char *)message_in->data.mode.remote_call.chars, message_in->data.mode.remote_call.count)).toUpper();

        tnc->tx_text_buffs.remote_callsign->clear();
        tnc->tx_text_buffs.remote_callsign->append(tnc->m_PTremoteCall.toLatin1());

        tnc->m_pactorDemodulator->setThreadCallsign(&tnc->tx_text_buffs);

        // update settings
        //tnc->writeSettings(e_remote_call);
    }
    PACTOR_TNC_DEBUG << "e_mode_button" << (int)e_mode_button;
    // call modem slot (activates changeMode() slot)
    //!    emit setMode(message_in->data.mode.mode_button);
    // update settings
    //tnc->writeSettings(e_mode_button);
    Q_UNUSED(set);
    return "";
}

void Pactor::setSuffix(QString x) { Q_UNUSED(x); }

bool Pactor::setVox(bool vox) { Q_UNUSED(vox); return false; }

void Pactor::showTNC(bool shw)
{
    if (tnc->window_visible == true)
    {
        tnc->window_visible  = false;
        tnc->m_pactor_session->hide();
    }
    else
    {
        tnc->window_visible  = true;
        tnc->m_pactor_session->show();
    }
    //  === Pactor settings   === //
    // TODO: let me suggest not doing this directly here -
    //       but expose a tnc->setvisible() that will save it in the tnc
    tnc->settings->beginGroup("PactorTNC");
     tnc->settings->setValue("visible", tnc->window_visible);
    tnc->settings->endGroup();
    Q_UNUSED(shw);
}

int  Pactor::squelchLvl(int lvl) { Q_UNUSED(lvl); return 0; }

void Pactor::twoToneText(bool t) { Q_UNUSED(t); }

QString Pactor::version() { return ""; }

bool Pactor::setHuffman(bool huff)
{
    tnc->m_pactorTNCThread->PT_allow_huffman = huff;
    //tnc->writeSettings(e_general_parameters);
    return false;
}

int Pactor::setUCMD(int index, int value)
{
    switch (index)
    {
    // UCMD 0: 100Bd packet count to speedup to 200Bd
    case 0:
        if (value > UCMD0_MAX)
        {
            tnc->m_pactorTNCThread->UCMD[0] = UCMD0_MAX;
        }
        else
        {
            tnc->m_pactorTNCThread->UCMD[0] = value;
        }
        break;

        // UCMD 1: 200Bd packet error count to speeddown to 100Bd
    case 1:
        if (value > UCMD1_MAX)
        {
            tnc->m_pactorTNCThread->UCMD[1] = UCMD1_MAX;
        }
        else
        {
            tnc->m_pactorTNCThread->UCMD[1] = value;
        }
        break;

        // UCMD 2: 200Bd packet count without error to stay to 200Bd
    case 2:
        if (value > UCMD2_MAX)
        {
            tnc->m_pactorTNCThread->UCMD[2] = UCMD2_MAX;
        }
        else
        {
            tnc->m_pactorTNCThread->UCMD[2] = value;
        }
        break;

    // UCMD 3: Mem ARQ packet count combined before clear
    case 3:
        if (value > UCMD3_MAX)
        {
            tnc->m_pactorTNCThread->UCMD[3] = UCMD3_MAX;
        }
        else
        {
            tnc->m_pactorTNCThread->UCMD[3] = value;
        }
        break;

    default:
        break;
    }
    tnc->m_pactorTNCThread->UCMD[index] = value;
    //tnc->writeSettings(e_general_parameters);
    return 0;
}
