/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#include "pactorhuffman.h"
#include "pactordecoder.h"
#include "pactortnc.h"


#define PACTOR_IDLE     0x1e

typedef struct
{
    unsigned short len;
    unsigned short code;
} t_huff_table;
//    0          1           2          3
//    4          5           6          7
//    8          9           a          b
//    c          d           e          f
static t_huff_table huffman[] = {{15, 0x31cf}, {15, 0x51cf}, {15, 0x11cf}, {15, 0x61cf}, // 0
    {15, 0x21cf}, {15, 0x41cf}, {15, 0x1cf}, {15, 0x7f0f},
    {15, 0x3f0f}, {15, 0x5f0f}, { 6,  0x2c}, {15, 0x1f0f},
    {15, 0x6f0f}, { 6,   0xc}, {15, 0x2f0f}, {15, 0x4f0f},
    {15, 0x770f}, {15, 0x370f}, {15, 0x570f}, {15, 0x170f}, // 10
    {15, 0x670f}, {15, 0x270f}, {15, 0x470f}, {15, 0x70f},
    {15, 0x7b0f}, {15, 0x3b0f}, {15, 0x71cf}, {15, 0x5b0f},
    {10, 0x263}, {15, 0x1b0f}, {15, 0xf0f}, {15, 0x7f63},
    { 2,   0x1}, {11, 0x5cf}, {12, 0x363}, {13, 0x1b14}, // 20
    {13, 0x13a8}, {12, 0xb63}, {12, 0x9cf}, {12, 0x763},
    { 9, 0x1b3}, { 9,  0x73}, {12, 0x314}, {12, 0x7cf},
    { 7,  0x53}, {11, 0x7a8}, { 7,  0x13}, {11, 0x3cf},
    { 8, 0x0e3}, { 9,  0x14}, {10, 0x168}, {10, 0x368}, // 30
    {10, 0x0e8}, {10, 0x2a8}, {10, 0x2e8}, {10, 0x1e8},
    {10, 0x3e8}, {10, 0x128}, {11, 0x714}, {14, 0xb0f},
    {13, 0xba8}, {10, 0x10f}, {12, 0xfcf}, {10, 0x2b3},
    {13, 0x3a8}, { 8,  0x94}, { 8,  0xf3}, { 8,  0x8f}, // 40
    { 7,  0x58}, { 8,   0x3}, { 8,  0x33}, {11, 0x728},
    {10, 0x114}, { 8,  0x4f}, {10, 0x183}, {10,  0xb3},
    { 9, 0x173}, { 9,  0xaf}, { 9,   0xf}, { 9,  0x28},
    { 9,  0x68}, {10, 0x3af}, { 9,  0x83}, { 7,  0x6f}, // 50
    { 8,  0x2f}, {10, 0x383}, {10,  0x63}, {10,  0xa8},
    {13, 0x1ba8}, {14, 0x2b14}, { 9,  0xcf}, {15, 0xb14},
    {14, 0x2b0f}, {15, 0x4b14}, {15, 0x3f63}, {12, 0x30f},
    {15, 0x5f63}, { 5,   0x2}, { 7,  0x30}, { 6,  0x32}, // 60
    { 5,  0x1c}, { 3,   0x6}, { 7,  0x70}, { 6,  0x38},
    { 6,   0x8}, { 4,   0xb}, {11, 0x328}, { 7,  0x54},
    { 6,  0x10}, { 6,  0x34}, { 4,   0xa}, { 6,  0x12},
    { 8,  0x43}, {10, 0x1af}, { 4,   0x7}, { 5,   0x4}, // 7
    { 5,   0x0}, { 5,  0x1f}, { 8,  0xc3}, { 7,  0x18},
    {10, 0x163}, {10, 0x1a8}, { 7,  0x23}, {15, 0x1f63},
    {15, 0x6f63}, {15, 0x2f63}, {15, 0x4f63}, {15, 0xf63}
};

static const unsigned short bitmasks[] =
{
    0, 0x1, 0x3, 0x7, 0xf, 0x1f, 0x3f, 0x7f, 0xff,
    0x1ff, 0x3ff, 0x7ff, 0xfff, 0x1fff, 0x3fff, 0x7fff, 0xffff
};

static t_ps     ps;

t_ps *PactorDecoder::huffman_decode_packet(unsigned char *data, int pktlen)
{
    int i;
    char buf[1024];
    unsigned char *bp, *pp;
    int bitptr, pktbitlen;
    unsigned bitbuf;

    t_huff_table   *huff;

    //initialize packet structure
    ps.pkt_data[0] = '\0';
    ps.pkt_data_len = 0;
    bp = (unsigned char *)buf;
    bitptr = 0;
    // pktlen = size in bytes
    // compute bit length
    pktbitlen = pktlen * 8;
    // walk through bit buffer
    while (bitptr < pktbitlen)
    {
        // compute byte address from bit count
        pp = data + (bitptr >> 3);
        // fill a new bit buffer with 24 bit
        bitbuf = (pp[2] << 16) | (pp[1] << 8) | pp[0];
        // shift right according to bitptr which points to first new bit address
        bitbuf >>= bitptr & 7;
        // look for pattern in huffman table
        for (i = 0, huff = huffman;
             (i < 128) && (((bitbuf & bitmasks[huff->len]) != huff->code) || (bitptr + huff->len > pktbitlen));
             i++, huff++)
        {
            ;
        }
        if (i > 127)
        {
            break;
        }
        // comment ?
        switch (i)
        {
            case
                    PACTOR_IDLE:
                break;
            case
                    14:
                ps.pkt_data[ps.pkt_data_len++] = *bp++ = 0204;
                break;

            case
                    15:
                ps.pkt_data[ps.pkt_data_len++] = *bp++ = 0224;
                break;

            case
                    16:
                ps.pkt_data[ps.pkt_data_len++] = *bp++ = 0201;
                break;

            case
                    20:
                ps.pkt_data[ps.pkt_data_len++] = *bp++ = 0216;
                break;

            case
                    21:
                ps.pkt_data[ps.pkt_data_len++] = *bp++ = 0231;
                break;

            case
                    22:
                ps.pkt_data[ps.pkt_data_len++] = *bp++ = 0232;
                break;

            case
                    23:
                ps.pkt_data[ps.pkt_data_len++] = *bp++ = 0341;
                break;

            default
                    :
                // store char value in ps struct
                ps.pkt_data[ps.pkt_data_len++] = i;
                *bp++ = (i >= 32) ? i : '.';
                break;
        }
        // move to next Huffman pattern in bit buffer
        bitptr += huff->len;
    }
    // store 0 after last char
    ps.pkt_data[ps.pkt_data_len] = 0;
    PACTOR_TNC_DEBUG << "huffman out:" << QString((const char *)ps.pkt_data);
    return &ps;
}

// this encoder encodes a text buffer and returns a QByteArray pointer
QByteArray *PactorTNC::huffman_encode_buffer(QByteArray *text_buffer)
{
    static QByteArray   huffman_encoded;
    unsigned char       txt_char;
    unsigned int        encoded_chars;
    int                 encoded_bit_len;
    int                 bit_size;
    unsigned int        pattern;
#ifdef HUFFMAN_TEST
    int                 output_length = 0;
#endif
    // clear huffman buffer
    huffman_encoded.clear();
    // clear result chars
    encoded_chars   = 0;
    encoded_bit_len = 0;

    for (int i = 0; i < text_buffer->size(); i++)
    {
        txt_char    = (unsigned char)text_buffer->at(i);
        // replace chars greater than 0x7F with '?'
        if (txt_char > 0x7F)
        {
            txt_char    = '?';
        }
        // get huffman values: bit size and pattern
        bit_size    = (int)huffman[(int)txt_char].len;
        pattern     = (unsigned int)huffman[(int)txt_char].code;
        // insert bits into encoded_chars
        encoded_chars   = encoded_chars | (pattern << encoded_bit_len);
        encoded_bit_len = encoded_bit_len + bit_size;
        // transfer bits into output buffer when possible
        while (encoded_bit_len > 8)
        {
            huffman_encoded.append((char)(encoded_chars & 0x000000FF));
            encoded_chars   = encoded_chars >> 8;
            encoded_bit_len = encoded_bit_len - 8;
#ifdef HUFFMAN_TEST
            output_length++;
#endif
        }
    }
    // store last bits
    if (encoded_bit_len > 0)
    {
        huffman_encoded.append((char)(encoded_chars & 0x000000FF));
#ifdef HUFFMAN_TEST
        output_length++;
#endif
    }
#ifdef HUFFMAN_TEST
    PACTOR_TNC_DEBUG << "remaining bits" << encoded_bit_len << "encoded len" << output_length;
#endif

    return &huffman_encoded;
}

