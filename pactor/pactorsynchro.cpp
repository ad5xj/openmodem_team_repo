/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#include <math.h>
#include "pactorsynchro.h"
#include "pactordemodulator.h"
#include "pactoraudioengine.h"

#define DISPLAY_COUT

#define STARTING            0
#define EXPECT_HDR200       1
#define EXPECT_HDR100_CS1   2
#define EXPECT_CS2          3
#define EXPECT_CS3          4
#define EXPECT_CS4          5
#define EXPECT_CS1          6

#define BIT_UKWN            0
#define BIT_200             1
#define BIT_100             2
#define BIT_2_100           4
#define BIT_VALID           5

#define DISPLAY_DEBUG_SYNC

#define NO_TRANSITION   0
#define LO_TO_HI        1
#define HI_TO_LO        2

#define SHORT_GAP_200       (CORREL_BIT_LENGTH_200 - GAP_200_MARGIN)
#define LONG_GAP_200        (CORREL_BIT_LENGTH_200 + GAP_200_MARGIN)
#define SHORT_GAP_100       (CORREL_BIT_LENGTH_100 - GAP_100_MARGIN)
#define SHORT_GAP_2_100     (2 * (CORREL_BIT_LENGTH_100 - GAP_100_MARGIN))
#define REGUL_GAP_100       CORREL_BIT_LENGTH_100
#define REGUL_GAP_2_100     (2 * CORREL_BIT_LENGTH_100)
#define LONG_GAP_100        (CORREL_BIT_LENGTH_100 + GAP_100_MARGIN)
#define LONG_GAP_2_100      (2 * (CORREL_BIT_LENGTH_100 + GAP_100_MARGIN))

#define BIT_COUNT_HDR100        8
#define BIT_COUNT_HDR200        8
#define BIT_COUNT_CS1_2         10
#define BIT_COUNT_CS3_4         8
typedef struct
{
    int     sample;
    float   phase;
    float   fifoval;
} t_visu;

t_visu  visu[1024];
int     visu_idx = 0;

extern qint64              global_date_time_mSecs;

#define CS1_ID                  0
#define CS2_ID                  1
#define CS3_ID                  2
#define CS4_ID                  3
#define HDR100_ID               4
#define HDR200_ID               5

// bit count depending on burst Id
const int   burst_bit_count[6]   =
{
    10, // CS1_ID                  0
    10, // CS2_ID                  1
    8,  // CS3_ID                  2
    8,  // CS4_ID                  3
    8,  // HDR100_ID               4
    8   // HDR200_ID               5
};

// bursts gap length
const int             burst_gap_short[4][8]   =
{
    {
        SHORT_GAP_100, SHORT_GAP_100, SHORT_GAP_100, SHORT_GAP_100,
        SHORT_GAP_100, SHORT_GAP_2_100, SHORT_GAP_2_100, SHORT_GAP_100
    },   // CS1
    {
        SHORT_GAP_100, SHORT_GAP_2_100, SHORT_GAP_2_100, SHORT_GAP_100,
        SHORT_GAP_100, SHORT_GAP_100, SHORT_GAP_100, SHORT_GAP_100
    },       // CS2
    {
        SHORT_GAP_100, SHORT_GAP_100, SHORT_GAP_2_100, SHORT_GAP_100,
        SHORT_GAP_100, SHORT_GAP_2_100, SHORT_GAP_2_100, SHORT_GAP_2_100
    }, // CS3
    {
        SHORT_GAP_2_100, SHORT_GAP_100, SHORT_GAP_100, SHORT_GAP_2_100,
        SHORT_GAP_100, SHORT_GAP_100, SHORT_GAP_2_100, SHORT_GAP_2_100
    }    // CS4
};

const int             burst_gap_long[4][8]   =
{
    {
        LONG_GAP_100, LONG_GAP_100, LONG_GAP_100, LONG_GAP_100,
        LONG_GAP_100, LONG_GAP_2_100, LONG_GAP_2_100, LONG_GAP_100
    },       // CS1
    {
        LONG_GAP_100, LONG_GAP_2_100, LONG_GAP_2_100, LONG_GAP_100,
        LONG_GAP_100, LONG_GAP_100, LONG_GAP_100, LONG_GAP_100
    },           // CS2
    {
        LONG_GAP_100, LONG_GAP_100, LONG_GAP_2_100, LONG_GAP_100,
        LONG_GAP_100, LONG_GAP_2_100, LONG_GAP_2_100, LONG_GAP_100
    },       // CS3
    {
        LONG_GAP_2_100, LONG_GAP_100, LONG_GAP_100, LONG_GAP_2_100,
        LONG_GAP_100, LONG_GAP_100, LONG_GAP_2_100, LONG_GAP_2_100
    }        // CS4
};


const char *msg_trans[HI_TO_LO + 1];

/*------------------------------------------------------------------
implementation of function synchronization variables initialization

arguments:
- none

returns: none
------------------------------------------------------------------*/
void PactorDemodThread::initSynchro(void)
{
    sample_count                = 0;
    valid_crossings             = 0;
    active_burst                = false;

    first_data_sample           = 0;
    last_phase_transition       = NO_TRANSITION;
    last_amplitude_transition   = NO_TRANSITION;
    first_phase_transition      = true;
    samples_since_last_high     = 0;
    samples_since_last_low      = 0;

    sync_amplitude_average      = 0.0;

    transition_FIFO_index       = 0;

    delta_transition            = 0;

    msg_trans[LO_TO_HI]         = "LO_TO_HI";
    msg_trans[HI_TO_LO]         = "HI_TO_LO";
    msg_trans[NO_TRANSITION]    = "NO_TRANSITION";
    burst_length                = 0;

    // first sample in identifyBurst()
    stored_transitions_count    = 0;
}

/*------------------------------------------------------------------
implementation of function burstPhase
extracting phase from burst Id and data buffer content

arguments:
- none

returns: none
------------------------------------------------------------------*/
int PactorDemodThread::burstPhase(int burst_type, int transition_edge)
{
    e_PT_phase    return_value;

    return_value    = USE_ANY_PHASE;
    //PACTOR_TNC_DEBUG << "EDGE"<<transition_edge;
    // depending on transition edge, return true if lsb is 0 or false if lsb is 1
    // header 100 and 200 are considered as 0x55
    switch (burst_type)
    {
        case
                CS1_ID:
            if (transition_edge == LO_TO_HI)
            {
                return_value    = PT_PH_INVERTED;
            }
            else
            {
                return_value    = PT_PH_NORMAL;
            }
            break;

        case
                CS2_ID:
            if (transition_edge == LO_TO_HI)
            {
                return_value    = PT_PH_NORMAL;
            }
            else
            {
                return_value    = PT_PH_INVERTED;
            }
            break;

        case
                CS3_ID:
            if (transition_edge == LO_TO_HI)
            {
                return_value    = PT_PH_INVERTED;
            }
            else
            {
                return_value    = PT_PH_NORMAL;
            }
            break;

        case
                CS4_ID:
            if (transition_edge == LO_TO_HI)
            {
                return_value    = PT_PH_NORMAL;
            }
            else
            {
                return_value    = PT_PH_INVERTED;
            }
            break;

        case
                HDR100_ID:
            if (transition_edge == LO_TO_HI)
            {
                return_value    = PT_PH_INVERTED;
            }
            else
            {
                return_value    = PT_PH_NORMAL;
            }
            break;

        case
                HDR200_ID:
            if (transition_edge == LO_TO_HI)
            {
                return_value    = PT_PH_INVERTED;
            }
            else
            {
                return_value    = PT_PH_NORMAL;
            }
            break;
    }
    return return_value;
}

/*------------------------------------------------------------------
    implementation of function synchronization variables initialization

    arguments:
    - none

    returns: none
    ------------------------------------------------------------------*/
int PactorDemodThread::analyzeSynchro(int burst_id, int bit_size, int *transitions_table, int first_bit_pos)
{
    int     barycenter;
    int     i;
    int     center_count;
    int     return_sync;
    int     bit_count;
    int     estim_transition[3];

    return_sync = -1;

    // compute bit_count
    bit_count   = burst_bit_count[burst_id] - 2;
    // initialize analyse table.
    for (i = 0 ; i < 16 ; i++)
    {
        analyse[i]  = 0;
    }
    // store sync positions count
    for (i = 0; i < bit_count; i++)
    {
        analyse[transitions_table[i] % bit_size]++;
    }
    // compute sync pos
    if (bit_size == CORREL_BIT_LENGTH_100)
    {
        // compute average synchronization for 100 Bd data (16 samples per bit)
        // if zero crossings are at beginning and end of analyse[] table, do a wraparound operation
        center_count    = 0;
        sample_count    = 0;
        for (i = 0 ; i < 16 ; i++)
        {
            if ((i >= 4) && (i < 12))
            {
                center_count    += analyse[i];
            }
            else
            {
                sample_count    += analyse[i];
            }
        }

        barycenter  = 0;
        // normal processing if points are centered
        if (center_count > sample_count)
        {
            sample_count    = 0;
            // analyse result table and compute table barycenter
            for (i = 0 ; i < 16 ; i++)
            {
                barycenter      += (analyse[i] * (i + 1));
                sample_count    += analyse[i];
            }
            // avoid divide by zero error
            if (sample_count > 0)
            {
                return_sync    = (int)(((float)barycenter / (float)sample_count) - 0.5);
            }
        }
        else
            // zero crossing are toward the ends of analyse[] table
        {
#ifdef _DISPLAY_DEBUG_SYNC
            PACTOR_TNC_DEBUG << "=========> shifted result 16";
#endif
            sample_count    = 0;
            // analyse result table and compute table barycenter
            int     j;
            j       = 4;
            for (i = 0 ; i < 16 ; i++)
            {
                barycenter      += (analyse[j] * (i + 1));
                sample_count    += analyse[j];
                j       = (j + 1) % 16;
            }
            // avoid divide by zero error
            if (sample_count > 0)
            {
                return_sync    = ((int)(((float)barycenter / (float)sample_count) + 3.5) & 0x0f);
            }
        }
    }
    else
    {
        // compute average synchronization for 200 Bd data (8 samples per bit)
        // if zero crossings are at beginning and end of analyse[] table, do a wraparound operation
        center_count    = 0;
        sample_count    = 0;
        for (i = 0 ; i < 8 ; i++)
        {
            if ((i >= 2) && (i < 6))
            {
                center_count    += analyse[i];
            }
            else
            {
                sample_count    += analyse[i];
            }
        }
        barycenter  = 0;
        // normal processing if points are centered
        if (center_count > sample_count)
        {
            sample_count    = 0;
            // analyse result table and compute table barycenter
            for (i = 0 ; i < 8 ; i++)
            {
                barycenter      += (analyse[i] * (i + 1));
                sample_count    += analyse[i];
            }
            // avoid divide by zero error
            if (sample_count > 0)
            {
                return_sync    = (int)(((float)barycenter / (float)sample_count) - 0.5);
            }
        }
        else
            // zero crossing are toward the ends of analyse[] table
        {
#ifdef _DISPLAY_DEBUG_SYNC
            PACTOR_TNC_DEBUG << "=========> shifted result 8";
#endif
            sample_count    = 0;
            // analyse result table and compute table barycenter
            int     j;
            j       = 2;
            for (i = 0 ; i < 8 ; i++)
            {
                barycenter      += (analyse[j] * (i + 1));
                sample_count    += analyse[j];
                j       = (j + 1) % 8;
            }
            // avoid divide by zero error
            if (sample_count > 0)
            {
                return_sync    = ((int)(((float)barycenter / (float)sample_count) + 1.5) & 0x07);
            }
        }
    }

    // estimate transitions
    // initialize analyse table.
    int sync_error;
    int sync_min_error  = 10000;
    int sync_idx;
    for (i = 0 ; i < 3 ; i++)
    {
        estim_transition[i] = ((int)(first_bit_pos / bit_size) * bit_size) + (bit_size * (i - 1)) + return_sync;
        sync_error  = abs(first_bit_pos - estim_transition[i]);
        if (sync_error < sync_min_error)
        {
            sync_min_error  = sync_error;
            sync_idx        = i;
        }
    }
    // return corrected first position
    return_sync = (estim_transition[sync_idx] + DEMOD_BUFFER_SIZE) % DEMOD_BUFFER_SIZE;

#ifdef DISPLAY_DEBUG_SYNC
    PACTOR_TNC_DEBUG << "=========> corrected position" << return_sync;
#endif
    return return_sync;
}

/*------------------------------------------------------------------
implementation of function identifyBurst()
    If gap = -1 -> ramp-up
arguments:
- last_transition_pos  index of last transition recorded in gap_length_table

returns: t_ident_info
------------------------------------------------------------------*/
void PactorDemodThread::identifyBurst(t_gap_table_elmt gap_table[], int last_transition_pos, qint64 timeStampMsecs)
{
    int         bit_index_200;
    int         bit_index_100;
    int         sample_index;
    int         transition_edge;
    int         bit_position_table[12];
    int         last_transition_sample_index;
    int         samples_count;

#undef DISPLAY_SYNC_PROGRESS
#ifdef DISPLAY_SYNC_PROGRESS
    char    progress_table[1024];
    int     progress_idx;
    progress_idx = 0;
#endif

    // first time initialization
    if (stored_transitions_count <= CORREL_BITS_CS)
    {
        // store index of first transition
        if (stored_transitions_count == 0)
        {
            first_to_process_200    = last_transition_pos;
            first_to_process_100    = last_transition_pos;
            first_to_process_CS1    = last_transition_pos;
            first_to_process_CS2    = last_transition_pos;
            first_to_process_CS3    = last_transition_pos;
            first_to_process_CS4    = last_transition_pos;
            sample_count_200        = 0;
            sample_count_100        = 0;
            sample_count_CS1        = 0;
            sample_count_CS2        = 0;
            sample_count_CS3        = 0;
            sample_count_CS4        = 0;
        }
        // increment stored count and return
        stored_transitions_count++;
        sample_count_200++;
        sample_count_100++;
        sample_count_CS1++;
        sample_count_CS2++;
        sample_count_CS3++;
        sample_count_CS4++;
        return;
    }

#ifdef _DISPLAY_DEBUG_SYNC
    PACTOR_TNC_DEBUG << "identifyBurst: trans at" << gap_table[last_transition_pos].position
                     << "gap_length" << gap_table[last_transition_pos].length;
#endif
    last_transition_sample_index    = gap_table[last_transition_pos].position;
    /**********************************************************************************************/
    /*                     200 Bd header processing                                               */
    /**********************************************************************************************/
    // available transitions count
    sample_count_200++;
    // bit index in header (0 to 7)
    bit_index_200       = 0;
    // 200 Bd header
    while (sample_count_200 >= BIT_COUNT_HDR200)
    {
        // index of transition processed during this iteration
        sample_index    = (first_to_process_200 + bit_index_200 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
#ifdef _DISPLAY_DEBUG_SYNC
        PACTOR_TNC_DEBUG << "identifyBurst: process bit" << bit_index_200 << "sample" << sample_index << "pos" << gap_table[sample_index].position
                         << "length" << gap_table[sample_index].length
                         << "sample_count_200" << sample_count_200
                         << "first_to_process_200" << first_to_process_200;
#endif
        switch (bit_index_200)
        {
                // first bit may be longer than a normal 200 Bd bit
            case
                    0:
                // correct gap length: check next transition/bit
                if (gap_table[sample_index].length >= SHORT_GAP_200)
                {
#ifdef DISPLAY_SYNC_PROGRESS
                    progress_table[progress_idx++]    = bit_index_200 + '0';
#endif
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "BIT 0 at" << sample_index << "pos" << gap_table[sample_index].position
                                     << "gap" << gap_table[sample_index].length << "spl cnt" << sample_count_200;
#endif
                    // update position table used to compute accurate sync position
                    bit_position_table[bit_index_200]   = gap_table[sample_index].position;
                    // next bit
                    bit_index_200++;
                    // store first transition edge
                    transition_edge = gap_table[sample_index].lo_to_hi;
                    // compute first bit timestamp
                    samples_count   = (last_transition_sample_index - gap_table[sample_index].position + DEMOD_BUFFER_SIZE) % DEMOD_BUFFER_SIZE;
                    burst_parameters.hdr200.time_msecs    = timeStampMsecs - (((samples_count - SHORT_GAP_200) * SAMPLE_DWNSPL_DURATION_US) / 1000);
                    burst_parameters.hdr200.new_burst   = true;
                    burst_parameters.hdr200.first_trans_index   = sample_index;
                }
                // wrong gap length: move starting point
                else
                {
                    // move starting point
                    first_to_process_200  = (first_to_process_200 + 1 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
                    // we used 1 transition
                    sample_count_200--;
                    // reset bit index because of error
                    bit_index_200         = 0;
                    // reset new burst flag
                    burst_parameters.hdr200.new_burst   = false;
                }
                break;
                // other bits should be normal 200 Bd bit
            case
                    1:
            case
                    2:
            case
                    3:
            case
                    4:
            case
                    5:
            case
                    6:
                // correct gap length: check next transition/bit
                if ((gap_table[sample_index].length >= SHORT_GAP_200)
                    && (gap_table[sample_index].length <= LONG_GAP_200))
                {
#ifdef DISPLAY_SYNC_PROGRESS
                    progress_table[progress_idx++]    = bit_index_200 + '0';
#endif
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "BIT" << bit_index_200 << "at" << sample_index << "pos" << gap_table[sample_index].position
                                     << "gap" << gap_table[sample_index].length << "spl cnt" << sample_count_200;
#endif
                    // update position table used to compute accurate sync position
                    bit_position_table[bit_index_200]   = gap_table[sample_index].position;
                    // next bit
                    bit_index_200++;
                }
                // wrong gap length: move starting point
                else
                {
                    // move starting point
                    first_to_process_200  = (first_to_process_200 + bit_index_200 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
                    // we used bit_index_200 + 1 transitions
                    sample_count_200    -= bit_index_200;
                    // reset bit index because of error
                    bit_index_200         = 0;
                    // reset new burst flag
                    burst_parameters.hdr200.new_burst   = false;
                }
                break;
                // last bit may be longer than a normal 200 Bd bit
            case
                    7:
                // correct gap length: check next transition/bit
                if (gap_table[sample_index].length >= SHORT_GAP_200)
                {
#ifdef DISPLAY_SYNC_PROGRESS
                    progress_table[progress_idx++]    = bit_index_200 + '0';
#endif
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "HDR200 BIT" << bit_index_200 << "at" << sample_index << "pos" << gap_table[sample_index].position
                                     << "gap" << gap_table[sample_index].length << "spl cnt" << sample_count_200;
#endif
                    // update position table used to compute accurate sync position
                    bit_position_table[bit_index_200]   = gap_table[sample_index].position;
                    // update returned value
                    burst_parameters.hdr200.valid     = true;
                    burst_parameters.hdr200.pos       = (gap_table[first_to_process_200].position
                                                         + DEMOD_BUFFER_SIZE - CORREL_BIT_LENGTH_200) % DEMOD_BUFFER_SIZE;
                    // look for phase
                    burst_parameters.hdr200.phase = burstPhase(HDR200_ID, transition_edge);
                    PACTOR_TNC_DEBUG << "\nhdr200 (synchro.cpp)" << burst_parameters.hdr200.time_msecs
                                     << "phase" << burst_parameters.hdr200.phase;
                }
                // reset new burst flag
                burst_parameters.hdr200.new_burst   = false;
                // reset bit
                bit_index_200   = 0;
                // move starting point (last transition, burst header found)
                first_to_process_200  = (first_to_process_200 + BIT_COUNT_HDR200 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
                // we lost 1 transition (wrong gap length) or we are finished. All previously processed gaps are dumped
                // we used 8 transitions
                sample_count_200    -= BIT_COUNT_HDR200;
                break;
        }
        // analyse synchronization points if header was found
        if (burst_parameters.hdr200.valid == true)
        {
            burst_parameters.hdr200.fine_sync = analyzeSynchro(HDR200_ID, CORREL_BIT_LENGTH_200, bit_position_table, burst_parameters.hdr200.pos);
#ifdef DISPLAY_DEBUG_SYNC
            PACTOR_TNC_DEBUG << "FOUND HDR200 at" << burst_parameters.hdr200.pos
                             << "last input" << gap_table[last_transition_pos].position
                             << "fine sync" << burst_parameters.hdr200.fine_sync
                             << "pos+sync" << gap_table[last_transition_pos].position
                             - (gap_table[last_transition_pos].position % 8) + burst_parameters.hdr200.fine_sync;
#endif
            break;
        }
    }
    /**********************************************************************************************/
    /*                     100 Bd header processing                                               */
    /**********************************************************************************************/
    // available transitions count
    sample_count_100++;
    // bit index in header (0 to 7)
    bit_index_100       = 0;
    // 100 Bd header
    while (sample_count_100 >= BIT_COUNT_HDR100)
    {
        // index of transition processed during this iteration
        sample_index    = (first_to_process_100 + bit_index_100 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
#ifdef _DISPLAY_DEBUG_SYNC
        PACTOR_TNC_DEBUG << "identifyBurst: process bit" << bit_index_100 << "sample" << sample_index << "pos" << gap_table[sample_index].position
                         << "length" << gap_table[sample_index].length
                         << "sample_count_100" << sample_count_100
                         << "first_to_process_100" << first_to_process_100;
#endif
        switch (bit_index_100)
        {
                // first bit may be longer than a normal 100 Bd bit
            case
                    0:
                // correct gap length: check next transition/bit
                if (gap_table[sample_index].length >= SHORT_GAP_100)
                {
#ifdef DISPLAY_SYNC_PROGRESS
                    progress_table[progress_idx++]    = bit_index_100 + '0';
#endif
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "BIT 0 at" << sample_index << "pos" << gap_table[sample_index].position
                                     << "gap" << gap_table[sample_index].length << "spl cnt" << sample_count_100;
#endif
                    // update position table used to compute accurate sync position
                    bit_position_table[bit_index_100]   = gap_table[sample_index].position;
                    // next bit
                    bit_index_100++;
                    // store first transition edge
                    transition_edge = gap_table[sample_index].lo_to_hi;
                    // compute first bit timestamp
                    samples_count   = (last_transition_sample_index - gap_table[sample_index].position + DEMOD_BUFFER_SIZE) % DEMOD_BUFFER_SIZE;
                    burst_parameters.hdr100.time_msecs    = timeStampMsecs - (((samples_count - SHORT_GAP_100) * SAMPLE_DWNSPL_DURATION_US) / 1000);
                    burst_parameters.hdr100.new_burst   = true;
                    burst_parameters.hdr100.first_trans_index   = sample_index;
                }
                // wrong gap length: move starting point
                else
                {
                    // move starting point
                    first_to_process_100  = (first_to_process_100 + 1 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
                    // we used 1 transition
                    sample_count_100--;
                    // reset bit index because of error
                    bit_index_100         = 0;
                    // reset new burst flag
                    burst_parameters.hdr100.new_burst   = false;
                }
                break;
                // other bits should be normal 100 Bd bit
            case
                    1:
            case
                    2:
            case
                    3:
            case
                    4:
            case
                    5:
            case
                    6:
                // correct gap length: check next transition/bit
                if ((gap_table[sample_index].length >= SHORT_GAP_100)
                    && (gap_table[sample_index].length <= LONG_GAP_100))
                {
#ifdef DISPLAY_SYNC_PROGRESS
                    progress_table[progress_idx++]    = bit_index_100 + '0';
#endif
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "BIT" << bit_index_100 << "at" << sample_index << "pos" << gap_table[sample_index].position
                                     << "gap" << gap_table[sample_index].length << "spl cnt" << sample_count_100;
#endif
                    // update position table used to compute accurate sync position
                    bit_position_table[bit_index_100]   = gap_table[sample_index].position;
                    // next bit
                    bit_index_100++;
                }
                // wrong gap length: move starting point
                else
                {
                    // move starting point
                    first_to_process_100  = (first_to_process_100 + bit_index_100 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
                    // we used bit_index_100 + 1 transitions
                    sample_count_100    -= bit_index_100;
                    // reset bit index because of error
                    bit_index_100         = 0;
                    // reset new burst flag
                    burst_parameters.hdr100.new_burst   = false;
                }
                break;
                // last bit may be longer than a normal 100 Bd bit
            case
                    7:
                // correct gap length: check next transition/bit
                if (gap_table[sample_index].length >= SHORT_GAP_100)
                {
#ifdef DISPLAY_SYNC_PROGRESS
                    progress_table[progress_idx++]    = bit_index_100 + '0';
#endif
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "HDR100 BIT" << bit_index_100 << "at" << sample_index << "pos" << gap_table[sample_index].position
                                     << "gap" << gap_table[sample_index].length << "spl cnt" << sample_count_100;
#endif
                    // update position table used to compute accurate sync position
                    bit_position_table[bit_index_100]   = gap_table[sample_index].position;
                    // update returned value
                    burst_parameters.hdr100.valid     = true;
                    burst_parameters.hdr100.pos       = (gap_table[first_to_process_100].position
                                                         + DEMOD_BUFFER_SIZE - CORREL_BIT_LENGTH_100) % DEMOD_BUFFER_SIZE;
                    // look for phase
                    burst_parameters.hdr100.phase = burstPhase(HDR100_ID, transition_edge);
                    PACTOR_TNC_DEBUG << "\nHDR100 (synchro.cpp)" << burst_parameters.hdr100.time_msecs
                                     << "phase" << burst_parameters.hdr100.phase;
                }
                // reset bit
                bit_index_100   = 0;
                // move starting point (last transition, burst header found)
                first_to_process_100  = (first_to_process_100 + BIT_COUNT_HDR100 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
                // we lost 1 transition (wrong gap length) or we are finished. All previously processed gaps are dumped
                // we used 8 transitions
                sample_count_100    -= BIT_COUNT_HDR100;
                // reset new burst flag
                burst_parameters.hdr100.new_burst   = false;
                break;
        }
        // analyse synchronization points if header was found
        if (burst_parameters.hdr100.valid == true)
        {
            burst_parameters.hdr100.fine_sync = analyzeSynchro(HDR100_ID, CORREL_BIT_LENGTH_100, bit_position_table, burst_parameters.hdr100.pos);
#ifdef _DISPLAY_DEBUG_SYNC
            PACTOR_TNC_DEBUG << "FOUND HDR100 at" << burst_parameters.hdr100.pos
                             << "last input" << gap_table[last_transition_pos].position
                             << "fine sync" << burst_parameters.hdr100.fine_sync
                             << "pos+sync" << gap_table[last_transition_pos].position
                             - (gap_table[last_transition_pos].position % 8) + burst_parameters.hdr100.fine_sync;
#endif
            break;
        }
    }

    /**********************************************************************************************/
    /*                     CS1 processing                                               */
    /**********************************************************************************************/
    // available transitions count
    sample_count_CS1++;
    // bit index in header (0 to 7)
    bit_index_100       = 0;
    // CS1 Bd header
    while (sample_count_CS1 >= BIT_COUNT_CS1_2)
    {
        // index of transition processed during this iteration
        sample_index    = (first_to_process_CS1 + bit_index_100 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
#ifdef _DISPLAY_DEBUG_SYNC
        PACTOR_TNC_DEBUG << "identifyBurst: process bit" << bit_index_100 << "sample" << sample_index << "pos" << gap_table[sample_index].position
                         << "length" << gap_table[sample_index].length
                         << "sample_count_CS1" << sample_count_CS1
                         << "first_to_process_CS1" << first_to_process_CS1;
#endif
        switch (bit_index_100)
        {
                // first bit may be longer than a normal CS1 Bd bit
            case
                    0:
                // correct gap length: check next transition/bit
                if (gap_table[sample_index].length >= SHORT_GAP_100)
                {
#ifdef DISPLAY_SYNC_PROGRESS
                    progress_table[progress_idx++]    = bit_index_100 + '0';
#endif
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "BIT 0 at" << sample_index << "pos" << gap_table[sample_index].position
                                     << "gap" << gap_table[sample_index].length << "spl cnt" << sample_count_CS1
                                     << "lo to hi" << gap_table[sample_index].lo_to_hi;
#endif
                    // update position table used to compute accurate sync position
                    bit_position_table[bit_index_100]   = gap_table[sample_index].position;
                    // next bit
                    bit_index_100++;
                    // store first transition edge
                    transition_edge = gap_table[sample_index].lo_to_hi;
                    // compute first bit timestamp
                    samples_count   = (last_transition_sample_index - gap_table[sample_index].position + DEMOD_BUFFER_SIZE) % DEMOD_BUFFER_SIZE;
                    burst_parameters.CS100.time_msecs    = timeStampMsecs - (((samples_count - SHORT_GAP_100) * SAMPLE_DWNSPL_DURATION_US) / 1000);
                    burst_parameters.CS100.new_burst   = true;
                    burst_parameters.CS100.first_trans_index   = sample_index;
                }
                // wrong gap length: move starting point
                else
                {
                    // move starting point
                    first_to_process_CS1  = (first_to_process_CS1 + 1 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
                    // we used 1 transition
                    sample_count_CS1--;
                    // reset bit index because of error
                    bit_index_100         = 0;
                    // reset new burst flag
                    burst_parameters.CS100.new_burst   = false;
                }
                break;
                // other bits should be normal CS1 Bd bit
            case
                    1:
            case
                    2:
            case
                    3:
            case
                    4:
            case
                    5:
            case
                    6:
            case
                    7:
            case
                    8:
                // correct gap length: check next transition/bit
                if ((gap_table[sample_index].length >= burst_gap_short[CS1_ID][bit_index_100 - 1])
                    && (gap_table[sample_index].length <= burst_gap_long[CS1_ID][bit_index_100 - 1]))
                {
#ifdef DISPLAY_SYNC_PROGRESS
                    progress_table[progress_idx++]    = bit_index_100 + '0';
#endif
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "BIT" << bit_index_100 << "at" << sample_index << "pos" << gap_table[sample_index].position
                                     << "gap" << gap_table[sample_index].length << "spl cnt" << sample_count_CS1;
#endif
                    // update position table used to compute accurate sync position
                    bit_position_table[bit_index_100]   = gap_table[sample_index].position;
                    // next bit
                    bit_index_100++;
                }
                // wrong gap length: move starting point
                else
                {
                    // move starting point
                    first_to_process_CS1  = (first_to_process_CS1 + bit_index_100 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
                    // we used bit_index_100 + 1 transitions
                    sample_count_CS1    -= bit_index_100;
                    // reset bit index because of error
                    bit_index_100         = 0;
                    // reset new burst flag
                    burst_parameters.CS100.new_burst   = false;
                }
                break;
                // last bit may be longer than a normal CS1 Bd bit
            case
                    9:
                // correct gap length: check next transition/bit
                if (gap_table[sample_index].length >= SHORT_GAP_100)
                {
#ifdef DISPLAY_SYNC_PROGRESS
                    progress_table[progress_idx++]    = bit_index_100 + '0';
#endif
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "CS1 BIT" << bit_index_100 << "at" << sample_index << "pos" << gap_table[sample_index].position
                                     << "gap" << gap_table[sample_index].length << "spl cnt" << sample_count_CS1
                                     << "transition_edge" << transition_edge;
#endif
                    // update position table used to compute accurate sync position
                    bit_position_table[bit_index_100]   = gap_table[sample_index].position;
                    // update returned value
                    burst_parameters.CS100.valid     = true;
                    burst_parameters.CS100.pos       = (gap_table[first_to_process_CS1].position
                                                        + DEMOD_BUFFER_SIZE - CORREL_BIT_LENGTH_100) % DEMOD_BUFFER_SIZE;
                    burst_parameters.CS100.CS_type   = CS1_ID;
                    // look for phase
                    burst_parameters.CS100.phase = burstPhase(CS1_ID, transition_edge);
                    //                    PACTOR_TNC_DEBUG << "\nCS1 (synchro.cpp)"<<burst_parameters.CS100.time_usecs/1000;
                }
                // reset new burst flag
                burst_parameters.CS100.new_burst   = false;

                // reset bit
                bit_index_100   = 0;
                // move starting point (last transition, burst header found)
                first_to_process_CS1  = (first_to_process_CS1 + BIT_COUNT_CS1_2 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
                // we lost 1 transition (wrong gap length) or we are finished. All previously processed gaps are dumped
                // we used 8 transitions
                sample_count_CS1    -= BIT_COUNT_CS1_2;
                break;
        }
        // analyse synchronization points if header was found
        if ((burst_parameters.CS100.valid == true)
            && (burst_parameters.CS100.CS_type == CS1_ID))
        {
            burst_parameters.CS100.fine_sync      = analyzeSynchro(CS1_ID, CORREL_BIT_LENGTH_100, bit_position_table, burst_parameters.CS100.pos);
#ifdef _DISPLAY_DEBUG_SYNC
            PACTOR_TNC_DEBUG << "FOUND CS1 at" << burst_parameters.CS100.pos
                             << "last input" << gap_table[last_transition_pos].position
                             << "fine sync" << burst_parameters.CS100.fine_sync
                             << "pos+sync" << gap_table[last_transition_pos].position
                             - (gap_table[last_transition_pos].position % 8) + burst_parameters.CS100.fine_sync;
#endif
            break;
        }
    }

    /**********************************************************************************************/
    /*                     CS2 processing                                               */
    /**********************************************************************************************/
    // available transitions count
    sample_count_CS2++;
    // bit index in header (0 to 7)
    bit_index_100       = 0;
    // CS1 Bd header
    while (sample_count_CS2 >= BIT_COUNT_CS1_2)
    {
        // index of transition processed during this iteration
        sample_index    = (first_to_process_CS2 + bit_index_100 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
#ifdef _DISPLAY_DEBUG_SYNC
        PACTOR_TNC_DEBUG << "identifyBurst: process bit" << bit_index_100 << "sample" << sample_index << "pos" << gap_table[sample_index].position
                         << "length" << gap_table[sample_index].length
                         << "sample_count_CS1" << sample_count_CS1
                         << "first_to_process_CS1" << first_to_process_CS1;
#endif
        switch (bit_index_100)
        {
                // first bit may be longer than a normal CS2 Bd bit
            case
                    0:
                // correct gap length: check next transition/bit
                if (gap_table[sample_index].length >= SHORT_GAP_100)
                {
#ifdef DISPLAY_SYNC_PROGRESS
                    progress_table[progress_idx++]    = bit_index_100 + '0';
#endif
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "BIT 0 at" << sample_index << "pos" << gap_table[sample_index].position
                                     << "gap" << gap_table[sample_index].length << "spl cnt" << sample_count_CS2;
#endif
                    // update position table used to compute accurate sync position
                    bit_position_table[bit_index_100]   = gap_table[sample_index].position;
                    // next bit
                    bit_index_100++;
                    // store first transition edge
                    transition_edge = gap_table[sample_index].lo_to_hi;
                    // compute first bit timestamp
                    samples_count   = (last_transition_sample_index - gap_table[sample_index].position + DEMOD_BUFFER_SIZE) % DEMOD_BUFFER_SIZE;
                    burst_parameters.CS100.time_msecs    = timeStampMsecs - (((samples_count - SHORT_GAP_100) * SAMPLE_DWNSPL_DURATION_US) / 1000);
                    burst_parameters.CS100.new_burst   = true;
                    burst_parameters.CS100.first_trans_index   = sample_index;
                }
                // wrong gap length: move starting point
                else
                {
                    // move starting point
                    first_to_process_CS2  = (first_to_process_CS2 + 1 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
                    // we used 1 transition
                    sample_count_CS2--;
                    // reset bit index because of error
                    bit_index_100         = 0;
                    // reset new burst flag
                    burst_parameters.CS100.new_burst   = false;
                }
                break;
                // other bits should be normal CS1 Bd bit
            case
                    1:
            case
                    2:
            case
                    3:
            case
                    4:
            case
                    5:
            case
                    6:
                // correct gap length: check next transition/bit
                if ((gap_table[sample_index].length >= burst_gap_short[CS2_ID][bit_index_100 - 1])
                    && (gap_table[sample_index].length <= burst_gap_long[CS2_ID][bit_index_100 - 1]))
                {
#ifdef DISPLAY_SYNC_PROGRESS
                    progress_table[progress_idx++]    = bit_index_100 + '0';
#endif
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "BIT" << bit_index_100 << "at" << sample_index << "pos" << gap_table[sample_index].position
                                     << "gap" << gap_table[sample_index].length << "spl cnt" << sample_count_CS2;
#endif
                    // update position table used to compute accurate sync position
                    bit_position_table[bit_index_100]   = gap_table[sample_index].position;
                    // next bit
                    bit_index_100++;
                }
                // wrong gap length: move starting point
                else
                {
                    // move starting point
                    first_to_process_CS2  = (first_to_process_CS2 + bit_index_100 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
                    // we used bit_index_100 + 1 transitions
                    sample_count_CS2    -= bit_index_100;
                    // reset bit index because of error
                    bit_index_100         = 0;
                    // reset new burst flag
                    burst_parameters.CS100.new_burst   = false;
                }
                break;
                // last bit may be longer than a normal CS2 Bd bit
            case
                    7:
                // correct gap length: check next transition/bit
                if (gap_table[sample_index].length >= SHORT_GAP_100)
                {
#ifdef DISPLAY_SYNC_PROGRESS
                    progress_table[progress_idx++]    = bit_index_100 + '0';
#endif
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "CS2 BIT" << bit_index_100 << "at" << sample_index << "pos" << gap_table[sample_index].position
                                     << "gap" << gap_table[sample_index].length << "spl cnt" << sample_count_CS2;
#endif
                    // update position table used to compute accurate sync position
                    bit_position_table[bit_index_100]   = gap_table[sample_index].position;
                    // update returned value
                    burst_parameters.CS100.valid     = true;
                    burst_parameters.CS100.pos       = (gap_table[first_to_process_CS2].position
                                                        + DEMOD_BUFFER_SIZE - CORREL_BIT_LENGTH_100) % DEMOD_BUFFER_SIZE;
                    burst_parameters.CS100.CS_type   = CS2_ID;
                    // look for phase
                    burst_parameters.CS100.phase = burstPhase(CS2_ID, transition_edge);
                    //                    PACTOR_TNC_DEBUG << "\nCS2 (synchro.cpp)"<<burst_parameters.CS100.time_usecs/1000;
                }
                // reset bit
                bit_index_100   = 0;
                // move starting point (last transition, burst header found)
                first_to_process_CS2  = (first_to_process_CS2 + BIT_COUNT_CS1_2 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
                // we lost 1 transition (wrong gap length) or we are finished. All previously processed gaps are dumped
                // we used 8 transitions
                sample_count_CS2    -= BIT_COUNT_CS1_2;
                // reset new burst flag
                burst_parameters.CS100.new_burst   = false;
                break;
        }
        // analyse synchronization points if header was found
        if ((burst_parameters.CS100.valid == true)
            && (burst_parameters.CS100.CS_type == CS2_ID))
        {
            burst_parameters.CS100.fine_sync      = analyzeSynchro(CS2_ID, CORREL_BIT_LENGTH_100, bit_position_table, burst_parameters.CS100.pos);
#ifdef _DISPLAY_DEBUG_SYNC
            PACTOR_TNC_DEBUG << "FOUND CS2 at" << burst_parameters.CS100.pos
                             << "last input" << gap_table[last_transition_pos].position
                             << "fine sync" << burst_parameters.CS100.fine_sync
                             << "pos+sync" << gap_table[last_transition_pos].position
                             - (gap_table[last_transition_pos].position % 8) + burst_parameters.CS100.fine_sync;
#endif
            break;
        }
    }

    /**********************************************************************************************/
    /*                     CS3 processing                                               */
    /**********************************************************************************************/
    // available transitions count
    sample_count_CS3++;
    // bit index in header (0 to 7)
    bit_index_100       = 0;
    // CS1 Bd header
    while (sample_count_CS3 >= BIT_COUNT_CS3_4)
    {
        // index of transition processed during this iteration
        sample_index    = (first_to_process_CS3 + bit_index_100 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
#ifdef _DISPLAY_DEBUG_SYNC
        PACTOR_TNC_DEBUG << "identifyBurst: process bit" << bit_index_100 << "sample" << sample_index << "pos" << gap_table[sample_index].position
                         << "length" << gap_table[sample_index].length
                         << "sample_count_CS1" << sample_count_CS1
                         << "first_to_process_CS1" << first_to_process_CS1;
#endif
        switch (bit_index_100)
        {
                // first bit may be longer than a normal CS3 Bd bit
            case
                    0:
                // correct gap length: check next transition/bit
                if (gap_table[sample_index].length >= SHORT_GAP_2_100)
                {
#ifdef DISPLAY_SYNC_PROGRESS
                    progress_table[progress_idx++]    = bit_index_100 + '0';
#endif
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "BIT 0 at" << sample_index << "pos" << gap_table[sample_index].position
                                     << "gap" << gap_table[sample_index].length << "spl cnt" << sample_count_CS3;
#endif
                    // update position table used to compute accurate sync position
                    bit_position_table[bit_index_100]   = gap_table[sample_index].position;
                    // next bit
                    bit_index_100++;
                    // store first transition edge
                    transition_edge = gap_table[sample_index].lo_to_hi;
                    // compute first bit timestamp
                    samples_count   = (last_transition_sample_index - gap_table[sample_index].position + DEMOD_BUFFER_SIZE) % DEMOD_BUFFER_SIZE;
                    burst_parameters.CS100.time_msecs    = timeStampMsecs - (((samples_count - SHORT_GAP_2_100) * SAMPLE_DWNSPL_DURATION_US) / 1000);
                    burst_parameters.CS100.new_burst   = true;
                    burst_parameters.CS100.first_trans_index   = sample_index;
                }
                // wrong gap length: move starting point
                else
                {
                    // move starting point
                    first_to_process_CS3  = (first_to_process_CS3 + 1 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
                    // we used 1 transition
                    sample_count_CS3--;
                    // reset bit index because of error
                    bit_index_100         = 0;
                    // reset new burst flag
                    burst_parameters.CS100.new_burst   = false;
                }
                break;
                // other bits should be normal CS1 Bd bit
            case
                    1:
            case
                    2:
            case
                    3:
            case
                    4:
            case
                    5:
            case
                    6:
                // correct gap length: check next transition/bit
                if ((gap_table[sample_index].length >= burst_gap_short[CS3_ID][bit_index_100 - 1])
                    && (gap_table[sample_index].length <= burst_gap_long[CS3_ID][bit_index_100 - 1]))
                {
#ifdef DISPLAY_SYNC_PROGRESS
                    progress_table[progress_idx++]    = bit_index_100 + '0';
#endif
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "BIT" << bit_index_100 << "at" << sample_index << "pos" << gap_table[sample_index].position
                                     << "gap" << gap_table[sample_index].length << "spl cnt" << sample_count_CS3;
#endif
                    // update position table used to compute accurate sync position
                    bit_position_table[bit_index_100]   = gap_table[sample_index].position;
                    // next bit
                    bit_index_100++;
                }
                // wrong gap length: move starting point
                else
                {
                    // move starting point
                    first_to_process_CS3  = (first_to_process_CS3 + bit_index_100 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
                    // we used bit_index_100 + 1 transitions
                    sample_count_CS3    -= bit_index_100;
                    // reset bit index because of error
                    bit_index_100         = 0;
                    // reset new burst flag
                    burst_parameters.CS100.new_burst   = false;
                }
                break;
                // last bit may be longer than a normal CS3 Bd bit
            case
                    7:
                // correct gap length: check next transition/bit
                if (gap_table[sample_index].length >= SHORT_GAP_2_100)
                {
#ifdef DISPLAY_SYNC_PROGRESS
                    progress_table[progress_idx++]    = bit_index_100 + '0';
#endif
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "CS3 BIT" << bit_index_100 << "at" << sample_index << "pos" << gap_table[sample_index].position
                                     << "gap" << gap_table[sample_index].length << "spl cnt" << sample_count_CS3;
#endif
                    // update position table used to compute accurate sync position
                    bit_position_table[bit_index_100]   = gap_table[sample_index].position;
                    // update returned value
                    burst_parameters.CS100.valid     = true;
                    burst_parameters.CS100.pos       = (gap_table[first_to_process_CS3].position
                                                        + DEMOD_BUFFER_SIZE - (2 * CORREL_BIT_LENGTH_100)) % DEMOD_BUFFER_SIZE;
                    burst_parameters.CS100.CS_type   = CS3_ID;
                    // look for phase
                    burst_parameters.CS100.phase = burstPhase(CS3_ID, transition_edge);
                    //                   PACTOR_TNC_DEBUG << "\nCS3 (synchro.cpp)"<<burst_parameters.CS100.time_usecs/1000;
                }
                // reset new burst flag
                burst_parameters.CS100.new_burst   = false;

                // reset bit
                bit_index_100   = 0;
                // move starting point (last transition, burst header found)
                first_to_process_CS3  = (first_to_process_CS3 + BIT_COUNT_CS3_4 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
                // we lost 1 transition (wrong gap length) or we are finished. All previously processed gaps are dumped
                // we used 8 transitions
                sample_count_CS3    -= BIT_COUNT_CS3_4;
                break;
        }
        // analyse synchronization points if header was found
        if ((burst_parameters.CS100.valid == true)
            && (burst_parameters.CS100.CS_type == CS3_ID))
        {
            burst_parameters.CS100.fine_sync      = analyzeSynchro(CS3_ID, CORREL_BIT_LENGTH_100, bit_position_table, burst_parameters.CS100.pos);
#ifdef _DISPLAY_DEBUG_SYNC
            PACTOR_TNC_DEBUG << "FOUND CS3 at" << burst_parameters.CS100.pos
                             << "last input" << gap_table[last_transition_pos].position
                             << "fine sync" << burst_parameters.CS100.fine_sync
                             << "pos+sync" << gap_table[last_transition_pos].position
                             - (gap_table[last_transition_pos].position % 8) + burst_parameters.CS100.fine_sync;
#endif
            break;
        }
    }

    /**********************************************************************************************/
    /*                     CS4 processing                                               */
    /**********************************************************************************************/
    // available transitions count
    sample_count_CS4++;
    // bit index in header (0 to 7)
    bit_index_100       = 0;
    // CS1 Bd header
    while (sample_count_CS4 >= BIT_COUNT_CS3_4)
    {
        // index of transition processed during this iteration
        sample_index    = (first_to_process_CS4 + bit_index_100 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
#ifdef _DISPLAY_DEBUG_SYNC
        PACTOR_TNC_DEBUG << "identifyBurst: process bit" << bit_index_100 << "sample" << sample_index << "pos" << gap_table[sample_index].position
                         << "length" << gap_table[sample_index].length
                         << "sample_count_CS1" << sample_count_CS1
                         << "first_to_process_CS1" << first_to_process_CS1;
#endif
        switch (bit_index_100)
        {
                // first bit may be longer than a normal CS4 Bd bit
            case
                    0:
                // correct gap length: check next transition/bit
                if (gap_table[sample_index].length >= SHORT_GAP_2_100)
                {
#ifdef DISPLAY_SYNC_PROGRESS
                    progress_table[progress_idx++]    = bit_index_100 + '0';
#endif
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "BIT 0 at" << sample_index << "pos" << gap_table[sample_index].position
                                     << "gap" << gap_table[sample_index].length << "spl cnt" << sample_count_CS4;
#endif
                    // update position table used to compute accurate sync position
                    bit_position_table[bit_index_100]   = gap_table[sample_index].position;
                    // next bit
                    bit_index_100++;
                    // store first transition edge
                    transition_edge = gap_table[sample_index].lo_to_hi;
                    // compute first bit timestamp
                    samples_count   = (last_transition_sample_index - gap_table[sample_index].position + DEMOD_BUFFER_SIZE) % DEMOD_BUFFER_SIZE;
                    burst_parameters.CS100.time_msecs    = timeStampMsecs - (((samples_count - SHORT_GAP_2_100) * SAMPLE_DWNSPL_DURATION_US) / 1000);
                    burst_parameters.CS100.new_burst   = true;
                    burst_parameters.CS100.first_trans_index   = sample_index;
                }
                // wrong gap length: move starting point
                else
                {
                    // move starting point
                    first_to_process_CS4  = (first_to_process_CS4 + 1 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
                    // we used 1 transition
                    sample_count_CS4--;
                    // reset bit index because of error
                    bit_index_100         = 0;
                    // reset new burst flag
                    burst_parameters.CS100.new_burst   = false;
                }
                break;
                // other bits should be normal CS1 Bd bit
            case
                    1:
            case
                    2:
            case
                    3:
            case
                    4:
            case
                    5:
            case
                    6:
                // correct gap length: check next transition/bit
                if ((gap_table[sample_index].length >= burst_gap_short[CS4_ID][bit_index_100 - 1])
                    && (gap_table[sample_index].length <= burst_gap_long[CS4_ID][bit_index_100 - 1]))
                {
#ifdef DISPLAY_SYNC_PROGRESS
                    progress_table[progress_idx++]    = bit_index_100 + '0';
#endif
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "BIT" << bit_index_100 << "at" << sample_index << "pos" << gap_table[sample_index].position
                                     << "gap" << gap_table[sample_index].length << "spl cnt" << sample_count_CS4;
#endif
                    // update position table used to compute accurate sync position
                    bit_position_table[bit_index_100]   = gap_table[sample_index].position;
                    // next bit
                    bit_index_100++;
                }
                // wrong gap length: move starting point
                else
                {
                    // move starting point
                    first_to_process_CS4  = (first_to_process_CS4 + bit_index_100 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
                    // we used bit_index_100 + 1 transitions
                    sample_count_CS4    -= bit_index_100;
                    // reset bit index because of error
                    bit_index_100         = 0;
                    burst_parameters.CS100.new_burst   = false;
                }
                break;
                // last bit may be longer than a normal CS4 Bd bit
            case
                    7:
                // correct gap length: check next transition/bit
                if (gap_table[sample_index].length >= SHORT_GAP_2_100)
                {
#ifdef DISPLAY_SYNC_PROGRESS
                    progress_table[progress_idx++]    = bit_index_100 + '0';
#endif
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "CS4 BIT" << bit_index_100 << "at" << sample_index << "pos" << gap_table[sample_index].position
                                     << "gap" << gap_table[sample_index].length << "spl cnt" << sample_count_CS4;
#endif
                    // update position table used to compute accurate sync position
                    bit_position_table[bit_index_100]   = gap_table[sample_index].position;
                    // update returned value
                    burst_parameters.CS100.valid     = true;
                    burst_parameters.CS100.pos       = (gap_table[first_to_process_CS4].position
                                                        + DEMOD_BUFFER_SIZE - (2 * CORREL_BIT_LENGTH_100)) % DEMOD_BUFFER_SIZE;
                    burst_parameters.CS100.CS_type   = CS4_ID;
                    // look for phase
                    burst_parameters.CS100.phase = burstPhase(CS4_ID, transition_edge);
                    PACTOR_TNC_DEBUG << "\nCS4 (synchro.cpp)" << burst_parameters.CS100.time_msecs;
                }
                // reset new burst flag
                burst_parameters.CS100.new_burst   = false;

                // reset bit
                bit_index_100   = 0;
                // move starting point (last transition, burst header found)
                first_to_process_CS4  = (first_to_process_CS4 + BIT_COUNT_CS3_4 + GAP_TABLE_SIZE) % GAP_TABLE_SIZE;
                // we lost 1 transition (wrong gap length) or we are finished. All previously processed gaps are dumped
                // we used 8 transitions
                sample_count_CS4    -= BIT_COUNT_CS3_4;
                break;
        }
        // analyse synchronization points if header was found
        if ((burst_parameters.CS100.valid == true)
            && (burst_parameters.CS100.CS_type == CS4_ID))
        {
            burst_parameters.CS100.fine_sync      = analyzeSynchro(CS4_ID, CORREL_BIT_LENGTH_100, bit_position_table, burst_parameters.CS100.pos);
#ifdef _DISPLAY_DEBUG_SYNC
            PACTOR_TNC_DEBUG << "FOUND CS4 at" << burst_parameters.CS100.pos
                             << "last input" << gap_table[last_transition_pos].position
                             << "fine sync" << burst_parameters.CS100.fine_sync
                             << "pos+sync" << gap_table[last_transition_pos].position
                             - (gap_table[last_transition_pos].position % 8) + burst_parameters.CS100.fine_sync;
#endif
            break;
        }
    }
#ifdef DISPLAY_SYNC_PROGRESS
    if (progress_idx > 0)
    {
        progress_table[progress_idx++]    = 0;
        PACTOR_TNC_DEBUG << "progress" << progress_table;
    }
#endif

    //#ifdef SHOW_WINDOW
#if 1
    if (burst_parameters.hdr200.valid == true)
    {

        PACTOR_TNC_DEBUG << "\nidentified burst hdr200 at global_date_time_mSecs" << global_date_time_mSecs
                         << "start" << burst_parameters.hdr200.time_msecs
                         << "depuis win open" << burst_parameters.hdr200.time_msecs
                         - m_pactorTNC->m_pactoraudioengine->thread_info.next_Rx_time_ON;
    }
    else
        if (burst_parameters.hdr100.valid == true)
        {

            PACTOR_TNC_DEBUG << "\nidentified burst hdr100 at global_date_time_mSecs" << global_date_time_mSecs
                             << "start" << burst_parameters.hdr100.time_msecs
                             << "depuis win open" << burst_parameters.hdr100.time_msecs
                             - m_pactorTNC->m_pactoraudioengine->thread_info.next_Rx_time_ON;
        }
        else
            if (burst_parameters.CS100.valid == true)
            {

                PACTOR_TNC_DEBUG << "\nidentified burst CS"
                                 << burst_parameters.CS100.CS_type + 1
                                 << "at global_date_time_mSecs" << global_date_time_mSecs
                                 << "start" << burst_parameters.CS100.time_msecs
                                 << "depuis win open" << burst_parameters.CS100.time_msecs
                                 - m_pactorTNC->m_pactoraudioengine->thread_info.next_Rx_time_ON;
            }
#endif
}

/*------------------------------------------------------------------
implementation of function
arguments:
- data_buffer           demodulated and downsampled data
- first_sync_sample     first non-processed sample modulo DEMOD_BUFFER_SIZE
- last_sync_sample      last non-processed sample modulo DEMOD_BUFFER_SIZE

returns: none
------------------------------------------------------------------*/
bool PactorDemodThread::ZeroCrossing(t_phase_ampl data_buffer[],
                                     int begin_sync_sample,
                                     int last_sync_sample,
                                     int burst_processing_state,
                                     qint64 window_open, qint64 window_close,
                                     qint64 timeStampMsecs) // ms
{
    int     i;
    int     sample_index;
    int     sample_count;
    bool    new_phase_transition;
    bool    new_amplitude_transition;
    int     transition_pos;
    float   phase_sum;
    int     phase_count;
    bool    found_sync  = false;
    double  sample_timestamp;
    qint64  difftime;

    // initialize returned value
    if (burst_parameters.hdr200.new_burst == false)
    {
        burst_parameters.hdr200.valid     = false;
    }
    if (burst_parameters.hdr100.new_burst == false)
    {
        burst_parameters.hdr100.valid     = false;
    }
    if (burst_parameters.CS100.new_burst == false)
    {
        burst_parameters.CS100.valid      = false;
    }
    // compute average frequency and amplitude values
    sample_count    = (last_sync_sample + DEMOD_BUFFER_SIZE - begin_sync_sample) % DEMOD_BUFFER_SIZE;
    sample_index    = begin_sync_sample;
    phase_sum       = 0.0;
    phase_count     = 0;
#ifdef _DISPLAY_DEBUG_SYNC
    PACTOR_TNC_DEBUG << "FUNC call: sample_index" << sample_index << "sample_count" << sample_count;
#endif

    difftime    = 0;
    // add new samples, then compute
    for (i = 0; i < sample_count; i++)
    {
        // compute sample timestamp in microseconds
        sample_timestamp    = (timeStampMsecs * 1000) - (sample_count * SAMPLE_DWNSPL_DURATION_US);
        old_phase_value             = new_phase_value;
        old_fifo_amplitude_sum      = fifo_amplitude_sum;
        // remove oldest sample in fifo
        fifo_amplitude_sum          -= samples_fifo[fifo_index].amplitude;
        // add new sample to fifo
        new_phase_value             = data_buffer[sample_index].phase;

        //compute sample count in-between high and low thresholds
        if ((old_phase_value > MAX_THRESHOLD)
            && (new_phase_value < MAX_THRESHOLD))
        {
            samples_since_last_high     = 0;
        }
        else
            if (new_phase_value < MAX_THRESHOLD)
            {
                samples_since_last_high++;
            }

        if ((old_phase_value < MIN_THRESHOLD)
            && (new_phase_value > MIN_THRESHOLD))
        {
            samples_since_last_low     = 0;
        }
        else
            if (new_phase_value > MIN_THRESHOLD)
            {
                samples_since_last_low++;
            }

        fifo_amplitude_sum          += data_buffer[sample_index].amplitude;
        samples_fifo[fifo_index].amplitude  = data_buffer[sample_index].amplitude;
        // increment fifo index
        fifo_index  = (fifo_index + 1) % ZERO_CROSS_FIFO_DEPTH;

        // zero crossing decision
        new_phase_transition        = false;
        new_amplitude_transition    = false;

        // ramp-up
        int old_gap_length;
        if ((old_fifo_amplitude_sum < SQUELCH_LEVEL) && (fifo_amplitude_sum > SQUELCH_LEVEL))
        {
            last_amplitude_transition       = LO_TO_HI;
            last_phase_transition           = NO_TRANSITION;
            new_amplitude_transition        = true;
            burst_length                    = 0;
            active_burst                    = true;
            samples_since_last_high         = 0;
            samples_since_last_low          = 0;
            // re-init FIFO
            old_phase_value                 = 0.0;
            // increment fifo index
            fifo_index  = (fifo_index + 1) % ZERO_CROSS_FIFO_DEPTH;
            // count valid crossings
            valid_crossings                 = 0;
            crossing_analyse_state          = STARTING;
            transition_pos                  = sample_index;
            // reset gap length when a new transition occurs
#ifdef _DISPLAY_DEBUG_SYNC
            PACTOR_TNC_DEBUG << "ramp-up gap length reset. was" << gap_length;
#endif
            old_gap_length                  = gap_length;
            gap_length                      = 0;
            // call identifyBurst() to signal a new burst
            gap_table_index = (gap_table_index + 1) % GAP_TABLE_SIZE;
            gap_length_table[gap_table_index].length    = -1;
            gap_length_table[gap_table_index].position  = transition_pos;
            gap_length_table[gap_table_index].lo_to_hi  = last_phase_transition;
            // if not looking for a new burst, just update zero crossings table
            if (burst_processing_state == CORR_SEARCH_SYNC)
            {
                // call burst identifier
                identifyBurst(gap_length_table, gap_table_index, (qint64)(sample_timestamp / 1000.0));
                // keep last good start of burst
                if (burst_parameters.hdr100.valid)
                {
                    difftime    = burst_parameters.hdr100.time_msecs
                                  + m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time
                                  - window_open;
                    difftime    = difftime % m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time;
                    burst_parameters.hdr100.time_from_winstart  = difftime;
                    if ((difftime > PACKET_WINDOW_MARGIN_MS)
                        && (window_open != 0))
                    {
                        burst_parameters.hdr100.valid   = false;
#ifdef DISPLAY_DEBUG_SYNC
                        PACTOR_TNC_DEBUG << "!!!!!!!!!!!1 invalid hdr100 time" << burst_parameters.hdr100.time_msecs
                                         << "margin" << burst_parameters.hdr100.time_from_winstart
                                         << "open" << window_open;
#endif
                    }
                    else
                    {
                        found_sync      = true;
#ifdef DISPLAY_DEBUG_SYNC
                        PACTOR_TNC_DEBUG << "!!!!!!!!!!!1 found hdr100 time" << burst_parameters.hdr100.time_msecs
                                         << "margin" << burst_parameters.hdr100.time_from_winstart
                                         << "global time" << global_date_time_mSecs;
#endif
                    }
                }
                if (burst_parameters.hdr200.valid)
                {
                    difftime    = burst_parameters.hdr200.time_msecs
                                  + m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time
                                  - window_open;
                    difftime    = difftime % m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time;
                    burst_parameters.hdr200.time_from_winstart  = difftime;
                    if ((difftime > PACKET_WINDOW_MARGIN_MS)
                        && (window_open != 0))
                    {
                        burst_parameters.hdr200.valid   = false;
#ifdef DISPLAY_DEBUG_SYNC
                        PACTOR_TNC_DEBUG << "!!!!!!!!!!!1 invalid hdr200 time" << burst_parameters.hdr200.time_msecs
                                         << "margin" << burst_parameters.hdr200.time_from_winstart
                                         << "open" << window_open;
#endif
                    }
                    else
                    {
                        found_sync      = true;
#ifdef DISPLAY_DEBUG_SYNC
                        PACTOR_TNC_DEBUG << "!!!!!!!!!!!1 found hdr200 time" << burst_parameters.hdr200.time_msecs
                                         << "margin" << burst_parameters.hdr200.time_from_winstart
                                         << "global time" << global_date_time_mSecs;
#endif
                    }
                }
                if (burst_parameters.CS100.valid)
                {
                    difftime    = burst_parameters.CS100.time_msecs
                                  + m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time
                                  - window_open;
                    difftime    = difftime % m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time;
                    burst_parameters.CS100.time_from_winstart  = difftime;
                    if ((difftime > PACKET_WINDOW_MARGIN_MS)
                        && (window_open != 0))
                    {
                        burst_parameters.CS100.valid   = false;
#ifdef DISPLAY_DEBUG_SYNC
                        PACTOR_TNC_DEBUG << "!!!!!!!!!!!1 invalid CS100 time" << burst_parameters.CS100.time_msecs
                                         << "margin" << burst_parameters.CS100.time_from_winstart
                                         << "open" << window_open;
#endif
                    }
                    else
                    {
                        found_sync      = true;
#ifdef DISPLAY_DEBUG_SYNC
                        PACTOR_TNC_DEBUG << "!!!!!!!!!!!1 found CS100 time" << burst_parameters.CS100.time_msecs
                                         << "margin" << burst_parameters.CS100.time_from_winstart
                                         << "global time" << global_date_time_mSecs;
#endif
                    }
                }
                if (found_sync == true)
                {
                    burst_parameters.timestamp   = global_date_time_mSecs;
                }
            }
        }
        else
            // ramp down
            if ((old_fifo_amplitude_sum > SQUELCH_LEVEL) && (fifo_amplitude_sum < SQUELCH_LEVEL))
            {
#ifdef _DISPLAY_DEBUG_SYNC
                PACTOR_TNC_DEBUG << "ramp down amplitude" << fifo_amplitude_sum << "gap" << gap_length;
#endif
                last_amplitude_transition   = HI_TO_LO;
                last_phase_transition       = NO_TRANSITION;
                new_amplitude_transition    = true;
                new_phase_transition        = false;
                active_burst                = false;
                transition_pos              = sample_index;
            }
        // phase transition during a burst or at burst start?
        if (active_burst)
        {
            // accumulate phase if amplitude is not 0
            phase_sum                   += new_phase_value;
            phase_count++;

            // phase transition from low to high
            if (new_phase_value > MAX_THRESHOLD)
            {
                // transition at start of burst
                if (last_phase_transition == NO_TRANSITION)
                {
                    last_phase_transition       = LO_TO_HI;
                    new_phase_transition        = true;
                    transition_pos              = sample_index;
                    samples_since_last_low      = 0;
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "PHASE start of burst LO_TO_HI at" << transition_pos;
#endif
                }
                else
                    // normal transition during burst
                    if ((samples_since_last_low < 5)
                        && (last_phase_transition != LO_TO_HI))
                    {
                        last_phase_transition     = LO_TO_HI;
                        new_phase_transition      = true;
                        transition_pos  = (sample_index + DEMOD_BUFFER_SIZE - (samples_since_last_low / 2)) % DEMOD_BUFFER_SIZE;
#ifdef _DISPLAY_DEBUG_SYNC
                        PACTOR_TNC_DEBUG << "PHASE during burst LO_TO_HI at" << transition_pos;
#endif
                    }
            }
            else
                // phase transition from high to low
                if (new_phase_value < MIN_THRESHOLD)
                {
                    // transition at start of burst
                    if (last_phase_transition == NO_TRANSITION)
                    {
                        last_phase_transition       = HI_TO_LO;
                        new_phase_transition        = true;
                        transition_pos              = sample_index;
                        samples_since_last_high     = 0;
#ifdef _DISPLAY_DEBUG_SYNC
                        PACTOR_TNC_DEBUG << "PHASE start of burst HI_TO_LO at" << transition_pos;
#endif
                    }
                    else
                        // normal transition during burst
                        if ((samples_since_last_high < 5)
                            && (last_phase_transition != HI_TO_LO))
                        {
                            last_phase_transition     = HI_TO_LO;
                            new_phase_transition      = true;
                            transition_pos  = (sample_index + DEMOD_BUFFER_SIZE - (samples_since_last_high / 2)) % DEMOD_BUFFER_SIZE;
#ifdef _DISPLAY_DEBUG_SYNC
                            PACTOR_TNC_DEBUG << "PHASE during burst HI_TO_LO at" << transition_pos;
#endif
                        }
                }
        }
        // a transition occured
        if ((new_phase_transition == true) || (new_amplitude_transition == true))
        {
            if (new_phase_transition == true)
            {
#ifdef _DISPLAY_DEBUG_SYNC
                PACTOR_TNC_DEBUG << "PHASE trans at" << sample_index
                                 << "real pos" << transition_pos
                                 << "last_phase_transition" << msg_trans[last_phase_transition]
                                 << "gap_length" << gap_length;
#endif
                // is the gap valid ?
                if (gap_length >= (CORREL_BIT_LENGTH_200 - GAP_200_MARGIN))
                    // gap too short: reject
                {
                    gap_table_index = (gap_table_index + 1) % GAP_TABLE_SIZE;
                    gap_length_table[gap_table_index].length    = gap_length;
                    gap_length_table[gap_table_index].position  = transition_pos;
                    gap_length_table[gap_table_index].lo_to_hi  = last_phase_transition;
                    // if not looking for a new burst, just update zero crossings table
                    if ((burst_processing_state == CORR_SEARCH_SYNC)
                        && (found_sync == false))
                    {
                        // call burst identifier
                        identifyBurst(gap_length_table, gap_table_index, (qint64)(sample_timestamp / 1000.0));
                        // keep last good start of burst
                        if (burst_parameters.hdr100.valid)
                        {
                            difftime    = burst_parameters.hdr100.time_msecs
                                          + m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time
                                          - window_open;
                            difftime    = difftime % m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time;
                            burst_parameters.hdr100.time_from_winstart  = difftime;
                            if ((difftime > PACKET_WINDOW_MARGIN_MS)
                                && (window_open != 0))
                            {
                                burst_parameters.hdr100.valid   = false;
#ifdef DISPLAY_DEBUG_SYNC
                                PACTOR_TNC_DEBUG << "!!!!!!!!!!!2 invalid hdr100 time" << burst_parameters.hdr100.time_msecs
                                                 << "margin" << burst_parameters.hdr100.time_from_winstart
                                                 << "open" << window_open;
#endif
                            }
                            else
                            {
                                found_sync      = true;
#ifdef DISPLAY_DEBUG_SYNC
                                PACTOR_TNC_DEBUG << "!!!!!!!!!!!2 found hdr100 time" << burst_parameters.hdr100.time_msecs
                                                 << "margin" << burst_parameters.hdr100.time_from_winstart
                                                 << "global time" << global_date_time_mSecs;
#endif
                            }
                        }
                        if (burst_parameters.hdr200.valid)
                        {
                            difftime    = burst_parameters.hdr200.time_msecs
                                          + m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time
                                          - window_open;
                            difftime    = difftime % m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time;
                            burst_parameters.hdr200.time_from_winstart  = difftime;
                            if ((difftime > PACKET_WINDOW_MARGIN_MS)
                                && (window_open != 0))
                            {
                                burst_parameters.hdr200.valid   = false;
#ifdef DISPLAY_DEBUG_SYNC
                                PACTOR_TNC_DEBUG << "!!!!!!!!!!!2 invalid hdr200 time" << burst_parameters.hdr200.time_msecs
                                                 << "margin" << burst_parameters.hdr200.time_from_winstart
                                                 << "open" << window_open
                                                 << "cycle" << m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time;
#endif
                            }
                            else
                            {
                                found_sync      = true;
#ifdef DISPLAY_DEBUG_SYNC
                                PACTOR_TNC_DEBUG << "!!!!!!!!!!!2 found hdr200 time" << burst_parameters.hdr200.time_msecs
                                                 << "margin" << burst_parameters.hdr200.time_from_winstart
                                                 << "global time" << global_date_time_mSecs;
#endif
                            }
                        }
                        if (burst_parameters.CS100.valid)
                        {
                            difftime    = burst_parameters.CS100.time_msecs
                                          + m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time
                                          - window_open;
                            difftime    = difftime % m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time;
                            burst_parameters.CS100.time_from_winstart  = difftime;
                            if ((difftime > PACKET_WINDOW_MARGIN_MS)
                                && (window_open != 0))
                            {
                                burst_parameters.CS100.valid   = false;
#ifdef DISPLAY_DEBUG_SYNC
                                PACTOR_TNC_DEBUG << "!!!!!!!!!!!2 invalid CS100 time" << burst_parameters.CS100.time_msecs
                                                 << "margin" << burst_parameters.CS100.time_from_winstart
                                                 << "open" << window_open;
#endif
                            }
                            else
                            {
                                found_sync      = true;
#ifdef DISPLAY_DEBUG_SYNC
                                PACTOR_TNC_DEBUG << "!!!!!!!!!!!2 found CS100 time" << burst_parameters.CS100.time_msecs
                                                 << "margin" << burst_parameters.CS100.time_from_winstart
                                                 << "global time" << global_date_time_mSecs;
#endif
                            }
                        }
                        if (found_sync == true)
                        {
                            burst_parameters.timestamp   = global_date_time_mSecs;
                        }
                    }
                }
                // reset gap length when a new transition occurs
#ifdef _DISPLAY_DEBUG_SYNC
                PACTOR_TNC_DEBUG << "gap length reset. was" << gap_length;
#endif
                gap_length              = 0;
            }
            // amplitude transition
            if (new_amplitude_transition == true)
            {
                if (last_amplitude_transition == LO_TO_HI)
                {
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "++++++++ AMPL trans at" << transition_pos
                                     << "gap_length [[[[" << old_gap_length << "]]]]"
                                     << "Time:" << global_date_time_mSecs;
#endif
                }
                else
                {
#ifdef _DISPLAY_DEBUG_SYNC
                    PACTOR_TNC_DEBUG << "-------- AMPL trans at" << transition_pos
                                     << "gap_length" << gap_length
                                     << "burst_length ((((" << burst_length << "))))"
                                     << "Time:" << global_date_time_mSecs;
#endif
                    gap_table_index = (gap_table_index + 1) % GAP_TABLE_SIZE;
                    gap_length_table[gap_table_index].length    = gap_length;
                    gap_length_table[gap_table_index].position  = transition_pos;
                    // invert phase transition
                    if (last_phase_transition == LO_TO_HI)
                    {
                        last_phase_transition       = HI_TO_LO;
                    }
                    else
                    {
                        last_phase_transition       = LO_TO_HI;
                    }
                    gap_length_table[gap_table_index].lo_to_hi  = last_phase_transition;
                    // if not looking for a new burst, just update zero crossings table
                    if ((burst_processing_state == CORR_SEARCH_SYNC)
                        && (found_sync == false))
                    {
                        // call burst identifier
                        identifyBurst(gap_length_table, gap_table_index, (qint64)(sample_timestamp / 1000.0));
                        // keep last good start of burst
                        if (burst_parameters.hdr100.valid)
                        {
                            difftime    = burst_parameters.hdr100.time_msecs
                                          + m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time
                                          - window_open;
                            difftime    = difftime % m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time;
                            burst_parameters.hdr100.time_from_winstart  = difftime;
                            if ((difftime > PACKET_WINDOW_MARGIN_MS)
                                && (window_open != 0))
                            {
                                burst_parameters.hdr100.valid   = false;
#ifdef DISPLAY_DEBUG_SYNC
                                PACTOR_TNC_DEBUG << "!!!!!!!!!!!3 invalid hdr100 time" << burst_parameters.hdr100.time_msecs
                                                 << "margin" << burst_parameters.hdr100.time_from_winstart
                                                 << "open" << window_open;
#endif
                            }
                            else
                            {
                                found_sync      = true;
#ifdef DISPLAY_DEBUG_SYNC
                                PACTOR_TNC_DEBUG << "!!!!!!!!!!!3 found hdr100 time" << burst_parameters.hdr100.time_msecs
                                                 << "margin" << burst_parameters.hdr100.time_from_winstart
                                                 << "open" << window_open;
#endif
                            }
                        }
                        if (burst_parameters.hdr200.valid)
                        {
                            difftime    = burst_parameters.hdr200.time_msecs
                                          + m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time
                                          - window_open;
                            difftime    = difftime % m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time;
                            burst_parameters.hdr200.time_from_winstart  = difftime;
                            if ((difftime > PACKET_WINDOW_MARGIN_MS)
                                && (window_open != 0))
                            {
                                burst_parameters.hdr200.valid   = false;
#ifdef DISPLAY_DEBUG_SYNC
                                PACTOR_TNC_DEBUG << "!!!!!!!!!!!3 invalid hdr200 time" << burst_parameters.hdr200.time_msecs
                                                 << "margin" << burst_parameters.hdr200.time_from_winstart
                                                 << "open" << window_open;
#endif
                            }
                            else
                            {
                                found_sync      = true;
#ifdef DISPLAY_DEBUG_SYNC
                                PACTOR_TNC_DEBUG << "!!!!!!!!!!!3 found hdr200 time" << burst_parameters.hdr200.time_msecs
                                                 << "margin" << burst_parameters.hdr200.time_from_winstart
                                                 << "open" << window_open;
#endif
                            }
                        }
                        if (burst_parameters.CS100.valid)
                        {
                            difftime    = burst_parameters.CS100.time_msecs
                                          + m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time
                                          - window_open;
                            difftime    = difftime % m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time;
                            burst_parameters.CS100.time_from_winstart  = difftime;
                            if ((difftime > PACKET_WINDOW_MARGIN_MS)
                                && (window_open != 0))
                            {
                                burst_parameters.CS100.valid   = false;
#ifdef DISPLAY_DEBUG_SYNC
                                PACTOR_TNC_DEBUG << "!!!!!!!!!!!3 invalid CS100 time" << burst_parameters.CS100.time_msecs
                                                 << "margin" << burst_parameters.CS100.time_from_winstart
                                                 << "open" << window_open;
#endif
                            }
                            else
                            {
                                found_sync      = true;
#ifdef DISPLAY_DEBUG_SYNC
                                PACTOR_TNC_DEBUG << "!!!!!!!!!!!3 found CS100 time" << burst_parameters.CS100.time_msecs
                                                 << "margin" << burst_parameters.CS100.time_from_winstart
                                                 << "open" << window_open;
#endif
                            }
                        }
                        if (found_sync == true)
                        {
                            burst_parameters.timestamp   = global_date_time_mSecs;
                        }
                    }
                }
            }
        }

        // increment gap length
        gap_length++;
        if (active_burst)
        {
            burst_length++;
#ifdef _DISPLAY_DEBUG_SYNC
            PACTOR_TNC_DEBUG << "GL" << gap_length << "BL" << burst_length << "amplitude" << fifo_amplitude_sum
                             << "index" << sample_index << "i=" << i << "active" << active_burst;
#endif
        }
        // increment sample index
        sample_index    = (sample_index + 1) % DEMOD_BUFFER_SIZE;
        // compute next sample timestamp
        sample_timestamp    = sample_timestamp + (double)SAMPLE_DWNSPL_DURATION_US;
    }
    // store average value
    if (phase_count > 0)
    {
        burst_parameters.average_value   = phase_sum / (float)phase_count;
    }
    else
    {
        burst_parameters.average_value   = 0.0;
    }

    // difftime too long, reset window
    if (difftime > WINDOW_TIMEOUT)
    {
        m_pactorTNC->m_pactoraudioengine->thread_info.window_state      = e_PT_rcv_win_not_init;
        m_pactorTNC->m_pactoraudioengine->thread_info.next_Rx_time_ON   = global_date_time_mSecs;
    }
    // return burst identification and position (if available)
    return found_sync;
}

/*------------------------------------------------------------------
implementation of function
arguments:
- hdr_CS_found          type of burst 100 or 200 Bd
- burst_parameters      all burst parameters
- gap_length_table      fsk transitions table

returns: bit duration in samples, floating point
------------------------------------------------------------------*/
#define MARGIN_SAMPLE_COUNT     100
#define MARGIN_BIT_COUNT        4

void PactorDemodThread::recoverBitFromTransitions(int hdr_CS_found, t_burst_parameters &burst_parameters, t_gap_table_elmt *gap_length_table, bool decode_header)
{
    int     burst_length;
    int     gap_table_index;
    bool    started    = false;
    // compute limits
    int     medium_length;
    int     first_gap_length;
    char    *demod_bit_buffer;
    int     max_burst_bit_count;
    int     sample_count;
    int     bit_index;
    bool    bit_positive;
    int     rounded_sample_count;
    int     rounded_chunk_count;
    int     max_sample_count;
    int     max_bit_count;
#ifdef  RECOVER_BIT_DEBUG
    int     hrd_speed;
    QString debug_string;

    debug_string.clear();
#endif

    // compute first gap length which depends on packet detected speed
    if (hdr_CS_found == BURST_TYPE_HDR100)
    {
        first_gap_length   = 16;
    }
    else
    {
        first_gap_length   = 8;
    }

    // first iteration: decoding 100 Bd (real 100 Bd + 200 Bd QRT packets)
    // second iteration: decoding 200 Bd (real 200 Bd + 100 Bd connection packets)
    for (int iter = 0; iter < 2; iter++)
    {
        burst_length            = 0;
        // set average bit length which depends on decoding packet speed
        if (iter == 0)
            // decoding speed 100 Bd
        {
            if (decode_header)
            {
                max_bit_count       = CORREL_BITS_CS + MARGIN_BIT_COUNT;
                max_sample_count    = max_bit_count * CORREL_BIT_LENGTH_100;
            }
            else
            {
                max_bit_count       = LONG_BURST_BIT_COUNT_100 + MARGIN_BIT_COUNT;
                max_sample_count    = LONG_BURST_SAMPLE_COUNT + MARGIN_SAMPLE_COUNT;
            }
            demod_bit_buffer    = demod_bit_buffer_100;
            medium_length       = 16;
            max_burst_bit_count = max_bit_count;
        }
        else
            // decoding speed 200 Bd
        {
            if (decode_header)
            {
                max_bit_count       = CORREL_BITS_HDR + MARGIN_BIT_COUNT;
                max_sample_count    = max_bit_count * CORREL_BIT_LENGTH_200;
            }
            else
            {
                max_bit_count       = LONG_BURST_BIT_COUNT_200 + MARGIN_BIT_COUNT;
                max_sample_count    = LONG_BURST_SAMPLE_COUNT + MARGIN_SAMPLE_COUNT;
            }
            demod_bit_buffer    = demod_bit_buffer_200;
            medium_length       = 8;
            max_burst_bit_count = max_bit_count;
        }
#ifdef  RECOVER_BIT_DEBUG
        qDebug() << "first gap index" << burst_parameters.first_trans_index << "last gap index" << burst_parameters.last_trans_index;
#endif

#ifdef  RECOVER_BIT_DEBUG
        for (int j = 0; j < max_burst_bit_count; j++)
        {
            demod_bit_buffer[j]  = '.';
        }
#endif
        // integrate and dump between transitions and store in a bit buffer
        bit_index           = 0;
        bit_positive        = true;
        started             = false;
        // analyse table
        gap_table_index     = burst_parameters.first_trans_index;
        do
        {
            // add transition length to burst length
            if (started == false)
            {
                sample_count        = first_gap_length;
                started             = true;
            }
            else
            {
                sample_count        = gap_length_table[gap_table_index].length;
            }
            // dump integrated samples
            rounded_chunk_count     = (sample_count + (medium_length / 2)) / medium_length;
            rounded_sample_count    = rounded_chunk_count * medium_length;
            burst_length            += rounded_sample_count;
#ifdef  RECOVER_BIT_DEBUG
            debug_string.append(QString("(%1 (%2): ").arg(sample_count).arg(rounded_chunk_count));
#endif
            for (int j = 0; j < rounded_chunk_count; j++)
            {
#ifdef  RECOVER_BIT_DEBUG
                debug_string.append(QString("%1,").arg(j));
#endif
                if (bit_positive)
                {
#ifdef  RECOVER_BIT_DEBUG
                    demod_bit_buffer[bit_index]  = '1';
#else
                    demod_bit_buffer[bit_index]  = 1;
#endif
                }
                else
                {
#ifdef  RECOVER_BIT_DEBUG
                    demod_bit_buffer[bit_index]  = '0';
#else
                    demod_bit_buffer[bit_index]  = 0;
#endif
                }
                bit_index++;
            }
            if (bit_positive)
            {
                bit_positive    = false;
            }
            else
            {
                bit_positive    = true;
            }
#ifdef  RECOVER_BIT_DEBUG
            debug_string.append("), ");
#endif
            // increment gap table index
            gap_table_index     = (gap_table_index + 1) % GAP_TABLE_SIZE;
        }
        while ((gap_table_index != burst_parameters.last_trans_index)
               && (burst_length <= max_sample_count));
#ifdef  RECOVER_BIT_DEBUG
        qDebug() << "stopped at length" << burst_length << "gap index" << gap_table_index;
        //        qDebug()<<"gaps"<<debug_string;
        demod_bit_buffer[bit_index]  = '#';
        if (iter == 0)
            // decoding speed 100 Bd
        {
            hrd_speed   = BURST_TYPE_HDR100;
        }
        else
            // decoding speed 200 Bd
        {
            hrd_speed   = BURST_TYPE_HDR200;
        }
        displayRecoverDebug(demod_bit_buffer, hrd_speed, bit_index);
#endif
    }
}

#ifdef  RECOVER_BIT_DEBUG
void PactorDemodThread::displayRecoverDebug(char *in_demod_bit_buffer, int hdr_CS_found, int last_bit_index)
{
    int bit_index    = 0;
    char            demod_bit_buffer[LONG_BURST_BIT_COUNT_200 + 16];
    for (int i = 0; i < LONG_BURST_BIT_COUNT_200 + 16; i++)
    {
        demod_bit_buffer[i] = in_demod_bit_buffer[i];
    }

    qDebug() << "bit buffer";
    qDebug() << demod_bit_buffer[bit_index]
             << demod_bit_buffer[bit_index + 1]
             << demod_bit_buffer[bit_index + 2]
             << demod_bit_buffer[bit_index + 3] << ""
             << demod_bit_buffer[bit_index + 4]
             << demod_bit_buffer[bit_index + 5]
             << demod_bit_buffer[bit_index + 6]
             << demod_bit_buffer[bit_index + 7] << ""
             << demod_bit_buffer[bit_index + 8]
             << demod_bit_buffer[bit_index + 9]
             << demod_bit_buffer[bit_index + 10]
             << demod_bit_buffer[bit_index + 11] << ""
             << demod_bit_buffer[bit_index + 12]
             << demod_bit_buffer[bit_index + 13]
             << demod_bit_buffer[bit_index + 14]
             << demod_bit_buffer[bit_index + 15] << ""
             << demod_bit_buffer[bit_index + 16]
             << demod_bit_buffer[bit_index + 17]
             << demod_bit_buffer[bit_index + 18]
             << demod_bit_buffer[bit_index + 19] << ""
             << demod_bit_buffer[bit_index + 20]
             << demod_bit_buffer[bit_index + 21]
             << demod_bit_buffer[bit_index + 22]
             << demod_bit_buffer[bit_index + 23];
    bit_index += 24;
    qDebug() << demod_bit_buffer[bit_index]
             << demod_bit_buffer[bit_index + 1]
             << demod_bit_buffer[bit_index + 2]
             << demod_bit_buffer[bit_index + 3] << ""
             << demod_bit_buffer[bit_index + 4]
             << demod_bit_buffer[bit_index + 5]
             << demod_bit_buffer[bit_index + 6]
             << demod_bit_buffer[bit_index + 7] << ""
             << demod_bit_buffer[bit_index + 8]
             << demod_bit_buffer[bit_index + 9]
             << demod_bit_buffer[bit_index + 10]
             << demod_bit_buffer[bit_index + 11] << ""
             << demod_bit_buffer[bit_index + 12]
             << demod_bit_buffer[bit_index + 13]
             << demod_bit_buffer[bit_index + 14]
             << demod_bit_buffer[bit_index + 15] << ""
             << demod_bit_buffer[bit_index + 16]
             << demod_bit_buffer[bit_index + 17]
             << demod_bit_buffer[bit_index + 18]
             << demod_bit_buffer[bit_index + 19] << ""
             << demod_bit_buffer[bit_index + 20]
             << demod_bit_buffer[bit_index + 21]
             << demod_bit_buffer[bit_index + 22]
             << demod_bit_buffer[bit_index + 23];
    bit_index += 24;
    qDebug() << demod_bit_buffer[bit_index]
             << demod_bit_buffer[bit_index + 1]
             << demod_bit_buffer[bit_index + 2]
             << demod_bit_buffer[bit_index + 3] << ""
             << demod_bit_buffer[bit_index + 4]
             << demod_bit_buffer[bit_index + 5]
             << demod_bit_buffer[bit_index + 6]
             << demod_bit_buffer[bit_index + 7] << ""
             << demod_bit_buffer[bit_index + 8]
             << demod_bit_buffer[bit_index + 9]
             << demod_bit_buffer[bit_index + 10]
             << demod_bit_buffer[bit_index + 11] << ""
             << demod_bit_buffer[bit_index + 12]
             << demod_bit_buffer[bit_index + 13]
             << demod_bit_buffer[bit_index + 14]
             << demod_bit_buffer[bit_index + 15] << ""
             << demod_bit_buffer[bit_index + 16]
             << demod_bit_buffer[bit_index + 17]
             << demod_bit_buffer[bit_index + 18]
             << demod_bit_buffer[bit_index + 19] << ""
             << demod_bit_buffer[bit_index + 20]
             << demod_bit_buffer[bit_index + 21]
             << demod_bit_buffer[bit_index + 22]
             << demod_bit_buffer[bit_index + 23];
    bit_index += 24;
    qDebug() << demod_bit_buffer[bit_index]
             << demod_bit_buffer[bit_index + 1]
             << demod_bit_buffer[bit_index + 2]
             << demod_bit_buffer[bit_index + 3] << ""
             << demod_bit_buffer[bit_index + 4]
             << demod_bit_buffer[bit_index + 5]
             << demod_bit_buffer[bit_index + 6]
             << demod_bit_buffer[bit_index + 7] << ""
             << demod_bit_buffer[bit_index + 8]
             << demod_bit_buffer[bit_index + 9]
             << demod_bit_buffer[bit_index + 10]
             << demod_bit_buffer[bit_index + 11] << ""
             << demod_bit_buffer[bit_index + 12]
             << demod_bit_buffer[bit_index + 13]
             << demod_bit_buffer[bit_index + 14]
             << demod_bit_buffer[bit_index + 15] << ""
             << demod_bit_buffer[bit_index + 16]
             << demod_bit_buffer[bit_index + 17]
             << demod_bit_buffer[bit_index + 18]
             << demod_bit_buffer[bit_index + 19] << ""
             << demod_bit_buffer[bit_index + 20]
             << demod_bit_buffer[bit_index + 21]
             << demod_bit_buffer[bit_index + 22]
             << demod_bit_buffer[bit_index + 23];
    if (hdr_CS_found == BURST_TYPE_HDR200)
    {
        bit_index += 24;
        qDebug() << demod_bit_buffer[bit_index]
                 << demod_bit_buffer[bit_index + 1]
                 << demod_bit_buffer[bit_index + 2]
                 << demod_bit_buffer[bit_index + 3] << ""
                 << demod_bit_buffer[bit_index + 4]
                 << demod_bit_buffer[bit_index + 5]
                 << demod_bit_buffer[bit_index + 6]
                 << demod_bit_buffer[bit_index + 7] << ""
                 << demod_bit_buffer[bit_index + 8]
                 << demod_bit_buffer[bit_index + 9]
                 << demod_bit_buffer[bit_index + 10]
                 << demod_bit_buffer[bit_index + 11] << ""
                 << demod_bit_buffer[bit_index + 12]
                 << demod_bit_buffer[bit_index + 13]
                 << demod_bit_buffer[bit_index + 14]
                 << demod_bit_buffer[bit_index + 15] << ""
                 << demod_bit_buffer[bit_index + 16]
                 << demod_bit_buffer[bit_index + 17]
                 << demod_bit_buffer[bit_index + 18]
                 << demod_bit_buffer[bit_index + 19] << ""
                 << demod_bit_buffer[bit_index + 20]
                 << demod_bit_buffer[bit_index + 21]
                 << demod_bit_buffer[bit_index + 22]
                 << demod_bit_buffer[bit_index + 23];
        bit_index += 24;
        qDebug() << demod_bit_buffer[bit_index]
                 << demod_bit_buffer[bit_index + 1]
                 << demod_bit_buffer[bit_index + 2]
                 << demod_bit_buffer[bit_index + 3] << ""
                 << demod_bit_buffer[bit_index + 4]
                 << demod_bit_buffer[bit_index + 5]
                 << demod_bit_buffer[bit_index + 6]
                 << demod_bit_buffer[bit_index + 7] << ""
                 << demod_bit_buffer[bit_index + 8]
                 << demod_bit_buffer[bit_index + 9]
                 << demod_bit_buffer[bit_index + 10]
                 << demod_bit_buffer[bit_index + 11] << ""
                 << demod_bit_buffer[bit_index + 12]
                 << demod_bit_buffer[bit_index + 13]
                 << demod_bit_buffer[bit_index + 14]
                 << demod_bit_buffer[bit_index + 15] << ""
                 << demod_bit_buffer[bit_index + 16]
                 << demod_bit_buffer[bit_index + 17]
                 << demod_bit_buffer[bit_index + 18]
                 << demod_bit_buffer[bit_index + 19] << ""
                 << demod_bit_buffer[bit_index + 20]
                 << demod_bit_buffer[bit_index + 21]
                 << demod_bit_buffer[bit_index + 22]
                 << demod_bit_buffer[bit_index + 23];
        bit_index += 24;
        qDebug() << demod_bit_buffer[bit_index]
                 << demod_bit_buffer[bit_index + 1]
                 << demod_bit_buffer[bit_index + 2]
                 << demod_bit_buffer[bit_index + 3] << ""
                 << demod_bit_buffer[bit_index + 4]
                 << demod_bit_buffer[bit_index + 5]
                 << demod_bit_buffer[bit_index + 6]
                 << demod_bit_buffer[bit_index + 7] << ""
                 << demod_bit_buffer[bit_index + 8]
                 << demod_bit_buffer[bit_index + 9]
                 << demod_bit_buffer[bit_index + 10]
                 << demod_bit_buffer[bit_index + 11] << ""
                 << demod_bit_buffer[bit_index + 12]
                 << demod_bit_buffer[bit_index + 13]
                 << demod_bit_buffer[bit_index + 14]
                 << demod_bit_buffer[bit_index + 15] << ""
                 << demod_bit_buffer[bit_index + 16]
                 << demod_bit_buffer[bit_index + 17]
                 << demod_bit_buffer[bit_index + 18]
                 << demod_bit_buffer[bit_index + 19] << ""
                 << demod_bit_buffer[bit_index + 20]
                 << demod_bit_buffer[bit_index + 21]
                 << demod_bit_buffer[bit_index + 22]
                 << demod_bit_buffer[bit_index + 23];
        bit_index += 24;
        qDebug() << demod_bit_buffer[bit_index]
                 << demod_bit_buffer[bit_index + 1]
                 << demod_bit_buffer[bit_index + 2]
                 << demod_bit_buffer[bit_index + 3] << ""
                 << demod_bit_buffer[bit_index + 4]
                 << demod_bit_buffer[bit_index + 5]
                 << demod_bit_buffer[bit_index + 6]
                 << demod_bit_buffer[bit_index + 7] << ""
                 << demod_bit_buffer[bit_index + 8]
                 << demod_bit_buffer[bit_index + 9]
                 << demod_bit_buffer[bit_index + 10]
                 << demod_bit_buffer[bit_index + 11] << ""
                 << demod_bit_buffer[bit_index + 12]
                 << demod_bit_buffer[bit_index + 13]
                 << demod_bit_buffer[bit_index + 14]
                 << demod_bit_buffer[bit_index + 15] << ""
                 << demod_bit_buffer[bit_index + 16]
                 << demod_bit_buffer[bit_index + 17]
                 << demod_bit_buffer[bit_index + 18]
                 << demod_bit_buffer[bit_index + 19] << ""
                 << demod_bit_buffer[bit_index + 20]
                 << demod_bit_buffer[bit_index + 21]
                 << demod_bit_buffer[bit_index + 22]
                 << demod_bit_buffer[bit_index + 23];
    }
    bit_index += 24;
    qDebug() << demod_bit_buffer[bit_index]
             << demod_bit_buffer[bit_index + 1]
             << demod_bit_buffer[bit_index + 2]
             << demod_bit_buffer[bit_index + 3] << ""
             << demod_bit_buffer[bit_index + 4]
             << demod_bit_buffer[bit_index + 5]
             << demod_bit_buffer[bit_index + 6]
             << demod_bit_buffer[bit_index + 7] << ""
             << demod_bit_buffer[bit_index + 8]
             << demod_bit_buffer[bit_index + 9]
             << demod_bit_buffer[bit_index + 10]
             << demod_bit_buffer[bit_index + 11] << ""
             << demod_bit_buffer[bit_index + 12]
             << demod_bit_buffer[bit_index + 13]
             << demod_bit_buffer[bit_index + 14]
             << demod_bit_buffer[bit_index + 15];

    // invert bits for display
    for (int j = 0; j < last_bit_index; j++)
    {
        if (demod_bit_buffer[j] == '1')
        {
            demod_bit_buffer[j]  = '0';
        }
        else
        {
            demod_bit_buffer[j]  = '1';
        }
    }

    bit_index    = 0;
    qDebug() << "bit buffer inv";
    qDebug() << demod_bit_buffer[bit_index]
             << demod_bit_buffer[bit_index + 1]
             << demod_bit_buffer[bit_index + 2]
             << demod_bit_buffer[bit_index + 3] << ""
             << demod_bit_buffer[bit_index + 4]
             << demod_bit_buffer[bit_index + 5]
             << demod_bit_buffer[bit_index + 6]
             << demod_bit_buffer[bit_index + 7] << ""
             << demod_bit_buffer[bit_index + 8]
             << demod_bit_buffer[bit_index + 9]
             << demod_bit_buffer[bit_index + 10]
             << demod_bit_buffer[bit_index + 11] << ""
             << demod_bit_buffer[bit_index + 12]
             << demod_bit_buffer[bit_index + 13]
             << demod_bit_buffer[bit_index + 14]
             << demod_bit_buffer[bit_index + 15] << ""
             << demod_bit_buffer[bit_index + 16]
             << demod_bit_buffer[bit_index + 17]
             << demod_bit_buffer[bit_index + 18]
             << demod_bit_buffer[bit_index + 19] << ""
             << demod_bit_buffer[bit_index + 20]
             << demod_bit_buffer[bit_index + 21]
             << demod_bit_buffer[bit_index + 22]
             << demod_bit_buffer[bit_index + 23];
    bit_index += 24;
    qDebug() << demod_bit_buffer[bit_index]
             << demod_bit_buffer[bit_index + 1]
             << demod_bit_buffer[bit_index + 2]
             << demod_bit_buffer[bit_index + 3] << ""
             << demod_bit_buffer[bit_index + 4]
             << demod_bit_buffer[bit_index + 5]
             << demod_bit_buffer[bit_index + 6]
             << demod_bit_buffer[bit_index + 7] << ""
             << demod_bit_buffer[bit_index + 8]
             << demod_bit_buffer[bit_index + 9]
             << demod_bit_buffer[bit_index + 10]
             << demod_bit_buffer[bit_index + 11] << ""
             << demod_bit_buffer[bit_index + 12]
             << demod_bit_buffer[bit_index + 13]
             << demod_bit_buffer[bit_index + 14]
             << demod_bit_buffer[bit_index + 15] << ""
             << demod_bit_buffer[bit_index + 16]
             << demod_bit_buffer[bit_index + 17]
             << demod_bit_buffer[bit_index + 18]
             << demod_bit_buffer[bit_index + 19] << ""
             << demod_bit_buffer[bit_index + 20]
             << demod_bit_buffer[bit_index + 21]
             << demod_bit_buffer[bit_index + 22]
             << demod_bit_buffer[bit_index + 23];
    bit_index += 24;
    qDebug() << demod_bit_buffer[bit_index]
             << demod_bit_buffer[bit_index + 1]
             << demod_bit_buffer[bit_index + 2]
             << demod_bit_buffer[bit_index + 3] << ""
             << demod_bit_buffer[bit_index + 4]
             << demod_bit_buffer[bit_index + 5]
             << demod_bit_buffer[bit_index + 6]
             << demod_bit_buffer[bit_index + 7] << ""
             << demod_bit_buffer[bit_index + 8]
             << demod_bit_buffer[bit_index + 9]
             << demod_bit_buffer[bit_index + 10]
             << demod_bit_buffer[bit_index + 11] << ""
             << demod_bit_buffer[bit_index + 12]
             << demod_bit_buffer[bit_index + 13]
             << demod_bit_buffer[bit_index + 14]
             << demod_bit_buffer[bit_index + 15] << ""
             << demod_bit_buffer[bit_index + 16]
             << demod_bit_buffer[bit_index + 17]
             << demod_bit_buffer[bit_index + 18]
             << demod_bit_buffer[bit_index + 19] << ""
             << demod_bit_buffer[bit_index + 20]
             << demod_bit_buffer[bit_index + 21]
             << demod_bit_buffer[bit_index + 22]
             << demod_bit_buffer[bit_index + 23];
    bit_index += 24;
    qDebug() << demod_bit_buffer[bit_index]
             << demod_bit_buffer[bit_index + 1]
             << demod_bit_buffer[bit_index + 2]
             << demod_bit_buffer[bit_index + 3] << ""
             << demod_bit_buffer[bit_index + 4]
             << demod_bit_buffer[bit_index + 5]
             << demod_bit_buffer[bit_index + 6]
             << demod_bit_buffer[bit_index + 7] << ""
             << demod_bit_buffer[bit_index + 8]
             << demod_bit_buffer[bit_index + 9]
             << demod_bit_buffer[bit_index + 10]
             << demod_bit_buffer[bit_index + 11] << ""
             << demod_bit_buffer[bit_index + 12]
             << demod_bit_buffer[bit_index + 13]
             << demod_bit_buffer[bit_index + 14]
             << demod_bit_buffer[bit_index + 15] << ""
             << demod_bit_buffer[bit_index + 16]
             << demod_bit_buffer[bit_index + 17]
             << demod_bit_buffer[bit_index + 18]
             << demod_bit_buffer[bit_index + 19] << ""
             << demod_bit_buffer[bit_index + 20]
             << demod_bit_buffer[bit_index + 21]
             << demod_bit_buffer[bit_index + 22]
             << demod_bit_buffer[bit_index + 23];
    if (hdr_CS_found == BURST_TYPE_HDR200)
    {
        bit_index += 24;
        qDebug() << demod_bit_buffer[bit_index]
                 << demod_bit_buffer[bit_index + 1]
                 << demod_bit_buffer[bit_index + 2]
                 << demod_bit_buffer[bit_index + 3] << ""
                 << demod_bit_buffer[bit_index + 4]
                 << demod_bit_buffer[bit_index + 5]
                 << demod_bit_buffer[bit_index + 6]
                 << demod_bit_buffer[bit_index + 7] << ""
                 << demod_bit_buffer[bit_index + 8]
                 << demod_bit_buffer[bit_index + 9]
                 << demod_bit_buffer[bit_index + 10]
                 << demod_bit_buffer[bit_index + 11] << ""
                 << demod_bit_buffer[bit_index + 12]
                 << demod_bit_buffer[bit_index + 13]
                 << demod_bit_buffer[bit_index + 14]
                 << demod_bit_buffer[bit_index + 15] << ""
                 << demod_bit_buffer[bit_index + 16]
                 << demod_bit_buffer[bit_index + 17]
                 << demod_bit_buffer[bit_index + 18]
                 << demod_bit_buffer[bit_index + 19] << ""
                 << demod_bit_buffer[bit_index + 20]
                 << demod_bit_buffer[bit_index + 21]
                 << demod_bit_buffer[bit_index + 22]
                 << demod_bit_buffer[bit_index + 23];
        bit_index += 24;
        qDebug() << demod_bit_buffer[bit_index]
                 << demod_bit_buffer[bit_index + 1]
                 << demod_bit_buffer[bit_index + 2]
                 << demod_bit_buffer[bit_index + 3] << ""
                 << demod_bit_buffer[bit_index + 4]
                 << demod_bit_buffer[bit_index + 5]
                 << demod_bit_buffer[bit_index + 6]
                 << demod_bit_buffer[bit_index + 7] << ""
                 << demod_bit_buffer[bit_index + 8]
                 << demod_bit_buffer[bit_index + 9]
                 << demod_bit_buffer[bit_index + 10]
                 << demod_bit_buffer[bit_index + 11] << ""
                 << demod_bit_buffer[bit_index + 12]
                 << demod_bit_buffer[bit_index + 13]
                 << demod_bit_buffer[bit_index + 14]
                 << demod_bit_buffer[bit_index + 15] << ""
                 << demod_bit_buffer[bit_index + 16]
                 << demod_bit_buffer[bit_index + 17]
                 << demod_bit_buffer[bit_index + 18]
                 << demod_bit_buffer[bit_index + 19] << ""
                 << demod_bit_buffer[bit_index + 20]
                 << demod_bit_buffer[bit_index + 21]
                 << demod_bit_buffer[bit_index + 22]
                 << demod_bit_buffer[bit_index + 23];
        bit_index += 24;
        qDebug() << demod_bit_buffer[bit_index]
                 << demod_bit_buffer[bit_index + 1]
                 << demod_bit_buffer[bit_index + 2]
                 << demod_bit_buffer[bit_index + 3] << ""
                 << demod_bit_buffer[bit_index + 4]
                 << demod_bit_buffer[bit_index + 5]
                 << demod_bit_buffer[bit_index + 6]
                 << demod_bit_buffer[bit_index + 7] << ""
                 << demod_bit_buffer[bit_index + 8]
                 << demod_bit_buffer[bit_index + 9]
                 << demod_bit_buffer[bit_index + 10]
                 << demod_bit_buffer[bit_index + 11] << ""
                 << demod_bit_buffer[bit_index + 12]
                 << demod_bit_buffer[bit_index + 13]
                 << demod_bit_buffer[bit_index + 14]
                 << demod_bit_buffer[bit_index + 15] << ""
                 << demod_bit_buffer[bit_index + 16]
                 << demod_bit_buffer[bit_index + 17]
                 << demod_bit_buffer[bit_index + 18]
                 << demod_bit_buffer[bit_index + 19] << ""
                 << demod_bit_buffer[bit_index + 20]
                 << demod_bit_buffer[bit_index + 21]
                 << demod_bit_buffer[bit_index + 22]
                 << demod_bit_buffer[bit_index + 23];
        bit_index += 24;
        qDebug() << demod_bit_buffer[bit_index]
                 << demod_bit_buffer[bit_index + 1]
                 << demod_bit_buffer[bit_index + 2]
                 << demod_bit_buffer[bit_index + 3] << ""
                 << demod_bit_buffer[bit_index + 4]
                 << demod_bit_buffer[bit_index + 5]
                 << demod_bit_buffer[bit_index + 6]
                 << demod_bit_buffer[bit_index + 7] << ""
                 << demod_bit_buffer[bit_index + 8]
                 << demod_bit_buffer[bit_index + 9]
                 << demod_bit_buffer[bit_index + 10]
                 << demod_bit_buffer[bit_index + 11] << ""
                 << demod_bit_buffer[bit_index + 12]
                 << demod_bit_buffer[bit_index + 13]
                 << demod_bit_buffer[bit_index + 14]
                 << demod_bit_buffer[bit_index + 15] << ""
                 << demod_bit_buffer[bit_index + 16]
                 << demod_bit_buffer[bit_index + 17]
                 << demod_bit_buffer[bit_index + 18]
                 << demod_bit_buffer[bit_index + 19] << ""
                 << demod_bit_buffer[bit_index + 20]
                 << demod_bit_buffer[bit_index + 21]
                 << demod_bit_buffer[bit_index + 22]
                 << demod_bit_buffer[bit_index + 23];
    }
    bit_index += 24;
    qDebug() << demod_bit_buffer[bit_index]
             << demod_bit_buffer[bit_index + 1]
             << demod_bit_buffer[bit_index + 2]
             << demod_bit_buffer[bit_index + 3] << ""
             << demod_bit_buffer[bit_index + 4]
             << demod_bit_buffer[bit_index + 5]
             << demod_bit_buffer[bit_index + 6]
             << demod_bit_buffer[bit_index + 7] << ""
             << demod_bit_buffer[bit_index + 8]
             << demod_bit_buffer[bit_index + 9]
             << demod_bit_buffer[bit_index + 10]
             << demod_bit_buffer[bit_index + 11] << ""
             << demod_bit_buffer[bit_index + 12]
             << demod_bit_buffer[bit_index + 13]
             << demod_bit_buffer[bit_index + 14]
             << demod_bit_buffer[bit_index + 15];
}
#endif


















