/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#include <qwt_text.h>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_knob.h>

#include <QtCore/QTimerEvent>

#include <QtCore/QSettings>
#include <QtGui/QIcon>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSlider>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QProgressBar>


#include "globals.h"
#include "waveform.h"
#include "simplewaterfall.h"
#include "pactortnc.h"
#include "pactoraudioengine.h"

#include "pactorsession.h"

#define     FAST_TIMER_INTERVAL         50
#define     SLOW_TIMER_INTERVAL         200
#define     SPECTRUM_TIMER_INTERVAL     200

const int NullTimerId = -1;

class   Waveform;
class   SWaterfall;


PactorSession::PactorSession(QWidget *parent) : QWidget(parent)
    ,m_freq_slider(new QSlider(Qt::Horizontal, this))
    ,m_frequency(new QLabel(this))
    ,m_outputText(new QLineEdit(this))
    ,m_inputText(new QLineEdit(this))
    ,multi_purpose_timer(new QTimer(this))
    ,m_RBwave(new QRadioButton(this))
    ,m_RBwaterfall(new QRadioButton(this))
    ,m_RBspectrum(new QRadioButton(this))
    ,m_RBnone(new QRadioButton(this))
    ,m_RBlowSpWave(new QCheckBox(this))
    ,m_RBFreezeWave(new QCheckBox(this))
    ,my_waveform(new Waveform(this))
    ,my_waterfall(new SWaterfall(this))
    ,help_menu(new QMenu(this))
    ,file_menu(new QMenu(this))
    ,menu_bar(new QMenuBar(this))
    ,about_page(new QMessageBox(this))
{
    t_icon_struct   temp_icon_info[] =
    {
        {"Send", true, GREY_ICON, false, 5 * FAST_TIMER_INTERVAL, 0},
        {"Rx", true, GREY_ICON, false, 5 * FAST_TIMER_INTERVAL, 0},
        {"Phase", true, GREEN_ICON, false, -1, 0},
        {"Req", true, GREY_ICON, false, -1, 0},
        {"Trans", true, GREY_ICON, false, 8 * FAST_TIMER_INTERVAL, 0},
        {"Error", true, GREEN_ICON, false, 8 * FAST_TIMER_INTERVAL, 0},
        {"IRS", true, GREY_ICON, false, -1, 0},
        {"ISS", true, GREY_ICON, false, -1, 0},
        {"Idle", true, GREEN_ICON, false, -1, 0},
        {"Vox", true, GREY_ICON, false, 3 * FAST_TIMER_INTERVAL, 0},
        {"Comm", true, GREY_ICON, false, 4 * FAST_TIMER_INTERVAL, 0},
        {"----", false, GREY_ICON, false, 3 * FAST_TIMER_INTERVAL, 0}
    };

    for (int i = 0; i < LED_COUNT; i++)
    {
        icon_info[i] = temp_icon_info[i];
    }
    // create timers
    frequency_timer     = new QTimer();
    frequency_timer->setSingleShot(true);
    multi_purpose_timer = new QTimer();
    received_frequency  = 0;
    center_frequency = 0;

    // initialize waveform
    // one tile is 20 pixels, one audio buffer is 20 samples
    my_waveform->reset();
    my_waveform->initialize(TILE_SIZE_SPL, TILE_SIZE_HEIGHT, WAVEFORM_SIZE);

    // create interface and connect signals
    createUi();

    connectUi();

    CHECKED_CONNECT(multi_purpose_timer, SIGNAL(timeout(void)), this, SLOT(any_useTimerCallback(void)));

    m_RBnone->setChecked(true);

    // set spectrum widget center frequency
    my_waterfall->setCenterFrequency(center_frequency);

    // communication activity
    comm_active      = false;

    timer_counter       = 0;
    multi_purpose_timer->start(FAST_TIMER_INTERVAL);


    // create TNC
    m_PactorTNC = new PactorTNC(this);
    // set buttons state
    audio_gain->setValue(m_PactorTNC->m_pactoraudioengine->thread_info.audio_in_gain);

    // speed limit
    //
    // TODO: Let me suggest using setSpeedLimit() in TNC rather than doing it directly
    //
    //    qDebug()<<"panel init speed limit"<<m_PactorTNC->m_pactorTNCThread->PT_speed_limit;
    if ( m_PactorTNC->m_pactorTNCThread->PT_speed_limit == PT_SPEED_100 )
    {
        speedLimitAction->setChecked(true);
        btn_speed100->setIcon(m_Icon[RED_ICON]);
    }
    else
    {
        speedLimitAction->setChecked(false);
        btn_speed100->setIcon(m_Icon[GREY_ICON]);
    }

    if ( m_PactorTNC->m_pactorTNCThread->PT_allow_huffman )
    {
        huffmanAction->setChecked(true);
        btn_Huffman->setIcon(m_Icon[GREEN_ICON]);
    }
    else
    {
        huffmanAction->setChecked(false);
        btn_Huffman->setIcon(m_Icon[GREY_ICON]);
    }
    if ( m_PactorTNC->AFC_on )
    {
        afcAction->setChecked(true);
        btn_AFC->setIcon(m_Icon[GREEN_ICON]);
    }
    else
    {
        afcAction->setChecked(false);
        btn_AFC->setIcon(m_Icon[GREY_ICON]);
    }
    if ( m_PactorTNC->AGC_on )
    {
        agcAction->setChecked(true);
        btn_AGC->setIcon(m_Icon[GREEN_ICON]);
    }
    else
    {
        agcAction->setChecked(false);
        btn_AGC->setIcon(m_Icon[GREY_ICON]);
    }
}

PactorSession::~PactorSession()
{
    delete  m_PactorTNC;
}

//-----------------------------------------------------------------------------
// Event Handlers
//-----------------------------------------------------------------------------
void PactorSession::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event);
    QSettings settings("OpenModem", QLatin1String("Config"));
    //  === Pactor settings   === //
    settings.beginGroup(QLatin1String("PactorTNC"));
    int win_height      = height() - 28;
    int win_width       = width() - 10;
    resize(win_width,win_height);
    settings.setValue("pos", x());
    settings.setValue("size", size());
    settings.setValue("visible", this->isVisible());
    settings.endGroup();
    PACTOR_TNC_DEBUG << "closeWidget" << "visible" << this->isVisible();
    // the file is updated immediately
    settings.sync();  // so that the file is updated immediately
}

//-----------------------------------------------------------------------------
// Public slots
//-----------------------------------------------------------------------------
// universal timer called every 50 milliseconds
void PactorSession::any_useTimerCallback(void)
{
    timer_counter++;
    // comm LED update
    // setLEDdirect(LED_COMM, GREY_ICON);
    bool c = QMetaObject::invokeMethod(this, "setLEDdirect",
                                       Qt::QueuedConnection,
                                       Q_ARG(int, LED_COMM),
                                       Q_ARG(int, GREY_ICON));
    Q_ASSERT(c);
    // display audio gain and level
    if ((timer_counter % (SLOW_TIMER_INTERVAL / FAST_TIMER_INTERVAL)) == 0)
    {
        audio_gain->setValue(m_PactorTNC->m_pactoraudioengine->thread_info.audio_in_gain);
        progressbar->setValue((int)(400 * m_PactorTNC->m_pactoraudioengine->thread_info.AGC_amplitude));
    }
    // start a new spectrum acquisition
    if (((timer_counter % (SPECTRUM_TIMER_INTERVAL / FAST_TIMER_INTERVAL)) == 0)
        && ((wave_btn == BTN_WFALL)
            || (wave_btn == BTN_SPECTR)))
    {
        m_PactorTNC->m_pactorDemodulator->m_thread->wave_samples  = 0;
    }
    if (timer_counter >= (100 * SPECTRUM_TIMER_INTERVAL))
    {
        timer_counter   = 0;
    }
    // update panel LED status
    for (int i = 0; i < LED_COMM; i++)
    {
        if ((icon_info[i].timer_init_count != -1)
            && (icon_info[i].timer_count > 0))
        {
            icon_info[i].timer_count    -= FAST_TIMER_INTERVAL;
            if (icon_info[i].timer_count <= 0)
            {
                c = QMetaObject::invokeMethod(this, "setLEDdirect",
                                              Qt::QueuedConnection,
                                              Q_ARG(int, i),
                                              Q_ARG(int, GREY_ICON));
                Q_ASSERT(c);
            }
        }
    }
}

void PactorSession::send_spectrum_samples(void)
{
    if ((wave_btn == BTN_WFALL)
        || (wave_btn == BTN_SPECTR))
    {
        qRegisterMetaType<cdoubleptr>("cdoubleptr");
        const bool c = QMetaObject::invokeMethod(my_waterfall, "newData",
                       Qt::QueuedConnection,
                       Q_ARG(cdoubleptr, m_PactorTNC->m_pactorDemodulator->m_thread->spct_wave_buffer),
                       Q_ARG(qint16, FFT_POINTS));
        Q_ASSERT(c);
    }
}

//-----------------------------------------------------------------------------
// Private slots
//-----------------------------------------------------------------------------

void PactorSession::setGainCallback(double value)
{
    if (m_PactorTNC->m_pactoraudioengine->thread_info.AGC_on == false)
    {
        m_PactorTNC->m_pactoraudioengine->thread_info.audio_in_gain = value;
        //m_PactorTNC->writeSettings(e_jack_audio);
    }
}

// AFC ON/OFF button callback
void PactorSession::setAFCCallback(bool checked)
{
    if (checked == true)
    {
        m_PactorTNC->AFC_on = true;
        btn_AFC->setIcon(m_Icon[GREEN_ICON]);
    }
    else
    {
        m_PactorTNC->AFC_on = false;
        btn_AFC->setIcon(m_Icon[GREY_ICON]);
    }
    //m_PactorTNC->writeSettings(e_frequency_value); // AFC, AGC
}

// AGC ON/OFF button callback
void PactorSession::setAGCCallback(bool checked)
{
    if (checked == true)
    {
        m_PactorTNC->m_pactoraudioengine->thread_info.AGC_on    = true;
        m_PactorTNC->AGC_on                                     = true;
        btn_AGC->setIcon(m_Icon[GREEN_ICON]);
    }
    else
    {
        m_PactorTNC->m_pactoraudioengine->thread_info.AGC_on    = false;
        m_PactorTNC->AGC_on                                     = false;
        btn_AGC->setIcon(m_Icon[GREY_ICON]);
    }
    //m_PactorTNC->writeSettings(e_frequency_value); // AFC, AGC
}

// speed limitation callback
void PactorSession::setSpeedCallback(bool checked)
{
    if (checked == true)
    {
        //        qDebug()<<"settings checked read speed limit"<<m_PactorTNC->m_pactorTNCThread->PT_speed_limit;
        m_PactorTNC->m_pactorTNCThread->PT_speed_limit    = PT_SPEED_100;
        btn_speed100->setIcon(m_Icon[RED_ICON]);
    }
    else
    {
        //        qDebug()<<"settings unchecked read speed limit"<<m_PactorTNC->m_pactorTNCThread->PT_speed_limit;
        m_PactorTNC->m_pactorTNCThread->PT_speed_limit    = PT_SPEED_200;
        btn_speed100->setIcon(m_Icon[GREY_ICON]);
    }
    //m_PactorTNC->writeSettings(e_mode_button); // speed, Huffman
}

// Huffman button
void PactorSession::setHuffmanCallback(bool checked)
{
    if (checked == true)
    {
        m_PactorTNC->m_pactorTNCThread->PT_allow_huffman    = true;
        btn_Huffman->setIcon(m_Icon[GREEN_ICON]);
    }
    else
    {
        m_PactorTNC->m_pactorTNCThread->PT_allow_huffman    = false;
        btn_Huffman->setIcon(m_Icon[GREY_ICON]);
    }
    //m_PactorTNC->writeSettings(e_mode_button); // speed, Huffman
}

// help menu, help page
void PactorSession::helpHelpCallback(bool checked)
{
    checked = checked; // remove compiler warning
    help_dialog->setVisible(true);
}

// help menu, about page
void PactorSession::helpAboutCallback(bool checked)
{
    checked = checked; // remove compiler warning
    about_page->setVisible(true);
}

//-----------------------------------------------------------------------------
// Private functions
//-----------------------------------------------------------------------------

void PactorSession::createUi()
{
    // LED group
    const QSize iconSize(40, 32);
    const QSize bigIconSize(56, 32);

    m_Icon[RED_ICON]    = QPixmap(":red-img.png");
    m_Icon[GREEN_ICON]  = QPixmap(":green-img.png");
    m_Icon[GREY_ICON]   = QPixmap(":grey-img.png");

    m_tx_frame          = new QGroupBox(tr("Transmit"));
    m_tx_text           = new QGroupBox(tr("Tx frame"));
    m_rx_frame          = new QGroupBox(tr("Receive"));
    m_rx_text           = new QGroupBox(tr("Rx frame"));

    wholeDisplay        = new QVBoxLayout(this);

    highDisplay         = new QHBoxLayout(); // 1st vertical separation
    lowDisplay          = new QHBoxLayout();

    leftDisplay         = new QVBoxLayout(); // left

    LEDLayout           = new QGridLayout(); // status LED

    TncStateDisplay     = new QGridLayout(); // TNC state

    txWidget            = new QGridLayout(); // transmit widget

    // right
    rightDisplay        = new QVBoxLayout();
    rxWidget            = new QGridLayout(); // receive widget
    // left
    AfcDisplay          = new QVBoxLayout();

    tx_frame_label      = new QLabel(this);
    rx_frame_label      = new QLabel(this);

    rx_frame_VLayout    = new QVBoxLayout();
    m_tx_control        = new QHBoxLayout();

    // create LED rows in a grid box
    // first line: modem status LEDs
    // second line: modem status LEDs text
    // third line: station status LEDs
    // fourth line: station status LEDs text
    int LED_row;
    int diff_LED_col;
    for ( quint32 i = 0; i <= LED_COMM; i++ )
    {
        if ( i < LED_IRS )
        {
            LED_row      = 0;
            diff_LED_col = 0;
        }
        else
        {
            LED_row = 2;
            diff_LED_col    = LED_IRS;
        }
        LED[i] = new QLabel(this);
        LED_text[i] = new QLabel(this);
        LED[i]->setFixedHeight(16);
        LED[i]->setFixedWidth(24);
        LED[i]->setStyleSheet("QLabel { background-color : darkGrey;}");
        LED_text[i]->setText(icon_info[i].text);
        LED_text[i]->setFixedWidth(36);
        icon_info[i].timer_count    = 0;

        LEDLayout->addWidget(LED[i], LED_row, i - diff_LED_col, Qt::AlignHCenter);
        LEDLayout->addWidget(LED_text[i], LED_row + 1, i - diff_LED_col, Qt::AlignHCenter);
    }
    m_LEDStatus = new QGroupBox(tr("Status"));
    m_LEDStatus->setLayout(LEDLayout);

    btn_AFC     = new QToolButton(this);
    btn_AFC->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    btn_AFC->setIcon(m_Icon[GREY_ICON]);
    btn_AFC->setText(tr("AFC"));
    btn_AFC->setEnabled(true);
    btn_AFC->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    btn_AFC->setMaximumSize(bigIconSize);

    btn_AGC     = new QToolButton(this);
    btn_AGC->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    btn_AGC->setIcon(m_Icon[GREY_ICON]);
    btn_AGC->setText(tr("AGC"));
    btn_AGC->setEnabled(true);
    btn_AGC->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    btn_AGC->setMaximumSize(bigIconSize);

    btn_speed100     = new QToolButton(this);
    btn_speed100->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    btn_speed100->setIcon(m_Icon[GREY_ICON]);
    btn_speed100->setText("100 Bd");
    btn_speed100->setEnabled(true);
    btn_speed100->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    btn_speed100->setMaximumSize(bigIconSize);

    btn_Huffman     = new QToolButton(this);
    btn_Huffman->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    btn_Huffman->setIcon(m_Icon[GREY_ICON]);
    btn_Huffman->setText("Huffman");
    btn_Huffman->setEnabled(true);
    btn_Huffman->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    btn_Huffman->setMaximumSize(bigIconSize);

    // radio buttons "FSK"
    m_RBwave->setEnabled(true);
    m_RBwave->setText("FSK");
    // radio buttons "Waterfall"
    m_RBwaterfall->setEnabled(true);
    m_RBwaterfall->setText(tr("Waterfall"));
    // radio buttons "Spectrum"
    m_RBspectrum->setEnabled(true);
    m_RBspectrum->setText(tr("Spectrum"));
    // radio buttons "None"
    m_RBnone->setEnabled(true);
    m_RBnone->setText(tr("None"));
    m_RBnone->setChecked(true);
    wave_btn    = BTN_NONE;
    my_waveform->setVisible(false);
    m_RBlowSpWave->setVisible(false);
    m_RBFreezeWave->setVisible(false);
    my_waterfall->setVisible(false);

    // Button panel
    center_frequency    = NCO_FREQUENCY;
    m_freq_slider->setEnabled(true);
    m_freq_slider->setMinimum(FREQ_SLIDER_MIN);
    m_freq_slider->setMaximum(FREQ_SLIDER_MAX);
    m_freq_slider->setValue(center_frequency);

    // radio button low speed waveform
    m_RBlowSpWave->setEnabled(true);
    m_RBlowSpWave->setText(tr("Slow"));
    m_RBlowSpWave->setCheckable(true);
    m_RBlowSpWave->setChecked(true);
    m_RBlowSpWave->setIconSize(QSize(40, 32));
    // radio button freeze waveform
    m_RBFreezeWave->setEnabled(true);
    m_RBFreezeWave->setText(tr("Freeze"));
    m_RBFreezeWave->setCheckable(true);
    m_RBFreezeWave->setChecked(false);

    m_frequency->setEnabled(true);
    // display frequency in main widget
    char temp_char[64];
    sprintf(temp_char, "%d Hz", center_frequency);
    QString freq_text(temp_char);
    m_frequency->setText(freq_text);

    m_inputText->setReadOnly(true);
    m_inputText->setText(tr("received text"));
    m_inputText->show();

    m_outputText->setReadOnly(true);
    m_outputText->setText(tr("transmitted text"));
    m_outputText->show();
    // plot
    audio_gain  = new QwtKnob(this);
    audio_gain->setScale(0.0, 20.0);
    audio_gain->setScaleStepSize(1.0);
    audio_gain->setObjectName(tr("Audio gain"));

    progressbar = new QProgressBar();
    progressbar->setOrientation(Qt::Vertical);
    progressbar->setMinimum(0);
    progressbar->setMaximum(100);
    progressbar->setFixedHeight(80);

    // 1st vertical separation
    highDisplay->addLayout(leftDisplay);
    highDisplay->addStretch(0);
    // move first content 30 lines down otherise it is superimposed to menu
    highDisplay->setContentsMargins(0, 30, 0, 0);

    highDisplay->addLayout(rightDisplay);
    // left
    leftDisplay->addWidget(m_LEDStatus);
    m_tx_frame->setLayout(TncStateDisplay);    // give a title
    leftDisplay->addWidget(m_tx_frame);
    leftDisplay->addLayout(txWidget);
    leftDisplay->addLayout(m_tx_control);
    leftDisplay->addStretch(0);

    m_tx_control->addWidget(btn_speed100);
    m_tx_control->addWidget(btn_Huffman);

    tx_frame_label->setText(tr("Tx frame"));
    txWidget->addWidget(tx_frame_label);
    txWidget->addWidget(m_outputText);
    // right
    rightDisplay->addWidget(m_rx_frame);

    rx_frame_label->setText(tr("Rx frame"));
    rx_frame_VLayout->addWidget(rx_frame_label);
    rx_frame_VLayout->addWidget(m_inputText);
    rightDisplay->addLayout(rx_frame_VLayout);
    rightDisplay->addLayout(rxWidget);

    rightDisplay->addStretch(0);

    rxWidget->addLayout(AfcDisplay, 0, 0);
    // receive panel row 0 col 0
    AfcDisplay->addWidget(m_frequency, 0, Qt::AlignHCenter);
    AfcDisplay->addWidget(m_freq_slider);
    AfcDisplay->addWidget(btn_AFC, 0, Qt::AlignHCenter);
    AfcDisplay->addStretch(0);
    // row 0 col 1
    QVBoxLayout *gain_layout = new QVBoxLayout();
    gain_layout->addWidget(audio_gain);
    gain_layout->addWidget(btn_AGC, 0, Qt::AlignHCenter);
    rxWidget->addLayout(gain_layout, 0, 1);

    // row 0 col 2
    rxWidget->addWidget(progressbar, 0, 2);

    wholeDisplay->addLayout(highDisplay);

    // left lower frame
    waveLayout      = new QVBoxLayout();
    buttonsLayout   = new QButtonGroup(this);
    buttonsLayout->addButton(m_RBwave, e_panel_wfrm_wave);
    buttonsLayout->addButton(m_RBwaterfall, e_panel_wfrm_wfall);
    buttonsLayout->addButton(m_RBspectrum, e_panel_wfrm_spectr);
    buttonsLayout->addButton(m_RBnone, e_panel_wfrm_none);
    waveLayout->addWidget(m_RBwave);
    waveLayout->addWidget(m_RBwaterfall);
    waveLayout->addWidget(m_RBspectrum);
    waveLayout->addWidget(m_RBnone);
    controlGroup = new QGroupBox(tr("Scope"));
    controlGroup->setLayout(waveLayout);

    QVBoxLayout *wave_mode   = new QVBoxLayout();
    lowDisplay->addWidget(controlGroup);
    wave_mode->addWidget(m_RBlowSpWave);
    wave_mode->addWidget(m_RBFreezeWave);
    lowDisplay->addLayout(wave_mode);
    lowDisplay->addWidget(my_waveform);
    lowDisplay->addWidget(my_waterfall);

    // menus

    agcAction = new QAction(tr("AGC"), this);
    agcAction->setCheckable(true);

    afcAction = new QAction(tr("AFC"), this);
    afcAction->setCheckable(true);

    huffmanAction = new QAction(tr("Hufman encoding"), this);
    huffmanAction->setCheckable(true);

    speedLimitAction = new QAction(tr("Speed limit 100Bd"), this);
    speedLimitAction->setCheckable(true);

    help_help = new QAction(tr("&Help"), this);

    help_about = new QAction(tr("&About"), this);

    file_menu->setTitle(tr("&File"));
    file_menu->addAction(agcAction);
    file_menu->addAction(afcAction);
    file_menu->addSeparator();
    file_menu->addAction(huffmanAction);
    file_menu->addAction(speedLimitAction);
    help_menu->setTitle(tr("&Help"));
    help_menu->addAction(help_help);
    help_menu->addAction(help_about);

    menu_bar->addMenu(file_menu);
    menu_bar->addMenu(help_menu);

    wholeDisplay->addLayout(lowDisplay);

    setWindowTitle(tr("Pactor TNC"));

    // Apply layout
    setLayout(wholeDisplay);

    // help page
    help_dialog         = new QDialog(this);
    help_text           = new QTextEdit(this);
    help_file           = new QFile("../doc/help_text.txt", this);
    help_stream         = new QTextStream(help_file);

    help_file->open(QIODevice::ReadOnly | QIODevice::Text);
    QString   help_text_value = help_stream->readAll();

    help_layout         = new QVBoxLayout();

    help_text->acceptRichText();
    help_text->setText(help_text_value);
    help_text->setReadOnly(true);
    help_layout->addWidget(help_text);

    help_dialog->setWindowTitle(tr("Help"));
    help_dialog->setLayout(help_layout);
    help_dialog->setGeometry(200, 200, 400, 400);

    // about page
    about_page->setWindowTitle(tr("About"));
    about_page->addButton(QMessageBox::Close);
    about_page->setText(tr("Version 0.1"));
    // resize bug workaround
    QSpacerItem *horizontalSpacer = new QSpacerItem(400, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
    QGridLayout *layout = (QGridLayout *)about_page->layout();
    layout->addItem(horizontalSpacer, layout->rowCount(), 0, 1, layout->columnCount());
}

void PactorSession::connectUi()
{

    CHECKED_CONNECT(m_freq_slider, SIGNAL(valueChanged(int)), this, SLOT(SetCenterFrequencyInt(int)));
    CHECKED_CONNECT(frequency_timer, SIGNAL(timeout(void)), this, SLOT(centerFrequencyTimerCallback(void)));
    CHECKED_CONNECT(audio_gain, SIGNAL(valueChanged(double)), this, SLOT(setGainCallback(double)));
    CHECKED_CONNECT(afcAction, SIGNAL(toggled(bool)), this, SLOT(setAFCCallback(bool)));
    CHECKED_CONNECT(agcAction, SIGNAL(toggled(bool)), this, SLOT(setAGCCallback(bool)));
    CHECKED_CONNECT(speedLimitAction, SIGNAL(toggled(bool)), this, SLOT(setSpeedCallback(bool)));
    CHECKED_CONNECT(huffmanAction, SIGNAL(toggled(bool)), this, SLOT(setHuffmanCallback(bool)));
    CHECKED_CONNECT(buttonsLayout, SIGNAL(buttonClicked(int)), this, SLOT(waveformMode(int)));
    CHECKED_CONNECT(help_help, SIGNAL(triggered(bool)), this, SLOT(helpHelpCallback(bool)));
    CHECKED_CONNECT(help_about, SIGNAL(triggered(bool)), this, SLOT(helpAboutCallback(bool)));
}

//-----------------------------------------------------------------------------
// set center frequency for modulator and demodulator integer argument
//-----------------------------------------------------------------------------
void PactorSession::SetCenterFrequencyInt(int frequency)
{
    center_frequency    = frequency;
    // display frequency in main widget
    char temp_char[64];
    sprintf(temp_char, "%d Hz", frequency);
    QString freq_text(temp_char);
    m_frequency->setText(freq_text);
    // restart timer to send data after a while to avoid sending single characters
    frequency_timer->start(100);
}

void PactorSession::centerFrequencyTimerCallback(void)
{
    my_waterfall->setCenterFrequency(center_frequency);

    //    qDebug()<<"panel set freq INT"<<center_frequency;
    if (center_frequency != received_frequency)
    {
        received_frequency  = center_frequency;
        const bool b = QMetaObject::invokeMethod(m_PactorTNC, "SetCenterFrequencyInt",
                       Qt::AutoConnection,
                       Q_ARG(int, center_frequency),
                       Q_ARG(int, e_freq_mod_panel));
        Q_ASSERT(b);
    }
}

void PactorSession::setLEDdirect(int led_index, int color)
{
    switch (color)
    {
        case
                GREY_ICON:
            LED[led_index]->setStyleSheet("QLabel { background-color : darkGrey;}");
            break;
        case
                GREEN_ICON:
            LED[led_index]->setStyleSheet("QLabel { background-color : green;}");
            if (icon_info[led_index].timer_init_count != -1)
            {
                icon_info[led_index].timer_count        = icon_info[led_index].timer_init_count;
            }
            break;
        case
                RED_ICON:
            LED[led_index]->setStyleSheet("QLabel { background-color : red;}");
            if (icon_info[led_index].timer_init_count != -1)
            {
                icon_info[led_index].timer_count        = icon_info[led_index].timer_init_count;
            }
            break;
    }
}

void PactorSession::setfrequency(int freq)
{
    received_frequency  = freq;
    center_frequency    = freq;
    // display frequency in main widget
    char temp_char[64];
    sprintf(temp_char, "%d Hz", freq);
    QString freq_text(temp_char);
    m_frequency->setText(freq_text);
    m_freq_slider->setValue(center_frequency);
}

void PactorSession::waveformMode(int mode)
{
    switch (mode)
    {
    case e_panel_wfrm_wave:
        wave_btn    = BTN_WAVE;
        my_waterfall->setVisible(false);
        my_waveform->setVisible(true);
        m_RBlowSpWave->setVisible(true);
        m_RBFreezeWave->setVisible(true);
        break;

    case e_panel_wfrm_wfall:
        wave_btn    = BTN_WFALL;
        my_waveform->setVisible(false);
        m_RBlowSpWave->setVisible(false);
        m_RBFreezeWave->setVisible(false);
        my_waterfall->initializeWaterfall(SPECTRUM_HEIGHT, SPECTRUM_WIDTH);
        my_waterfall->setVisible(true);
        break;

    case e_panel_wfrm_spectr:
        wave_btn    = BTN_SPECTR;
        my_waveform->setVisible(false);
        m_RBlowSpWave->setVisible(false);
        m_RBFreezeWave->setVisible(false);
        my_waterfall->initializeSpectrum(SPECTRUM_HEIGHT, SPECTRUM_WIDTH);
        my_waterfall->setVisible(true);
        break;

    case e_panel_wfrm_none:
        wave_btn    = BTN_NONE;
        my_waveform->setVisible(false);
        m_RBlowSpWave->setVisible(false);
        m_RBFreezeWave->setVisible(false);
        my_waterfall->setVisible(false);
        break;

    default:
        break;
    }
}
