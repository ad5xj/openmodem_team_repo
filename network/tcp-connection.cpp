/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#include <QtCore/QDebug>
#include <QtCore/QTimer>
#include <QtCore/QSettings>
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QNetworkSession>
#include <QtNetwork/QNetworkConfigurationManager>

#include "tcp-connection.h"

TCPConnection::TCPConnection(QObject *parent) : QObject(parent)
{
    // find out which IP to connect to
    // if we did not find one, use IPv4 localhost
    if (ipAddress.isEmpty())
    {
        ipAddress = QHostAddress(QHostAddress::LocalHost).toString();
    }

    tcpSocket = new QTcpSocket(this);

    connect(tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(displayError()));

    // Get saved network configuration
    Id = "";
    readSettings();
    // If the saved network configuration is not currently discovered use the system default
    QNetworkConfigurationManager manager;
    QNetworkConfiguration config = manager.configurationFromIdentifier(Id);
    if ( manager.capabilities() &
         QNetworkConfigurationManager::NetworkSessionRequired
       )
    {
        if ( (config.state() & QNetworkConfiguration::Discovered) !=
             QNetworkConfiguration::Discovered
           )
        {
            config = manager.defaultConfiguration();
        }

    }
    networkSession = new QNetworkSession(config, this);
    connect(networkSession, SIGNAL(opened()), this, SLOT(sessionOpened()));
    QMetaObject::connectSlotsByName(this);
}

void TCPConnection::open()
{
    qDebug() << "Opening network session.";
    networkSession->open();
    emit tcpconnected();
}

void TCPConnection::requestCMD()
{
    blockSize = 0;
    tcpSocket->abort();
    tcpSocket->connectToHost(m_host,m_port,QTcpSocket::ReadWrite);
}

void TCPConnection::readCMD()
{
    QDataStream in(tcpSocket);
    in.setVersion(QDataStream::Qt_4_0);


    textCMD = "MODE BPSK";  // for debug
    if ( blockSize == 0 )
    {
        if ( tcpSocket->bytesAvailable() < (int)sizeof(quint16) ) return;
        in >> blockSize;
    }

    if ( tcpSocket->bytesAvailable() < blockSize ) return;

    in >> nextCMD;

    if ( nextCMD == textCMD )
//    {
//        QTimer::singleShot(0, this, SLOT(requestCMD()));
        return;
//    }
    textCMD = nextCMD;
    if ( textCMD == "RMSLink Host" ) qDebug() << textCMD << " found";
}

void TCPConnection::displayError()
{
    lastError = tcpSocket->error();
    errorString = tcpSocket->errorString();
    switch ( tcpSocket->error() )
    {
        case QAbstractSocket::RemoteHostClosedError:
            break;
        case QAbstractSocket::HostNotFoundError:
            qDebug() << "TCP Client - The host was not found.  " << m_host << "/" << m_port;
            break;
        case QAbstractSocket::ConnectionRefusedError:
            qDebug() << "TCP Client - The connection was refused by the peer.  " << m_host << "/" << m_port;
            break;
        default:
            qDebug() << QString("TCP Client - The following error occurred: %1.").arg(tcpSocket->errorString());
            break;
    }
    emit connError();
}

void TCPConnection::sessionOpened()
{
    // Save the used configuration
    QNetworkConfiguration config = networkSession->configuration();
    if (config.type() == QNetworkConfiguration::UserChoice)
        Id = networkSession->sessionProperty(QLatin1String("UserChoiceConfiguration")).toString();
    else
        Id = config.identifier();

    saveSettings();
    emit sessionOpened();
    qDebug() << "Waiting for RMSLink ...";
}

void TCPConnection::readSettings()
{
    QSettings settings("RMSLink","Config");
    settings.beginGroup("Network");
      //use local host if not set
      m_host = settings.value("host","127.0.0.1").toString();
      //use RMSLink default port if not set
      m_port = settings.value("port",8500).toUInt();
      Id = settings.value(QLatin1String("DefaultNetworkConfiguration")).toString();
    settings.endGroup();
}


void TCPConnection::saveSettings()
{
    QSettings settings("RMSLink","Config");
    settings.beginGroup("Modem");
     settings.setValue("Defai;tMetworkConfiguration", Id);
    settings.endGroup();
    settings.sync();
}
