/*******************************************************************
** (C) Copyright 2011 OpenMOR developers
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
** ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
** CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
** OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
** SUCH DAMAGE.
**
**  tcpclient.cpp - tcpclient connection  implementation
**
**  Patterened after the non-blocking fortune client example
**  in the Qt SDK examples.  Many thanks to the Qt trolls for
**  their great example.
**
********************************************************************/
#include <QtCore/QSettings>
#include <QtCore/QTimer>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QMessageBox>
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QHostAddress>

#include "tcpclient.h"

TCPClient::TCPClient(QWidget *parent) : QTcpSocket()
{
    nextCMD = "";
    // find out which IP to connect to
    readSettings();
}

TCPClient::~TCPClient()
{
    // destructor
    // TODO: be sure all objects get destroyed
}

// ===========  Sets and Gets ===================
QString TCPClient::getCMD()
{
    return currentCMD;
}
// ===========  end Sets and Gets ===============

// ===========  Public Slots  ===================
void TCPClient::requestSession()
{
    blockSize = 0;
    const int Timeout = 5 * 1000; // 5 sec arbitary timeout

    if( tcpSocket->isOpen() ) tcpSocket->abort();
    emit signalConnecting();
    tcpSocket->connectToHost(m_host,m_port.toInt());
    if (!tcpSocket->waitForConnected(Timeout))
    {
        qDebug() << "TCPSocket::requestSession 73: Error Connecting - " << tcpSocket->errorString();
        errMsg = "TCPSocket Error::requestSession() 75: ";
        errMsg += QString(tcpSocket->error()) + tcpSocket->errorString();
        emit signalError();
        return;
    }
    errMsg = "";
}

void TCPClient::closeSession()
{
    tcpSocket->close();
    close();
}

QString TCPClient::getErr()
{
    return errMsg;
}
// ===========  End Public Slots ================

// ===========  Private Slots  ==================
void TCPClient::slotConnected()
{
    emit signalConnected();
    readCMD();
}

void TCPClient::slotNextCMD()
{
    // Read next command line from socket
    readCMD();
}

void TCPClient::slotSendCMD()
{
    sendCMD();
}


void TCPClient::readCMD()
{
    if ( !tcpSocket->isOpen() )
    {
        qDebug() << "TCPClient:: readCMD 139: Opening new socket-";
        tcpSocket->connectToHost(m_host,m_port.toInt());
    }
    if ( !tcpSocket->isReadable() )
        qDebug() << "TCPClient::readCMD 144: Socket not readable";
    QDataStream in(tcpSocket);
    in.setVersion(QDataStream::Qt_4_0);
    qDebug() << "TCPClient::readCMD() 149: Reading TCP commands.";
    QString arg = QString(" %1").arg(tcpSocket->bytesAvailable());
    if (blockSize > 0)
    {
        if ( tcpSocket->bytesAvailable() < (int)sizeof(quint16) ) return;
        qDebug() << "TCPClient::readCMD() 152: Reading TCP commands..bytes " + arg;
        in >> blockSize;
    }
    else
    {
        qDebug() << "TCPClient::readCMD() 152: blockSize zero - no commands read";
        currentCMD = "";
        return;
    }
    qDebug() << "TCPClient::readCMD() 164: Reading TCP commands...";

    if (tcpSocket->bytesAvailable() < blockSize) return;

    QString nextCMD;
    in >> nextCMD;
    qDebug() << "Commands Read: " << nextCMD;

    if ( nextCMD == currentCMD )
    {
        QTimer::singleShot(0, this, SLOT(slotNextCMD()));
        return;
    }
    currentCMD = nextCMD;
    if ( currentCMD == "MODE winmor") emit signalStartWinMor();
    if ( currentCMD == "MODE pactor") emit signalStartPactor();
    if ( currentCMD == "BW" ) getBW();
    if ( currentCMD == "BW 500" )  m_bw = 500;
    if ( currentCMD == "BW 1600" ) m_bw = 1600;
    if ( currentCMD == "CLOSE") closeSession();
}

void TCPClient::sendCMD()
{
    QDataStream out(tcpSocket);
    out.setVersion(QDataStream::Qt_4_0);
    out << nextCMD;
}

void TCPClient::formatSendBuffers()
{
    nextCMD = "BUFFERS ";
    nextCMD += QString(m_Quedbufs);
    nextCMD += " " + QString(m_InSeqBufs);
    nextCMD += " " + QString(m_OutSeqBufs);
    nextCMD += " " + QString(m_BufsConf);
    if ( nextCMD != "" )
    {
        QTimer::singleShot(0, this, SLOT(slotSendCMD()));
        return;
    }
}

void TCPClient::getBW()
{
    // send current BW setting
    nextCMD = "BW ";
    nextCMD += QString(m_bw);
    if ( nextCMD != "" )
    {
        QTimer::singleShot(0, this, SLOT(slotSendCMD()));
        return;
    }
}

void TCPClient::displayError(QAbstractSocket::SocketError socketError)
{

    switch (socketError)
    {
      case QAbstractSocket::RemoteHostClosedError:
        break;

      case QAbstractSocket::HostNotFoundError:
        errMsg = tr("The host was not found. Please check the host name and port settings.");
        emit signalError();
        break;

      case QAbstractSocket::ConnectionRefusedError:
      {
        errMsg = tr("TCPClient::displayError: The connection was refused by the server. ");
        errMsg += "TCP: " + m_host + " Port: " + m_port;
        emit signalError();
        break;
      }

      default:
        qDebug() << QString("RMSLink TCP Client Error - The following error occurred: %1.").arg(tcpSocket->errorString());
    }
}

void TCPClient::sessionOpened()
{
    //
}

void TCPClient::readSettings()
{
    // Get saved network configuration
    QSettings settings("RMSLink","Config");
    settings.beginGroup(QLatin1String("Network"));
    m_host = settings.value("host","127.0.0.1").toString();
    m_port = settings.value("port","8500").toString();
    const QString id = settings.value(QLatin1String("DefaultNetworkConfiguration")).toString();
    settings.endGroup();
}

void TCPClient::saveSettings()
{
    QSettings settings("RMSLink","Config");
    settings.beginGroup("Network");
      settings.setValue("host", m_host);
      settings.setValue("port", m_port);
    settings.endGroup();
    settings.sync();
}
