#include <qwt_thermo.h>

#include "audiometer.h"

AudioMeter::AudioMeter(QWidget *parent) : QwtThermo(parent)
{
    // set defaults
    this->setPipeWidth(6);
    this->setScale(-40, 10);
    this->setFillBrush(Qt::green);
    this->setAlarmBrush(Qt::red);
    this->setAlarmLevel(0.0);
    this->setAlarmEnabled(true);
}

AudioMeter::~AudioMeter()
{
    //
}


void AudioMeter::setValue(double value)
{
    if (value > maxValue()) emit overrange();
    this->setValue(value);
}

void AudioMeter::setSize(int w, int h)
{
    this->setBaseSize(w,h);
}

void AudioMeter::setMeterRange(double minval, double maxval)
{
    this->setScale(minval, maxval);
}
