#include "vu_meter.hpp"

VUMeter::VUMeter(QWidget *parent) : QwtThermo(parent)
{
    setLowerBound(-40);
    setUpperBound(10);
    setFillBrush( Qt::green );
    setAlarmBrush( Qt::red );
    setAlarmLevel( 0.0 );
    setAlarmEnabled( true );
}

VUMeter::~VUMeter()
{
    //
}


void VUMeter::set_value(double val)
{
    setValue(val);
}
