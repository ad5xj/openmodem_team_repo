/**********************************************************************
 * iqdata.cpp - implements the thread definition iqdata.h
 *
 * Purpose:  In this implementation we must collect datapoints to be
 *           painted.  Data my be from a separate decode module but
 *           must come here in a format to be plotted (i.e. amp and phase)
 *           The range of each must be limited to the range of the scale
 *           of the plot.
 *
 * TODO:     Set up range of plot to match data from decoder.
 *           Develop other signals and public slots or functions needed.
 *
 *********************************************************************/
#include <QtCore/QDebug>

#include "iqdata.hpp"

string inttostring(int i)
{
    string s;
    //stringstream out;
    //out << i;
    //s = out.str();

    return s;
}

IQData::IQData(QWidget *parent) : QThread(parent)
{
    //
}

IQData::~IQData()
{
    //
}

void IQData::run()
{
    qDebug() << "iqdata run()";

    x.clear();
    y.clear();
    int loop_counter = persistence;
    // this may not be needed as we plot data as it arrives
    // or after End Of Transmission.
    do
    {
        if (loop_counter) loop_counter--;
        sweep();
    }
    while (loop_counter || loop);
    ////
    close(frontend_fd);
}

void IQData::sweep()
{
    /*
    // used in Linix only  //
    struct dvb_fe_constellation_samples const_samples;
    struct dvb_fe_constellation_sample samples[DTV_MAX_CONSTELLATION_SAMPLES];
    //
    const_samples.num = DTV_MAX_CONSTELLATION_SAMPLES;
    const_samples.samples = samples;

    //if ((ioctl(frontend_fd, FE_GET_CONSTELLATION_SAMPLES, &const_samples)) == -1) {
    //    qDebug() << "ERROR: FE_GET_CONSTELLATION_SAMPLES";
    //    return;
    //}
    for (unsigned int i = 0 ; i < const_samples.size ; i++) {
        while (x.size() >= const_samples.size * persistence) {
            x.erase(x.begin());
            y.erase(y.begin());
        }
        x.append(samples[i].imaginary / 256);
        y.append(samples[i].real / 256);
    }
    emit signaldraw(x, y);
    */
    qDebug() << "sweep() complete";
}

void IQData::setup(int t_adapter, int t_loop, int t_persistence)
{
    adapter = t_adapter;
    loop = t_loop;
    persistence = t_persistence;

    qDebug() << "setup() end";
}

void IQData::closeadapter()
{
    //
}

