/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/

/*****************************************************************************
 * scrolling SWaterfall display
 *
 * creation:
 *      call to SWaterfall constructor
 *
 * initialization:
 *      call to SWaterfall::initialize(int tileWidth, int tileHeight, int windowSize)
 *              args:
 *                  tileWidth   width of one tile in pixels
 *                  tileHeight  height of tile, also SWaterfall window height
 *                  windowSize  window width in pixels
 *      This will redraw SWaterfall widget to appropriate size.
 *
 * drawing:
 *      call to SWaterfall::newData(ccharptr buffer)
 *              args:
 *                  buffer  signed char buffer. Buffer size must at least tileWidth
 *                      do not forget to register ccharptr before attempting  connection
 *                      between signal and slot:
 *                          typedef const char *  ccharptr;
 *                          qRegisterMetaType<ccharptr>("ccharptr");
 *
 * Data length must be equal to tileWidth
 *
 * When data is added by a call to newData(), previously first tile is erased and is updated
 * with the new data. It becomes the rightmost tile.
 * Then window is updated.
 * CPU time is just required to erase and draw curve on one tile, and to
 * move tiles to the canvas.
 * CPU load to draw a SWaterfall is about 1/10th of CPU required to demodulate and decode
 * Pactor.
 ****************************************************************************/
#include <QtCore/QDebug>
#include <QtGui/QPainter>
#include <QtGui/QResizeEvent>

#include "globals.h"

#include "simplewaterfall.h"


SWaterfall::SWaterfall(QWidget *parent) : QWidget(parent)
    ,m_active(false)
    ,m_dataLength(0)
    ,m_position(0)
    ,m_tileHeight(0)
    ,m_tileArrayStart(0)
    ,m_windowPosition(0)
    ,m_windowLength(0)
    ,init_mode(NOT_INITIALIZED)
    ,fft_object(new SimpleFFT(this, FFT_POINTS))
    ,m_tiles(NULL)
{
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

    setMinimumHeight(50);
    setMinimumWidth(400);
    // allocate buffers
    fft_input   = new float[FFT_POINTS];
}

SWaterfall::~SWaterfall()
{
    deletePixmaps();
    delete     fft_input;
    delete     fft_output;
}

void SWaterfall::paintEvent(QPaintEvent * /*event*/)
{
    QPainter painter(this);
    QRect destRect;
    QRect sourceRect;

    if (init_mode == SPECTRUM_INITIALIZED)
    {
        painter.fillRect(rect(), Qt::lightGray);

        if (m_active)
        {
            Tile &tile         = m_tiles[0];

            if (tile.painted)
            {
                destRect = rect();

                sourceRect = QRect(QPoint(), m_pixmapSize);
                painter.drawPixmap(destRect, *tile.pixmap, sourceRect);
            }
        }
    }
    else
        if (init_mode == WATERFALL_INITIALIZED)
        {
            if (m_active)
            {
                int pixmap_width    = m_tileWidth;
                int destLeft        = 0;
                int destRight       = pixmap_width;

                int     tile_index  = first_tile_index;
                for (int i = 0; i < m_tiles_count; i++)
                {
                    Tile &tile = m_tiles[tile_index];

                    if (tile.painted)
                    {
                        // x, y, w, h
                        destRect = QRect((int)0, (int)i, (int)m_tileWidth, (int)1);

                        sourceRect = QRect((int)0, (int)0, (int)m_tileWidth, (int)1);

                        WAVEFORM_DEBUG << "SWaterfall::paintEvent" << "tileIndex" << tile_index
                                       << "source" << 0 << pixmap_width
                                       << "dest" << destLeft << destRight;

                        painter.drawPixmap(destRect, *tile.pixmap, sourceRect);
                    }
                    // next tile
                    tile_index  = (tile_index + 1) % m_tiles_count;
                }
            }
        }
}

void SWaterfall::resizeEvent(QResizeEvent *event)
{
    if ((event->size() != event->oldSize())
        && (init_mode == WATERFALL_INITIALIZED))
    {
        createPixmaps(event->size());
    }
}

void SWaterfall::initializeSpectrum(int windowHeight, int windowWidth)
{
    QSize   tile_size;

    PACTOR_TNC_DEBUG/*WAVEFORM_DEBUG*/ << "Spectrum::initialize"
                                       << "windowSize" << windowHeight << windowWidth;

    init_mode   = SPECTRUM_INITIALIZED;
    setMinimumWidth(windowWidth);
    setMinimumHeight(windowHeight);
    reset();

    /************************ create tile *********************/
    // Calculate tile size
    m_tileHeight    = windowHeight;
    m_tileWidth     = windowWidth;
    tile_size.setHeight(m_tileHeight);
    tile_size.setWidth(m_tileWidth);

    // Calculate window size
    m_tiles_count   = 1;

    // allocate tiles array
    if (m_tiles != NULL)
    {
        try
        {
            delete m_tiles;
        }
        catch
            (...)
        {
            PACTORUI_DEBUG << "delete error 6";
        }
    }
    m_tiles         = new Tile[m_tiles_count];

    WAVEFORM_DEBUG << "SWaterfall::initialize"
                   << "tile size" << m_tileHeight << m_tileWidth
                   << "nTiles" << m_tiles_count;

    m_pixmaps.fill(0, m_tiles_count);

    createPixmaps(tile_size);

    m_active = true;
}

void SWaterfall::initializeWaterfall(int windowHeight, int windowWidth)
{
    QSize   tile_size;

    PACTOR_TNC_DEBUG/*WAVEFORM_DEBUG*/ << "SWaterfall::initialize"
                                       << "windowSize" << windowHeight << windowWidth;

    init_mode   = WATERFALL_INITIALIZED;

    setMinimumWidth(windowWidth);
    setMinimumHeight(windowHeight);
    reset();

    /************************ create tile *********************/
    // Calculate tile size
    m_tileHeight    = 1;
    m_tileWidth     = windowWidth;
    tile_size.setHeight(m_tileHeight);
    tile_size.setWidth(m_tileWidth);

    // Calculate window size
    m_tiles_count   = windowHeight;

    // allocate tiles array
    if (m_tiles != NULL)
    {
        try
        {
            delete m_tiles;
        }
        catch
            (...)
        {
            PACTORUI_DEBUG << "delete error 7";
        }
    }
    m_tiles         = new Tile[m_tiles_count];

    WAVEFORM_DEBUG << "SWaterfall::initialize"
                   << "tile size" << m_tileHeight << m_tileWidth
                   << "nTiles" << m_tiles_count;

    m_pixmaps.fill(0, m_tiles_count);

    createPixmaps(tile_size);

    m_active = true;
}

void SWaterfall::reset()
{
    WAVEFORM_DEBUG << "SWaterfall::reset";

    m_dataLength = 0;
    m_position = 0;
    m_active = false;
    deletePixmaps();
    m_tileHeight = 0;
    m_tileArrayStart = 0;
    m_windowPosition = 0;
    m_windowLength = 0;
    painted_tiles   = 0;
    previousCurveValue = 0.0;
}

// public SLOT
void SWaterfall::newData(cdoubleptr buffer, qint16 count)
{
    // compute fft
    fft_output  = fft_object->compute_fft((const double *)buffer);

    if (init_mode == SPECTRUM_INITIALIZED)
    {
        // start painting tiles
        paintTile(0, fft_output, count / 2);
        update();
    }
    else
        if (init_mode == WATERFALL_INITIALIZED)
        {
            // start painting tiles
            if (painted_tiles < m_tiles_count)
            {
                const Tile &tile = m_tiles[painted_tiles];

                if (!tile.painted)
                {
                    paintTile(painted_tiles, fft_output, count / 2);
                    current_tile    = painted_tiles;
                    next_free_tile  = (current_tile + 1) % m_tiles_count;
                }
                painted_tiles++;
            }
            // shift tiles
            else
            {
                shuffleTiles(1);
                WAVEFORM_DEBUG << "SWaterfall::paint tile" << current_tile ;
                paintTile(current_tile, fft_output, count / 2);
            }
            update();
        }

}

void SWaterfall::deletePixmaps()
{
    QPixmap *pixmap;
    foreach(pixmap, m_pixmaps)
    {
        try
        {
            delete pixmap;
        }
        catch
            (...)
        {
            PACTORUI_DEBUG << "delete error 4";
        }
    }
    m_pixmaps.clear();
    painted_tiles       = 0;
    previousCurveValue  = 0.0;
    next_free_tile      = 0;
    first_tile_index    = 0;
}

void SWaterfall::createPixmaps(const QSize &widgetSize)
{
    painted_tiles   = 0;
    m_pixmapSize = widgetSize;

    WAVEFORM_DEBUG << "SWaterfall::createPixmaps"
                   << "widgetSize" << widgetSize
                   << "pixmapSize" << m_pixmapSize;

    // (Re)create pixmaps
    for (int i = 0; i < m_pixmaps.size(); ++i)
    {
        try
        {
            delete m_pixmaps[i];
        }
        catch
            (...)
        {
            PACTORUI_DEBUG << "delete error 5";
        }
        m_pixmaps[i] = 0;
        m_pixmaps[i] = new QPixmap(m_pixmapSize);
    }

    // Update tile pixmap pointers, and mark for repainting
    for (int i = 0; i < m_tiles_count; ++i)
    {
        m_tiles[i].pixmap = m_pixmaps[i];
        m_tiles[i].painted = false;
        m_tiles[i].index    = i;
    }
}

int SWaterfall::tilePixelOffset(qint64 positionOffset) const
{
    Q_ASSERT(positionOffset >= 0 && positionOffset <= m_tileHeight);
    const int result = (qreal(positionOffset) / m_tileHeight) * m_pixmapSize.width();
    return result;
}

void SWaterfall::paintTile(int index, cdoubleptr buffer, int count)
{
    WAVEFORM_DEBUG << "SWaterfall::paintTile" << "index" << index;
    int     red;
    int     green;
    int     blue;
    int     pos_x;
    int     freqLo;
    int     freqHi;

    Tile &tile = m_tiles[index];

    QPainter painter(tile.pixmap);
    painter.fillRect(tile.pixmap->rect(), Qt::lightGray);

    if (init_mode == SPECTRUM_INITIALIZED)
    {
        freqLo        = (m_pixmapSize.width() * (center_frequency - 100)) / 4000;
        freqHi        = (m_pixmapSize.width() * (center_frequency + 100)) / 4000;

        QPen pen(Qt::darkBlue);
        painter.setPen(pen);
        // Calculate initial point
        const QPoint origin(0, m_pixmapSize.height());

        QLine line(origin, origin);
        for (int i = 0; i < count; ++i)
        {
            pos_x               = (int)((float)i / (float)count * (float)m_tileWidth);
            previousCurveValue  = buffer[i];

            //int y = m_pixmapSize.height() - (((buffer[i]) / 4.0) * m_pixmapSize.height());
            int y = m_pixmapSize.height() - (((buffer[i] + 3.0) / 5.0) * m_pixmapSize.height());
            if (y < 0)
            {
                y   = 0;
            }
            else
                if (y > m_pixmapSize.height())
                {
                    y   = m_pixmapSize.height();
                }

            line.setP2(QPoint(pos_x, y));
            painter.drawLine(line);
            line.setP1(line.p2());
        }
        // draw vertical lines for frequency
        for (int freq = 0; freq < m_pixmapSize.width(); freq += ((m_pixmapSize.width() * 500) / 4000))
        {
            line.setP1(QPoint(freq, 0));
            line.setP2(QPoint(freq, m_tileHeight));
            painter.drawLine(line);
        }
        // draw horizontal lines for amplitudes (dB)
        for (int ampl = 0; ampl < m_pixmapSize.height(); ampl += (m_pixmapSize.height() / 5))
        {
            line.setP1(QPoint(0, ampl));
            line.setP2(QPoint(m_tileWidth, ampl));
            painter.drawLine(line);
        }
        // draw vertical lines for FSK frequencies
        QPen pen2(Qt::darkRed);
        painter.setPen(pen2);
        line.setP1(QPoint(freqLo, 0));
        line.setP2(QPoint(freqLo, m_tileHeight));
        painter.drawLine(line);
        line.setP1(QPoint(freqHi, 0));
        line.setP2(QPoint(freqHi, m_tileHeight));
        painter.drawLine(line);
    }
    else
        if (init_mode == WATERFALL_INITIALIZED)
        {
            freqLo    = (int)((float)count * (((float)center_frequency - (float)100.0) / (float)4000.0));
            freqHi    = (int)((float)count * (((float)center_frequency + (float)100.0) / (float)4000.0));

            // Calculate initial point
            const QPoint origin(0, 0);

            QLine line(origin, origin);

            for (int i = 0; i < count; ++i)
            {
                if ((i == freqLo) || (i == freqHi))
                {
                    red         = 255;
                    green       = 50;
                    blue        = 50;
                }
                else
                {
                    red = ((buffer[i] + 3.0) / 5.0) * 255.0;
                    if (red > 255)
                    {
                        red = 255;
                    }
                    else
                        if (red < 0)
                        {
                            red = 0;
                        }
                    green       = red;
                    blue        = 255 - red;
                }
                QPen pen(QColor(red, green, blue));
                painter.setPen(pen);

                pos_x               = (int)((float)i / (float)count * (float)m_tileWidth);
                line.setP2(QPoint(pos_x, 0));
                painter.drawLine(line);
                line.setP1(line.p2());
            }
        }
    tile.painted = true;
}

void SWaterfall::shuffleTiles(int n)
{
    WAVEFORM_DEBUG << "SWaterfall::shuffleTiles" << "n" << n;

    while (n--)
    {
        Tile &tile     = m_tiles[next_free_tile];
        tile.painted    = false;
        current_tile    = next_free_tile;
        first_tile_index    = (first_tile_index + 1) % m_tiles_count;
        next_free_tile  = (current_tile + 1) % m_tiles_count;
    }

    m_tileArrayStart    = m_tileHeight * next_free_tile;
    WAVEFORM_DEBUG << "SWaterfall::shuffleTiles" << "tileArrayStart" << m_tileArrayStart
                   << "next index" << next_free_tile
                   << "painted" << m_tiles[next_free_tile].painted;
}

void SWaterfall::setCenterFrequency(int frequency)
{
    center_frequency    = frequency;
}
