#include "asciipacker.hpp"

AsciiPacker::AsciiPacker(QObject *parent) : QObject(parent)
{
    //
}
/// \brief AsciiPacker::pack Pack 6 bit ascii chars into 8 bit bytes
/// This method packs ascii chars into 8-bit bytes according to a method
/// used in WinMOR.
///
/// The packing method works as follows. Imagine that we have an array
/// of characters, which are all guaranteed to be not less than
/// 32 and less than 96 ASCII, which includes all uppercase letters and digits.
/// If we subtract 32 from each of them we lose no information and
/// now each is less than 64. That means that they all fit in 6 bits, so two
/// bits have been saved. We will take 4 of these 6-bit chars, which is 24 bits
/// and pack them into 3 8-bit bytes, also 24 bits.
///
/// The scheme for doing this is diagrammed below. ABCD are the input
/// characters, with 32 subtracted so that the top two bits are zero.
/// A is the character with the lowest index.  Similarly XYZ are the output
/// bytes but all 8 bits are used. X has the lowest index.
///
/// Input bytes with 6 bits each:  AAAAAAAABBBBBBBBCCCCCCCCDDDDDDDD
/// Bit numbers with 'x' ignored:  xx543210xx543210xx543210xx543210
/// Output bytes with 8 bits each: XXXXXX  XXYYYY  YYYYZZ  ZZZZZZ
/// Bit numbers:                   765432  107654  321076  543210
///
/// The code disassembles the input characters in 4 byte chunks
/// and reassembles them into 8 byte chunks using the above mapping.
/// \param An array of characters to pack, all >=32 and <96 ASCII.
/// \return A packed vector of bytes.
///
outbytes AsciiPacker::pack(inchars chars)
{
    outbytes outs;
    int ichars = 0;
    while ( ichars < chars.size() )
    {
        // Take a group of four chars off the input.
        // If the input is not a multiple of 4 (should be) just
        // assume the remaining chars are spaces; i.e. zeroes.
        //TODO: inchars has been defined as QString - which may be utf-8 depending on system
        //      default so the QString utilities should be used here
        quint8 A = chars[ichars+0].toAscii() - 32;
        quint8 B = (ichars+1<chars.size()?chars[ichars+1].toAscii()-32:0);
        quint8 C = (ichars+2<chars.size()?chars[ichars+2].toAscii()-32:0);
        quint8 D = (ichars+3<chars.size()?chars[ichars+3].toAscii()-32:0);

        outs.push_back( ((A & 0x3f)<<2) | ((B & 0x30)>>4) ); // X
        outs.push_back( ((B & 0x0f)<<4) | ((C & 0x3c)>>2) ); // Y
        outs.push_back( ((C & 0x03)<<6) | (D & 0x3f)      ); // Z

        ichars += 4;
    }
    return outs;
}

outbytes AsciiPacker::unpack(outbytes)
{
    return outbytes();
}
