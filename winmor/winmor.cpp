#include "winmor.hpp"

/*  this is just stub code for you to use until you have your own code to put here */
WinMor::WinMor(QObject *parent) : QObject(parent)
{
    // This is the public codec interface to the modem module.

    tnc     = new WinMorTNC();     // create WinMor TNC
    tnc->show();
    tnc->initTNC();
}

WinMor::~WinMor() { }

// talk to your codec from here to respond to commands from API
int  WinMor::setBW(int bw)
{
    // this one changes bandwidth in the modulator, demodulator using the TNC
    if ( !bw )
    {
        qDebug() << "Changing Bandwidth to " << bw << " hz";
        // use methods exposed in the TNC that allow for bandwidth changes
        tnc->setBW(bw);
        return true;
    }
    else
    {
        qDebug() << "Returning Bandwidth " << m_bw << " hz";
        return m_bw;
    }
}
/*
void WinMor::slotOutboundDataReady()
{
    // here because TNC signaled there is outbound data
    // data should be collected and sent to modem interface
    // which will send data over UDP channel to API
}
*/
void WinMor::init_modem()
{
    // This is the public codec interface to the modem module.
    tnc = new WinMorTNC();     // create WinMor TNC
    tnc->initTNC();
    tnc->show();
}

bool WinMor::autoBreak(bool ab) { Q_UNUSED(ab); return false; }
bool WinMor::blocked() { return false; }
bool WinMor::blockSignals(bool b) { Q_UNUSED(b); return false; }
int  WinMor::buffers() { return 0; }
bool WinMor::busy() { return false; }
bool WinMor::busylock(bool lock) { Q_UNUSED(lock); return false; }
QString WinMor::capture(QString devname) { Q_UNUSED(devname); return ""; }
QString WinMor::captureDevices(QString devlist) { Q_UNUSED(devlist); return ""; }
void WinMor::close() {}
QString WinMor::codec(QString codecname) { Q_UNUSED(codecname); return ""; }
QString WinMor::connectRemote(QString callsign) { Q_UNUSED(callsign); return ""; }
QString WinMor::curState() { return "Idle"; }
bool WinMor::cwID(bool id) { Q_UNUSED(id); return false; }
bool WinMor::debugLog(bool log) { Q_UNUSED(log); return false; }
void WinMor::dirtyDisconnect() {}
int  WinMor::driveLevel(int lvl) { Q_UNUSED(lvl); return 0; }
QString WinMor::fault() { return errString; }  // return codec errString
bool WinMor::fecRcv(bool frcv) { Q_UNUSED(frcv); return false; }
void WinMor::fecsend(QString snd) { Q_UNUSED(snd); }
QString WinMor::gridSquare(QString grid) { Q_UNUSED(grid); return ""; }
int  WinMor::leaderExt(int ext) { Q_UNUSED(ext); return 0; }
bool WinMor::listen(bool lis) { Q_UNUSED(lis); return false; }
int  WinMor::maxConReq(int max) { Q_UNUSED(max); return 0; }
QString WinMor::monCall() { return ""; }
QString WinMor::myAux(QString aux) { Q_UNUSED(aux); return ""; }
QString WinMor::myC(QString call) { Q_UNUSED(call); return ""; }
QString WinMor::newState(QString newstate) { Q_UNUSED(newstate); return ""; }
int  WinMor::offset() { return 0; }
int  WinMor::outQueued(int out) { Q_UNUSED(out); return 0; }
void WinMor::over() {}
QString WinMor::playback(QString dev) { Q_UNUSED(dev); return ""; }
QString WinMor::playDevs() { return ""; }
QString WinMor::ports() { return ""; }
int  WinMor::responseDly(int dly) { Q_UNUSED(dly); return 0; }
bool WinMor::robust(bool setRobust) { Q_UNUSED(setRobust); return false; }
void WinMor::sendBreak() {}
int WinMor::sendID(int id) { Q_UNUSED(id); return 0; }
QString WinMor::setMode(QString set) { Q_UNUSED(set); return ""; }
void WinMor::setSuffix(QString x) {Q_UNUSED(x); }
bool WinMor::setVox(bool vox) { Q_UNUSED(vox); return false; }
void WinMor::showTNC(bool shw) { Q_UNUSED(shw); }
int  WinMor::squelchLvl(int lvl) { Q_UNUSED(lvl); return 0; }
void WinMor::twoToneText(bool t) { Q_UNUSED(t); }
QString WinMor::version() { return ""; }
