#include "modulator.hpp"

quint8 Modulator::hamming84Table[16] = {0x00, 0x71, 0xE2, 0x93, 0xE4, 0xA5, 0x36, 0x47, 0xB8, 0xC9, 0x5A, 0x2B, 0x6C, 0x1D, 0x8E, 0xFF};

Modulator::Modulator(QString portname, quint32 buffersize, QObject *parent) : QOutPort(portname,buffersize,parent)
{
    // Create and initialize the fft object.
    icfft128 = new InverseComplexFFT(samplesPerSymbol);    // TODO: I don't think we need a separate one for each frame, but...
    // Create one for 256 sample jobs.
    icfft256 = new InverseComplexFFT(samplesPerSymbol*2);   // TODO: Investigate if using the icfft128 twice might be better.
    for(int i=0;i<samplesPerSymbol;i++)
    {
        rootRaisedCosineWindow[i] = sin(i * M_PI / (samplesPerSymbol-1)); //TODO: why -1? I'd think that it should be -0 actually.
    }
    txIdle = true;
}

void Modulator::setData(void *data, quint32 nFrames)
{
    float *fdata = (float *)data;
    if ( txIdle )
    {
        // Transmitter is idle. This means that we are to send
        // only zeroes into the signal stream.
        for ( quint32 i = 0; i < nFrames; i++ )
        {
            fdata[i] = 0;
        }
    }
    else // Tx is not idle.
    {
        // We need nFrames samples of real data.
        for ( quint32 i = 0; i < nFrames; i++ )
        {
            // Generate a single sample of random data. Copy it in from
            // the wave buffer.
            // TODO: There is an issue here about what to do if we don't
            // TODO: have enough data in the wave buffer to satisfy JACK.
            // TODO: For the time being we are going to just insert some
            // TODO: silence - a dropout, in other words.
            // TODO: there may be a way to write the data directly into
            // TODO: the JACK buffer - but I don't know how right now.
            if ( waveBuffer.empty() )
            {
                fdata[i] = 0.;  // Silence for one sample.
                txIdle = true;  // Silence forever afterward.
                emit txFinished();  // Done with this frame, bye.
            }
            else
            {
                fdata[i] = waveBuffer.front(); // Signal.
                waveBuffer.pop();
            }
        }
    }
}

void Modulator::startTx()
{
    txIdle = false; //TODO: will be replaced by proper SM logic
}

void Modulator::appendPilot()
{
    // Put the right number of pilot tones on the beginning.

    // Initialize ifft input arrays to zeroes.
    // TODO: we can probably use a cheaper real-only fft here.
    icfft128->clearInData();

    // Add a single coefficient at center freq of pilot tone:
    //   samplingRate*16/samplesPerSymbol = 12000Hz*16/128 = 1500Hz
    // This the 16th harmonic of the fundamental wave at 12000Hz/128.
    // TODO: Allowing for a future sampling rate other than 12000Hz gives:
    //   harmonic = 1500Hz * samplesPerSymbol/samplingRate
    int pilotHarmonic = pilotFrequency * samplesPerSymbol / samplingRate;
    real(icfft128->inData[pilotHarmonic]) = 1;

//    fcout("Pilot tone Coefficients data input:",samplesPerSymbol,icfft128->inData);

    // Do an inverse fft to generate this tone.
    icfft128->execute();
//    fcout("Inverse transform data out:",samplesPerSymbol,icfft128->outData);

    // A temporary table we'll use to generate successive pilot tones cheaply.
    sample_t pilottable[samplesPerSymbol];
    for(int i=0;i<samplesPerSymbol;i++)
    {
        // Calculate waveform of pilot burst including envelope.
        pilottable[i] = real(icfft128->outData[i]) * peakAmplitude * rootRaisedCosineWindow[i];
    }

//    cout<<"pilottable:"<<endl;
//    for(int i=0;i<samplesPerSymbol;i++)
//    {
//        cout<<i<<": "<<pilottable[i]<<endl;
//    }
//    cout<<endl;

    // The min number of bursts to generate is fixed by the protocol at 20
    // but the vox config allows you to add up to 12 more.
    // The vox config just adds some bursts to acommodate the time it takes
    // for the vox to actuate the PTT on slow radios.
    // TODO: actually make this a setting.
    const int numbursts = leaderLength + voxPilotExtension;

    // Now write the pilot bursts onto the wave buffer.
    // The pilot bursts alternate in phase, either 0 deg, or 180 deg (inverted).
    // The spec says that the last two must be the same phase and scope shows
    // it to be positive phase, same as the following 4FSK.
    // So whether we need to start
    // with an inverted or uninverted burst depends on how many bursts we
    // are going to transmit; specifically whether it is even or odd.
    // If it is even the last one will be opposite in phase to the first
    // one. The two coming after the last one must be positive so last one
    // has to be negative, thus the first one must be positive.
    // If it is an odd number the reverse argument applies so the first
    // one must be negative.
    // TODO: this isn't exactly how Rick does it but it should work anyway
    // and it is one hell of a lot easier to understand and code!
    int burstsign = ( (leaderLength + voxPilotExtension) % 2 == 0 ? 1 : -1 );

    for ( int iburst = 0; iburst < numbursts - 2; iburst++ )
    {
        // Copy the data into the wave buffer with the right sign.
        // TODO: there is probably a faster way to get this done.
        for ( int isample = 0; isample < samplesPerSymbol; isample++ )
        {
            waveBuffer.push(burstsign * pilottable[isample]);
        }
        burstsign *= -1; // Invert the sign for the next burst.
    }

    // Add the last two bursts with positive phase.
    for ( int iburst = 0; iburst < 2; iburst++ )
    {
        for ( int isample = 0; isample < samplesPerSymbol; isample++ )
        {
            waveBuffer.push( pilottable[isample] );
        }
    }

//    //TODO: debugging crap
//    cout<<"waveBuffer:"<<endl;
//    for(int i=0;i<(int)waveBuffer.size();i++)
//    {
//        cout<<i<<": "<<waveBuffer[i]<<endl;
//    }

}


void Modulator::appendFrameType(int frameType)
{

    // frameType is 4 bits, look it up in the 16-element Hamming8,4 table.
    // The result it 8 bits.
    quint8 hamm84ft = hamming84Table[frameType];

    // We are going to transmit this as 4 symbols of 4FSK, 2 bits per symbol.
    // This could be a loop but there are only 4 indexes and two of them are
    // special so let's just write it out, starting as MSB end.

    // Take the first slice of two bits,
    quint8 symb = 0x03 & (hamm84ft>>6);  // xx?????? -> 000000xx

    // The modulation to be used is 4FSK but does not match any of the
    // modes mentioned in the protocol document.
    // The frequencies used are actually the 31,32,33,35 harmonics
    // of sampling rate/256, or:
    // 1453.125 = 00, 1500.00 = 01, 1546.875 = 10, 1593.75 = 11
    // Set the appropriate coefficient as follows:
    int baseFreq = pilotFrequency * 2 * samplesPerSymbol / samplingRate - 1;
    icfft256->clearInData();
    icfft256->inData[baseFreq+symb] = peakAmplitude;
    icfft256->execute();

    // Now append the data onto the wave buffer but avoid an initial click
    // with a rising slope.
    for ( int isample = 0; isample < samplesPerSymbol / 2; isample++ )
    {
        waveBuffer.push( real(icfft256->outData[isample])*rootRaisedCosineWindow[isample] );
    }
    // Continue at full scale amplitude.
    for ( int isample = samplesPerSymbol / 2; isample < samplesPerSymbol * 2; isample++ )
    {
        waveBuffer.push( real(icfft256->outData[isample]) );
    }

    // Take the second slice of the hamming encoded frametype.
    symb = 0x03 & (hamm84ft>>4);  // ??xx???? -> 000000xx

    icfft256->clearInData();
    icfft256->inData[baseFreq+symb] = peakAmplitude;
    icfft256->execute();

    // No need this time to shape the start because it is already full-scale.
    for ( int isample = 0; isample < samplesPerSymbol * 2; isample++ )
    {
        waveBuffer.push( real(icfft256->outData[isample]) );
    }

    // Take the third slice of the hamming encoded frametype.
    symb = 0x03 & (hamm84ft>>2);  // ????xx?? -> 000000xx

    icfft256->clearInData();
    icfft256->inData[baseFreq+symb] = peakAmplitude;
    icfft256->execute();

    // Still no need to shape the start.
    for ( int isample = 0; isample < samplesPerSymbol * 2; isample++ )
    {
        waveBuffer.push( real(icfft256->outData[isample]) );
    }

    // Take the last slice of the hamming encoded frametype.
    symb = 0x03 & (hamm84ft);  // ??????xx -> 000000xx

    icfft256->clearInData();
    icfft256->inData[baseFreq+symb] = peakAmplitude;
    icfft256->execute();

    // No shaping for the first 3/4 of the wave
    for ( int isample = 0; isample < (samplesPerSymbol * 2 - samplesPerSymbol / 2); isample++ )
    {
        waveBuffer.push( real(icfft256->outData[isample]) );
    }
    // Then a falling envelope for the last quarter. We need the second half
    // of the root raised cosine table here too.
    for ( int isample = ( samplesPerSymbol * 2 - samplesPerSymbol / 2); isample < samplesPerSymbol * 2; isample++ )
    {
        //int rrcosindex = isample - (samplesPerSymbol*2 - samplesPerSymbol/2) + samplesPerSymbol/2;
        int rrcosindex = isample - samplesPerSymbol; // Do the algebra!
        waveBuffer.push( real(icfft256->outData[isample]) * rootRaisedCosineWindow[rrcosindex] );
    }
}

void Modulator::modulateMultiCarrier4FSK(byte_vec symbols, int nCarriers)
{
    //TODO: replace symbols input with encoded data input. Modulator
    // should know how to symbolize its own data.

    int fftLength = 256;    // Number of samples in one FFT.
    int centerBin = fftLength*centerFrequency/samplingRate; // 32 for 12kHz

    // There will be nCarriers frequencies in use, each with its own bin.
    // There are 4 frequencies, and so 4 bins, per carrier.
    // They are contiguous, except the center one is not used.
    // So half the bins are below and center and half above.
    // THe lowest bin is for the lowest freq of the lowest carrier
    // so it is below the center by half of the number bins required:
    int lowestBin = centerBin - 4*nCarriers/2;

    // Because we are adding a bunch of carriers together (maybe 8)
    // the amplitude is going to go up as we add carriers. Therefore
    // we scale them back a bit. As the number of carriers goes up
    // we can scale them a little less because they tend to cancel
    // out more. This is a trial-and-error thing...
    float carrierScale = peakAmplitude;    // Should not happen.
    if(nCarriers == 2) carrierScale = 1.0 * peakAmplitude/nCarriers;
    if(nCarriers == 8) carrierScale = 1.14 * peakAmplitude/nCarriers;

    int symbolsPerCarrier = symbols.size()/nCarriers;

    for ( int iSymbol = 0; iSymbol < symbolsPerCarrier; iSymbol++ )
    {
        icfft256->clearInData(); // Wipe all coefficients to zero.

//        qDebug("iSymbol: %d",iSymbol);
        // This loop covers the lower half of the carriers.
        for ( int iCarrier = 0; iCarrier < nCarriers / 2; iCarrier++ )
        {
//            qDebug("iCarrier: %d",iCarrier);
            // The symbols are laid out in stripes, each stripe is
            // symbolsPerCarrier symbols long, nCarriers of them.
            quint8 symb = symbols[iCarrier*symbolsPerCarrier + iSymbol];
//            qDebug("symb: %d",symb);
            real(icfft256->inData[lowestBin + 4*iCarrier + symb]) = carrierScale;
        }
        // This loop covers the upper half of the carriers.
        for ( int iCarrier = nCarriers / 2; iCarrier < nCarriers; iCarrier++ )
        {
//            qDebug("iCarrier: %d",iCarrier);
            quint8 symb = symbols[iCarrier*symbolsPerCarrier + iSymbol];
//            qDebug("symb: %d",symb);
            // Note the "+1" below. That's because the center bin is skipped.
            // Not sure but guessing that the negative frequency is to
            // make half of them out of phase to get lower amplitude peaks.
            real(icfft256->inData[lowestBin + 4*iCarrier + symb + 1]) = -carrierScale;
        }

//        qDebug("ifft reals: ");
//        for(int i=0;i<fftLength;i++)
//        {
//            complex<double> c = icfft256->inData[i];
//            qDebug("Real: %g  Imag: %g ",real(c),imag(c));
//        }

        icfft256->execute();
        for ( int i = 0; i < fftLength; i++ )
        {
            // Dump the computed samples onto the output
            waveBuffer.push(real(icfft256->outData[i]));
        }
    }
}

