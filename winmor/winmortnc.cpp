#include <QtCore/QSettings>
#include <QtCore/QState>
#include <QtCore/QStateMachine>
#include <QtCore/QScopedPointer>
#include <QtCore/QDebug>
#include <QtGui/QPicture>
#include <QtWidgets/QStyle>
#include <QtWidgets/QApplication>
#include <QtWidgets/QAction>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>

//#define SHOW_WINMORTNC_DEBUG

#include "winmorsession.hpp"

#include "winmortnc.hpp"

const int NullTimerId = -1;
//
// This is the GUI for the WinMOR modem...displays of mode and status, etc. //
//
WinMorTNC::WinMorTNC() : QMainWindow()
{
#ifdef SHOW_WINMORTNC_DEBUG
    WINMORTNC_DEBUG("Constructor Starting...");
#endif
    readSettings();   // get stored values for control block
    m_mode = ModeIdle;
    m_infoMessageTimerId = NullTimerId;
#ifdef SHOW_WINMORTNC_DEBUG
    WINMORTNC_DEBUG("Setting up GUI ");
#endif
    createObjects();
    createActions();
    createMenus();
    connectSignals();
    translateText();

#ifdef SHOW_WINMORTNC_DEBUG
    WINMORTNC_DEBUG("Creating modem ");
#endif
    //modem = new OpenModem();
    session = new WinMorSession(); // create session UI
    session->show();
}

WinMorTNC::~WinMorTNC()
{
    //
}

//-------------------------------------------------------------------
//     Event Handlers
//-------------------------------------------------------------------
void WinMorTNC::closeEvent(QCloseEvent *)
{
    if ( settings_dirty ) writeSettings();

    try
    {
        session->close();
        delete session;
        session = 0;
    }
    catch (...) {};

    try
    {
        delete modem;
        modem = 0;
    }
    catch (...) {};

    /*  enable when state machine is ready for use
     *  state machine destruction is not automatic
     *  as other Qt objects are
    try
    {
        m_statemachine->stop();
        m_statemachine = 0;
    }
    catch (...) {};
    */

    try
    {
        delete buttonPanel;
        buttonPanel=0;
    }
    catch (...) {};
}

void WinMorTNC::resizeEvent(QResizeEvent *)
{
    // This method is prefered over layouts
    int w, h;

    h = centralwidget->height();
    w = centralwidget->width();
    pos = QPoint(this->x(),this->y());
    size = QSize(w,h);
    writeSettings();
    //grpLED->resize(w - 10,35);
    //grpDisplay->resize(w - 10,20);
    //group->resize(w - 10, h - 110);
}

void WinMorTNC::moveEvent(QMoveEvent *)
{
    writeSettings();
}

void WinMorTNC::changeEvent(QEvent *e)
{
  QMainWindow::changeEvent(e);
  switch (e->type())
  {
  case QEvent::LanguageChange:
    translateText();
    break;

  default:
    break;
  }
}


//-------------------------------------------------------------------
//     Public slots
//-------------------------------------------------------------------
void WinMorTNC::setTestOn()
{
    //
#ifdef SHOW_WINMORTNC_DEBUG
    WINMORTNC_DEBUG("Test Mode Set On");
#endif
    ledXMIT->setPixmap(redon->scaled(QSize(15,5)));
    ledXMIT->setEnabled(true);
    //soundinput->readInput();
}

void WinMorTNC::setTestOff()
{
#ifdef SHOW_WINMORTNC_DEBUG
    WINMORTNC_DEBUG("Test Mode Set OFF");
#endif
    //
    ledXMIT->setPixmap(blueon->scaled(QSize(15,5)));
    ledXMIT->setEnabled(true);
}


quint32 WinMorTNC::sampleAudioLvl()
{
    // TODO: when common audio ring buffer is established - develop this
    // called periodically to update vu_meter  //
    // period is often enough to be effective  //
    // but not often enough to impact decoder  //
    // which operates in another thread        //
    // - invoke sound method to get audio input buffer
    // - average amplitude in all samples
    // - return RMS value of average.
    // - set value in vu_meter
    return 0;
}

void WinMorTNC::slotDispConstl()
{
    // TODO: when decoder is more complete - develop this
    // called periodically to update constellation display //
    // period is triggered by decoded fft data as a series //
    // from a signal from the decoder where the data       //
    // stream is derived, which operates in another thread //
    // - invoke decoder method to get fft buffer with freq, amp,
    //   and phase data for the series
    // - iterate through series
    // - display phase and amp for each sample in the series
}

//void WinMorTNC::demodStateChanged(PactorState m_state)
//{
    // change indicators based on demod state
//}

//      TNC EVENT TRANSITIONS IN STATE MACHINE    //
//
// NOTE:  Each of these transitions must be created according to state
//        diagram from Rick Muething. The transitions may or may not do
//        anything before the destination event function
//        See docs for details v4.8.1 and v4.8.4
/*
QEventTransition* WinMorTNC::transModeChgIRSISS() {}
QEventTransition* WinMorTNC::transISStoIRS() {}
QEventTransition* WinMorTNC::transIRSACKConnReq() {}
QEventTransition* WinMorTNC::transISStoIdle() {}
QEventTransition* WinMorTNC::transIRStoIdle() {}
QEventTransition* WinMorTNC::trans500to1600() {}
QEventTransition* WinMorTNC::trans1600to500() {}
QEventTransition* WinMorTNC::transDisconRcvACK() {}
QEventTransition* WinMorTNC::transDisconToConn() {}
QEventTransition* WinMorTNC::transDisconToConnPend() {}
QEventTransition* WinMorTNC::transConPendToConn() {}
QEventTransition* WinMorTNC::transConnToDisconn() {}
QEventTransition* WinMorTNC::transIdletoSendID() {}
*/
//      TNC EVENTS IN STATE MACHINE    //
//
// NOTE:  Each of these events must be created as a custom event
//        with a custom event number. That event must be registered
//        as an event type using the registerEventType() function
//        to be used as a QEvent and become thread safe. See docs
//        for details v4.8.1
//
void WinMorTNC::eventIRS2ISS_ACK00Rcvd()
{
    // Send Data (data pending or IDLE)
    // next state ISS  IRS2ISS->ISS
}

void WinMorTNC::eventIRS2ISS_disconnRcvd()
{
    // Send ID Frame (8 sec ID delay)
    // next state SENDID  IRS2ISS->SENDID
}

void WinMorTNC::eventIRS2ISS_inactivTimeout()
{
    // Send ID Frame (no ID delay)
    // next state SENDID  IRS2ISS->SENDID
}

void WinMorTNC::eventIRS2ISS_NOACKTimeout()
{
    // Repeat BREAK
    // next state IRS2ISS
}

void WinMorTNC::eventIRS_ctrlRcvdReqLstPSN()
{
    // Session ID match
    // Sumcheck OK
    // Send ACK containing Last PSN
    // next state IRS_MODESHIFT  IRS->IRS_MODESHIFT
}

void WinMorTNC::eventIRS_inactivTimeout()
{
    // Send ID Frame (no ID delay)
    // next state SENDID    IRS->SENDID
}

void WinMorTNC::eventIRS_rcvConnReqFrame()
{
    // Session ID Match
    // Sumcheck OK
    // Send Bandwidth specific ACK
    // next state IRS
}

void WinMorTNC::eventIRS_rcvDataBADID()
{
    // Session ID mismatch on all carriers ID error signal from decoder
    // next state IRS
}

void WinMorTNC::eventIRS_rcvDatagoodID()
{
    // Session ID match on at least one carrier signal from decoder
    // Send ACK for each carrier correct, no repeats
    // next state IRS
}

void WinMorTNC::eventIRS_rcvDisconReqFrame()
{
    // Send ID Frame (8 sec ID delay)
    // next state SENDID    IRS->SENDID
}

void WinMorTNC::eventIRS_rcvIDFrame()
{
    // Send ACK 00
    // next state IRS
}

void WinMorTNC::eventDiscond_rcvConReqFrame()
{
    // Decode Frame type Connect Request
    // next state CONNPENDING    DISCONNECTED->CONNPENDING
}

void WinMorTNC::eventConnecting_bwreqACKrcvd()
{
    // Matches current session ID
    // Send IDLE
    // set Repeat ON if no Data pending
    // next state ISS  Connecting->ISS
}

void WinMorTNC::eventConnecting_connreqTimeout()
{
    // Send ID Frame (no ID delay)
    // next state SENDID CONNECTING->SENDID
}

void WinMorTNC::eventConnPend_callDecoded()
{
    // signal from demod ID frame decoded good ID
    // Send bandwidth specific ACK, set Repeat OFF
    // next state IRS  CONNPENDING->IRS
}

void WinMorTNC::eventConnPend_callDecodeFail()
{
    // Decoder signals Sumcheck fail or Target <> Local call sign
    // next state DISCONNECTED  CONNPENDING->DISCONNECTED
}

void WinMorTNC::eventDisconing_ctlTimeout()
{
    // Disconnect Repeat count < 5
    // Send Disconnect Request
    // set Repeat ON
    // next state DISCONNECTING
    // Disconnect Repeat count > = 5
    // Send ID Frame (no ID delay)
    // next state SENDID    DISCONNECTING->SENDID
}

void WinMorTNC::eventIRSMS_rcvData()
{
    // Decoder Session ID Match signal
    // Send ACK for each carrier no repeats
    // next state IRS     IRSMODESHIFT->IRS
}

void WinMorTNC::eventIRSMS_rcvCtlFrameIdle()
{
    // ACK(0), No repeat
    // nest state IRS    IRSMODESHIFT->IRS
}

void WinMorTNC::eventIRSMS_inactivTimeout()
{
    // Send ID Frame (no delay)
    // next state SENDID     IRSMODESHIFT->SENDID
}

void WinMorTNC::eventIRSMS_disconReqRcv()
{
    // Send ID Frame (8 sec ID delay)
    // next state SENDID     IRSMODESHIFT->SENDID
}

void WinMorTNC::eventISS_rcvACK()
{
    // OB bytes pending after ACK processed
    // no speed shift
    // Send next OB Packet with Repeat
    // next state ISS

    // OB bytes pending after ACK processed, speed shift
    // Send Control request last PSN with repeat
    // next state ISSMODESHIFT

    // no OB bytes pending after ACK processed
    // Send Control Idle with repeat
    // next state ISS
}

void WinMorTNC::eventISS_inactivTimeout()
{
    // Send ID Frame (no ID delay)
    // next state SENDID
}

void WinMorTNC::eventISS_disconReqRcv()
{
    // Send ID Frame (8 sec ID delay)
    // next state SENDID
}

void WinMorTNC::eventISS_idTimeout()
{
    // Timeout (10 minutes) expired
    // Send ID Frame (no delay or CWID)
    // repeat until ACK 00
    // next state ISS
}

void WinMorTNC::eventISSMS_psnRcvd()
{
    // Session ID Match signal from demod
    // Sumcheck OK
    // OB Packets Remaining
    // Send next Data packet
    // next state ISS    ISSMODESHIFT->ISS

    // Session ID Match signal from demod
    // Sumcheck OK
    // no OB Packets Remaining
    // Send Idle
    // set repeat
    // next state ISSMODESHIFT
}

void WinMorTNC::eventISSMS_inactivTimeout()
{
    // Send ID Frame (no ID delay)
    // next state SENDID    ISSMODESHIFT->SENDID
}

void WinMorTNC::eventISSMS_disconReqRcvd()
{
    // Send ID Frame (8 sec ID delay)
    // next state SENDID      ISSMODESHIFT->SENDID
}

void WinMorTNC::eventSENDID_idTimeout()
{
    // Timeout Reached (nominally 0 or 8 seconds)
    // Send ID Frame followed by CWID if CWID is enabled
    // next state DISCONNECTED    SENDID->DISCONNECTED
    //
    // Request received (while waiting for ID timeout)
    // Session ID match signal from decoder
    // Send ACK(FF) with session ID
    // Extend ID Timeout by 8 seconds
    // next state SENDID
}

void WinMorTNC::eventFEC500_startCMD()
{
    // Must be in the disconnected state
    // Enter FEC500 or FEC 1600 State
    // Send staggered FEC data
    // next state FEC500 or FEC1600
}

void WinMorTNC::eventFEC500_allDataSent()
{
    // All fecdata sent from mod signal rcvd
    // Enter SENDID state
    // next state SENDID       FEC500->SENDID
}

void WinMorTNC::eventFEC500_abortFEC()
{
    // Abort FEC command (FEC OFF)
    // Clear outbound buffer.
    // Enter SENDID State
    // next state SENDID     FEC500->SENDID
}

void WinMorTNC::eventFEC500_rcvData()
{
    // FEC 500 Data received (frames 13 , 14)
    // Must have FEC reception enabled
    // Must be in DISCONNECTED state
    // Decode FEC
    // remove redundant PSN’s
    // Flag incorrectable PSN’s
    // next state DISCONNECTED    FEC500->DISCONNECTED
}

void WinMorTNC::eventFEC1600_startCMD()
{
    // Must be in the disconnected state
    // Enter FEC500 or FEC 1600 State
    // Send staggered FEC data
    // next state FEC500 or FEC1600
}

void WinMorTNC::eventFEC1600_allDataSent()
{
    // All fecdata sent from mod signal rcvd
    // Enter SENDID state
    // next state SENDID       FEC1600->SENDID
}

void WinMorTNC::eventFEC1600_abortFEC()
{
    // Abort FEC command (FEC OFF)
    // Clear outbound buffer.
    // Enter SENDID State
    // next state SENDID     FEC1600->SENDID
}

void WinMorTNC::eventFEC1600_rcvData()
{
    // FEC 1600 Data received (frames 13 , 14)
    // Must have FEC reception enabled
    // Must be in DISCONNECTED state
    // Decode FEC
    // remove redundant PSN’s
    // Flag incorrectable PSN’s
    // next state DISCONNECTED    FEC1600->DISCONNECTED
}
//   END STATE MACHINE EVENTS    //

/*
void WinMorTNC::engineFormatChanged(const QAudioFormat & format)
{
    //engineInfoMessage(formatToString(format));
}
*/



//-------------------------------------------------------------------
//     Public Functions and methods
//-------------------------------------------------------------------
void WinMorTNC::setBW(int bw)
{
    // invoke the method to change modulator and demodulator to bw selection
    // if not same as in winmor module control block start mode change
    // if this is on init set up modlator and demodulator for bw and
    // notify winmor module of mode change complete to mark control block
    Q_UNUSED(bw)
#ifdef SHOW_WINMORTNC_DEBUG
    WINMORTNC_DEBUG("Setting Bandwidth directly to " + bw);
#endif
}

//-------------------------------------------------------------------
//     Action Handlers
//-------------------------------------------------------------------
void WinMorTNC::updateMenuStates()
{
    if (view == 0)
        m_viewSmallPanel->setChecked(true);
    else
        m_viewLrgPanel->setChecked(true);
}

//-------------------------------------------------------------------
//     PRIVATE SLOTS
//-------------------------------------------------------------------
void WinMorTNC::slotSetIdleState()
{
    // change state to Idle and set LEDs to show state
#ifdef SHOW_WINMORTNC_DEBUG
    WINMORTNC_DEBUG("Idle State imposed");
#endif
}

void WinMorTNC::slotShowFileDialog()
{
    const QString dir;
    const QStringList fileNames = QFileDialog::getOpenFileNames(this, tr("Open WAV file"), dir, "*.wav");

#ifdef SHOW_WINMORTNC_DEBUG
    WINMORTNC_DEBUG("File Dialog called");
#endif
    if ( fileNames.count() )
    {
        reset();
        //setMode(LoadFileMode);
        //TODO: create following function in JackSound
        //audio->loadFile(fileNames.front());
        updateButtonStates();
    }
    else
    {
        updateModeMenu();
    }
    setWindowTitle(tr("Pactor TNC - Playing Audio File "));
    m_infoMessage->setText(fileNames.front());
}

void WinMorTNC::slotAudioInfo()
{
    setWindowTitle(tr("Pactor TNC - Playing Audio File"));
}

void WinMorTNC::slotAudioBufferLengthChanged(qint64 len)
{
    //
    Q_UNUSED(len)
}

void WinMorTNC::slotAudioStateChanged()
{
    //
}

void WinMorTNC::slotAudioPositionChanged(qint64 position)
{
    //
}

void WinMorTNC::slotFileLoaded()
{
    setWindowTitle(tr("Pactor TNC - Playing Audio File "));
}

void WinMorTNC::slotStartPlay()
{
    //TODO: create function in JackAudio
    //audio->startPlayback();
    //used for file playback not general audio data capture
}

void WinMorTNC::slotPlayStarted()
{
    setWindowTitle(tr("Pactor TNC - Playing Audio File"));
}

void WinMorTNC::slotPlayPositionChanged(qint64 p, qint64 l, const QByteArray b)
{
    //TODO: create function in JackAudio
    //l = audio->bufferLength();
    l = 1048;
    if ( l )
    {
        qreal play = qreal(p) / l;
        m_progressbar->setValue(int(play * 100));
    }
}

void WinMorTNC::slotAudioPosChanged(qint64 l, qint64 p, const QByteArray& b)
{
    //
}

void WinMorTNC::infoMessage(const QString &message, int timeoutMs)
{
    m_infoMessage->setText(message);

    if (NullTimerId != m_infoMessageTimerId)
    {
        killTimer(m_infoMessageTimerId);
        m_infoMessageTimerId = NullTimerId;
    }

    //if (NullMessageTimeout != timeoutMs) m_infoMessageTimerId = startTimer(timeoutMs);
}

/*
void  WinMorTNC::slotDrawDataCurve(const double * xs, const double * ys, int graph_first_sample, char * title)
{
    curve1->setRawSamples(xs, ys, graph_first_sample);
    curve1->attach(myPlot);

    myPlot->setTitle(QwtText(title));
    myPlot->replot();
    myPlot->show();
}
*/


void WinMorTNC::slotPTToff()
{
    ledXMIT->setPixmap(blueon->scaled(QSize(15,5)));
    ledRCV->setPixmap(redon->scaled(QSize(15,5)));
    ledXMIT->setEnabled(true);

#ifdef SHOW_WINMORTNC_DEBUG
    WINMORTNC_DEBUG("PTT Off");
#endif
    // TODO: we must interface with serial device to change PTT line
    //       as well as change the control block status and the state machine
}

void WinMorTNC::slotPTTon()
{
    ledXMIT->setPixmap(redon->scaled(QSize(15,5)));
    ledRCV->setPixmap(blueon->scaled(QSize(15,5)));
    ledXMIT->setEnabled(true);

#ifdef SHOW_WINMORTNC_DEBUG
    WINMORTNC_DEBUG("PTT On");
#endif
    // TODO: we must interface with serial device to change PTT line
    //       as well as change the control block status and the state machine
}

void WinMorTNC::slotSmallPanel()
{
    m_viewLrgPanel->setChecked(false);
    //pnlDatascope->setVisible(false);
    resize(800, 200);
}

void WinMorTNC::slotLrgPanel()
{
    m_viewSmallPanel->setChecked(false);
    resize(800, 600);
    //pnlDatascope->setVisible(true);
}

void WinMorTNC::slotExit()
{
    // TODO: we must make sure audio source and sink are closed and state
    //       machine stopped as well as contol block updated.
    close();
}

void WinMorTNC::slotTxLevelTest()
{
    // generate tones for xmit setup
#ifdef SHOW_WINMORTNC_DEBUG
    WINMORTNC_DEBUG("Tx Level Test ...");
#endif
}

void WinMorTNC::slotSendData()
{
    // send data over udp data channel to modem api
    emit signalOutboundData();
}

void WinMorTNC::slotUpdateDisplay()
{
#ifdef SHOW_WINMORTNC_DEBUG
    WINMORTNC_DEBUG("Updating Display panel");
#endif
    // TODO: update panel when state changes

    // Update TNC Panel display with any change in state
    // first reset panel //
    ledXMIT->setEnabled(false);
    ledRCV->setEnabled(true);
    ledCONN->setEnabled(false);
    ledERR->setEnabled(false);
    //  various send/rcv state indicators  //
    ledIRS->setEnabled(false);
    ledLISTEN->setEnabled(true);
    ledDREQ->setEnabled(false);
    ledISS->setEnabled(false);
    ledBUSY->setEnabled(false);
    // test states for update  //
    //if ( state_ISS->entered() ) ledISS->setEnabled(true);
}


//-------------------------------------------------------------------
//     PRIVATE METHODS
//-------------------------------------------------------------------
void WinMorTNC::createActions()
{
    m_modechgAction   = new QAction(m_modeIcon,"",this);
    m_loadFileAction  = new QAction(m_fileIcon,"",this);
    m_playFileAction  = new QAction(m_playIcon,"",this);
    m_pauseFileAction = new QAction(m_pauseIcon,"",this);
    m_recordAction    = new QAction(m_recordIcon,"",this);
    m_settingsAction  = new QAction(m_settingsIcon,"",this);
    m_exitWinMorAction = new QAction(m_exitIcon,"",this);
    m_sendDataAction  = new QAction(m_testIcon, "", this);

    buttonPanel->addAction(m_modechgAction);
    buttonPanel->addSeparator();
    buttonPanel->addAction(m_loadFileAction);
    buttonPanel->addSeparator();
    buttonPanel->addAction(m_playFileAction);
    buttonPanel->addAction(m_pauseFileAction);
    buttonPanel->addSeparator();
    buttonPanel->addAction(m_recordAction);
    buttonPanel->addSeparator();
    buttonPanel->addSeparator();
    buttonPanel->addAction(m_settingsAction);
    buttonPanel->addSeparator();
    buttonPanel->addSeparator();
    buttonPanel->addAction(m_exitWinMorAction);
    buttonPanel->addSeparator();
    buttonPanel->addAction(m_sendDataAction);

    m_txLevelTestAction = new QAction(this);
    m_txLevelTestAction->setObjectName(QString::fromUtf8("m_txLevelTestAction"));

    m_viewSmallPanel = new QAction(this);
    m_viewSmallPanel->setObjectName(QString::fromUtf8("m_viewSmallPanel"));
    m_viewSmallPanel->setCheckable(true);
    m_viewSmallPanel->setChecked(true);

    m_viewLrgPanel = new QAction(this);
    m_viewLrgPanel->setObjectName(QString::fromUtf8("m_viewLrgPanel"));
    m_viewLrgPanel->setCheckable(true);
}

void WinMorTNC::createMenus()
{
    menubar = new QMenuBar(this);
    menubar->setObjectName(QString::fromUtf8("menubar"));
    menubar->setGeometry(QRect(0, 0, width(), 27));
    setMenuBar(menubar);

    fileMenu = new QMenu(menubar);
    fileMenu->setObjectName(QString::fromUtf8("fileMenu"));

    setupMenu = new QMenu(menubar);
    setupMenu->setObjectName(QString::fromUtf8("setupMenu"));

    modeMenu = new QMenu(menubar);
    modeMenu->setObjectName(QString::fromUtf8("modeMenu"));

    viewMenu = new QMenu(menubar);
    viewMenu->setObjectName(QString::fromUtf8("viewMenu"));

    exitMenu = new QMenu(menubar);
    exitMenu->setObjectName(QString::fromUtf8("menuExit"));

    menubar->addAction(exitMenu->menuAction());
    menubar->addAction(viewMenu->menuAction());
    menubar->addAction(setupMenu->menuAction());

    exitMenu->addAction(m_exitWinMorAction);

    setupMenu->addAction(m_txLevelTestAction);

    viewMenu->addAction(m_viewSmallPanel);
    viewMenu->addAction(m_viewLrgPanel);
}

void WinMorTNC::createObjects()
{
    statusbar = new QStatusBar(this);
    statusbar->setObjectName(QString::fromUtf8("statusbar"));
    setStatusBar(statusbar);

    centralwidget = new QWidget(this);
    centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
    setCentralWidget(centralwidget);
    mainLayout = new QVBoxLayout(centralwidget);
    setLayout(mainLayout);

    font = new QFont("Arial", 11);
    font->setBold(true);
    font->setWeight(75);
    // Button panel
    buttonPanelLayout = new QHBoxLayout(this);
    buttonPanelLayout->addStretch();
    buttonPanelLayout->setSpacing(5);

    buttonPanel = new QToolBar(this);
    buttonPanel->setMovable(false);
    buttonPanel->setMinimumHeight(30);
    buttonPanel->setMaximumHeight(90);
#ifdef Q_OS_WIN32
    buttonPanel->setGeometry(5,25,width()-10,32);
#endif
#ifdef Q_OS_LINUX
    buttonPanel->setGeometry(5,1,width()-10,33);
#endif
    buttonPanel->setLayout(buttonPanelLayout);
    mainLayout->addWidget(buttonPanel);

    m_fileIcon     = QIcon(":images/sound-juicer.png");
    m_modeIcon     = QIcon(":images/mode_button.png");
    m_playIcon     = QIcon(":images/play.png");
    m_pauseIcon    = QIcon(":images/pause.png");
    m_recordIcon   = QIcon(":images/record.png");
    m_settingsIcon = QIcon(":images/settings.png");
    m_exitIcon     = QIcon(":images/system-shutdown.png");
    m_testIcon     = QIcon(":images/sound-juicer.png");

    //  Layout the led display and labels   //
    //    No need for translation of text
    //    for theses labels
    redon = new QPixmap(":/images/redled.png");
    greenon = new QPixmap(":/images/greenled.png");
    yelon = new QPixmap(":/images/yelled.png");
    blueon = new QPixmap(":/images/blueled.png");

    grpLED = new QGroupBox(this);
#ifdef Q_OS_WIN32
    grpLED->setGeometry(5,58,width()-10,38);
#endif
#ifdef Q_OS_LINUX
    grpLED->setGeometry(3,37,width() -10,38);
#endif
    grpLED->setStyleSheet("QGroupBox {border: 2px solid gray; border-radius: 3px; border-width: 1px;margin: 0;}");
    grpLED->setMinimumHeight(35);
    grpLED->setMaximumHeight(90);

    ledLayout = new QGridLayout(this);
    ledLayout->setMargin(2);
    ledLayout->setSpacing(1);
    grpLED->setLayout(ledLayout);

    grpLED->setContentsMargins(6,2,6,2);

    ledXMIT = new QLabel(this);
    ledXMIT->setFixedSize(20,10);
    ledXMIT->setPixmap(redon->scaled(QSize(20,10)));
    ledLayout->addWidget(ledXMIT,0,0,1,1);

    ledRCV = new QLabel(this);
    ledRCV->setFixedSize(20,10);
    ledRCV->setPixmap(blueon->scaled(QSize(20,10)));
    ledLayout->addWidget(ledRCV,0,1,1,1);

    ledERR = new QLabel(this);
    ledERR->setFixedSize(20,10);
    ledERR->setPixmap(redon->scaled(QSize(20,10)));
    ledLayout->addWidget(ledERR,0,2,1,1);

    ledBUSY = new QLabel(this);
    ledBUSY->setFixedSize(20,10);
    ledBUSY->setPixmap(redon->scaled(QSize(20,10)));
    ledLayout->addWidget(ledBUSY,0,3,1,1);

    //  various send/rcv state indicators  //
    ledCONN = new QLabel(this);
    ledCONN->setFixedSize(20,10);
    ledCONN->setPixmap(blueon->scaled(QSize(20,10)));
    ledLayout->addWidget(ledCONN,0,5,1,1);

    ledIRS = new QLabel(this);
    ledIRS->setFixedSize(20,10);
    ledIRS->setPixmap(greenon->scaled(QSize(20,10)));
    ledLayout->addWidget(ledIRS,0,6,1,1);

    ledUNPROTO = new QLabel(this);
    ledUNPROTO->setFixedSize(20,10);
    ledUNPROTO->setPixmap(greenon->scaled(QSize(20,10)));
    ledLayout->addWidget(ledUNPROTO,0,7,1,1);

    ledLISTEN = new QLabel(this);
    ledLISTEN->setFixedSize(20,10);
    ledLISTEN->setPixmap(greenon->scaled(QSize(20,10)));
    ledLayout->addWidget(ledLISTEN,0,8,1,1);

    ledDREQ = new QLabel(this);
    ledDREQ->setFixedSize(20,10);
    ledDREQ->setPixmap(yelon->scaled(QSize(20,10)));
    ledLayout->addWidget(ledDREQ,0,9,1,1);

    ledISS = new QLabel(this);
    ledISS->setFixedSize(20,10);
    ledISS->setPixmap(redon->scaled(QSize(20,10)));
    ledLayout->addWidget(ledISS,0,10,1,1);

    // labels for led indicators  //
    lblXMIT = new QLabel(this);
    lblXMIT->setFixedSize(30,20);
    lblXMIT->setFont(QFont("DejaVu Sans",8));
    ledLayout->addWidget(lblXMIT,1,0,1,1);

    lblRCV = new QLabel(this);
    lblRCV->setFixedSize(30,20);
    lblRCV->setFont(QFont("DejaVu Sans",8));
    ledLayout->addWidget(lblRCV,1,1,1,1);

    lblERR = new QLabel(this);
    lblERR->setFixedSize(30,20);
    lblERR->setFont(QFont("DejaVu Sans",8));
    ledLayout->addWidget(lblERR,1,2,1,1);

    lblBUSY = new QLabel(this);
    lblBUSY->setFixedSize(38,20);
    lblBUSY->setFont(QFont("DejaVu Sans",8));
    ledLayout->addWidget(lblBUSY,1,3,1,1);

    // labels for various state indicators
    lblCONN = new QLabel(this);
    lblCONN->setFixedSize(30,20);
    lblCONN->setFont(QFont("DejaVu Sans",8));
    ledLayout->addWidget(lblCONN,1,5,1,1);

    lblIRS = new QLabel(this);
    lblIRS->setFixedSize(30,20);
    lblIRS->setFont(QFont("DejaVu Sans",8));
    ledLayout->addWidget(lblIRS,1,6,1,1);

    lblUNPROTO = new QLabel(this);
    lblUNPROTO->setFixedSize(30,20);
    lblUNPROTO->setFont(QFont("DejaVu Sans",8));
    ledLayout->addWidget(lblUNPROTO,1,7,1,1);

    lblLISTEN = new QLabel(this);
    lblLISTEN->setFixedSize(30,20);
    lblLISTEN->setFont(QFont("DejaVu Sans",8));
    ledLayout->addWidget(lblLISTEN,1,8,1,1);

    lblDREQ = new QLabel(this);
    lblDREQ->setFixedSize(30,20);
    lblDREQ->setFont(QFont("DejaVu Sans",8));
    ledLayout->addWidget(lblDREQ,1,9,1,1);

    lblISS = new QLabel(this);
    lblISS->setFixedSize(30,20);
    lblISS->setFont(QFont("DejaVu Sans",8));
    ledLayout->addWidget(lblISS,1,10,1,1);

    mainLayout->addWidget(grpLED);

    // set style for button type objects
    QString style;
    style = "QPushButton {";
    style += "border-style:outset; ";
    style += "border-radius: 3px; ";
    style += "border-width:1px; ";
    style += "border-color:black;";
    style += "background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #f6f7fa, stop: 1 #aaabae); ";
    style += "font: normal 11px;";
    style += "} ";
    style += "QPushButton:pressed {";
    style += "background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:1 #f6f7fa, stop: 0 #aaabae); ";
    style += "border-style:inset; ";
    style += "}";
    style += "QPushButton:released {";
    style += "border-style:outset; ";
    style += "border-radius: 3px; ";
    style += "border-width:1px; ";
    style += "border-color:black;";
    style += "background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #f6f7fa, stop: 1 #aaabae); ";
    style += "font: normal 11px;";
    style += "} ";

    m_infoMessage = new QLabel(this);
    m_infoMessage->setMaximumHeight(60);
    m_infoMessage->setFont(QFont("DejaVu Sans",10));
    m_infoMessage->setStyleSheet("QFrame {color: blue; border: 1px solid #999999; border-radius: 3em;}");
    m_infoMessage->setText("Active");
    mainLayout->addWidget(m_infoMessage);

    // set up other displays  //
    grpDisplay = new QGroupBox(this);
#ifdef Q_OS_WIN32
    grpDisplay->setGeometry(4,98,width()-10,30);
#endif
#ifdef Q_OS_LINUX
    grpDisplay->setGeometry(4,85,width()-10,30);
#endif
    grpDisplay->setStyleSheet("QGroupBox {border: 2px solid gray; border-radius: 5px; border-width: 1px;}");

    infoLayout = new QHBoxLayout;
    grpDisplay->setLayout(infoLayout);

    vu_meter = new VUMeter(this);
    vu_meter->resize(50,200);
    vu_meter->move(6,130);
    vu_meter->setMaximumWidth(70);
    vu_meter->setMinimumWidth(20);
    vu_meter->setMinimumHeight(100);
    vu_meter->setValue(-40);         // set to minimum value on creation
    infoLayout->addWidget(vu_meter);

    waterfall = new Waterfall(this);
    waterfall->setMinimumSize(100,50);
    waterfall->move(65,135);
    infoLayout->addWidget(waterfall);

    scope = new Constellation(this);
    scope->setNewSize(185,185);
    scope->move(105,135);
    scope->setMinimumSize(100,100);
    infoLayout->addWidget(scope);
    mainLayout->addWidget(grpDisplay);

    vuTimer = new QTimer();
    vuTimer->setInterval(100);
}


void WinMorTNC::connectSignals()
{
    CHECKED_CONNECT(m_loadFileAction, SIGNAL(triggered()), this, SLOT(slotShowFileDialog()));
    CHECKED_CONNECT(m_playFileAction, SIGNAL(triggered()),  this, SLOT(slotStartPlay()));
    CHECKED_CONNECT(m_exitWinMorAction, SIGNAL(triggered()), this, SLOT(close()));
    CHECKED_CONNECT(m_txLevelTestAction, SIGNAL(triggered()),this, SLOT(slotTxLevelTest()));
    CHECKED_CONNECT(m_viewSmallPanel, SIGNAL(triggered()),this, SLOT(slotSmallPanel()));
    CHECKED_CONNECT(m_viewLrgPanel, SIGNAL(triggered()),this, SLOT(slotLrgPanel()));

    CHECKED_CONNECT(vuTimer, SIGNAL(timeout()),this, SLOT(sampleAudioLvl()));

    QMetaObject::connectSlotsByName(this);
}

void WinMorTNC::translateText()
{
    setWindowTitle(tr("WINMOR Sound Card TNC"));
    exitMenu->setTitle(tr("File"));
    setupMenu->setTitle(tr("Setup"));
    viewMenu->setTitle(tr("View"));
    m_exitWinMorAction->setText(tr("Exit WinMor"));
    m_viewSmallPanel->setText(tr("Hide Datascope"));
    m_viewLrgPanel->setText(tr("View Datascope"));

    m_modechgAction->setToolTip(tr("Change Mode"));
    m_loadFileAction->setToolTip(tr("File"));
    m_playFileAction->setToolTip(tr("Play"));
    m_pauseFileAction->setToolTip(tr("Pause"));
    m_settingsAction->setToolTip(tr("Settings"));

    lblXMIT->setText("XMT");
    lblRCV->setText("RCV");
    lblERR->setText("ERR");
    lblBUSY->setText("BZY");
    lblCONN->setText("CON");
    lblIRS->setText("IRS");
    lblUNPROTO->setText("UNP");
    lblLISTEN->setText("LIS");
    lblDREQ->setText("DRQ");
    lblISS->setText("ISS");
}

void WinMorTNC::createStateMachine()
{
    //  WinMOR State Machine Implementation   //
    m_statemachine = new QStateMachine(this);

    // winmor states   //
    state_ISS = new QState();
    state_IRS = new QState();
    state_IDLE = new QState();
    state_SENDID = new QState();
    state_ISS2IRS = new QState();
    state_IRS2ISS = new QState();
    state_CONNECTED = new QState();
    state_DISCONNECTED = new QState();
    state_CONNPENDING = new QState();
    state_DISCONNPENDING = new QState();
    state_IRS_MODESHIFT = new QState();
    state_ISS_MODESHIFT = new QState();
    state_FEC500 = new QState();
    state_FEC1600 = new QState();

    m_statemachine->addState(state_IRS2ISS);

    m_statemachine->addState(state_IRS);

    //  TODO: Add transitions as needed for each state  //
    m_statemachine->addState(state_ISS);
    m_statemachine->addState(state_SENDID);
    m_statemachine->addState(state_ISS2IRS);
    m_statemachine->addState(state_CONNECTED);
    m_statemachine->addState(state_DISCONNECTED);
    m_statemachine->addState(state_CONNPENDING);
    m_statemachine->addState(state_DISCONNPENDING);
    m_statemachine->addState(state_IRS_MODESHIFT);
    m_statemachine->addState(state_ISS_MODESHIFT);
    m_statemachine->addState(state_FEC500);
    m_statemachine->addState(state_FEC1600);

    m_statemachine->setInitialState(state_DISCONNECTED);
    // create connections to update TNC panel and  //
    // perform other local funcitons as needed     //
    QObject::connect(state_ISS, SIGNAL(entered()), this, SLOT(update()));

    //
    // TODO: create transitions to move from one state to another
    //

    // all objects and transitions created so now //
    // start the state machine                   //
    // NOTE: this state machine does not finish until deconstructed //
    m_statemachine->start();
}


void WinMorTNC::initTNC()
{
    ledXMIT->setEnabled(false);
    ledRCV->setEnabled(true);
    ledCONN->setEnabled(false);
    ledERR->setEnabled(false);
    //  various send/rcv state indicators  //
    ledIRS->setEnabled(false);
    ledUNPROTO->setEnabled(true);
    ledLISTEN->setEnabled(true);
    ledDREQ->setEnabled(false);
    ledISS->setEnabled(false);
    ledBUSY->setEnabled(false);
    //createStateMachine();
    //slotSetIdleState();
    vuTimer->start();
}

void WinMorTNC::reset()
{
    m_progressbar->reset();
}

void WinMorTNC::setMode(ModemMode mode)
{
    m_mode = mode;
    updateModeMenu();
}

void WinMorTNC::updateButtonStates()
{
    //
}

void WinMorTNC::updateModeMenu()
{
    //m_loadFileAction->setChecked(LoadFileMode == m_mode);
    //m_generateToneAction->setChecked(GenerateToneMode == m_mode);
    //m_recordAction->setChecked(RecordMode == m_mode);
}

void WinMorTNC::readSettings()
{
    QSettings settings("OpenModem","Config");
    // Settings unique to TNC Panel only  //
    settings.beginGroup("WinMorTNC");
     pos = settings.value("pos", QPoint(130, 330)).toPoint();
     size = settings.value("size", QSize(540, 350)).toSize();
     view = settings.value("view", 1).toInt();
    settings.endGroup();

    resize(size);
    move(pos);

    settings.beginGroup("Sound");
      WMCB.CaptureDevice = settings.value("CaptureDevice","default").toString();
      WMCB.PlaybackDevice = settings.value("PlaybackDevice","default").toString();
    settings.endGroup();
    settings.beginGroup("User");
      WMCB.MyCallsign = settings.value("MyCall","unknown").toString();
      WMCB.MyGridsquare = settings.value("GridSquare","XX00xx").toString();
      WMCB.Registration = settings.value("RegistrationNum","unreg").toString();
    settings.endGroup();
    settings.beginGroup("Modem");
      // general settings to local vars here
      WMCB.CWId = settings.value("CWId",false).toBool();
      WMCB.debugLogON = settings.value("DebugLog",false).toBool();
      WMCB.Bandwidth = settings.value("BandWidth",500).toInt();
      WMCB.DriveLevel = settings.value("DriveLevel",50).toInt();
    settings.endGroup();
    settings.beginGroup("Network");
      // general settings to local vars here
      WMCB.tcpAddress = settings.value("Address","127.0.0.1").toString();
      WMCB.TCPControlPort = settings.value("TCPControlPort",8600).toInt();
    settings.endGroup();
}

void WinMorTNC::writeSettings()
{
    // store settings for TNC only here //
    // general settings are set elsewhere //
    QSettings settings("OpenModem","Config");
    // Settings unique to TNC Panel only  //
    settings.beginGroup("WinMorTNC");
     settings.setValue("pos", pos);
     settings.setValue("size", size);
    settings.endGroup();
    settings.beginGroup("Sound");
      settings.setValue("CaptureDevice",WMCB.CaptureDevice);
      settings.setValue("PlaybackDevice",WMCB.PlaybackDevice);
    settings.endGroup();
    settings.beginGroup("User");
      settings.setValue("MyCall",WMCB.MyCallsign);
      settings.setValue("GridSquare",WMCB.MyGridsquare);
      settings.setValue("RegistrationNum",WMCB.Registration);
    settings.endGroup();
    settings.beginGroup("Modem");
      // general settings to local vars here
      settings.setValue("CWId",WMCB.CWId);
      settings.setValue("DebugLog",WMCB.debugLogON);
      settings.setValue("BandWidth",WMCB.Bandwidth);
      settings.setValue("DriveLevel",WMCB.DriveLevel);
    settings.endGroup();
    settings.sync();
}

